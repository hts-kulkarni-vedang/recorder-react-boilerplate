#ifndef PLAYER_H    // To make sure you don't declare the function more than once by including the header multiple times.
#define PLAYER_H
#include<napi.h>
// #include <node_api.h>
#include<windows.h>
#include <iostream> 
#include <fstream>
using namespace std;
void  DisplayError(char *pszAPI);
void ReadAndHandleOutput(Napi::Env env,HANDLE hPipeRead,std::string fileName,Napi::Function cb);
void PrepAndLaunchRedirectedChild(HANDLE hChildStdOut,
                                    HANDLE hChildStdIn,
                                    HANDLE hChildStdErr,
                                    std::string *strCommands,
                                    int commandCounter,
                                    HANDLE hChildProcess,
                                    Napi::Env env);
DWORD WINAPI GetAndSendInputThread(LPVOID lpvThreadParam);
void SetAndSendInputThread( HANDLE hStdIn_p,BOOL bRunThread_p);
std::string createCommand(  std::vector <std::string> args_cmd);
void setId_fileName(int id_s,std::string filename);
#endif