var electronInstaller = require('electron-winstaller');

resultPromise = electronInstaller.createWindowsInstaller({
    appDirectory: '../packaged-apps/HiFi_Recorder-win32-x64/',
    outputDirectory: './release/installer',
    authors: 'HiFi_Technology_Solutions',
    iconUrl : 'http://13.58.84.186/recorder/img/icon.ico',
    loadingGif : './assets/images/loader-small.gif',
    // remoteReleases : 'http://13.58.84.186/recorder/release/'
  });

resultPromise.then(() => console.log("It worked!"), (e) => console.log(`No dice: ${e.message}`));
