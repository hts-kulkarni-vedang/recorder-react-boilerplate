const { ipcRenderer, shell } = require("electron")
const electron = require("electron");
const { dialog } = require("electron").remote
const fs = require('fs');
const os = require('os');
const documentsFolderPath = os.homedir + '/Documents/HiFi_Recorder Files/' + 'recording_' + (Math.floor(Date.now() / 1000));
const globalSettingsFilePath = os.homedir + '/Documents/HiFi_Recorder Files/';
const extension = '.mp4';
const videoUtils = require('./assets/js/videoUtils');
const swal = require('sweetalert2');
const env = require("./environment");
var isUpdateDownloading = false;
const checkDiskSpace = require('check-disk-space');
const log = require('electron-log');
const path = require('path');
if (!fs.existsSync(os.homedir + '/Documents/HiFi_Recorder Files')) {
	fs.mkdir(os.homedir + '/Documents/HiFi_Recorder Files', (err) => {
		if (err) {
			showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
		}
	});
}
if (!fs.existsSync(os.homedir + '/Documents/HiFi_Recorder Files/assets')) {
	fs.mkdir(os.homedir + '/Documents/HiFi_Recorder Files/assets', (err) => {
		if (err) {
			showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
		}
	});
}
// variables
// ! setting this to true because we do not need previewWindow anymore
// ! CPU Usage optimization patch
var isPreviewReady = true;
var audioSourceList = [];
var videoSourceList = [];
var currentAudioSource = null;
var currentVideoSource = null;
var s_recording = false;
var s_recorder;
var c_recorder;
var screenCount = 0;
var preSendCount = 0;
var camCount = 0;
var startTime = "";
var totalRecordingTime = 0;
var s_fileReader = null;
var c_fileReader = null;
var errorFilesCreated = false;
var screenStream;
var writeStreamCam, writeStreamScr, camStreamFlag = false, scrStreamFlag = false;
var cameraRecorderf;
var cameraPreview = false;
var logInterval = undefined;
var timerInterval = null;
var camChunksWritten = 0;
var screenChunksWritten = 0;
var camChunksRead = 0;
var screenChunksRead = 0;
var cameraReadyStateFailedCount = 0;
var screenReadyStateFailedCount = 0;
var cameraBlobDataArr = [];
var screenBlobDataArr = [];
let secs = 0;
let mins = 0;
let hours = 0;
const chunkRecordInterval = (1000 * 60); // ? multiply by number of seconds you need
// const videoBitsPerSecond = 5000000; // ? for 1080p 30 fps
var videoBitsPerSecond = 2048 * 1000;
var audioBitsPerSecond = 128 * 1000;
const audioSampleRate = 441000;
var currentCameraStream = null;
const playPauseTimings = [];
let currentPlayPauseTimingObj = {};
/*Updater Code*/
// ? write empty object in settings.json file
createSettingsJsonIfDoesNotExist();
checkPreviewSettingsFile();
initBitrates();
/************** IPC RENDERER  ****************/
ipcRenderer.on('pause-both-recorders', (event, data) => {
	pauseBothRecorders();
});
ipcRenderer.on('shortcuts', (event, param) => {
	$(".play-pause-shortcut").text("Ctrl + " + param.playPauseS);
	$(".show-hide-shortcut").text("Ctrl + " + param.showHideScreenS);
	$(".stop-shortcut").text("Ctrl + " + param.stopRecS);
})
ipcRenderer.send('get-shortcuts');
ipcRenderer.on('shortcut-ev', (event, param) => {
	//
	if (param == 'play-pause') {
		playPause();
	}
	else if (param == 'show-hide')//only for previewWindow
	{
		// if (currentRecording !== '') {
			$('#camera-stream-preview').toggle();
		// }
	}
	else if (param == 'stop') {
		if (electron.remote.getCurrentWindow().isMinimized()) {
			electron.remote.getCurrentWindow().show()
    }
		if (currentRecording !== '') {
      $(".stop-btn").click()
      recordingflag = false;
    }
	}
});
ipcRenderer.on('update-current-time', (event, param) => {
	handleUpdateCurrentTime(event, param);
})
ipcRenderer.on('preview-ready', (event, param) => {
	handlePreviewReady();
});
ipcRenderer.on('preview-window-closed', (event, param) => {
	handlePreviewWindowClose();
});
ipcRenderer.on('starter-window-open-close', (event, param) => {
	starterWindowOpenCloseHandler(param);
});
$(document).ready(function () {
	$("#extra-info-rec-mode > .extra-info-text").text("Please select recording mode !");
	refreshSources();
	checkRemainingDiskSpace();
	let win = electron.remote.getCurrentWindow();
	win.setSize(800, 600);
	win.setResizable(false);
	let bounds = electron.screen.getPrimaryDisplay().bounds;
	let x = bounds.x + ((bounds.width - 800) / 2);
	let y = bounds.y + ((bounds.height - 600) / 2);
	win.setPosition(x, y);
	log.info('Document ready of renderer new ui...');
});
$(document).on('click', '#settingsImg', function () {
	sessionStorage.setItem('currentSettingsMode', 'show-settings');
	sessionStorage.getItem('currentSettingsMode');
	if (checkIfRecording()) {
		swal({
			type: "warning",
			title: "Please stop recording first!"
		});
	} else {
		// checkUpdateDownloading().then((res)=>{
		// 	if(res.downloading == false){
		electron.remote.getCurrentWindow().setResizable(true);
		electron.remote.getCurrentWindow().loadFile("./settings/settings.html");
		// 	}
		// });
	}
});
$(document).on('click', '#minimize', function () {
	electron.remote.getCurrentWindow().minimize();
});
$(document).on('click', '#close', function () {
	checkUpdateDownloading().then((res) => {
		if (res.downloading == false) {
			ipcRenderer.send('close-app');
		}
	}).catch((err) => {
	});
});
$(document).on('click', '.custom-card', function () {
	initCheck(this);
});
$(".open-existing").on("click", function () {
	openExisting();
});
$(".reprocess-video").on("click", function () {
	reprocessVideo();
});
$(document).on("click", ".source-btn", function () {
	let sourceType = this.getAttribute("source-type");
	listAVSources(sourceType);
	$("#source-list-modal").modal();
});
$(document).on("click", ".a-source", function () {
	sourceChanged(this);
});
$(document).on("click", ".video-play-button", function () {
	playPause();
});
$(document).on("click", ".stop-btn", function () {
	stopRecFinal()
});
$(document).on("click", ".new-image", function () {
	onUpdateAvailableClick();
});
$(document).on("click", ".install-update", function () {
	onUpdateDownloadClick();
});
$(document).on("dblclick", "#camera-stream-preview", function () {
	if (document.webkitIsFullScreen) {
		document.webkitExitFullscreen();
	} else {
		openFullscreen(this);
	}
});
$(document).on("webkitfullscreenchange", function (a, b) {
	const currentWindow = electron.remote.getCurrentWindow();
	if (document.webkitIsFullScreen) {
		currentWindow.setResizable(true);
		currentWindow.maximize();
	} else {
		currentWindow.setResizable(false);
		currentWindow.unmaximize();
		currentWindow.setSize(800, 600);
		currentWindow.center();
	}
});
/************ Named Functions *********************/
function reprocessVideo() {
	if (checkIfRecording()) {
		swal({
			type: 'warning',
			title: 'Please stop recording'
		});
	} else {
		sessionStorage.clear();
		sessionStorage.setItem("sessionFor", "reprocess-video");
		electron.remote.getCurrentWindow().setResizable(true);
		electron.remote.getCurrentWindow().maximize();
		electron.remote.getCurrentWindow().loadFile('preview-output-window/output-preview.html');
	}
}
function openExisting() {
	if (checkIfRecording()) {
		swal({
			type: 'warning',
			title: 'Please stop recording'
		});
	} else {
		dialog.showOpenDialog({
			browserWindow: electron.remote.getCurrentWindow(),
			title: 'Select File',
			buttonLabel: 'Select File',
			filters: [
				{ name: 'Configuration', extensions: ['CONFIG'] }
			],
			properties: ['openFile']
		}, function (filename) {
			if (filename != undefined) {
				// ipcRenderer.send('close-camera-preview');
				try {
					let obj = JSON.parse(fs.readFileSync(filename[0], 'utf8'));
					obj.configFolderPath = filename[0];
					obj.configFolderPath = filename[0].replace('settings.config', '');
					obj.sourceFile = 'preview-output-window/output-preview.html';
					sessionStorage.clear();
					sessionStorage.setItem("configFolderPath", obj.configFolderPath);
					sessionStorage.setItem("sessionFor", "process-config-folder");
					electron.remote.getCurrentWindow().setResizable(true);
					electron.remote.getCurrentWindow().loadFile(obj.sourceFile);
				} catch (exp) {
					showSimpleAlert('Error', 'Something went wrong', exp.message);
				}
			}
		});
	}
}
function refreshSources(param) {
	$(".source-list-wrap").empty();
	$("#source-list-modal").modal('hide');
	if (param && param.enumerate == false) {
		gotDevices(param.deviceInfos);
	} else {
		navigator.mediaDevices.enumerateDevices()
			.then(gotDevices)
			.catch(errorCallback);
	}
}
function gotDevices(deviceInfos) {
	videoSourceList = [];
	audioSourceList = [];
	for (var i = 0; i !== deviceInfos.length; ++i) {
		var deviceInfo = deviceInfos[i];
		if (deviceInfo.deviceId == "communications" || deviceInfo.deviceId == "default")
			continue;
		if (deviceInfo.kind === 'videoinput') {
			deviceInfo.text = deviceInfo.label || 'Camera ' + (videoSourceList.length + 1);
			videoSourceList.push(deviceInfo);
		}
		if (deviceInfo.kind === 'audioinput') {
			deviceInfo.text = deviceInfo.label || 'Mic ' + (audioSourceList.length + 1);
			audioSourceList.push(deviceInfo);
		}
	}
	// ! now allowing to record even if camera or audio source is not connected
	if (videoSourceList.length == 0) {
		$("#current-video-source-btn").text("No sources found !");
		currentVideoSource = null;
		type = "Video ";
	} else {
		$("#current-video-source-btn").text(videoSourceList[0].text);
		currentVideoSource = videoSourceList[0];
		$("#current-video-source-btn").attr("title", videoSourceList[0].text);
	}
	if (audioSourceList.length == 0) {
		$("#current-audio-source-btn").text("No sources found !");
		currentAudioSource = null;
		type = " Audio"
	} else {
		$("#current-audio-source-btn").text(audioSourceList[0].text);
		currentAudioSource = audioSourceList[0];
		$("#current-audio-source-btn").attr("title", audioSourceList[0].text);
	}
	// ? initialize camera stream
	if (currentVideoSource) {
		// NOTE -> show preview only if camera device was found
		showCameraPreview();
	}
	// ? initialize screen stream
	reInitScreenStream();
	// check if any device was selected earlier in selected-devices.json
	let selectedDevicesFile = env.recorderFilesPath + "\\selected-devices.json";
	if (fs.existsSync(selectedDevicesFile)) {
		let previousSources = JSON.parse(fs.readFileSync(selectedDevicesFile, 'utf8'));
		// check if videoSourceList has previously connected video device
		for (let i = 0; i < videoSourceList.length; i++) {
			if (videoSourceList[i].deviceId == previousSources.video.deviceId) {
				currentVideoSource = videoSourceList[i];
				$("#current-video-source-btn").text(currentVideoSource.text);
				$("#current-video-source-btn").prop("title", currentVideoSource.text);
				break;
			}
		}
		for (let i = 0; i < audioSourceList.length; i++) {
			if (audioSourceList[i].deviceId == previousSources.audio.deviceId) {
				currentAudioSource = audioSourceList[i];
				$("#current-audio-source-btn").text(currentAudioSource.text);
				$("#current-audio-source-btn").prop("title", currentAudioSource.text);
				break;
			}
		}
	}
}
function errorCallback(error) {
	swal({
		type: "error",
		title: "Device Permission Error",
		text: error
	});
}
function listAVSources(sourceType) {
	let sources = [];
	let currentSelectedSource = null;
	if (sourceType == "audio") {
		$("#source-modal-title").text("Audio Sources");
		sources = audioSourceList;
		currentSelectedSource = currentAudioSource;
	} else if (sourceType == "video") {
		$("#source-modal-title").text("Video Sources");
		sources = videoSourceList;
		currentSelectedSource = currentVideoSource;
		// debugger
	}
	$(".source-list-wrap").empty();
	for (let i = 0; i < sources.length; i++) {
		let item = "<div class='a-source' deviceid='" + sources[i].deviceId + "' sourcetype='" + sourceType + "'>" + sources[i].text + "</div>";
		if (currentSelectedSource.deviceId == sources[i].deviceId) {
			item = "<div class='a-source active-source' deviceid='" + sources[i].deviceId + "' sourcetype='" + sourceType + "'>" + sources[i].text + "</div>";
		}
		$(".source-list-wrap").append(item);
	}
}
function initCheck(selectedRecElement) {
	let recType = selectedRecElement.getAttribute("rec-type");
	let flag = true;
	let toast = false;
	let position = "top-right";
	let text;
	let type = "error";
	if ((recType === 'camera' || recType === 'both') && currentVideoSource === null) {
		text = "cannot start recording without atleast one connected video device"
		flag = false;
	}
	if (flag) {
		$("#extra-info-rec-mode > .extra-info-text").text("Current Recording Mode : " + recType);
		oldRecording = currentRecording
		currentRecording = recType;
		$('.custom-card').removeClass('active-card');
		$(selectedRecElement).addClass('active-card');
		initRecorder();
	} else {
		swal({
			toast: true,
			timer: 4000,
			text: text,
			position: "top-right"
		});
	}
	if (oldRecording == '')
		oldRecording = currentRecording
}
function initRecorder() {
	/*Stop on switching mode*/
	// NOTE -> this will stop current recording
	// NOTE -> and also push in finalMerger
	if (c_recorder != undefined) {
		if (c_recorder.state != 'inactive')
			c_recorder.stop()
		closeAllStreams(c_recorder)
	}
	if (s_recorder != undefined) {
		if (s_recorder.state != 'inactive')
			s_recorder.stop()
		closeAllStreams(s_recorder)
	}
	if (recordingflag) {
		// TODO -> a.pause taskbar  b. pauseTimer
		let obj = {
			progress: 1,
			mode: "paused"
		};
		ipcRenderer.send("set-taskbar-progress", obj);
		pauseTimer();
		$('.rec-status-text').text('Recording Paused');
	}
	pausedFlag = false
	recordingflag = false
	s_recording = false
	c_recording = false
	cameraPreview = true
	let showPreview = true;
	// $(".fullscreen-wrap").css("visibility","visible");
	switch (currentRecording) {
		case 'camera':
			break;
		case 'screen':
			showPreview = false;
			break;
		case 'both':
			break;
	}
	// ? setting isPreviewReady to true from here for now
	// ? this should be set to true after all devices are loaded correctly
	handlePreviewReady();
	// ! removing this because we do not need it anymore-
	// ! CPU Usage optimization patch
	// ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedVideoDevice: JSON.stringify(currentVideoSource), selectedAudioDevice: JSON.stringify(currentAudioSource), reload: false, showAfterOpen: showPreview });
	// TODO - replacing camera-preview window with video tag
	if (currentRecording === 'both' || currentRecording === 'camera') {
		showCameraPreview();
	}
	if (currentRecording === 'both' || currentRecording === 'screen') {
		reInitScreenStream();
		if (currentRecording === 'screen') {
			$("#camera-stream-preview").hide();
		}
	}
}
function closeAllStreams(recorder) {
	recorder.stream.getTracks().forEach(track => track.stop()); // stop each of them
	var tracks = recorder.stream.getTracks()
	for (var i = 0; i < tracks.length; i++) {
		tracks[i].stop()
		tracks[i].enabled = false
	}
	const tmpStream = recorder.stream.getAudioTracks();
	if (tmpStream[0]) {
		tmpStream[0].stop();
	}
	var aTracks = recorder.stream.getAudioTracks()
	for (var i = 0; i < aTracks.length; i++) {
		aTracks[i].stop()
	}
	var vTracks = recorder.stream.getVideoTracks()
	for (var i = 0; i < vTracks.length; i++) {
		vTracks[i].stop()
	}
	recorder.stream = null
}
function pauseBothRecorders() {
	let obj = {
		progress: 1,
		mode: "paused"
	};
	ipcRenderer.send("set-taskbar-progress", obj);
	if (c_recorder != undefined) {
		if (c_recorder.state == 'recording') {
			c_recorder.pause();
			$("#play-pause-text").html("Resume Recording");
			$(".rec-status-text").html("Recording Paused");
			$(".video-play-button > img").attr('src', 'assets/images/play.png');
			pausedFlag = true;
			recordingflag = false;
		}
	}
	if (s_recorder != undefined) {
		if (s_recorder.state == 'recording') {
			s_recorder.pause();
			$("#play-pause-text").html("Resume Recording");
			$(".rec-status-text").html("Recording Paused");
			$(".video-play-button  > img").attr('src', 'assets/images/play.png');
			pausedFlag = true;
			recordingflag = false;
		}
	}
	pauseTimer();
}
function canPlay() {
	const obj = {
		flag: true,
		reason: ''
	}
	let isModeSelected = $(".video-play-button").hasClass("video-play-button-active");
	if (!isModeSelected) {
		obj.flag = false;
		obj.reason = 'Please Select Recording Mode'
	}
	if (!isPreviewReady) {
		obj.flag = false;
		obj.reason = 'Please Wait for stream to load'
	}
	return obj;
}
function playPause() {
	const canStartPlay = canPlay();
	if (canStartPlay.flag === true) {
		if (recordingflag == false)//play
		{
			if (isPreviewReady) {
				if (pausedFlag != false) {
					log.info('trying to resume timer after play event');
					resumeTimer();
				}
				$(".stop-btn-wrap").css('visibility', 'visible');
				// NOTE -> do not minimize app when recording mode is camera
				if (currentRecording !== 'camera') {
					ipcRenderer.send('minimize-app');
				}
				$(".video-play-button > img").attr('src', 'assets/images/pause.png');
				toggleUIWhileRecording('disable');
				let obj = {
					progress: 1,
					mode: "indeterminate"
				};
				ipcRenderer.send("set-taskbar-progress", obj);
				recordingflag = true;
				if (pausedFlag == false) {//start
					ipcRenderer.send("starter-window-open-close", "open");
					// ! for starting recording when window is open
					starterWindowOpenCloseHandler("open");
					$("#play-video").prop("disabled", "true");
					$(".custom-card").prop("disabled", "true");
				}
				else {//resume
					if (s_recorder != undefined) {
						if (s_recorder.state != 'inactive') {
							setTimeout(function () {
								s_recorder.resume();
							}, 500)
						}
						pausedFlag = false;
					}
					if (c_recorder != undefined) {
						if (c_recorder.state != 'inactive') {
							setTimeout(function () {
								c_recorder.resume();
								ipcRenderer.send('resumed-rec-preview', currentRecording);
							}, 500)
						}
						pausedFlag = false;
					}
				}
			}
			else {
				swal({
					toast: true,
					position: 'top-right',
					timer: 4000,
					title: 'Please wait for stream to load'
				});
			}
		}
		else //pause
		{
			let obj = {
				progress: 1,
				mode: "paused"
			};
			ipcRenderer.send("set-taskbar-progress", obj);
			// ! replacing all ipc calls for timer to functions in this window
			// pauseTimer(true);
			// ipcRenderer.send('timer', 'pause');
			//
			$("#play-pause-text").html("Resume Recording");
			$(".video-play-button > img").attr('src', 'assets/images/play.png');
			//
			toggleUIWhileRecording('enable');

			if (electron.remote.getCurrentWindow().isMinimized()) {
				electron.remote.getCurrentWindow().show();
			} else {
				pauseTimer();
			}
			ipcRenderer.send('paused-rec-preview', currentRecording)
			if (c_recorder != undefined) {
				if (c_recorder.state == 'recording') {
					c_recorder.pause();
					pausedFlag = true;
				}
			}
			if (s_recorder != undefined) {
				if (s_recorder.state == 'recording') {
					s_recorder.pause();
					pausedFlag = true;
				}
			}
			if (currentRecording == 'both') {
			}
			recordingflag = false;
			$('.rec-status-text').text('Recording Paused');
		}
		// ? show timer and set current time
		showRecordedTiming();
	} else {
		swal({
			toast: true,
			position: 'top-right',
			timer: 4000,
			title: canStartPlay.reason
		});
	}
}
function startScreenRec() {
	s_recordedChunks = [];
	ipcRenderer.send('start-rec', currentRecording);
	if (!s_recording) {
		s_handleStream(screenStream);
	}
	else {
		c_recorder.stop();
		s_recorder.stop();
		closeAllStreams(c_recorder)
		closeAllStreams(s_recorder)
		toggleFilePath = '/camera/';
		/*false for camera and true for screen*/
		startCameraRec()
		ipcRenderer.send('minimize-app');
		c_recording = true;
		s_recording = false;
	}
}
function startCameraRec() {
	// ipcRenderer.send('start-rec', currentRecording);
	// c_recordedChunks = [];
	// var videoValue = currentVideoSource.deviceId;
	// let c_constraints;
	// c_constraints = {
	// 	audio: false,
	// 	video: {
	// 		deviceId: { exact: videoValue },
	// 		width: { min: 320, ideal: 1280, max: 1920 },
	// 		height: { min: 144, ideal: 720, max: 1080 },
	// 		framerate: { min: 15, ideal: 30, max: 60 }
	// 	}
	// }
	// navigator.mediaDevices.getUserMedia(c_constraints)
	// 	.then((stream) => c_handleStream(stream))
	// 	.catch((e) => c_handleError(e));
	// ! we already have the stream in currentCameraStream variable
	c_handleStream(currentCameraStream);
}
function c_handleStream(stream) {
	cameraStream = stream;
	// /*navigator.webkitGetUserMedia({ audio: { deviceId : {exact : audioSelect.value}}, video: false },getMicroAudioForCamera, getUserMediaError);	*/
	// navigator.mediaDevices.getUserMedia(
	// 	{
	// 		audio: {
	// 			deviceId: { exact: currentAudioSource.deviceId },
	// 			sampleRate: 44100,
	// 			desiredSampleRate: 44100,
	// 			// audioBitsPerSecond: 128000,
	// 			channelCount: 2
	// 		},
	// 		video: false
	// 	})
	// 	.then((stream) => getMicroAudioForCamera(stream))
	// 	.catch((e) => getUserMediaError(e));
	// ! we already have added audio data in stream
	getMicroAudioForCamera(stream);
}
function c_handleError(e) {
	throw e;
	//errLogstream.write('\n' + e + '\n')
}
function s_handleStream(stream) {
	screenStream = stream;
	getMicroAudio(screenStream);
	// /*navigator.webkitGetUserMedia({ audio: {deviceId : { exact : audioSelect.value}}, video: false },getMicroAudio, getUserMediaError);	*/
	// navigator.mediaDevices.getUserMedia(
	// 	{
	// 		audio: {
	// 			mimeType: "video/mp4",
	// 			deviceId: { exact: currentAudioSource.deviceId },
	// 			sampleRate: sampleRate,
	// 			desiredSampleRate: sampleRate,
	// 			audioBitsPerSecond: audioBitsPerSecond,
	// 			channelCount: 1
	// 		},
	// 		video: false
	// 	})
	// 	.then((stream) => getMicroAudio(stream))
	// 	.catch((e) => getUserMediaError(e));
}
function s_handleError(e) {
	throw e;
	//errLogstream.write('\n' + e + '\n')
}
const c_recorderOnDataAvailable = (event) => {
	const usage = process.getCPUUsage();
	if (event.data && event.data.size > 0) {
		// ? Ready States are - 0=empty , 1=loading , 2=completed
		if (c_fileReader == null) {
			c_fileReader = new FileReader();
		}
		camChunksRead++;
		cameraBlobDataArr.push(event.data);
		if (c_fileReader.readyState != 1) {
			var options = { mimeType: "video/mp4" };
			var c_blob = new Blob(cameraBlobDataArr, options);
			c_fileReader.readAsArrayBuffer(c_blob);
			c_fileReader.onload = function () {
				var c_int8array = new Uint8Array(this.result);
				try {
					writeStreamCam.write(c_int8array);
				} catch (exception) {
				}
				const usage = process.getCPUUsage();
				camChunksWritten++;
				c_chunk = null;
				c_blob = null;
				c_int8array = null;
				cameraBlobDataArr = [];
				if (camStreamFlag == true) {
					writeStreamCam.end();
					camStreamFlag = false;
				}
			}
		} else {
			cameraReadyStateFailedCount++;
		}
		// }
	}
}
const getMicroAudioForCamera = (audioStream) => {
	cameraStream.addTrack(audioStream.getAudioTracks()[0])
	c_recorder.ondataavailable = c_recorderOnDataAvailable
	c_recorder.onstop = () => {
		camStreamFlag = true;
	}
	c_recorder.start(chunkRecordInterval);
	if (!fs.existsSync(documentsFolderPath)) {
		fs.mkdirSync(documentsFolderPath)
	}
	if (!fs.existsSync(documentsFolderPath + '/camera/')) {
		fs.mkdirSync(documentsFolderPath + '/camera');
	}
	if (!errorFilesCreated) {
		createErrorFiles();
		errorFilesCreated = true;
	}
	camCount++;
	writeStreamCam = fs.createWriteStream(documentsFolderPath + '/camera/rec' + camCount + extension);
	writeStreamCam.on('finish', function () {
		if (oldRecording == 'both') {
			preSendCheck(documentsFolderPath + '/camera/rec' + camCount + extension)
		}
		else // only camera
		{
			if (startTime == "") {
				startTime = 0
			}
			// ! removing start time fromt adding , because it generated incorrect timings
			// endTime = getSecondsFromCurrentTime() + startTime;
			endTime = getTotalRecordingSeconds();
			finalMerger.push({ type: 'camera', cpath: documentsFolderPath + '/camera/rec' + camCount + extension, startTime: startTime, endTime: endTime })
			updateFinalMergerInConfigFile();
			startTime = endTime
		}
		onFileLoaded();
	})
	ipcRenderer.send('started-rec-preview', currentRecording);
	c_recording = true;
}
function getTotalRecordingSeconds() {
	let totalTime = 0;
	// let limit = (playPauseTimings.length);
	// let limitObj = playPauseTimings[playPauseTimings.length - 1];
	// let limitIndex = playPauseTimings.length - 1;
	// if(limitObj && ! limitObj.end) {
	// 	limitIndex--;
	// }
	let secondsSinceLastPush = 0;
	// NOTE -> when length of playPauseTimings is odd number
	// ? this means currently the state of recorder is recording
	// ? now we calculate the duration which is recorded until the last push
	// ? in this array
	// if (limitObj && ! limitObj.end) {
	if (currentPlayPauseTimingObj.start) {
		let t1 = currentPlayPauseTimingObj.start;
		let t2 = new Date().getTime();
		secondsSinceLastPush = ((t2 - t1) / 1000);
	}
	// }
	// ? get seconds recorded using playPauseTimings array
	for (let index = 0; index < playPauseTimings.length; index++) {
		const t1 = playPauseTimings[index].start;
		const t2 = playPauseTimings[index].end;
		const secondsRecorded = ((t2 - t1) / 1000)
		totalTime += (secondsRecorded);
	}
	totalTime += secondsSinceLastPush;
	log.info('***************');
	log.info('getTotalRecordingSeconds function started');
	log.info('seconds since last push : ', secondsSinceLastPush);
	log.info('total recorded seconds calculated as : ', totalTime);
	log.info('getTotalRecordingSeconds function completed');
	log.info('***************');
	return totalTime;
}
function getUserMediaError(err) {
	//errLogstream.write('\n' + err + '\n')
	throw err;
}
const s_recorderOnDataAvailable = (event) => {
	if (event.data && event.data.size > 0) {
		if (s_fileReader == null) {
			s_fileReader = new FileReader()
		}
		screenChunksRead++;
		// ? Ready States are - 0=empty , 1=loading , 2=completed
		screenBlobDataArr.push(event.data);
		if (s_fileReader.readyState != 1) {
			var s_blob = new Blob(screenBlobDataArr, { mimeType: "video/mp4" });
			s_fileReader.readAsArrayBuffer(s_blob);
			s_fileReader.onload = function () {
				var s_int8array = new Uint8Array(this.result)
				// might return falsest
				try {
					writeStreamScr.write(s_int8array);
				} catch (exception) {
				}
				screenChunksWritten++;
				screenBlobDataArr = [];
				s_chunk = null;
				s_blob = null;
				s_int8array = null;
				if (scrStreamFlag == true) {
					writeStreamScr.end();
					scrStreamFlag = false;
				}
			}
		} else {
			screenReadyStateFailedCount++;
		}
		// }
	}
}
function s_handleStream(stream) {
	screenStream = stream;
	/*navigator.webkitGetUserMedia({ audio: {deviceId : { exact : audioSelect.value}}, video: false },getMicroAudio, getUserMediaError);	*/
	/*navigator.webkitGetUserMedia({ audio: {deviceId : { exact : audioSelect.value}}, video: false },getMicroAudio, getUserMediaError);	*/
	// navigator.mediaDevices.getUserMedia(
	// 	{
	// 		audio: {
	// 			mimeType: "video/mp4",
	// 			deviceId: { exact: currentAudioSource.deviceId },
	// 			// sampleRate: 44100,
	// 			// desiredSampleRate: 44100,
	// 			// audioBitsPerSecond: 128000,
	// 			// channelCount: 1
	// 		},
	// 		video: false
	// 	})
	// 	.then((stream) => getMicroAudio(stream))
	// 	.catch((e) => getUserMediaError(e));
	getMicroAudio(screenStream);
}
function s_handleError(e) {
	//errLogstream.write('\n' + e + '\n')
}
function stopRecFinal() {
	//
	recordingflag = false;
	if (canStopRec()) {
		let obj = {
			progress: -1,
			mode: "none"
		};
		ipcRenderer.send("set-taskbar-progress", obj);
		// clearInterval(logInterval) // remove this if unused
		// ipcRenderer.send('timer', 'reset')
		// ! replacing all ipc calls for timer to functions in this window
		resetTimer();
		recordingStop = false;
		//required for last case of 'both' object merger
		oldRecording = currentRecording // review this properly . find wherever it is used and whether used properly.
		if (c_recorder != undefined) {
			if (c_recorder.state == 'recording' || c_recorder.state == 'paused') {
				c_recorder.stop();
				recordingStop = true;
				closeAllStreams(c_recorder)
			}
		}
		if (s_recorder != undefined) {
			if (s_recorder.state == 'recording' || s_recorder.state == 'paused') {
				s_recorder.stop();
				recordingStop = true;
				closeAllStreams(s_recorder)
			}
		}
		c_recording = false;
		s_recording = false;
		//if(recordingStop)
		//{
		$("#play-pause-text").html("Start Recording");
		$(".video-play-button > img").attr('src', 'assets/images/play.png');
		recordingflag = false;
		pausedFlag = false;
		showHideIcon = false; // remove if unused
		// ipcRenderer.send('stop-rec', null)
		// backToHome();
		$(".menu-buttons").hide(); // remove if unused
		$("#progress-text").text("Loading Files ..") // remove if unused
		$("#progress-wrap").show(); // remove if unused
	}
	else {
		deselectAll();
		showRecordedTiming();
		showRecordingCompleteDialog();
		// recording state is inactive ;
		// user has not started current recording;
		// discard current recording and
		// proceed with previously completed recordings
		recordingStop = true;
		presendCount = 0;
	}
}
function canStopRec() {
	var flag = true;
	if (currentRecording == 'camera') {
		if (c_recorder == undefined || c_recorder.state == 'inactive') {
			flag = false;
		}
	}
	if (currentRecording == 'screen') {
		if (s_recorder == undefined || s_recorder.state == 'inactive') {
			flag = false;
		}
	}
	if (currentRecording == 'both') {
		if (c_recorder == undefined || c_recorder.state == 'inactive' || s_recorder == undefined || s_recorder.state == 'inactive') {
			flag = false;
		}
	}
	return flag;
}
function sourceChanged(sourceEl) {
	let previousConnectedAudioDevice = null;
	let previousConnectedVideoDevice = null;
	let previousConnectedFile = env.recorderFilesPath + "\\selected-devices.json";
	if (fs.existsSync(previousConnectedFile)) {
		let device = JSON.parse(fs.readFileSync(previousConnectedFile, 'utf8'));
		previousConnectedAudioDevice = device.audio;
		previousConnectedVideoDevice = device.video;
	}
	let sourceType = sourceEl.getAttribute('sourcetype');
	let deviceId = sourceEl.getAttribute('deviceid');
	$(".a-source").removeClass("active-source")
	$(sourceEl).addClass("active-source");
	let sources = [];
	if (sourceType == "audio") {
		sources = audioSourceList;
	} else if (sourceType == "video") {
		sources = videoSourceList;
	}
	for (let i = 0; i < sources.length; i++) {
		if (sources[i].deviceId == deviceId && sourceType == "audio") {
			currentAudioSource = sources[i];
			previousConnectedAudioDevice = currentAudioSource;
			// sessionStorage.setItem("currentAudioSource",JSON.stringify(currentAudioSource));
		} else if (sources[i].deviceId == deviceId && sourceType == "video") {
			currentVideoSource = sources[i];
			previousConnectedVideoDevice = currentVideoSource;
			// session not defined error
			// sessionStorage.setItem("currentVideoSource",JSON.stringify(currentVideoSource));
		}
	}
	let showPreview = true;
	if (currentRecording == "screen") {
		showPreview = false;
	}
	// ! removing this because we do not need it anymore-
	// ! CPU Usage optimization patch
	// ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedVideoDevice: JSON.stringify(currentVideoSource), selectedAudioDevice: JSON.stringify(currentAudioSource), reload: true, deviceChanged: true, showAfterOpen: showPreview });
	$("#current-audio-source-btn").text(currentAudioSource.text);
	$("#current-audio-source-btn").prop("title", currentAudioSource.text);
	$("#current-video-source-btn").text(currentVideoSource.text);
	$("#current-video-source-btn").prop("title", currentVideoSource.text);
	$("#source-list-modal").modal("hide");
	let device = {
		audio: currentAudioSource,
		video: currentVideoSource
	}
	if (fs.existsSync(previousConnectedFile)) {
		fs.truncateSync(previousConnectedFile);
	}
	fs.appendFileSync(previousConnectedFile, JSON.stringify(device), 'utf-8');
	reInitCameraStream();
	reInitScreenStream();
	// fs.appendFileSync(previousConnectedFile,JSON.stringify(device));
	// debugger;
}
function createErrorFiles() {
	// memLogStream = fs.createWriteStream(documentsFolderPath + '/memory-usage-log.txt');
	// fs.truncate(documentsFolderPath + '/memory-usage-log.txt', function () {
	// 	memLogStream.write('HiFi Recorder Memory Log file!\n');
	// 	memLogStream.write('Details of memory used : \n');
	// })
	// //errLogstream = fs.createWriteStream(documentsFolderPath + '/main-error-log.txt');
	// //errLogstream.write('HiFi Recorder Error Log file!\n');
	// fs.truncate(documentsFolderPath + '/main-error-log.txt', function () {
	// })
}
const getMicroAudio = (audioStream) => {
	s_recorder.ondataavailable = s_recorderOnDataAvailable
	s_recorder.onstop = () => {
		scrStreamFlag = true;
	}
	s_recorder.start(chunkRecordInterval);
	if (!fs.existsSync(documentsFolderPath)) {
		try {
			fs.mkdirSync(documentsFolderPath)
		} catch (exp) {
			if (exp) {
				showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
			}
		}
	}
	if (!fs.existsSync(documentsFolderPath + '/screen/')) {
		try {
			fs.mkdirSync(documentsFolderPath + '/screen');
		} catch (exp) {
			if (exp) {
				showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
			}
		}
	}
	if (!errorFilesCreated) {
		createErrorFiles();
		errorFilesCreated = true;
	}
	screenCount++;
	writeStreamScr = fs.createWriteStream(documentsFolderPath + '/screen/rec' + screenCount + extension)
	writeStreamScr.on('finish', function () {
		if (oldRecording == 'both') {
			preSendCheck(documentsFolderPath + '/screen/rec' + screenCount + extension)
		}
		else // only screen
		{
			if (startTime == "") {
				startTime = 0
			}
			// ! removing start time fromt adding , because it generated incorrect timings
			// endTime = getSecondsFromCurrentTime() + startTime;
			endTime = getTotalRecordingSeconds();
			finalMerger.push({ type: 'screen', cpath: documentsFolderPath + '/screen/rec' + screenCount + extension, windowCoords: windowCoords, startTime: startTime, endTime: endTime, fullScreenMode: oldFullScreenMode }) // remove oldfullscreenmode  if unused
			updateFinalMergerInConfigFile();
			startTime = endTime
		}
		onFileLoaded();
	})
	if (currentRecording == 'both' && c_recorder != undefined) {
		if (c_recorder.state == 'inactive' || c_recorder.state == 'paused') {
			c_recorder.stop();
			closeAllStreams(c_recorder)
			startCameraRec(false)
		}
	}
	s_recording = true;
	c_recording = true;
}
function onFileLoaded() {
	if (recordingStop && preSendCount == 0) {
		showRecordedTiming();
		showRecordingCompleteDialog();
	}
}
function showSaveDialog(backupFolderPath) {
	dialog.showSaveDialog({
		title: 'Save Backup Folder As',
		defaultPath: backupFolderPath,
		buttonLabel: 'Save Backup Folder',
	}, function (foldername) {
		backupFolderPath = backupFolderPath.replace(/\//g, "\\");
		if (foldername != undefined && (foldername.substr(0, foldername.lastIndexOf('\\')) != backupFolderPath)) {
			fs.mkdirSync(foldername)
			// disable recording until backup has created
			// Patch SeqNo 24102018
			$('.menu-buttons').hide()
			$('#progress-wrap').show()
			// disabled in videoUtils.copyFolders()
			$('#progress-text').text('Copying Files to New Folder ..')
			videoUtils.updateBackupFolder(backupFolderPath, foldername, electron.remote.getCurrentWindow())
		}
		else if (foldername != undefined && (foldername.substr(0,
			foldername.lastIndexOf('\\')) == backupFolderPath)) {
			alert("cannot copy to default folder; Please select another folder")
			showSaveDialog(backupFolderPath)
		}
		//errLogstream.end()
	})
}
function preSendCheck(file) {
	preSendCount++;
	if (preSendCount == 2) {
		preSendCount = 0;
		if (startTime == "") {
			startTime = 0
		}
		// ! removing start time fromt adding , because it generated incorrect timings
		// endTime = getSecondsFromCurrentTime() + startTime;
		endTime = getTotalRecordingSeconds();
		finalMerger.push({ type: 'both', cpath: documentsFolderPath + '/camera/rec' + camCount + extension, spath: documentsFolderPath + '/screen/rec' + screenCount + extension, windowCoords: windowCoords, startTime: startTime, endTime: endTime });
		updateFinalMergerInConfigFile();
		startTime = endTime
	}
}
navigator.mediaDevices.ondevicechange = function (event) {
	let audioSourceCount = 0, videoSourceCount = 0
	navigator.mediaDevices.enumerateDevices()
		.then((deviceInfos) => {
			let isCurrentAudioDeviceConnected = false;
			let isCurrentVideoDeviceConnected = false;
			for (var i = 0; i !== deviceInfos.length; ++i) {
				var deviceInfo = deviceInfos[i];
				if (deviceInfo.deviceId == "communications" || deviceInfo.deviceId == "default")
					continue;
				if (deviceInfo.kind === 'videoinput') {
					if (currentVideoSource) {
						if (deviceInfo.deviceId == currentVideoSource.deviceId) {
							isCurrentVideoDeviceConnected = true;
						}
					}
					videoSourceCount++;
				}
				if (deviceInfo.kind === 'audioinput') {
					if (currentAudioSource) {
						if (deviceInfo.deviceId == currentAudioSource.deviceId) {
							isCurrentAudioDeviceConnected = true;
						}
					}
					audioSourceCount++;
				}
			}
			if (audioSourceList.length != audioSourceCount || videoSourceList.length != videoSourceCount) {
				//
				// debugger
				if (!isCurrentAudioDeviceConnected || !isCurrentVideoDeviceConnected) {
					stopRecFinal();
					electron.remote.getCurrentWindow().show();
					oldRecording = currentRecording;
					recordingStop = true;
					checkIfRecording();
					// moved from start of function to here
					$("#current-audio-source-btn").text("No Device Selected");
					currentAudioSource = null;
					$("#current-audio-source-btn").attr("title", "No Device Selected");
					$("#current-video-source-btn").text("No Device Selected");
					currentVideoSource = null;
					$("#current-video-source-btn").attr("title", "No Device Selected");
					swal({
						title: 'Media Devices Changed',
						text: 'Please verify connected devices before proceeding',
						type: 'warning',
						showCancelButton: false,
						confirmButtonText: 'OK',
					});
				}
				refreshSources({ enumerate: false, deviceInfos: deviceInfos }); // use got devices instea
			}
		})
		.catch((error) => {
			//errLogstream.write('\n' + err + '\n');
			swal({
				type: "error",
				title: "Device Error",
				text: "Problem detecting connected devices!"
			});
		});
}
// function handleRecorderWindowClose() {
// 	if (s_recorder) {
// 		if (s_recorder.state == 'recording' || s_recorder.state == 'paused') {
// 			swal({
// 				type: 'info',
// 				title: 'Screen Area window is required for recording !',
// 				text: 'Please do not close windows , it will result in incomplete recording !'
// 			}).then(res => {
// 				stopRecFinal();
// 			});
// 		}
// 	}
// }
function handlePreviewWindowClose() {
	if (c_recorder) {
		if (c_recorder.state == 'recording' || c_recorder.state == 'paused') {
			swal({
				type: 'info',
				title: 'Preview window is required for recording !',
				text: 'Please do not close windows , it will result in incomplete recording !'
			}).then(res => {
				stopRecFinal();
			});
		}
	}
}
function checkIfRecording() {
	let flag = false;
	if (s_recorder) {
		if (s_recorder.state == 'paused' || s_recorder.state == 'recording') {
			flag = true;
		}
	}
	if (c_recorder) {
		if (c_recorder.state == 'paused' || c_recorder.state == 'recording') {
			flag = true;
		}
	}
	if (flag == false) {
		deselectAll();
	}
	return flag;
}
function deselectAll() {
	$(".active-card").removeClass("active-card");
	if (currentRecording == 'camera' || currentRecording == 'both')
		// ipcRenderer.send('close-camera-preview');
		if (currentRecording == 'screen' || currentRecording == 'both')
			ipcRenderer.send('close-recorder');
	$(".video-play-button").removeClass("video-play-button-active");
	$('.video-play-button > img').attr('src', 'assets/images/play-disabled.png');
	currentRecording = oldRecording = "";
}
function checkPreviewSettingsFile() {
	// ? if settings file does not exist..
	// ? then create an array and add a single object to it
	// ? we are using settings as object since allowing multiple templates
	// ? starting from version 1.0.18
	if (!fs.existsSync(env.settingsFilePath)) {
		const settingsObj = [{
			"preview": {
				"camera": {
					"cwidth": 1920, "cwidthRaw": 704, "cheight": 1080, "cheightRaw": 396, "cleft": 0, "cleftRaw": "0px", "ctop": 0, "ctopRaw": "0px"
				}
				,
				"screen": {
					"cwidth": 1920, "cwidthRaw": 704, "cheight": 1080, "cheightRaw": 396, "cleft": 0, "cleftRaw": "0px", "ctop": 0, "ctopRaw": "0px"
				}
				,
				"both": {
					"swidth": 1920, "swidthRaw": 704, "sheight": 1080, "sheightRaw": 396, "sleft": 0, "sleftRaw": "0px", "stop": 0, "stopRaw": "0px", "cwidth": 462, "cwidthRaw": 251, "cheight": 282, "cheightRaw": 155, "cleft": 822, "cleftRaw": "452px", "ctop": 436, "ctopRaw": "240px"
				}
			}
			,
			"outputRes": {
				"width": 1920, "height": 1080
			},
			"streamRes": {
				"width": 1920, "height": 1080
			}
			,
			"preset": "veryfast",
			"crf": 23,
			"backImgPath": "false",
			name : 'Auto Generated Template',
			defaultkey : true,
			templatePath : 'auto-generated-template'
		}];
		// fs.writeFile(env.settingsFilePath, JSON.stringify(settingsObj), (err) => {
		// 	if (err) {
		// 		showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
		// 	}
		// });
		try {
			fs.writeFileSync(env.settingsFilePath, JSON.stringify(settingsObj));
			// if(!fs.existsSync(env.settingsTemplatePath)) {
			// 	fs.mkdirSync(env.settingsTemplatePath);
			// }
			// const autoTemplatePath = path.join(env.settingsTemplatePath,'Auto Generated Template.png');
			// fs.copyFileSync('assets/images/auto-generated-template.png',autoTemplatePath);
		} catch (exception) {
			showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
		}
	} else {
		// ? if the file exists but it contains an object
		// ? which might be possible before using version 1.0.18
		const settings = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf-8'));
		if (settings.constructor === Object) {
			settings.name = 'Auto Generated Template';
			settings.defaultkey = true;
			settings.templatePath = 'auto-generated-template';
			const allTemplates = [settings];
			try {
				fs.writeFileSync(env.settingsFilePath, JSON.stringify(allTemplates),'utf-8');
				// if(!fs.existsSync(env.settingsTemplatePath)) {
				// 	fs.mkdirSync(env.settingsTemplatePath);
				// }
				// const autoTemplatePath = path.join(env.settingsTemplatePath,'Auto Generated Template.png');
				// fs.copyFileSync('assets/images/auto-generated-template.png',autoTemplatePath);
			} catch (exep) {
				showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
			}
		} else {
		}
	}
	if(!fs.existsSync(env.settingsTemplatePath)) {
		fs.mkdirSync(env.settingsTemplatePath);
	}
	// const autoTemplatePath = path.join(env.settingsTemplatePath,'Auto Generated Template.png');
	// fs.copyFileSync('assets/images/auto-generated-template.png',autoTemplatePath);
}
function showRecordingCompleteDialog() {
	let totalRecordingTime = getTotalRecordingSeconds();
	totalRecordingTime = secondsToHms(totalRecordingTime);
	// let config = videoUtils.copyErrorFilesAndCreateSettingsConfig(os.homedir + '/Documents/HiFi_Recorder Files', documentsFolderPath);
	videoUtils.changeFinalMergerPaths();
	if (fs.existsSync(documentsFolderPath + "/settings.config")) {
		try {
			fs.writeFileSync(documentsFolderPath + "/settings.config", JSON.stringify({ finalMerger: finalMerger }));
		} catch (exp) {
			showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
		}
	}
	backupFolderPath = documentsFolderPath;
	backupFolderPath = backupFolderPath.replace("\\resources\\app.asar\\", "\\")
	$("#progress-wrap").hide();
	recordingStop = false
	$(".menu-buttons").show();
	swal({
		title: "Recording Complete",
		text: "Video Duration " + totalRecordingTime + " Process files now ?",
		icon: "success",
		showCancelButton: true,
		focusConfirm: false,
		confirmButtonText:
			'Edit Video Now',
		confirmButtonAriaLabel: 'Now',
		cancelButtonText:
			'Edit Video Later',
		cancelButtonAriaLabel: 'Later',
		buttons: {
			later: {
				text: "Later",
				value: "later"
			},
			now: {
				text: "Now",
				value: "now",
			}
		},
	}).then((value => {
		if (value.value == true) {
			// checkUpdateDownloading().then((res)=>{
			// if(res.downloading == false){
			let obj = {
				process: 'now',
				documentsFolderPath: documentsFolderPath,
				sourceFile: './preview-output-window/output-preview.html',
				config: {
					finalMerger: finalMerger
				}
			};
			// ipcRenderer.send('process-videos', obj);
			let currentWindow = electron.remote.getCurrentWindow();
			sessionStorage.clear();
			sessionStorage.setItem("configFolderPath", documentsFolderPath);
			sessionStorage.setItem("sessionFor", "process-config-folder");
			sessionStorage.setItem("windowParam", JSON.stringify(obj));
			currentWindow.loadFile(obj.sourceFile);
			/*optname is set using functions in videoUtilsCons*/
			// }
			// });
		}
		else if (value.dismiss == "cancel") {
			swal({
				title: "Project Saved",
				text: "Please Locate files at \n " + path.normalize(backupFolderPath),
				icon: "info",
				// 		showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:
					'Open Folder',
				confirmButtonAriaLabel: 'Open',
				// cancelButtonText:
				//   'Change Now',
				// cancelButtonAriaLabel: 'change',
				buttons: {
					change: {
						text: "Change Now",
						value: "change"
					},
					open: {
						text: "Open Folder",
						value: "open",
					}
				},
			}).then((value => {
				if (value.dismiss == "cancel") {
					showSaveDialog(backupFolderPath);
				}
				else if (value.value == true) {
					//errLogstream.end()
					electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
						electron.remote.getCurrentWindow().reload();
					});
					shell.openItem(backupFolderPath);
				} else {
					//errLogstream.end()
					electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
						electron.remote.getCurrentWindow().reload();
					});
				}
			}));
		}
		else {
			// default is later
			electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
				electron.remote.getCurrentWindow().reload();
			});
		}
	}));
}
async function checkUpdateDownloading() {
	return new Promise((resolve, reject) => {
		if (isUpdateDownloading) {
			swal.fire({
				type: "warning",
				title: "Update Download in progress",
				cancelButtonText: "Cancel Update",
				confirmButtonText: "Continue Update",
				showCancelButton: true
			}).then((res) => {
				if (res.value) {
					resolve({ downloading: true });
				} else {
					resolve({ downloading: false });
				}
			})
		} else {
			resolve({ downloading: false });
		}
	});
}
function updateFinalMergerInConfigFile() {
	let settingsFilePath = document + "/settings.config";
	if (fs.existsSync(settingsFilePath)) {
		try {
			fs.truncateSync(settingsFilePath);
			videoUtils.changeFinalMergerPaths();
			fs.writeFileSync(settingsFilePath, JSON.stringify({ finalMerger: finalMerger }));
		} catch (exp) {
			showSimpleAlert('Error', 'File System Error', 'HiFi Recorder requires file access permission to run smoothly, Please check security settings such as "Controlled Folder Access"');
		}
	}
}
function handlePreviewReady() {
	// ! automatically setting isPreviewReady to  true,
	// ! because we do not need previewWindow anymore
	isPreviewReady = true;
	if (isPreviewReady) {
		$(".video-play-button").addClass("video-play-button-active");
		$(".video-play-button > img").prop("src", "assets/images/play.png");
	} else {
		$('.video-play-button > img').attr('src', 'assets/images/play-disabled.png');
	}
}
function resumeTimer() {
	const currentMilliTime = new Date().getTime();
	// const timingObj = { start: currentMilliTime };
	currentPlayPauseTimingObj['start'] = currentMilliTime;
	// playPauseTimings.push(timingObj);
	log.info('resuming timer inside resumeTimer function at : ', currentMilliTime);
	log.info('play pause timings now : ', JSON.stringify(playPauseTimings));
	timerInterval = setInterval(function () {
		showRecordedTiming();
		$('.rec-status-text').text('Now Recording ...');
	}, 500);
}
function pauseTimer() {
	const currentMilliTime = new Date().getTime();
	// if(playPauseTimings.length > 0) {
	// 	playPauseTimings[playPauseTimings.length - 1]['end'] = currentMilliTime;
	// } else {
	// 	
	// }
	if (currentPlayPauseTimingObj.start) {
		currentPlayPauseTimingObj['end'] = currentMilliTime;
		playPauseTimings.push(currentPlayPauseTimingObj);
	}
	currentPlayPauseTimingObj = {};
	log.info('PAUSING timer inside pauseTimer function at : ', currentMilliTime);
	log.info('play pause timings now : ', JSON.stringify(playPauseTimings));
	clearInterval(timerInterval);
	$('.rec-status-text').text('Recording Paused');
}
function resetTimer() {
	// NOTE -> only push if last item pushed was play type
	// ? that can be identified if length of array is odd
	// if (playPauseTimings.length % 2 !== 0) {
	// const currentMilliTime = new Date().getTime();
	// if(playPauseTimings.length > 0) {
	// 	playPauseTimings[playPauseTimings.length - 1]['end'] = currentMilliTime;
	// } else {
	// 	
	// }
	// }
	if (currentPlayPauseTimingObj.start) {
		currentPlayPauseTimingObj.end = new Date().getTime();
		playPauseTimings.push(currentPlayPauseTimingObj);
	}
	currentPlayPauseTimingObj = {};
	clearInterval(timerInterval);
	secs = 0;
	mins = 0;
	hours = 0;
	log.info('Stopping timer inside resetTimer function at : ', new Date().getTime());
	log.info('play pause timings now : ', JSON.stringify(playPauseTimings));
}
function handleUpdateCurrentTime(event, param) {
}
function reInitCameraStream() {
	const filePath = globalSettingsFilePath + 'settings.json';
	let idealWidth = 1080;
	let idealHeight = 1920;
	let idealFrameRate = 30;
	if (fs.existsSync(filePath)) {
		let obj = JSON.parse(fs.readFileSync(filePath, 'utf8'));
		if (obj && obj.streamRes && obj.streamRes.width !== 'Auto' && obj.streamRes.height !== 'Auto') {
			idealWidth = parseInt(obj.streamRes.width, 10);
			idealHeight = parseInt(obj.streamRes.height, 10);
		} else {
			idealWidth = 'Auto';
			idealHeight = 'Auto';
		}
		if (obj && obj.streamFrameRate) {
			idealFrameRate = parseInt(obj.streamFrameRate, 10);
		}
	}
	let audioObj = false;
	if (currentAudioSource) {
		audioObj = {
			deviceId: { exact: currentAudioSource.deviceId },
			sampleRate: audioSampleRate,
			desiredSampleRate: { exact: audioSampleRate },
			audioBitsPerSecond: audioBitsPerSecond,
			channelCount: 1,
			echoCancellation: false,
			sampleSize: { exact: 16 }
		}
	}
	const constraints = {
		video: {
			deviceId: { exact: currentVideoSource.deviceId },
			width: { min: 320, ideal: idealWidth, max: 1920 },
			height: { min: 144, ideal: idealHeight, max: 1080 },
			frameRate: { min: 15, ideal: idealFrameRate, max: 60 },
			videoBitsPerSecond: videoBitsPerSecond
		}, audio: audioObj
	}
	if (audioBitsPerSecond === 'Auto') {
		delete constraints['video']['audioBitsPerSecond'];
	}
	if (videoBitsPerSecond === 'Auto') {
		delete constraints['video']['videoBitsPerSecond'];
	}
	if (idealWidth === 'Auto' && idealHeight === 'Auto') {
		delete constraints['video']['width'];
		delete constraints['video']['height'];
	}
	navigator.mediaDevices.getUserMedia(constraints)
		.then((stream) => {
			// const allAudioTracks = stream.getAudioTracks();
			// allAudioTracks.forEach(track => {
			//
			// });
			// const allVideoTracks = stream.getVideoTracks();
			// allVideoTracks.forEach(track => {
			//
			// });
			currentCameraStream = stream;
			const videoEl = document.getElementById("camera-stream-preview");
			videoEl.srcObject = currentCameraStream;
			initMediaRecorderCamera();
		})
		.catch((err) => {
			throw err;
		});
}
function reInitScreenStream() {
	const s_constraints = {
		video: {
			mandatory: { chromeMediaSource: 'desktop' }
		}
	}
	navigator.mediaDevices.getUserMedia(s_constraints)
		.then((stream) => {
			screenStream = stream;
			// ? we cannot attach audio with screen in a single call to
			// ? getUserMedia if using chromeMediaSource as 'desktop'
			// TODO - call getUserMedia again to get audio stream and attach the stream
			// TODO - to current screenStream
			if (currentRecording === 'screen' && currentAudioSource !== null) {
				const screenAudioConstraints = {
					video: false,
					audio: {
						deviceId: { exact: currentAudioSource.deviceId }
					}
				}
				navigator.mediaDevices.getUserMedia(screenAudioConstraints).then((screenAudioStream) => {
					const screenAudioTracks = screenAudioStream.getAudioTracks();
					if (screenAudioTracks.length > 0) {
						screenStream.addTrack(screenAudioTracks[0]);
						initMediaRecorderScreen();
					} else {
						swal({
							type: "error",
							title: "Cannot read Audio from selected Audio Device"
						});
					}
				}).catch((err) => {
				});
			} else {
				initMediaRecorderScreen();
			}
		})
		.catch((err) => {
		});
}
function showCameraPreview() {
	reInitCameraStream();
	// ! should be handled independently of camera preview
	// reInitScreenStream();
	$("#camera-stream-preview").show();
}
function openFullscreen(videoElem) {
	if (videoElem.requestFullscreen) {
		videoElem.requestFullscreen();
	} else if (videoElem.mozRequestFullScreen) { /* Firefox */
		videoElem.mozRequestFullScreen();
	} else if (videoElem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
		videoElem.webkitRequestFullscreen();
	} else if (videoElem.msRequestFullscreen) { /* IE/Edge */
		videoElem.msRequestFullscreen();
	}
}
function initMediaRecorderCamera() {
	const options = {
		audioBitsPerSecond,
		videoBitsPerSecond
	};
	if (audioBitsPerSecond === 'Auto') {
		delete options['audioBitsPerSecond'];
	}
	if (videoBitsPerSecond === 'Auto') {
		delete options['videoBitsPerSecond'];
	}
	try {
		c_recorder = new MediaRecorder(currentCameraStream, options);
	} catch (e) {
		throw e;
	}
}
function initMediaRecorderScreen() {
	const options = {
		videoBitsPerSecond: (2048 * 1000)
	};
	if (videoBitsPerSecond === 'Auto') {
		delete options['videoBitsPerSecond'];
	}
	try {
		s_recorder = new MediaRecorder(screenStream, options);
	} catch (e) {
		throw e;
	}
}
function initBitrates() {
	const filePath = globalSettingsFilePath + 'settings.json';
	if (fs.existsSync(filePath)) {
		const fileReadData = fs.readFileSync(filePath, 'utf8');
		let obj = JSON.parse(fileReadData);
		if (obj && obj.audioBitsPerSecond) {
			if (obj.audioBitsPerSecond !== 'Auto') {
				audioBitsPerSecond = parseInt(obj.audioBitsPerSecond, 10);
			}
		}
		if (obj && obj.videoBitsPerSecond) {
			if (obj.videoBitsPerSecond !== 'Auto') {
				videoBitsPerSecond = parseInt(obj.videoBitsPerSecond, 10);
			}
		}
	}
}
function starterWindowOpenCloseHandler(param) {
	if (param == "close") {
		let obj = {
			progress: 1,
			mode: "indeterminate"
		};
		ipcRenderer.send("set-taskbar-progress", obj);
		if (currentRecording == 'screen') {
			startScreenRec();
		}
		else if (currentRecording == 'camera') {
			startCameraRec();
		}
		else {
			startCameraRec();
			startScreenRec();
		}
		resumeTimer();
		$("#play-video").prop("disabled", "false");
		$(".custom-card").prop("disabled", "false");
		// ? create config file and/or update finalMerger
		if (!fs.existsSync(documentsFolderPath)) {
			fs.mkdirSync(documentsFolderPath)
		}
		if (fs.existsSync(documentsFolderPath + '/settings.config')) {
			fs.truncateSync(documentsFolderPath + '/settings.config')  // creates empty file if it exists
		}
		let config = {
			finalMerger: finalMerger
		};
		fs.appendFileSync(documentsFolderPath + '/settings.config', JSON.stringify(config));
	}
}
function showSimpleAlert(type, title, text) {
	swal({
		type,
		title,
		text
	});
}
function createSettingsJsonIfDoesNotExist() {
	let incorrectJson = false;
	if (fs.existsSync(env.settingsFilePath)) {
		try {
			JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
		} catch (exception) {
			incorrectJson = true;
			throw exception;
		}
	} else {
		incorrectJson = true;
	}
	if (incorrectJson) {
		// ? reset json object
		const obj = [{
			"preview": {
				"camera": {
					"cwidth": 1920, "cwidthRaw": 704, "cheight": 1080, "cheightRaw": 396, "cleft": 0, "cleftRaw": "0px", "ctop": 0, "ctopRaw": "0px"
				}
				,
				"screen": {
					"cwidth": 1920, "cwidthRaw": 704, "cheight": 1080, "cheightRaw": 396, "cleft": 0, "cleftRaw": "0px", "ctop": 0, "ctopRaw": "0px"
				}
				,
				"both": {
					"swidth": 1920, "swidthRaw": 704, "sheight": 1080, "sheightRaw": 396, "sleft": 0, "sleftRaw": "0px", "stop": 0, "stopRaw": "0px", "cwidth": 462, "cwidthRaw": 251, "cheight": 282, "cheightRaw": 155, "cleft": 822, "cleftRaw": "452px", "ctop": 436, "ctopRaw": "240px"
				}
			}
			,
			"outputRes": {
				"width": 1920, "height": 1080
			},
			"streamRes": {
				"width": 1920, "height": 1080
			}
			,
			"preset": "veryfast",
			"crf": 23,
			"backImgPath": "false",
			"streamFrameRate": "30",
			"outputFrameRate": "auto",
			"defaultkey": true,
			"name": "Auto Generated Template",
			"templatePath" :"auto-generated-template"
		}];
		fs.writeFileSync(env.settingsFilePath, JSON.stringify(obj));
	}
}
function showRecordedTiming() {
	$('.rec-time-row').css('visibility', 'visible');
	const currentTime = getTotalRecordingSeconds();
	const currentTimeHMS = secondsToHms(currentTime);
	$('.rec-time-text').html(currentTimeHMS);
}
function checkRemainingDiskSpace() {
	if (process.platform === 'win32') {
		checkDiskSpace('C:/').then((diskSpace) => {
			let freeSpace = (diskSpace.free / (1024 * 1024 * 1024));
			if (freeSpace <= env.freeSpaceLimitGB) {
				showSimpleAlert('Info', 'Low Space On Disk', 'C Drive contains only ' + freeSpace.toFixed(2) + " GB, if you do not free up atleast " + env.freeSpaceLimitGB + " GB of space, then Your further recordings will be lost");
			}
		}).catch((err) => {
			showSimpleAlert('Info', '', 'Could not determine space requirements of this computer, Please make sure you have atleast 5 GB space remaining in C Drive');
		});
	} else if (process.platform === 'darwin' || process.platform === 'linux') {
		//  change this in mac version
	}
}
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
	if (errorObj.code === 'ENOSPC') {
		stopRecFinal();
	}
	alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
		+ ' Column: ' + column + ' StackTrace: ' + errorObj.toString() + '\n');
}
function secondsToHms(timeInSeconds) {
	const pad = function (num, size) { return ('000' + num).slice(size * -1); },
		time = parseFloat(timeInSeconds).toFixed(3),
		hours = Math.floor(time / 60 / 60),
		minutes = Math.floor(time / 60) % 60,
		seconds = Math.floor(time - minutes * 60);
	const milli = ((timeInSeconds - parseInt(timeInSeconds)) * 1000).toFixed(0);
	return pad(hours, 2) + ' hrs : ' + pad(minutes, 2) + ' min : ' + pad(seconds, 2) + ' secs';
}
function getLastFilledTimingObjIndex() {
	let limitIndex;
	playPauseTimings.forEach((timingObj, index) => {
		if (timingObj.start && timingObj.end) {
			limitIndex = index;
		}
	});
	return limitIndex;
}
function toggleUIWhileRecording(mode) {
	if(mode === 'enable') {
		$('#current-audio-source-btn').prop('disabled', false);
		$('#current-video-source-btn').prop('disabled', false);
		$('#settingsImg').prop('disabled', false);
		$('#reprocess-video').prop('disabled', false);
	} else if(mode === 'disable') {
		$('#current-audio-source-btn').prop('disabled', true);
		$('#current-video-source-btn').prop('disabled', true);
		$('#settingsImg').prop('disabled', true);
		$('#reprocess-video').prop('disabled', true);
	}
}