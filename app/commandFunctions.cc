#include <napi.h>
// #include <node_api.h>
#include <windows.h>
#include <string>
#include "commandFunctions.h"
#include <iostream>
#include <fstream>
#include <algorithm>

using namespace std;

HANDLE hStdIn_c; // Handle to parents std input. NULL
BOOL bRunThread_c;
int id;
std::string fileName;
DWORD currentGlobalProcessId = NULL;

HANDLE currentGlobalChildProcess = NULL;
void setId_fileName(int id_s, std::string filename)
{
    id = id_s;
    fileName = filename;
}
void DisplayError(char *pszAPI)
{
    LPVOID lpvMessageBuffer;
    CHAR szPrintBuffer[512];
    DWORD nCharsWritten;
    std::ofstream fout;
    errno = 0;
    //std::replace( fileName[0], fileName[fileName.length], " ", "\x20");
    //fopen(str_replace(" ", "\x20", $filename), "r");
    fout.open(fileName.c_str(), std::fstream::app);
    if (fout.fail())
    {
        // std::cout << "Couldn't open the file!" << endl;
    }
    else
    {
        FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
            NULL, GetLastError(),
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR)&lpvMessageBuffer, 0, NULL);
        wsprintf(szPrintBuffer,
                 "ERROR: API    = %s.\n   error code = %d.\n   message    = %s.\n",
                 pszAPI, GetLastError(), (char *)lpvMessageBuffer);

        WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE), szPrintBuffer,
                     lstrlen(szPrintBuffer), &nCharsWritten, NULL);

        fout << "\n********************************************************\nERROR: API= " << pszAPI
             << "\n error code =" << GetLastError()
             << "\n message= " << (char *)lpvMessageBuffer
             << "\n***********************************************\n";
        fout.close();
    }
    // std::cout<<"Error %d \n"<< errno;

    LocalFree(lpvMessageBuffer);
    // HANDLE hd;
    // hd= GetCurrentProcess();
    // int id_h;
    //     id_h=  GetProcessId(hd);
    //     if(id!=id_h)
    // TerminateProcess(hd,GetLastError());
    //ExitProcess(GetLastError());
}
DWORD WINAPI GetAndSendInputThread(LPVOID lpvThreadParam)
{
    CHAR read_buff[256];
    DWORD nBytesRead, nBytesWrote;
    HANDLE hPipeWrite = (HANDLE)lpvThreadParam;

    // Get input from our console and send it to child through the pipe.
    while (bRunThread_c)
    {
        if (!ReadConsole(hStdIn_c, read_buff, 1, &nBytesRead, NULL))
        {
            DisplayError("ReadConsole");
            //   cb.Call(env.Global(), { Napi::String:: New(env,"preprocess-complete") },errorObj); }

            read_buff[nBytesRead] = '\0'; // Follow input with a NULL.

            if (!WriteFile(hPipeWrite, read_buff, nBytesRead, &nBytesWrote, NULL))
            {
                if (GetLastError() == ERROR_NO_DATA)
                    break; // Pipe was closed (normal exit path).
                else
                    DisplayError("WriteFile");
            }
        }
    }
    return 1;
}
void SetAndSendInputThread(HANDLE hStdIn_p, BOOL bRunThread_p)
{
    hStdIn_c = hStdIn_p; // Handle to parents std input. NULL
    bRunThread_c = bRunThread_p;
}

void ReadAndHandleOutput(Napi::Env env, HANDLE hPipeRead, std::string fileName, Napi::Function cb)
{
    Napi::HandleScope scope(env);
    CHAR lpBuffer[200];
    DWORD nBytesRead;
    ofstream fout;
    fout.open(fileName, std::fstream::app);
    if (!fout)
    {
        DisplayError("error opening log file");
    }
    else
    {
        while (TRUE)
        {
            if (!ReadFile(hPipeRead, lpBuffer, sizeof(lpBuffer),
                          &nBytesRead, NULL) ||
                !nBytesRead)
            {
                if (GetLastError() == ERROR_BROKEN_PIPE)
                {
                    break; // pipe done - normal exit path.
                }
                else
                {
                    DisplayError("ReadFile"); // Something bad happened.
                }
            }
            // fout << lpBuffer;
            // cout << "lpBuffer printed";
            cb.Call(env.Global(), {Napi::String::New(env, lpBuffer)});
        }
        fout.close();
    }
}
void PrepAndLaunchRedirectedChild(HANDLE hChildStdOut,
                                  HANDLE hChildStdIn,
                                  HANDLE hChildStdErr,
                                  std::string *strCommands,
                                  int commandCounter,
                                  HANDLE hChildProcess,
                                  Napi::Env env)
{
    const int *NumberOfProcesses = &commandCounter;

    STARTUPINFO *si = new STARTUPINFO[*NumberOfProcesses];
    PROCESS_INFORMATION *pi = new PROCESS_INFORMATION[*NumberOfProcesses];

    for (int i = 0; i < commandCounter; i++)
    {
        // cout<<endl<<"came to run command index : "<<i;
        ZeroMemory(&si[i], sizeof(si[i]));
        si[i].cb = sizeof(si);
        si[i].dwFlags = STARTF_USESTDHANDLES;
        si[i].hStdOutput = hChildStdOut;
        si[i].hStdInput = hChildStdIn;
        si[i].hStdError = hChildStdErr;
        LPTSTR cmd = const_cast<char *>((strCommands[i]).c_str());

        if (!CreateProcess(NULL, cmd, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si[i], &pi[i]))
            DisplayError("CreateProcess");
        //        if (!CreateProcess(NULL,cmd,NULL,NULL,TRUE,CREATE_NO_WINDOW,NULL,NULL,&si[i],&pi[i]))

        // Set global child process handle to cause threads to exit.
        hChildProcess = pi[i].hProcess;
        currentGlobalChildProcess = pi[i].hProcess;
        currentGlobalProcessId = pi[i].dwProcessId;
        // WaitForSingleObject(pi[i].hProcess,INFINITE);

        // Close any unnecessary handles.
        if (!CloseHandle(pi[i].hThread))
            DisplayError("CloseHandle");
    }
}

std::string createCommand(std::vector<std::string> args_cmd)
{
    std::string cmd = "";
    for (int i = 0; i < args_cmd.size(); i++)
    {
        cmd = cmd + args_cmd[i] + " ";
    }
    return cmd;
}
