#include <iostream>
#include <direct.h>
#include <string>
#include <array>
#include <node.h>
#include <windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <napi.h>
#include "commandFunctions.h"
#include <psapi.h>
#include <errno.h>
#include "preprocess-validate.h"
// #include <thread>
// for thread async promise functions
#include <future>
#include <thread>
#include <utility>

using namespace std;
// #pragma comment(lib, "User32.lib")
HANDLE hChildProcess; //NULL
HANDLE hStdIn;        // Handle to parents std input. NULL
BOOL bRunThread;      // TRUE
int noOfCommands = 0;
int commandCounter = 0;
std::string(*strCommands);
//durations array maps with strCommands in such a way that duration for strCommands[i] th command is duration[i]
int *durations = NULL;
int screen_count = 1;
int camera_count = 1;

HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
HANDLE hInputWriteTmp, hInputRead, hInputWrite;
HANDLE hErrorWrite;
HANDLE hThread;
DWORD ThreadId;

std::string ReplaceAll(std::string str, const std::string &from, const std::string &to)
{
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}
int RunCommand2(std::string cmdx, int remainingCount, Napi::Function cb, Napi::Env env, int noOfCommands, int currentCount, int durationx)
{

    int duration = durationx;
    LPTSTR cmd = const_cast<char *>((cmdx).c_str());

    BOOL ok = TRUE;
    HANDLE hStdInPipeRead = NULL;
    HANDLE hStdInPipeWrite = NULL;
    HANDLE hStdOutPipeRead = NULL;
    HANDLE hStdOutPipeWrite = NULL;

    // Create two pipes.
    SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), NULL, TRUE};
    ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
    if (ok == FALSE)
        return -1;
    ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
    if (ok == FALSE)
        return -1;

    // Create the process.
    STARTUPINFO si = {};
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdError = hStdOutPipeWrite;
    si.hStdOutput = hStdOutPipeWrite;
    si.hStdInput = hStdInPipeRead;
    PROCESS_INFORMATION pi = {};
    LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
    LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
    LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
    LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
    BOOL bInheritHandles = TRUE;
    DWORD dwCreationFlags = 0;
    LPVOID lpEnvironment = NULL;
    LPCWSTR lpCurrentDirectory = NULL;
    // ok = CreateProcess(
    //     lpApplicationName,
    //     lpCommandLine,
    //     lpProcessAttributes,
    //     lpThreadAttribute,
    //     bInheritHandles,
    //     dwCreationFlags,
    //     lpEnvironment,
    //     lpCurrentDirectory,
    //     &si,
    //     &pi);
    ok = CreateProcess(NULL, cmd, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
    if (ok == FALSE)
        return -1;

    // Close pipes we do not need.
    CloseHandle(hStdOutPipeWrite);
    CloseHandle(hStdInPipeRead);

    // The main loop for reading output from the DIR command.
    char buf[1024 + 1] = {};
    DWORD dwRead = 0;
    DWORD dwAvail = 0;
    ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    // cout << endl << "value of ok ; " << ok << endl;
    while (ok == TRUE)
    {
        buf[dwRead] = '\0';
        OutputDebugStringA(buf);
        puts(buf);

        Napi::Object obj = Napi::Object::New(env);
        // cout << "duration of current file : " << duration << endl;
        obj.Set("currentCount", (currentCount + 1));
        obj.Set("remainingCount", (remainingCount));
        obj.Set("totalCount", (noOfCommands));
        obj.Set("duration", (duration));
        obj.Set("output", buf);
        cb.Call(env.Global(), {obj});

        ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    }

    // Clean up and exit.
    CloseHandle(hStdOutPipeRead);
    CloseHandle(hStdInPipeWrite);
    DWORD dwExitCode = 0;
    GetExitCodeProcess(pi.hProcess, &dwExitCode);
    return 0;
}

std::string getBrightnessFilter(Napi::Object currentRec) {
    std::string brightnessFilter = "";
    if(currentRec.Has("adjustBrightnessValues")) {
        cout << endl << "adjust brightness object present" << endl;
        Napi::Object brightnessValuesObj = currentRec.Get("adjustBrightnessValues").ToObject();
        std::string adjustBrightnessFlag = brightnessValuesObj.Get("enableAdjustBrightness").ToString();
        cout << endl << "adjust brightness object present : adjustBrightnessFlag" << adjustBrightnessFlag << endl;

        if(adjustBrightnessFlag == "true") {
            std::string gamma = brightnessValuesObj.Get("gamma").ToString();
            std::string saturation = brightnessValuesObj.Get("saturation").ToString();
            std::string brightness = brightnessValuesObj.Get("brightness").ToString();
            std::string contrast = brightnessValuesObj.Get("contrast").ToString();

            brightnessFilter = ",eq=gamma=" + gamma + ":saturation=" + saturation + ":brightness=" + brightness + ":contrast=" + contrast;
        }
    }
    return brightnessFilter;
}
// std::string getKey(std::string keyx,std::string type,Napi::Object currentObj,std::string keyFor){
std::string getKey(Napi::Env env, std::string keyx, std::string type, Napi::Object currentObject, std::string keyFor)
{
    Napi::Array keys = currentObject.GetPropertyNames();
    std::string tmpKey;
    for (int i = 0; i < keys.Length(); i++)
    {
        std::string key = keys.Get(i).ToString();
        // if type = camera & key = cwidth & keyx = width
        if (keyFor.compare("camera") == 0)
        {
            tmpKey = "c" + keyx;
        }
        else
        {
            tmpKey = "s" + keyx;
        }
        if (key.compare(tmpKey) == 0 && keyx.compare("width") == 0)
        {
            return currentObject.Get(Napi::String::New(env, key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("height") == 0)
        {
            return currentObject.Get(Napi::String::New(env, key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("left") == 0)
        {
            return currentObject.Get(Napi::String::New(env, key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("top") == 0)
        {
            return currentObject.Get(Napi::String::New(env, key)).As<Napi::String>().ToString();
        }
    }
}
Napi::Object sanitizeForFFMPEG(int outputWidth, int outputHeight,int width,int height,int left,int top, Napi::Env env) {
    Napi::Object dimensions = Napi::Object::New(env);
    // cout << endl << "old width : " << width;
    // cout << endl << "old height : " << height;
    // cout << endl << "old left : " << left;
    // cout << endl << "old top : " << top;
    if (width + left > outputWidth || (width % 2 != 0 && left != 0)) {
		left--;
		width++;
	}
	if (height + top > outputHeight || (height % 2 != 0 && top != 0)) {
		top--;
		height++;
	}
    // cout << endl << "new width : " << width;
    // cout << endl << "new height : " << height;
    // cout << endl << "new left : " << left;
    // cout << endl << "new top : " << top << endl << endl;
    dimensions.Set("width",width);
    dimensions.Set("height",height);
    dimensions.Set("left",left);
    dimensions.Set("top",top);

    return dimensions;
}
std::string getScrollingTextFilter(Napi::Object scrollingTextObj, Napi::Object currentRec, Napi::Object config, Napi::Env env, std::string configFolderPath, int finalMergerIndex) {


    std::string outputWidth = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
    std::string outputHeight = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

    int outputWidthInt = stoi(outputWidth);
    int outputHeightInt = stoi(outputHeight);

    std::string fixedTextWidthRawPx = scrollingTextObj.Get("fixedTextWidthRawPx").ToString();
    std::string topLocationRawPx = scrollingTextObj.Get("topLocationRawPx").ToString();

    std::string fixedTextWidthPx = scrollingTextObj.Get("fixedTextWidthPx").ToString();
    std::string topLocationPx = scrollingTextObj.Get("topLocationPx").ToString();


    std::string fontsize = scrollingTextObj.Get("fontSize").ToString();
    int fontSizeInt = std::stoi(fontsize);
    fontSizeInt = fontSizeInt * 4;
    fontsize =  to_string(fontSizeInt);
    // ! ask why +10 to neha, and write reason here
    int temp = std::stoi(fontsize);
    // NOTE -> +10 makes incorrect location
    // temp = temp + 10;
    // ! + 16 adds extra height -> no needed
    // std::string height =  to_string(temp + 16);
    std::string height =  to_string(temp);
    // cout << endl << "fixedTextWidthRawPx" << fixedTextWidthRawPx;
    // cout << endl << "topLocationRawPx" << topLocationRawPx;

    // cout << endl << "fixedTextWidthPx" << fixedTextWidthPx;
    // cout << endl << "topLocationPx" << topLocationPx;

    // cout << endl << "fontsize" << fontsize;
    // cout << endl << "height" << height;

    std::string totaltime = scrollingTextObj.Get("scrollTextRunningTimeSec").ToString();
    std::string totaltimeraw = totaltime;
    int totaltimeInt = std::stoi(totaltime);
    std::string starttime = scrollingTextObj.Get("scrollTextStartTimeSec").ToString();
    int starttimeInt = std::stoi(starttime);


    totaltimeInt = (starttimeInt + totaltimeInt);
    totaltime = to_string(totaltimeInt);

    // cout << endl << "totaltime" << totaltime;
    // cout << endl << "starttime" << starttime;

    std::string fixedTextColor = scrollingTextObj.Get("fixedTextColor").ToString();
    std::string fixedText = scrollingTextObj.Get("fixedText").ToString();
    std::string fixedTextBackColor = scrollingTextObj.Get("fixedTextBackColor").ToString();

    // cout << endl << "fixedTextColor" << fixedTextColor;
    // cout << endl << "fixedText" << fixedText;
    // cout << endl << "fixedTextBackColor" << fixedTextBackColor;

    std::string scrollTextColor = scrollingTextObj.Get("scrollTextColor").ToString();
    std::string scrollingText = scrollingTextObj.Get("scrollingText").ToString();
    std::string scrollTextBackColor = scrollingTextObj.Get("scrollTextBackColor").ToString();

    // TODO -> create temp files to write scrolling and fixed text
    std::string fixedTextContentFilePath = configFolderPath + "\\\\\\\\fixedTextContentTemp-"+ to_string(finalMergerIndex) +".txt";
    std::string scrollingTextContentFilePath = configFolderPath + "\\\\\\\\scrollingTextContentTemp-"+to_string(finalMergerIndex)+".txt";

    std::string fixedTextContentFilePathForFFMPEG = ReplaceAll(fixedTextContentFilePath, ":", "\\\\:");
    std::string scrollingTextContentFilePathForFFMPEG = ReplaceAll(scrollingTextContentFilePath, ":", "\\\\:");


    fstream fixedTextContentFileObj;
    fstream scrollingTextContentFileObj;
    fixedTextContentFileObj.open(fixedTextContentFilePath.c_str(), std::fstream::out | std::fstream::trunc);
    scrollingTextContentFileObj.open(scrollingTextContentFilePath.c_str(), std::fstream::out | std::fstream::trunc);

    // ? sanitize texts for ffmpeg
    fixedText = ReplaceAll(fixedText, "%", "\\%");
    scrollingText = ReplaceAll(scrollingText, "%", "\\%");

    // ? write text to files
    cout << endl << "content to be written in fixed text file : " << fixedText << endl;
    fixedTextContentFileObj << fixedText;
    cout << endl << "content to be written in scrolling text file : " << scrollingText << endl;
    scrollingTextContentFileObj << scrollingText;

    // ? close files
    fixedTextContentFileObj.close();
    scrollingTextContentFileObj.close();
    // cout << endl << "scrollingText" << scrollingText;
    // cout << endl << "scrollTextColor" << scrollTextColor;
    // cout << endl << "scrollTextBackColor" << scrollTextBackColor;

    // cout << endl << "outputWidthInt" << outputWidthInt;
    // cout << endl << "outputHeightInt" << outputHeightInt;
    std::string box1 = "drawbox=x=0:y="+topLocationPx+":"+"w="+outputWidth+":h="+height+"+10:color="+
                        scrollTextBackColor+"@1.0:t=fill:enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    std::string box2 = "drawbox=x=0:y="+topLocationPx+":"+"w="+fixedTextWidthPx+"*2.8:h="+height+"+10:color="+
    fixedTextBackColor+"@1.0:t=fill:enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    // ? backup command
    // std::string filter1 = "drawtext=fontfile=dosis.ttf:text="+scrollingText+":fontcolor="+scrollTextColor+"@1.0:fontsize="+fontsize+":"+
    //              "y="+ topLocationPx+":x=w-tw/10*mod(t\\,"+totaltime+"):"+
    //              "enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";
    std::string filter1 = "drawtext=fontfile=dosis.ttf:textfile="+scrollingTextContentFilePathForFFMPEG +":fontcolor="+scrollTextColor+"@1.0:fontsize="+fontsize+":"+
                 "y="+ topLocationPx+"+10:x=w-mod(max(t-"+starttime+"\\,0)*(w+tw)/"+totaltimeraw+"\\,(w+tw))"+
                 ":enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    std::string name1="drawtext=fontfile=dosis.ttf:textfile="+fixedTextContentFilePathForFFMPEG+":fontcolor="+fixedTextColor+"@1.0:fontsize="+fontsize+ ":"+
               "x=5:y="+topLocationPx+
                "+10:box=1:boxcolor="+fixedTextBackColor+":boxborderw=10:enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    // std::string final2 = "," + box1+","+filter1+","+name1;
    std::string final2 = "," + box1+"," +filter1+","+name1;


    std::string filter = final2;
    // std::string filter = "";
    return filter;
}
std::string getOverlayFilterComplex(Napi::Env env, Napi::Object currentObj, Napi::Object configObj, std::string subtype, int finalMergerIndex)
{
    std::string configFolderPath = configObj.Get(Napi::String::New(env, "configFolderPath")).As<Napi::String>().ToString();
    configFolderPath = ReplaceAll(configFolderPath, "\\", "\\\\");

    // [0] input is camera always
    // [1] input is background image if it is applied
    // [2] input is screen
    std::string filter = "";
    // we divide filter in 3 parts
    // 1st camera filter
    // 2nd screen filter
    // 3rd overlay filter
    std::string cameraFilter = "[0]";
    std::string screenFilter = "";
    std::string overlayFilter = "";
    std::string type = currentObj.Get(Napi::String::New(env, "type")).As<Napi::String>().ToString();
    Napi::Object outputRes = configObj.Get(Napi::String::New(env, "outputRes")).ToObject();
    // Napi::Object settings=configObj.Get(Napi::String::New(env,"settings")).ToObject();
    std::string outputW = outputRes.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
    std::string outputH = outputRes.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

    int outputWidthInt = stoi(outputW);
    int outputHeightInt = stoi(outputH);
    bool fullScreenMode = currentObj.Get(Napi::String::New(env, "fullScreenMode")).ToBoolean();

    std::string tmpScreenCropFilter = "";
    std::string tmpCameraCropFilter = "";
    if (fullScreenMode == true)
    {
        // no need to crop
    }
    else
    {
        bool isScreenWindowCoordsPresent = currentObj.Has("screenWindowCoords");
        if(isScreenWindowCoordsPresent == true) {
            Napi::Object cropCoords = currentObj.Get(Napi::String::New(env, "screenWindowCoords")).ToObject();
            std::string width = cropCoords.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
            std::string height = cropCoords.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
            std::string screenX = cropCoords.Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
            std::string screenY = cropCoords.Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();
            // screenFilter += "crop="+width+":"+height+":"+screenX+":"+screenY+",";
            tmpScreenCropFilter += "crop=" + width + ":" + height + ":" + screenX + ":" + screenY + ",";
        }

    }

    // TODO - get crop params for camera as well
    bool isCameraWindowCoordsPresent = currentObj.Has("cameraWindowCoords");
    if(isCameraWindowCoordsPresent == true) {
        Napi::Object cropCoords = currentObj.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject();
        std::string width = cropCoords.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
        std::string height = cropCoords.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
        std::string screenX = cropCoords.Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
        std::string screenY = cropCoords.Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();
        // screenFilter += "crop="+width+":"+height+":"+screenX+":"+screenY+",";
        tmpCameraCropFilter += "crop=" + width + ":" + height + ":" + screenX + ":" + screenY + ",";
    }



    // 1. scale for camera
    std::string cameraWidth = getKey(env, "width", type, currentObj, "camera");
    std::string cameraHeight = getKey(env, "height", type, currentObj, "camera");
    std::string cameraLeft = getKey(env, "left", type, currentObj, "camera");
    std::string cameraTop = getKey(env, "top", type, currentObj, "camera");

    int cleftInt = stoi(cameraLeft);
    int ctopInt = stoi(cameraTop);
    int cwidthInt = stoi(cameraWidth);
    int cheightInt = stoi(cameraHeight);

	Napi::Object ffmpegSanitizedDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, cwidthInt, cheightInt, cleftInt, ctopInt, env);
    cameraLeft = ffmpegSanitizedDimensions.Get("left").ToString();
    cameraTop = ffmpegSanitizedDimensions.Get("top").ToString();
    cameraWidth = ffmpegSanitizedDimensions.Get("width").ToString();
    cameraHeight = ffmpegSanitizedDimensions.Get("height").ToString();


    cameraFilter += tmpCameraCropFilter + "scale=" + cameraWidth + ":" + cameraHeight;

    // 2. scale for screen
    std::string screenWidth = getKey(env, "width", type, currentObj, "screen");
    std::string screenHeight = getKey(env, "height", type, currentObj, "screen");
    std::string screenLeft = getKey(env, "left", type, currentObj, "screen");
    std::string screenTop = getKey(env, "top", type, currentObj, "screen");

    int sleftInt = stoi(screenLeft);
    int stopInt = stoi(screenTop);
    int swidthInt = stoi(screenWidth);
    int sheightInt = stoi(screenHeight);

    ffmpegSanitizedDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, swidthInt, sheightInt, sleftInt, stopInt, env);
    screenLeft = ffmpegSanitizedDimensions.Get("left").ToString();
    screenTop = ffmpegSanitizedDimensions.Get("top").ToString();
    screenWidth = ffmpegSanitizedDimensions.Get("width").ToString();
    screenHeight = ffmpegSanitizedDimensions.Get("height").ToString();

    // 3. pad or add back image

    std::string backImgPath = configObj.Get(Napi::String::New(env, "backImgPath")).As<Napi::String>().ToString();
    // cout << endl
    //      << "backImgPath : " << backImgPath << endl;
    if (backImgPath.compare("false") == 0)
    {
        // if backimagenot set
        // if backimage is not specified ;
        // then screen input label is [1] else [2]
        screenFilter += "[1]" + tmpScreenCropFilter + "scale=" + screenWidth + ":" + screenHeight;
        screenFilter += ",pad=width=" + outputW + ":height=" + outputH + ":x=" + screenLeft + ":y=" + screenTop + ":color=white";
    }
    else
    {
        // backimage set
        // in both we add back image only to screen
        screenFilter += "[2]" + tmpScreenCropFilter + "scale=" + screenWidth + ":" + screenHeight;
        screenFilter += "[in];[1]scale=" + outputW + ":" + outputH + "[back];[back][in]overlay=" + screenLeft + ":" + screenTop;
    }
 // ? attach brightness filter for camera only
    std::string brightnessFilter = getBrightnessFilter(currentObj);
    // ! must always be before transparency
    cameraFilter += brightnessFilter;
    std::string transparencyFlag = currentObj.Get(Napi::String::New(env, "transparencyCheck")).ToString();
    if (transparencyFlag.compare("true") == 0)
    {
        std::string colorHex = currentObj.Get(Napi::String::New(env, "colorHex")).As<Napi::String>().ToString();
        std::string similarity = currentObj.Get(Napi::String::New(env, "transparencySimilarity")).As<Napi::String>().ToString();
        std::string opacity = currentObj.Get(Napi::String::New(env, "transparencyOpacity")).As<Napi::String>().ToString();
        cameraFilter += ",chromakey=0x" + colorHex + ":" + similarity + ":" + opacity + ",fps=30";
    }
    // ? Attach fps filter
    std::string fpsFilter = "";
    if(configObj.Has("outputFrameRate")) {
        std::string outputFrameRate = configObj.Get(Napi::String::New(env, "outputFrameRate")).As<Napi::String>().ToString();
        // std::cout << endl << "outputFrameRate: " <<  outputFrameRate << endl;
        if(outputFrameRate != "auto") {
            fpsFilter = ",fps=" + outputFrameRate;
        }
    }
    // ? Attach scrolling text filter
    std::string scrollingTextFilter = "";
    if(currentObj.Has("scrollingTextObj")) {
        Napi::Object scrollingTextObj = currentObj.Get("scrollingTextObj").ToObject();
        bool scrollingTextCheck = scrollingTextObj.Get(Napi::String::New(env, "scrollingTextCheck")).ToBoolean();
        if (scrollingTextCheck == true) {
            scrollingTextFilter = getScrollingTextFilter(scrollingTextObj,currentObj, configObj, env, configFolderPath, finalMergerIndex);
        }
    }

    cameraFilter += (fpsFilter +   "[camera];" );
    screenFilter += (fpsFilter +  "[screen];" );

    // ! shortest = 1 causes camera frame to not show, if screen is stopped 1 second earlier than camera
    filter = cameraFilter + screenFilter + "[screen][camera]overlay=" + cameraLeft + ":" + cameraTop;
    filter += scrollingTextFilter;
      // ? Attach fade effect filter
    std::string fadeEffectFilter = "";
    if(configObj.Has("enableFadeEffect")) {
        std::string enableFadeEffect = configObj.Get("enableFadeEffect").ToString();
        std::string fadeEffectDuration = configObj.Get("fadeEffectDuration").ToString();
        if(enableFadeEffect == "Enable") {
            // fade=type=in:duration=5,fade=type=out:duration=5:start_time=169
            int startTime = currentObj.Get(Napi::String::New(env, "startTime")).As<Napi::Number>().Int32Value();
            int endTime = currentObj.Get(Napi::String::New(env, "endTime")).As<Napi::Number>().Int32Value();

            // cout << endl << "startTime : " << startTime << endl;
            // cout << endl << "endTime : " << endTime << endl;

            int duration = (endTime - startTime);
            std::string durationString = to_string(duration);
            fadeEffectFilter = ",fade=type=in:duration=" + fadeEffectDuration + ",fade=type=out:duration=" +fadeEffectDuration+ ":start_time=" + durationString;
        }
    }
    filter += fadeEffectFilter;
    // filter = cameraFilter + screenFilter + "[screen][camera]overlay=" + cameraLeft + ":" + cameraTop + ":shortest=1";
    // filter = cameraFilter+screenFilter+"overlay=";
    // filter = cameraFilter+screenFilter+"overlay,setsar=sar=1/1,setdar=dar=16/9";
    // cout << endl
    //      << "returning filterComplex for camera : " << cameraFilter << endl;
    // cout << endl
    //      << "returning filterComplex for screen : " << screenFilter << endl;
    // cout << endl
    //      << "returning filterComplex for combined : " << filter << endl;
    return filter;
}
// called only for camera and screen mode,
// for both mode check getOverlayFilterComplex
std::string getFilterComplex(Napi::Env env, Napi::Object currentObj, Napi::Object configObj, std::string subtype, int finalMergerIndex)
{

    std::string configFolderPath = configObj.Get(Napi::String::New(env, "configFolderPath")).As<Napi::String>().ToString();
    configFolderPath = ReplaceAll(configFolderPath, "\\", "\\\\");

    std::string filter = "";
    std::string type = currentObj.Get(Napi::String::New(env, "type")).As<Napi::String>().ToString();
    Napi::Object outputRes = configObj.Get(Napi::String::New(env, "outputRes")).ToObject();
    // Napi::Object settings=configObj.Get(Napi::String::New(env,"settings")).ToObject();
    std::string outputW = outputRes.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
    std::string outputH = outputRes.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

    int outputWidthInt = stoi(outputW);
    int outputHeightInt = stoi(outputH);

   // ? Attach fps filter
    std::string fpsFilter = "";
    if(configObj.Has("outputFrameRate")) {
        std::string outputFrameRate = configObj.Get(Napi::String::New(env, "outputFrameRate")).As<Napi::String>().ToString();
        // std::cout << endl << "outputFrameRate: " <<  outputFrameRate << endl;
        if(outputFrameRate != "auto") {
            fpsFilter = ("fps=" + outputFrameRate + ",");
        }
    }
     // ? Attach scrolling text filter (set variable now but attach later)
    std::string scrollingTextFilter = "";
    if(currentObj.Has("scrollingTextObj")) {
        Napi::Object scrollingTextObj = currentObj.Get("scrollingTextObj").ToObject();
        bool scrollingTextCheck = scrollingTextObj.Get(Napi::String::New(env, "scrollingTextCheck")).ToBoolean();
        // cout << endl <<"scrollingTextCheck : " << scrollingTextCheck << endl;
        if (scrollingTextCheck == true) {
            scrollingTextFilter = getScrollingTextFilter(scrollingTextObj,currentObj, configObj, env, configFolderPath, finalMergerIndex);
        }
    }
    // ? Attach fade effect filter
    std::string fadeEffectFilter = "";
    if(configObj.Has("enableFadeEffect")) {
        std::string enableFadeEffect = configObj.Get("enableFadeEffect").ToString();
        std::string fadeEffectDuration = configObj.Get("fadeEffectDuration").ToString();
        if(enableFadeEffect == "Enable") {
            // fade=type=in:duration=5,fade=type=out:duration=5:start_time=169
            int startTime = currentObj.Get(Napi::String::New(env, "startTime")).As<Napi::Number>().Int32Value();
            int endTime = currentObj.Get(Napi::String::New(env, "endTime")).As<Napi::Number>().Int32Value();

            // cout << endl << "startTime : " << startTime << endl;
            // cout << endl << "endTime : " << endTime << endl;

            int duration = (endTime - startTime);
            std::string durationString = to_string(duration);
            fadeEffectFilter = "fade=type=in:duration=" + fadeEffectDuration + ",fade=type=out:duration=" +fadeEffectDuration+ ":start_time=" + durationString + ",";
        }
    }
    filter += (fpsFilter + fadeEffectFilter);
    // std::string outputW = "1280";
    // std::string outputH = "720";

    // 0. crop if screen or both
    // TODO - handle crop for screen
    if (type.compare("screen") == 0 || (type.compare("both") == 0 && subtype.compare("screen") == 0))
    {
        bool fullScreenMode = currentObj.Get(Napi::String::New(env, "fullScreenMode")).ToBoolean();
        if (fullScreenMode == true)
        {
            // no need to crop
        }
        else
        {

            // Napi::Object cropCoords = currentObj.Get(Napi::String::New(env, "windowCoords")).ToObject();
            // ? changed to allow crop for camera
            bool isScreenWindowCoordsPresent = currentObj.Has("screenWindowCoords");
            if(isScreenWindowCoordsPresent == true) {
                Napi::Object cropCoords = currentObj.Get(Napi::String::New(env, "screenWindowCoords")).ToObject();
                std::string width = cropCoords.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
                std::string height = cropCoords.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
                std::string screenX = cropCoords.Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
                std::string screenY = cropCoords.Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();
                filter += "crop=" + width + ":" + height + ":" + screenX + ":" + screenY + ",";
            }
        }
    }
    // TODO - apply camera crop filter
    bool isCameraWindowCoordsPresent = currentObj.Has("cameraWindowCoords");
    // cout << endl << "isCameraWindowCoordsPresent" << isCameraWindowCoordsPresent;
    if(isCameraWindowCoordsPresent == true) {
        if (type.compare("camera") == 0 || (type.compare("both") == 0 && subtype.compare("camera") == 0))
        {
            bool fullScreenMode = currentObj.Get(Napi::String::New(env, "fullScreenMode")).ToBoolean();
            if (fullScreenMode == true)
            {
                // no need to crop
            }
            else
            {

                // Napi::Object cropCoords = currentObj.Get(Napi::String::New(env, "windowCoords")).ToObject();
                // ? changed to allow crop for camera
                Napi::Object cropCoords = currentObj.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject();
                std::string width = cropCoords.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
                std::string height = cropCoords.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
                std::string screenX = cropCoords.Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
                std::string screenY = cropCoords.Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();
                filter += "crop=" + width + ":" + height + ":" + screenX + ":" + screenY + ",";
            }
        }
    }

    //     // 1. scale with
    std::string width = getKey(env, "width", type, currentObj, subtype);
    std::string height = getKey(env, "height", type, currentObj, subtype);
    std::string left = getKey(env, "left", type, currentObj, subtype);
    std::string top = getKey(env, "top", type, currentObj, subtype);

    int leftInt = stoi(left);
    int topInt = stoi(top);
    int widthInt = stoi(width);
    int heightInt = stoi(height);

	Napi::Object ffmpegSanitizedDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, widthInt, heightInt, leftInt, topInt, env);
    left = ffmpegSanitizedDimensions.Get("left").ToString();
    top = ffmpegSanitizedDimensions.Get("top").ToString();
    width = ffmpegSanitizedDimensions.Get("width").ToString();
    height = ffmpegSanitizedDimensions.Get("height").ToString();

    filter += "scale=" + width + ":" + height;
    std::string transparencyFilterStr = "";
    std::string brightnessFilter = "";
    if (type.compare("camera") == 0)
    {
        brightnessFilter = getBrightnessFilter(currentObj);
        // check for transparency ->
        std::string transparencyFlag = currentObj.Get(Napi::String::New(env, "transparencyCheck")).ToString();
        if (transparencyFlag.compare("true") == 0)
        {
            std::string colorHex = currentObj.Get(Napi::String::New(env, "colorHex")).As<Napi::String>().ToString();
            std::string similarity = currentObj.Get(Napi::String::New(env, "transparencySimilarity")).As<Napi::String>().ToString();
            std::string opacity = currentObj.Get(Napi::String::New(env, "transparencyOpacity")).As<Napi::String>().ToString();

            // filter += ",chromakey=0x"+colorHex+":"+similarity+":"+opacity;
            transparencyFilterStr = ",chromakey=0x" + colorHex + ":" + similarity + ":" + opacity;
        }
    }

    // 2. pad or add back image
    // ! must always be before transparency
    filter += brightnessFilter;
    std::string backImgPath = configObj.Get(Napi::String::New(env, "backImgPath")).As<Napi::String>().ToString();
    if (backImgPath.compare("false") == 0)
    {
        // if backimagenot set
        // if(!(type.compare("both") == 0 && subtype.compare("camera") == 0)){
        filter += ",pad=width=" + outputW + ":height=" + outputH + ":x=" + left + ":y=" + top + ":color=white";
        filter += transparencyFilterStr;
        // }
    }
    else
    {
        // backimage set
        // in both we add back image only to screen
        // if(!(type.compare("both") == 0 && subtype.compare("camera") == 0)){
        filter += transparencyFilterStr;
        filter += "[in];[1]scale=" + outputW + ":" + outputH + "[back];[back][in]overlay=" + left + ":" + top;
        // }
    }

    filter += scrollingTextFilter;
    // cout << endl
    //      << "returning filterComplex : " << filter << endl;
    return filter;
}
std::string getLogoInputIndex(std::string backImgPath, std::string currentRecType)
{
    int index = 0;

    // if only camera without back img, logo will be at 1;
    // if only screen without back img, logo will be at 1;

    // only camera with back img logo will be at 2
    // only screen with back img logo will be at 2

    // both without back img logo at 2
    // both with back img logo at 3

    if (currentRecType == "camera" || currentRecType == "screen")
    {
        index = index + 1;
    }
    else if (currentRecType == "both")
    {
        index = index + 2;
    }

    if (backImgPath != "false")
    {
        index = index + 1;
    }
    return std::to_string(index);
}
std::vector<std::string> getArgs(Napi::Env env, Napi::Object currentObj, Napi::Object configObj, int i, std::string subtype, int finalMergerLength,std::string basepath)
{
    std::vector<std::string> cmd;

    Napi::Object settings = configObj.Get(Napi::String::New(env, "settings")).ToObject();
    basepath += "ffmpeg";
    cmd.push_back(basepath);
    std::string type = currentObj.Get(Napi::String::New(env, "type")).As<Napi::String>().ToString();
    std::string configFolderPath = configObj.Get(Napi::String::New(env, "configFolderPath")).As<Napi::String>().ToString();
    // source
    std::string tmpPathKey = "";
    if (subtype.compare("camera") == 0)
    {
        tmpPathKey = "cpath";
    }
    else
    {
        tmpPathKey = "spath";
    }
    // cout << "subtype" << subtype << " tmpPathKey" << tmpPathKey;
    std::string cpath = currentObj.Get(Napi::String::New(env, tmpPathKey)).As<Napi::String>().ToString();
    std::string spath;
    if (type == "both")
    {
        spath = currentObj.Get(Napi::String::New(env, "spath")).As<Napi::String>().ToString();
    }
    // cout << "configFolderPath : " << configFolderPath << " cpath : " << cpath;

    if(currentObj.Has("cameraCutStartTimeSeconds")) {
        cmd.push_back("-ss");
        std::string cameraCutStartTime = currentObj.Get(Napi::String::New(env, "cameraCutStartTimeSeconds")).As<Napi::String>().ToString();
        cmd.push_back(cameraCutStartTime);
    }
    if(currentObj.Has("cameraCutEndTimeSeconds")) {
        cmd.push_back("-to");
        std::string cameraCutEndTime = currentObj.Get(Napi::String::New(env, "cameraCutEndTimeSeconds")).As<Napi::String>().ToString();
        cmd.push_back(cameraCutEndTime);
    }
    cmd.push_back("-i");
    cmd.push_back('"' + configFolderPath + "\\" + cpath + '"');

    std::string backImgPath = configObj.Get(Napi::String::New(env, "backImgPath")).As<Napi::String>().ToString();
    // cout << endl
    //      << "backImgPath : " << backImgPath << endl;
    if (backImgPath.compare("false") == 0)
    {
        // if backimage is not set
    }
    else
    {
        // if backimage is set
        // in both add back image only to screen
        // cout << endl
        //      << "type : " << type << endl;

        // if(type.compare("both") == 0){
        cmd.push_back("-i");
        cmd.push_back('"' + backImgPath + '"');
        // }
    }
    std::string filterComplex = "";
    if (type == "both")
    {
        // ! adding framerate 30 while reading screen file,
        // ! this is required because for HP laptop of vipul shah
        // ! the screen resolution is 1920x1080 and framerate is 60 FPS,
        // ! which causes lag if camera is added as overlay on top of it
        // cmd.push_back("-r");
        // cmd.push_back("30");

        if(currentObj.Has("screenCutStartTimeSeconds")) {
            cmd.push_back("-ss");
            std::string screenCutStartTime = currentObj.Get(Napi::String::New(env, "screenCutStartTimeSeconds")).As<Napi::String>().ToString();
            cmd.push_back(screenCutStartTime);
        }
        if(currentObj.Has("screenCutEndTimeSeconds")) {
            cmd.push_back("-to");
            std::string screenCutEndTime = currentObj.Get(Napi::String::New(env, "screenCutEndTimeSeconds")).As<Napi::String>().ToString();
            cmd.push_back(screenCutEndTime);
        }
        cmd.push_back("-i");
        cmd.push_back('"' + configFolderPath + "\\" + spath + '"');

        filterComplex = getOverlayFilterComplex(env, currentObj, configObj, subtype,i);
        // cout << endl
        //      << "got from getOverlayFilterComplex" << endl;
    }
    else
    {
        filterComplex = getFilterComplex(env, currentObj, configObj, subtype, i);
    }
    // add logoOverlay if exists
    std::string logoImgPath = configObj.Get("logoImgPath").ToString();
    // ! must add logo only if visibility is not false
    // ? null or undefined indicates that user has not specifically
    // ? set it to false
    bool isLogoPresentForCurrentRec = false;
    if(currentObj.Has("isLogoVisible")) {
        std::string logoVisibility = currentObj.Get("isLogoVisible").ToString();
        if(logoVisibility == "false") {
            isLogoPresentForCurrentRec = false;
        } else {
            isLogoPresentForCurrentRec = true;
        }
    } else {
        isLogoPresentForCurrentRec = true;
    }
    if (logoImgPath != "false" && isLogoPresentForCurrentRec == true)
    {
        Napi::Object logoCoords = configObj.Get("logoCoords").ToObject();

        std::string logoWidth = logoCoords.Get("width").ToString();
        std::string logoHeight = logoCoords.Get("height").ToString();
        std::string logoLeft = logoCoords.Get("left").ToString();
        std::string logoTop = logoCoords.Get("top").ToString();

        int logoLeftInt = stoi(logoLeft);
        int logoTopInt = stoi(logoTop);
        int logoWidthInt = stoi(logoWidth);
        int logoHeightInt = stoi(logoHeight);

        // cout << endl << "Logo coords before sanitization : " ;
        //  cout << endl
        //      << "Logo width : " << logoWidth << endl;
        // cout << endl
        //      << "Logo height : " << logoHeight << endl;
        // cout << endl
        //      << "Logo left : " << logoLeft << endl;
        // cout << endl
        //      << "Logo top : " << logoTop << endl;

        Napi::Object outputRes = configObj.Get(Napi::String::New(env, "outputRes")).ToObject();
        // Napi::Object settings=configObj.Get(Napi::String::New(env,"settings")).ToObject();
        std::string outputW = outputRes.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
        std::string outputH = outputRes.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

        int outputWidthInt = stoi(outputW);
        int outputHeightInt = stoi(outputH);
        // ! sanitize for ffmpeg
        Napi::Object ffmpegSanitizedLogoDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, logoWidthInt, logoHeightInt, logoLeftInt, logoTopInt, env);
        logoLeft = ffmpegSanitizedLogoDimensions.Get("left").ToString();
        logoTop = ffmpegSanitizedLogoDimensions.Get("top").ToString();
        logoWidth = ffmpegSanitizedLogoDimensions.Get("width").ToString();
        logoHeight = ffmpegSanitizedLogoDimensions.Get("height").ToString();

        cmd.push_back("-i");
        cmd.push_back('"' + logoImgPath + '"');
        std::string logoInputIndex = getLogoInputIndex(backImgPath, type);
        // filterComplex += " [over];["+logoInputIndex+"][over][logo]overlay="+logoLeft+":"+logoTop;
        filterComplex += "[over];";
        filterComplex += "[" + logoInputIndex + "]scale=" + logoWidth + ":" + logoHeight + "[scaled_logo];";
        filterComplex += "[over][scaled_logo]overlay=" + logoLeft + ":" + logoTop;
        filterComplex += ",setsar=sar=1/1,setdar=dar=16/9";
    }
    // NOTE -> must always apply before filter_complex
    // ? Attach Max muxing queue size filter
    if(configObj.Has("maxMuxingQueueSize")) {
        std::string size = configObj.Get("maxMuxingQueueSize").ToString();
        if(size != "Auto") {
            cmd.push_back("-max_muxing_queue_size");
            cmd.push_back(size);
        }
    }
    cmd.push_back("-filter_complex");
    cmd.push_back("\"" +  filterComplex + "\"");
    // cout << endl
    //      << "Final Filter Complex : " << filterComplex << endl;
    // std::string ouputCameraRecording=configFolderPath+"intermediate/camera_";
    std::string folderForOutput = "\\intermediate\\";

    if (finalMergerLength == 1)
    {
        folderForOutput = "\\output\\";
    }
    std::string out = configFolderPath + folderForOutput + subtype + "_" + std::to_string(i) + ".mp4";
    std::string preset = configObj.Get(Napi::String::New(env, "preset")).As<Napi::String>().ToString();
    std::string crf = configObj.Get(Napi::String::New(env, "crf")).As<Napi::String>().ToString();

    cmd.push_back("-preset");
    cmd.push_back(preset);
    cmd.push_back("-crf");
    cmd.push_back(crf);
    cmd.push_back("-r");
    cmd.push_back("30");

    // cmd.push_back("-preset"); cmd.push_back(settings.Get(Napi::String::New(env,"preset")).As<Napi::String>().Utf8Value());
    // cmd.push_back("-crf"); cmd.push_back(settings.Get(Napi::String::New(env,"crf")).As<Napi::String>().ToString());

    cmd.push_back("-y");
    cmd.push_back('"' + out + '"');

    return cmd;
}
// std::vector<std::string> getScreenArgs(Napi::Object currentObj,std::string basepath)
// {
//     std::vector<std::string> cmd;
//     basepath += "ffmpeg";
//     cmd.push_back(basepath);
//     return cmd;
// }
void runtimeExec(const char *cmd, Napi::Function cb, Napi::Env env, int commandIndex)
{
    // cout << "came to runtimeExec";
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);
    if (!pipe)
    {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr)
    {
        result += buffer.data();
        // cout << "result of command:" << result;
        Napi::Object res = Napi::Object::New(env);
        res.Set("commandIndex", commandIndex);
        res.Set("commandOutput", buffer.data());
        cb.Call(env.Global(), {res});
    }
}
int getDurationByCuts(Napi::Object currentObj, Napi::Env env) {
    int startTime = currentObj.Get(Napi::String::New(env, "startTime")).As<Napi::Number>().Int32Value();
    int endTime = currentObj.Get(Napi::String::New(env, "endTime")).As<Napi::Number>().Int32Value();

    if(currentObj.Has("cameraCutStartTimeSeconds") && currentObj.Has("cameraCutEndTimeSeconds") ) {
        startTime = currentObj.Get(Napi::String::New(env, "cameraCutStartTimeSeconds")).As<Napi::Number>().Int32Value();
        endTime = currentObj.Get(Napi::String::New(env, "cameraCutEndTimeSeconds")).As<Napi::Number>().Int32Value();
    }
    return (endTime - startTime);
}
Napi::Object preprocess(const Napi::CallbackInfo &info)
{
    const Napi::Env env = info.Env();
    std::ofstream ofs;
    ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
    ofs << "came to preprocess function call in c++" << endl;
    try
    {
        Napi::Object paramObj = Napi::Object::New(env);
        std::string homeDirx = info[4].As<Napi::String>().ToString();           //path to the folder wich contains recorded files
        std::string serverPathx = info[5].As<Napi::String>().ToString();        //path to the folder wich contains recorded files
        std::string basepathx = info[6].As<Napi::String>().ToString();          //path to the folder wich contains recorded files
        std::string appDataFolderPathx = info[8].As<Napi::String>().ToString(); //path to the folder wich contains recorded files

        appDataFolderPathx += "\\licenseInfo.dat";
        Napi::Function validationCB = info[7].As<Napi::Function>();
        paramObj.Set("homeDir", homeDirx);
        paramObj.Set("serverPath", serverPathx);
        paramObj.Set("basepath", basepathx);
        paramObj.Set("appDataFolderPath", appDataFolderPathx);


        basepathx += "src\\assets\\exe\\";
        // cout << endl << "************BASEPATH ************: " << basepathx << endl;
        // paramObj needs to set serverPath,homeDir and basepath
        ofs << "now calling isPreprocessLicenseValid function from preprocess.cc which is present in preporcess-validate.h" << endl;

        // ofs.close();

        // Napi::Object isValid = isPreprocessLicenseValid(paramObj, env);
        // bool isLicenseValid = true;

        // ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
        // ofs << "came out of preproprocess validateh" << endl;
        // std::string areContentsSimilar = isValid.Get("areContentsSimilar").ToString();
        // std::string areDatesValid = isValid.Get("areDatesValid").ToString();
        // ofs << "are Dates Valid : " << areDatesValid << endl;
        // ofs << "are Contents similar : " << areContentsSimilar << endl;

        // if (areContentsSimilar != "true" || areDatesValid != "true")
        // {
        //     isLicenseValid = false;
        // }
        // bool hasServerResponse = isValid.Has("ServerValidation");
        // ofs << "has server resposne : " << hasServerResponse << endl;

        // if (hasServerResponse == true)
        // {
        //     std::string isServerValidStr = isValid.Get("ServerValidation").ToString();
        //     if (isServerValidStr != "true")
        //     {
        //         isLicenseValid = false;
        //         ofs << "server response is NOT true " << endl;
        //     }
        //     else
        //     {
        //         ofs << "server response IS true " << endl;
        //     }
        // }
        // cout << endl
        //      << "Validating License : is Valid ->" << isLicenseValid;
        // ofs << "is license valid :  " << isLicenseValid << endl;

        // if (isLicenseValid == true)
        // {
        //     ofs << "license valid is true - before calling validation cb " << endl;

        //     validationCB.Call(env.Global(), {isValid});
        //     ofs << "license valid is true - after calling validation cb " << endl;
        int check;
        auto configObj = info[0].ToObject(); //consists the information about recorded files
        int id = info[3].As<Napi::Number>().Int32Value();
        std::string configFolderPath = info[1].As<Napi::String>().ToString(); //path to the folder wich contains recorded files
        std::string ouputCameraRecording = configFolderPath + "intermediate/camera_";
        std::string outputScreenRecording = configFolderPath + "intermediate/screen_";
        // std::string outPutfileName=configFolderPath+"errorLog/preprocessStdout.txt";
        Napi::Object outputRes = configObj.Get(Napi::String::New(env, "outputRes")).ToObject();
        Napi::Object settings = configObj.Get(Napi::String::New(env, "settings")).ToObject();
        outputRes.Get(Napi::String::New(env, "width")) = settings.Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
        outputRes.Get(Napi::String::New(env, "height")) = settings.Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

        Napi::Function cb = info[2].As<Napi::Function>();
        // cout << "error is not with cb";
        Napi::HandleScope scope(env);

        std::vector<std::string> _args_camera;
        std::vector<std::string> _args_screen;

        Napi::Array finalMerger = configObj.Get(Napi::String::New(env, "finalMerger")).As<Napi::Array>(); //finalMerger array is inside configobj and contians object for every recorded file
        ofs << "initialized variables in  preprocess fucntion before command for loop" << endl;

        for (unsigned int i = 0; i < finalMerger.Length(); i++)
        {
            Napi::Object currentObj = (finalMerger.Get(i)).ToObject();
            std::string type = currentObj.Get((Napi::String::New(env, "type"))).As<Napi::String>().ToString(); //type of recorded file eg camera,screen,both

            noOfCommands++;
        }
        ofs << "commands generated" << endl;

        strCommands = new std::string[noOfCommands];
        durations = new int[noOfCommands];
        for (unsigned int i = 0; i < finalMerger.Length(); i++)
        {
            _args_camera.clear();
            _args_screen.clear();
            Napi::Object intermidiate = Napi::Object::New(env);

            Napi::Object currentObj = (finalMerger.Get(i)).ToObject();
            std::string type = currentObj.Get(Napi::String::New(env, "type")).As<Napi::String>().ToString();
            int finalMergerLength = finalMerger.Length();
            _args_camera = getArgs(env, currentObj, configObj, i, "camera", finalMergerLength,basepathx);

            // int startTime = currentObj.Get(Napi::String::New(env, "startTime")).As<Napi::Number>().Int32Value();
            // int endTime = currentObj.Get(Napi::String::New(env, "endTime")).As<Napi::Number>().Int32Value();

            // // cout << endl << "startTime : " << startTime << endl;
            // // cout << endl << "endTime : " << endTime << endl;

            // int duration = (endTime - startTime);
            int duration = getDurationByCuts(currentObj, env);
            cout << endl << "duration : " << duration << endl;

            // if(type.compare("both") == 0){
            //     _args_screen = getArgs(env,currentObj,configObj,i,"screen",finalMergerLength);
            //     durations[commandCounter] = duration;
            //     strCommands[commandCounter++]= createCommand(_args_screen);
            //     int last = _args_screen.size();
            //     std::string lastPath = _args_screen[last - 1];
            //     intermidiate.Set(Napi::String::New(env,"spath"), lastPath);
            // }
            durations[commandCounter] = duration;
            strCommands[commandCounter++] = createCommand(_args_camera);

            // attatch outputs to intermidiate object
            int last = _args_camera.size();
            std::string lastPath = _args_camera[last - 1];
            intermidiate.Set(Napi::String::New(env, "cpath"), lastPath);
            (((configObj.Get(Napi::String::New(env, "finalMerger")).ToObject()).Get(i)).ToObject()).Set(Napi::String::New(env, "intermidiate"), intermidiate);
        }

        std::cout << "\nCommands in preprocess :\n";
        int res;
        cout << "all commands : " << endl;
        for (int i = 0; i < noOfCommands; i++)
        {
            cout << endl << strCommands[i] << endl;
        }
        ofs << "came to before running command in for loop inside preprocess function" << endl;
        ofs.close();
        for (int i = 0; i < noOfCommands; i++)
        {
            // runtimeExec(strCommands[i].c_str(),cb,env,i);
            // std::cout << "**\n** Command no : **" << i << "**\n Command : **" << strCommands[i] << "**\n";
            res = RunCommand2(strCommands[i], (noOfCommands - i), cb, env, noOfCommands, i, durations[i]);
        }
        // NOTE -> delete temp files
        std::string tmp1 ="";
        std::string tmp2 ="";
        tmp1 = configFolderPath + "\\fixedTextContentTemp.txt";
        tmp2 = configFolderPath + "\\scrollingTextContentTemp.txt";
        remove(tmp1.c_str());
        remove(tmp2.c_str());
        for (unsigned int i = 0; i < finalMerger.Length(); i++) {
            tmp1 = configFolderPath + "\\fixedTextContentTemp-"+ to_string(i) +".txt";
            tmp2 = configFolderPath + "\\scrollingTextContentTemp-"+to_string(i)+".txt";
            remove(tmp1.c_str());
            remove(tmp2.c_str());
        }
        ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
        ofs << "preprocessing completed" << endl;
        ofs.close();
        Napi::Value str = Napi::String::New(env, "preprocess-completedd\n");
        cb.Call(env.Global(), {str});
        //  Napi::Object::Delete(str);
        Napi::Object obj = Napi::Object::New(env);
        obj.Delete(str);
        // cb.Call(env.Global(),{str});
        //  str.free();
        // std::cout << "preprocess-complete\n";
        strCommands = NULL;
        env.Null();

        // TODO - wait for process validation to complete
        // ? directions of use -
        // ? pass a function to future object and it will run asynchrously
        // ? write an infinite loop - to check if the promise has been fullfilled or not
        // ? in infinite loop check every second (using future.wait_for(seconds_here))
        // ? and check if the async function has returned or not
        // ? async function returns when the future status resolves to "ready"
        int timeout = 0;
        // TODO - destruct paramObj and extract its keys as individual variables

        // std::string basepath = paramObj.Get("basepath")
        std::string basepath = paramObj.Get("basepath").ToString();
        std::string appDataFolderPath = paramObj.Get("appDataFolderPath").ToString();
        std::string serverPath = paramObj.Get("serverPath").ToString();
    	std::string homeDir = paramObj.Get("homeDir").ToString();

        std::future<bool> future = std::async(std::launch::async, [basepath,appDataFolderPath,serverPath,homeDir]() {
            // asyn function here
            // ? must return something
            // ! cannot directly call isPreprocessLicenseValid function from here
            // ! napi throws error - entering napi witout proper locking in place
            // ? trying to re-write validation code in isPreprocessLicenseValid2 - without napi calls
            // bool isValid = isPreprocessLicenseValid2(basepath,homeDir,serverPath,appDataFolderPath);
            bool isValid = true;
            return isValid;
        });
        std::future_status status;
        do
        {
            status = future.wait_for(std::chrono::seconds(1));
            if (status == std::future_status::deferred)
            {
                // std::cout << "deferred\n";
            }
            else if (status == std::future_status::timeout)
            {
                // ? use this login if timeout required
                // std::cout << timeout << " second timeout passed !" << endl;
                // timeout++;
                // if (timeout == 10)
                // {
                //     commandReturned = false;
                //     break;
                // }
            }
            else if (status == std::future_status::ready)
            {
                // std::cout << "ready!\n";
            }
        } while (status != std::future_status::ready);
        bool result = future.get();
        return configObj;
        // }
        // else
        // {
        //     ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
        //     ofs << "license is not valid - returning control to js" << endl;
        //     ofs.close();
        //     validationCB.Call(env.Global(), {isValid});
        //     Napi::Object obj1 = Napi::Object::New(env);
        //     obj1.Set("LicenseInvalid", "true");
        //     return obj1;
        // }
    }
    catch (const std::exception &e)
    {
        std::string exceptionMessage = "exception-in-try-catch-block";
        // std::cerr << e.what() << '\n';
        ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
        ofs << "some exception occured while processing - returning control to js" << endl;
        ofs << "ecception is " << e.what() << endl;
        ofs.close();
        Napi::Object obj2 = Napi::Object::New(env);
        obj2.Set("Exception", exceptionMessage);
        return obj2;
    }
}
Napi::Object mergeFiles(const Napi::CallbackInfo &info) {
    const Napi::Env env = info.Env();
    Napi::Object obj = Napi::Object::New(env);

    Napi::Object mergeFilesObj = info[0].As<Napi::Object>();

    Napi::Array selectedFilesArr = mergeFilesObj.Get("changedFiles").As<Napi::Array>();
    std::string homeDir = mergeFilesObj.Get("filePath").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
    std::string basepath = mergeFilesObj.Get("basepath").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
    std::string ffmpegPath = basepath + "\\src\\assets\\exe\\ffmpeg.exe";

    std::string fileExtension = mergeFilesObj.Get("extension").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
    std::string fileType = mergeFilesObj.Get("fileType").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
    std::string newFileName = mergeFilesObj.Get("newFileName").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
	Napi::Function cb = mergeFilesObj.Get("callback").As<Napi::Function>();
	ofstream fout;
	fout.open(homeDir + "\\mergingExternalFiles.txt", std::fstream::out | std::fstream::trunc);
    // TODO - Step 1 -> create list to merge files

	if (!fout){
		// std::cout << "file could not open";
		std::string s = "could not open mergingList Path : ";
		cb.Call(env.Global(), {Napi::String::New(env, s)});
	}
	else
	{
        // cout << endl << "selected files length : " << selectedFilesArr.Length();
        for (unsigned int i = 0; i < selectedFilesArr.Length(); i++) {
            std::string path = selectedFilesArr.Get(i).ToString();
			fout << "file '" << path << "'" << endl;
        }
        fout.close();
        std::string s = "merging-list-created";
        cb.Call(env.Global(), {Napi::String::New(env, s)});
    }

    std::string mergedFilePath = (homeDir + "\\" +fileType + "\\" + newFileName + "." + fileExtension);
    // cout << endl << "final merged file path : " << mergedFilePath << endl;

    // TODO - Step 2 -> merge files
    // std::string cmd = basepath + "ffmpeg -f concat -safe 0 -i \"" + (homeDir + "mergingExternalFiles.txt") + "\" -codec copy " + (homeDir + "merged-file.mp4");
    std::string cmd = "\"" + ffmpegPath + "\"" +  " -f concat -safe 0 -i \"" + (homeDir + "\\mergingExternalFiles.txt") + "\" -c:v copy -c:a copy \"" + mergedFilePath +  "\" -y";
    cout << endl << "command to merge files : " << cmd << endl;
    int res = RunCommand2(cmd, 1, cb, env, 1, 0, 1);
    // int res = system(cmd.c_str());
    // cout << endl << "command successfully returned " << cmd << endl;
    obj.Set("mergedFilePath", mergedFilePath);
    return obj;
}
Napi::Object generateWaveformFromVideo(const Napi::CallbackInfo &info) {
    const Napi::Env env = info.Env();
    Napi::Object obj = Napi::Object::New(env);

    Napi::Object waveformObject = info[0].As<Napi::Object>();
    Napi::Function cb = info[1].As<Napi::Function>();

    std::string basepath = waveformObject.Get("basepath").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
    std::string ffmpegPath = basepath + "\\src\\assets\\exe\\ffmpeg.exe";

    std::string videoFilePath = waveformObject.Get("videoFilePath").ToString();
    std::string outputFilePath = waveformObject.Get("outputFilePath").ToString();
    std::string pictureWidth = waveformObject.Get("waveformPictureWidth").ToString();
    std::string pictureHeight = waveformObject.Get("waveformPictureHeight").ToString();
    std::string cmd = "\"" + ffmpegPath + "\"" +  " -i \"" + videoFilePath + "\" -filter_complex \"compand,showwavespic=colors=0x00aaff:s=" +pictureWidth+ "x" +pictureHeight+ "\" -frames:v 1 \"" +outputFilePath + "\" -y";
    // cout << endl << "command to create waves file: " << cmd << endl;
    int res = RunCommand2(cmd, 1, cb, env, 1, 0, 1);
    // system(cmd.c_str());
    obj.Set("outputFilePath", outputFilePath);

    return obj;
}
Napi::Object convertToMp4(const Napi::CallbackInfo &info) {
    const Napi::Env env = info.Env();
    Napi::Object obj = Napi::Object::New(env);

    Napi::Object conversionObject = info[0].As<Napi::Object>();
    Napi::Function cb = conversionObject.Get("callback").As<Napi::Function>();

    std::string basepath = conversionObject.Get("basepath").As<Napi::String>().ToString();           //path to the folder wich contains recorded files
    std::string ffmpegPath = basepath + "\\src\\assets\\exe\\ffmpeg.exe";

    std::string filepath = conversionObject.Get("filepath").ToString();
    std::string outputFilePath = conversionObject.Get("newFileName").ToString();
    std::string cmd = "\"" + ffmpegPath + "\"" + " -i \"" + filepath + "\" -c copy \"" +outputFilePath + "\" -y";

    cout << endl << "command to convert to mp4 : " << cmd;
    int res = RunCommand2(cmd, 1, cb, env, 1, 0, 1);
    // system(cmd.c_str());
    cout << endl << "result of conversion: " << res;

    obj.Set("outputFilePath", outputFilePath);

    return obj;
}
Napi::Object Init(Napi::Env env, Napi::Object exports)
{
    exports.Set(Napi::String::New(env, "preprocess"),
                Napi::Function::New(env, preprocess));
    exports.Set(Napi::String::New(env, "mergeFiles"),
                Napi::Function::New(env, mergeFiles));
    exports.Set(Napi::String::New(env, "generateWaveformFromVideo"),
                Napi::Function::New(env, generateWaveformFromVideo));
    exports.Set(Napi::String::New(env, "convertToMp4"),
                Napi::Function::New(env, convertToMp4));

    return exports;
}
NODE_API_MODULE(preprocess, Init);
