#include <napi.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <direct.h>
// #include "commandFunctions_validate.h"

#include <psapi.h>
#include <errno.h>
#include <array>
#include <windows.h>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <wininet.h>
#include <tchar.h>
#include <fstream>
#include <Sensapi.h>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include "RSJparser.tcc"
#include <regex>

std::string serverPath;

#pragma comment(lib, "Sensapi.lib")

#include "encfileop.h"
std::string mainKey = "HRECKEY";

// std::string homeDir;
HANDLE hChildProcess1; //NULL
HANDLE hStdIn1;		   // Handle to parents std input. NULL
BOOL bRunThread1;
std::string(*strCommand1);

int noOfCommands1 = 1;
int commandCounter1 = 1;

HANDLE hOutputRead1Tmp1, hOutputRead1, hOutputWrite1;
HANDLE hInputWrite1Tmp1, hInputRead1, hInputWrite1;
HANDLE hErrorWrite1;
HANDLE hThread1;
DWORD ThreadId1;

char *RunCommandFileBuffer(std::string cmdx)
{

	// int duration = durationx;
	LPTSTR cmd = const_cast<char *>((cmdx).c_str());

	BOOL ok = TRUE;
	HANDLE hStdInPipeRead = NULL;
	HANDLE hStdInPipeWrite = NULL;
	HANDLE hStdOutPipeRead = NULL;
	HANDLE hStdOutPipeWrite = NULL;

	// Create two pipes.
	SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), NULL, TRUE};
	ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
	if (ok == FALSE)
		return "null";
	ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
	if (ok == FALSE)
		return "null";

	// Create the process.
	STARTUPINFO si = {};
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdError = hStdOutPipeWrite;
	si.hStdOutput = hStdOutPipeWrite;
	si.hStdInput = hStdInPipeRead;
	PROCESS_INFORMATION pi = {};
	LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
	LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
	LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
	LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
	BOOL bInheritHandles = TRUE;
	DWORD dwCreationFlags = 0;
	LPVOID lpEnvironment = NULL;
	LPCWSTR lpCurrentDirectory = NULL;
	// ok = CreateProcess(
	//     lpApplicationName,
	//     lpCommandLine,
	//     lpProcessAttributes,
	//     lpThreadAttribute,
	//     bInheritHandles,
	//     dwCreationFlags,
	//     lpEnvironment,
	//     lpCurrentDirectory,
	//     &si,
	//     &pi);
	ok = CreateProcess(NULL, cmd, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	if (ok == FALSE)
		return "null";

	// Close pipes we do not need.
	CloseHandle(hStdOutPipeWrite);
	CloseHandle(hStdInPipeRead);

	// The main loop for reading output from the DIR command.
	char buf[1024 + 1] = {};
	DWORD dwRead = 0;
	DWORD dwAvail = 0;

	ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
	while (ok == TRUE)
	{
		buf[dwRead] = '\0';
		OutputDebugStringA(buf);
		puts(buf);
		ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
	}

	// Clean up and exit.
	CloseHandle(hStdOutPipeRead);
	CloseHandle(hStdInPipeWrite);
	DWORD dwExitCode = 0;
	GetExitCodeProcess(pi.hProcess, &dwExitCode);

	return buf;
	// return 0;
}
/********** Registry Functions start **************/
void writeToRegistry(std::string subKey, std::string subKeyValue)
{
	HKEY hKey;
	LPCTSTR sk = TEXT(mainKey.c_str());
	LONG openRes = RegOpenKeyEx(HKEY_CURRENT_USER, sk, 0, KEY_ALL_ACCESS, &hKey);
	if (openRes == ERROR_SUCCESS)
	{
		printf("Success opening key.");
	}
	else
	{
		printf("Error opening key.");
	}
	LPCTSTR value = TEXT(subKey.c_str());
	LPCTSTR data = TEXT(subKeyValue.c_str());

	LONG setRes = RegSetValueEx(hKey, value, 0, REG_SZ, (LPBYTE)data, _tcslen(data) * sizeof(TCHAR));

	if (setRes == ERROR_SUCCESS)
	{
		printf("Success writing to Registry.");
	}
	else
	{
		printf("Error writing to Registry.");
	}
	cout << setRes << endl;

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS)
	{
		printf("Success closing key.");
	}
	else
	{
		printf("Error closing key.");
	}
}
std::string readFromRegistry(std::string subKey)
{
	HKEY hk;
	DWORD dwData;
	HKEY hKey;
	LONG returnStatus;
	DWORD dwType = REG_DWORD;
	DWORD dwSize = sizeof(DWORD) * 2;

	HKEY OpenResult;

	LPCSTR pValue = subKey.c_str();
	DWORD flags = RRF_RT_ANY;

	//Allocationg memory for a DWORD value.
	// REG_SZ dataType;

	WCHAR value[255];
	PVOID pvData = value;

	DWORD size = sizeof(value); // not 8192;

	std::string returnValue = "";

	// Notice that it's SOFTWARE instead of \\SOFTWARE:
	long n = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT(mainKey.c_str()), 0, KEY_QUERY_VALUE, &hk);
	if (n == ERROR_SUCCESS)
	{
		// cout << "HRECKEY is now open" << endl;
		n = RegGetValueA(OpenResult, NULL, pValue, flags, NULL, pvData, &size);

		// cout << endl << "type of result is : " << dataType <<endl ;
		if (n != ERROR_SUCCESS)
		{
			wprintf(L"Error getting value. Code: %x\n", n);
			returnValue = "sub-key-not-present";
		}
		else
		{
			returnValue = (char *)pvData;
		}
		RegCloseKey(OpenResult); // don't forget !
		return returnValue;
	}
	else
	{
		cout << "HRECKEY not present " << n << endl;
		returnValue = "main-key-not-present";
		return returnValue;
	}
}
/**
* Deletes all subkeys under mainkey
*
*/
std::string removeRegistryContents()
{
	HKEY hKey;
	LPCTSTR sk = TEXT(mainKey.c_str());
	LONG openRes = RegOpenKeyEx(HKEY_CURRENT_USER, sk, 0, KEY_ALL_ACCESS, &hKey);
	std::string response = "";
	if (openRes == ERROR_SUCCESS)
	{
		printf("Success opening main key for deletion.");
		response = "Success opening main key for deletion.";
		LONG l = RegDeleteTreeA(hKey, NULL);
		if (l == ERROR_SUCCESS)
		{
			printf("Success deleting the registry tree.");
			response = "Success deleting the registry tree.";
		}
		else
		{
			printf("Error while deleting Registry Tree.");
			response = "Error while deleting Registry Tree.";
		}
	}
	else
	{
		printf("Error opening key for RegDeleteTreeA.");
		response = "Error opening key for RegDeleteTreeA.";
	}
}
/********************** Registry Functions End *******************/
/**
* check if dates in reg are valid
* 
*/
bool areDatesValid()
{
	// ! if lifetime is set to true - always return true
	string startDayKey = "startday", startMonthKey = "startmonth", startYearKey = "startyear";
	std::string lastDayKey = "lastvalidday", lastMonthKey = "lastvalidmonth", lastYearKey = "lastvalidyear";
	std::string endDayKey = "endday", endMonthKey = "endmonth", endYearKey = "endyear";

	std::string isLifetimeValidStr = "isLifetimeValid";
	// cout << endl
	// 	 << "Encrypted isLifetimeValid : " << Encrypt(isLifetimeValidStr);
	// cout << endl
	// 	 << "After reading from registry : " << readFromRegistry(Encrypt(isLifetimeValidStr));
	std::string isLifetimeValid = Decrypt(readFromRegistry(Encrypt(isLifetimeValidStr)));

	//s start day month year
	int startDayReg = stoi(Decrypt(readFromRegistry(Encrypt(startDayKey))));
	int startMonthReg = stoi(Decrypt(readFromRegistry(Encrypt(startMonthKey))));
	int startYearReg = stoi(Decrypt(readFromRegistry(Encrypt(startYearKey))));
	// end day month year
	int endDayReg = stoi(Decrypt(readFromRegistry(Encrypt(endDayKey))));
	int endMonthReg = stoi(Decrypt(readFromRegistry(Encrypt(endMonthKey))));
	int endYearReg = stoi(Decrypt(readFromRegistry(Encrypt(endYearKey))));
	// last day month year
	int lastDayReg = stoi(Decrypt(readFromRegistry(Encrypt(lastDayKey))));
	int lastMonthReg = stoi(Decrypt(readFromRegistry(Encrypt(lastMonthKey))));
	int lastYearReg = stoi(Decrypt(readFromRegistry(Encrypt(lastYearKey))));

	time_t t = time(NULL);
	tm *timePtr = localtime(&t);

	int currday = timePtr->tm_mday;
	int currmonth = timePtr->tm_mon + 1;
	int curryear = timePtr->tm_year + 1900;

	// cout << endl
	// 	 << "is this valid for lifetime : " << isLifetimeValid << endl;
	if (isLifetimeValid == "true")
	{
		return true;
	}
	else
	{
		cout << endl
			 << "Not Valid for Lifetime : " << endl;

		if (curryear > endYearReg)
		{
			return false;
		}
		else if (currmonth > endMonthReg && curryear == endYearReg)
		{
			return false;
		}
		else if (curryear == endYearReg && currmonth == endMonthReg && currday > endDayReg)
		{
			return false;
		}
		else
		{
			if (curryear < lastYearReg)
			{
				return false;
			}
			else if (currmonth < lastMonthReg && curryear == lastYearReg)
			{
				return false;
			}
			else if (curryear == lastYearReg && currmonth == lastMonthReg && currday < lastDayReg)
			{
				return false;
			}
			else
			{
				std::string lastday = "lastvalidday";
				writeToRegistry(Encrypt(lastday), Encrypt(to_string(currday)));
				std::string lastmonth = "lastvalidmonth";
				writeToRegistry(Encrypt(lastmonth), Encrypt(to_string(currmonth)));
				std::string lastyear = "lastvalidyear";
				writeToRegistry(Encrypt(lastyear), Encrypt(to_string(curryear)));
				return true;
			}
		}
	}
}
/**
* reads provided key from licenseInfo.dat file present in appdata folder
* @param {string} encrypted key to read
* @returns {string} encrypted value associated with encrypted key in appdata folder licenseinfo.dat file
*/
std::string readFromAppDataFile(std::string encryptedKey, std::string appDataFilePath)
{
	std::string readValue = "";
	std::string tmpValue = "";
	std::ifstream infile;
	std::vector<std::string> words;

	infile.open(appDataFilePath.c_str());
	if (infile.good())
	{
		string readLine = "";
		while (std::getline(infile, readLine))
		{
			split3(readLine, words, ':');
			tmpValue = words.at(0);
			if (tmpValue == encryptedKey)
			{
				readValue = words.at(1);
				break;
			}
			words.clear();
		}
		infile.close();
	}
	else
	{
		readValue = "file-not-present";
	}
	return readValue;
}
/**
* Compares contents in licenseInfo.dat present in appdata folder with contents from registry
* @return - true - if contents are similar
* @return - false - if contents are not similar
*
*/
bool areContentsSimilar(std::string appDataFilePath)
{
	// keys in file
	bool areSimilar = true;
	string startDayKey = "startday", startMonthKey = "startmonth", startYearKey = "startyear";
	std::string lastDayKey = "lastvalidday", lastMonthKey = "lastvalidmonth", lastYearKey = "lastvalidyear";
	std::string endDayKey = "endday", endMonthKey = "endmonth", endYearKey = "endyear", serialNumberKey = "serialNumber";

	// Read Values from Registry
	// serialnumber
	std::string serialNumberReg = Decrypt(readFromRegistry(Encrypt(serialNumberKey)));
	// start day month year
	std::string startDayReg = Decrypt(readFromRegistry(Encrypt(startDayKey)));
	std::string startMonthReg = Decrypt(readFromRegistry(Encrypt(startMonthKey)));
	std::string startYearReg = Decrypt(readFromRegistry(Encrypt(startYearKey)));
	// end day month year
	std::string endDayReg = Decrypt(readFromRegistry(Encrypt(endDayKey)));
	std::string endMonthReg = Decrypt(readFromRegistry(Encrypt(endMonthKey)));
	std::string endYearReg = Decrypt(readFromRegistry(Encrypt(endYearKey)));

	// Do not compare last days because last days are going to be updated
	// // last day month year
	// std::string lastDayReg = Decrypt(readFromRegistry(Encrypt(lastDayKey)));
	// std::string lastMonthReg = Decrypt(readFromRegistry(Encrypt(lastMonthKey)));
	// std::string lastYearReg = Decrypt(readFromRegistry(Encrypt(lastYearKey)));

	// Read Values from File
	// serialnumber
	std::string serialNumberFile = Decrypt(readFromAppDataFile(Encrypt(serialNumberKey), appDataFilePath));

	// cout << endl << "key : " << serialNumberKey << endl;
	// cout << "appDataFilePath : " << appDataFilePath << endl;
	// cout << "encryptedKey : " << Encrypt(serialNumberKey) << endl;
	// cout << "read from file output  : " << readFromAppDataFile(Encrypt(serialNumberKey),appDataFilePath) << endl;
	// cout << "serialNumber from file : " << Decrypt(readFromAppDataFile(Encrypt(serialNumberKey),appDataFilePath)) << endl;
	// start day month year
	std::string startDayFile = Decrypt(readFromAppDataFile(Encrypt(startDayKey), appDataFilePath));
	std::string startMonthFile = Decrypt(readFromAppDataFile(Encrypt(startMonthKey), appDataFilePath));
	std::string startYearFile = Decrypt(readFromAppDataFile(Encrypt(startYearKey), appDataFilePath));
	// end day month year
	std::string endDayFile = Decrypt(readFromAppDataFile(Encrypt(endDayKey), appDataFilePath));
	std::string endMonthFile = Decrypt(readFromAppDataFile(Encrypt(endMonthKey), appDataFilePath));
	std::string endYearFile = Decrypt(readFromAppDataFile(Encrypt(endYearKey), appDataFilePath));
	// last day month year
	// std::string lastDayFile = Decrypt(readFromAppDataFile(Encrypt(lastDayKey),appDataFilePath));
	// std::string lastMonthFile = Decrypt(readFromAppDataFile(Encrypt(lastMonthKey),appDataFilePath));
	// std::string lastYearFile = Decrypt(readFromAppDataFile(Encrypt(lastYearKey),appDataFilePath));

	// Now compare
	// cout << "SerialNumber compare : " << serialNumberReg << " : " << serialNumberFile;
	if (serialNumberReg != serialNumberFile)
		areSimilar = false;

	if (startDayReg != startDayFile)
		areSimilar = false;
	if (startMonthReg != startMonthFile)
		areSimilar = false;
	if (startYearReg != startYearFile)
		areSimilar = false;

	if (endDayReg != endDayFile)
		areSimilar = false;
	if (endMonthReg != endMonthFile)
		areSimilar = false;
	if (endYearReg != endYearFile)
		areSimilar = false;

	// if(lastDayReg != lastDayFile)
	// 	areSimilar = false;
	// if(lastMonthReg != lastMonthFile)
	// 	areSimilar = false;
	// if(lastYearReg != lastYearFile)
	// 	areSimilar = false;

	return areSimilar;
}
std::string getObjectStr(std::string s)
{
	// std::cout << "Check contetnts : "<<endl;
	std::string result;
	std::size_t found = s.find_last_of('}');
	// cout << "last index of } is : " << found;
	for (std::string::size_type i = 0; i < s.size(); i++)
	{
		// std::cout << s[i] << "->";
		result += s[i];
		if (s[i] == '}' && i == found)
		{
			// cout << "end found";
			break;
		}
	}
	// cout << endl <<"trimmed result is : " <<result <<endl;
	return result;
}

/**
 *  Alternate Function for server validation which does not use napi
 */
std::string ServerValidation(std::string keyOrSerialNumber, std::string basepath, std::string validateMode, std::string serverPath)
{
	ofstream ofs;
	ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came to servervalidation function in preprocess- validate.g" << endl;
	std::string serialNumber = "";
	std::string toRead = "serialNumber";
	serialNumber = Decrypt(readFromRegistry(Encrypt(toRead)));

	ofs << "serialnumber from registry is :: " << serialNumber << endl;
	std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "recorder-validation-2-0-0.php" + " validation " + serialNumber + " sample-time-now";
	// ? enclose command in qoutes ; for the directory structure might contain spaces
	// cmd = "\"" + cmd + "\"";
	ofs << "command to run http is :: " << cmd << endl;

	std::string res = getObjectStr(RunCommandFileBuffer(cmd));
	ofs << "output of run command from server is : " << res << endl;
	// cout << endl << "server response : "<< res << endl;
	return res;
}

/**
* validates user license by making http request with key and or serialnumber
* @param {string} keyOrSerialNumber - when validateMode is "validate-with-key" this contains key , when validateMode is "validate-with-key" this contains serialNumber 
* @param {string} basepath - path to where recorder is installed
* @param {Napi::Env} env - environment for Napi
* @param {string} validateMode - either contains "validate-with-key" or "validate-with-serial-number"
*/
Napi::Object ServerValidation(std::string keyOrSerialNumber, std::string basepath, Napi::Env env, std::string validateMode, std::string serverPath)
{
	Napi::Object returnObj = Napi::Object::New(env);
	ofstream ofs;
	ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came to servervalidation function in preprocess- validate.g" << endl;
	std::string serialNumber = "";
	// serialnumber not present in registry
	if (validateMode == "validate-with-key")
	{
		// THIS MODE WILL NOT HAPPEN WHILE PROCESSING
		// serialNumber = GenerateSerialNumber(basepath,env);
		// // if serialNumber == "wmic-did-not-respond" then serialNumber will be generated online php code
		// std::string key = keyOrSerialNumber;
		// std::string cmd =  basepath+"src\\assets\\exe\\http.exe" + " " + serverPath+"recorder-validation-2-0-0.php"+" activation "+key+" "+serialNumber;
		// cout << endl << "executing command : " << cmd;
		// std::string res = getObjectStr(RunCommand(cmd, env));
		// cout << endl << "server response : "<< res << endl;
		// returnObj.Set("ServerPhpResponse",res);
	}
	else if (validateMode == "validate-with-serial-number")
	{
		std::string toRead = "serialNumber";
		serialNumber = Decrypt(readFromRegistry(Encrypt(toRead)));
		// cout << endl
		// 	 << "SerialNumber read " << serialNumber;
		// std::string cmd =  "\""+basepath+"src\\assets\\exe\\http.exe\"" + " " + serverPath+"recorder-validation-2-0-0.php"+" validation "+serialNumber + " " + lastModifiedLocalTime;
		ofs << "serialnumber from registry is :: " << serialNumber << endl;
		std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "recorder-validation-2-0-0.php" + " validation " + serialNumber + " sample-time-now";
		// ? enclose command in qoutes ; for the directory structure might contain spaces
		// cmd = "\"" + cmd + "\"";
		ofs << "command to run http is :: " << cmd << endl;

		std::string res = getObjectStr(RunCommandFileBuffer(cmd));
		ofs << "output of run command from server is : " << res << endl;
		// cout << endl << "server response : "<< res << endl;
		returnObj.Set("ServerPhpResponse", res);
	}
	// cout << endl
	// 	 << "SerialNumber generated  : " << serialNumber;
	ofs.close();
	return returnObj;
}

bool isPreprocessLicenseValid2(std::string basepath, std::string homeDir, std::string serverPath, std::string appDataFolderPath)
{
	// cout << endl
	// 	 << "basepath : " << basepath << endl;
	// cout << endl
	// 	 << "homeDir : " << homeDir << endl;
	// cout << endl
	// 	 << "serverPath : " << serverPath << endl;
	// cout << endl
	// 	 << "appDataFolderPath : " << appDataFolderPath << endl;
	std::ofstream ofs;
	ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came to ispreprocesslicensevalid function present in preprocess-validate.h" << endl;
	// cout << "came to ispreprocesslicensevalid function present in preprocess-validate.h" << endl;

	// TODO -  instead of object - now use flags for validation output
	bool regNotFound = false;
	bool areSimilar = true;
	bool areValid = true;
	bool shouldRemoveRegistry = false;
	bool isValidFlag = true;

	std::string serverValidationStr = "";
	std::string toRead = "serialNumber";
	std::string serialNumber = readFromRegistry(Encrypt(toRead));
	if (serialNumber == "main-key-not-present" || serialNumber == "sub-key-not-present")
	{
		shouldRemoveRegistry = true;
		ofs << "serial number not present" << endl;
		isValidFlag = false;
	}
	else
	{
		// validating local values
		ofs << "serialnumber present : " << endl;

		areSimilar = areContentsSimilar(appDataFolderPath);
		// cout << endl
		// 	 << "Are contents similar : " << areSimilar << endl;
		ofs << endl
			<< "Are contents similar : " << areSimilar << endl;
		if (areSimilar == true)
		{
			areValid = areDatesValid();
			if (areValid == true)
			{
				// cout << endl
				// 	 << "Are dates valid : " << areValid << endl;
				ofs << endl
					<< "Are dates valid : " << areValid << endl;
			}
			else
			{
				isValidFlag = false;
				shouldRemoveRegistry = true;
			}
		}
		else
		{
			isValidFlag = false;
			shouldRemoveRegistry = true;
		}
	}
	ofs << "came to ispreprocesslicensevalid function present in preprocess-validate.h" << endl;
	DWORD dwSens;
	if (IsNetworkAlive(&dwSens) == TRUE)
	{

		ofs << "internet connected" << endl;
		// cout << "internet connected" << endl;
		std::string validateMode = "validate-with-serial-number";
		std::string serverValidationResponse = ServerValidation(Decrypt(serialNumber), basepath, validateMode, serverPath);
		// std::string serverPhpResponse = serverValidationResponse.Get("ServerPhpResponse").ToString();
		std::string serverPhpResponse = serverValidationResponse;
		RSJresource phpResponseObj(serverPhpResponse);

		std::string isValid = phpResponseObj["isValid"].as<std::string>();
		// ! when connected to a network but no internet;
		// ! isValid returns "" (empty string)
		// ! in such case -> fallback to localValidation
		if (isValid == "")
		{
		}
		else
		{
			serverValidationStr = isValid;
			// returnObj.Set("ServerValidation", isValid);
		}
		ofs << "response of server validation is : " << isValid << endl;
		// std::cout<<response;
		// returnObj.Set("ServerValidation",response);
	}
	else
	{
		ofs << "Internet not connected : " << endl;
	}

	//  ? Content Imported from preprocess() function in preprocess.cc
	// ofs.close();
	// ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came out of preproprocess validateh" << endl;
	bool isLicenseValid = true;

	// std::string areContentsSimilar = returnObj.Get("areContentsSimilar").ToString();
	// std::string areDatesValid = returnObj.Get("areDatesValid").ToString();
	ofs << "are Dates Valid : " << areDatesValid << endl;
	ofs << "are Contents similar : " << areContentsSimilar << endl;

	if (areSimilar == false || areValid == false)
	{
		isValidFlag = false;
	}
	ofs << "has server resposne : " << serverValidationStr << endl;
	// cout << "has server resposne : " << serverValidationStr << endl;

	if(serverValidationStr != ""){
		if(serverValidationStr != "true"){
			isValidFlag = false;
		}
	}
	// bool hasServerResponse = returnObj.Has("ServerValidation");
	// ofs << "has server resposne : " << hasServerResponse << endl;

	// if (hasServerResponse == true)
	// {
	// 	std::string isServerValidStr = returnObj.Get("ServerValidation").ToString();
	// 	if (isServerValidStr != "true")
	// 	{
	// 		isLicenseValid = false;
	// 		ofs << "server response is NOT true " << endl;
	// 	}
	// 	else
	// 	{
	// 		ofs << "server response IS true " << endl;
	// 	}
	// }
	// cout << endl
	// 	 << "Validating License : is Valid ->" << isValidFlag;
	ofs << "is license valid :  " << isValidFlag << endl;

	if (isValidFlag == true)
	{
		ofs << "license valid is true - before calling validation cb " << endl;
	}
	else
	{
		// ! License Invalid "KILL WHOLE PROCESS"
		ofs << "license is not valid - returning control to js" << endl;
		ofs.close();
		exit(0);
	}
	// ? Content Imported from preproces()
	// cout << endl
	// 	 << "shouldRemoveRegistry : " << shouldRemoveRegistry << endl;
	if (shouldRemoveRegistry == true)
	{
		removeRegistryContents();
	}
	ofs.close();
	return isValidFlag;
}
// ? Runs in background
// ! kills current process if license is found to be invalid
Napi::Object isPreprocessLicenseValid(Napi::Object param, Napi::Env env)
{
	Napi::Object returnObj = Napi::Object::New(env);
	std::ofstream ofs;
	ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came to ispreprocesslicensevalid function present in preprocess-validate.h" << endl;
	// cout << "came to ispreprocesslicensevalid function present in preprocess-validate.h" << endl;

	bool shouldRemoveRegistry = false;
	std::string basepath = param.Get("basepath").ToString();
	homeDir = param.Get("homeDir").ToString();
	setHomeDir(homeDir);
	serverPath = param.Get("serverPath").ToString();

	std::string appDataFilePath = param.Get("appDataFolderPath").ToString();
	// cout << endl
	// 	 << "appDataFilePath : " << appDataFilePath;
	std::string toRead = "serialNumber";
	std::string serialNumber = readFromRegistry(Encrypt(toRead));
	if (serialNumber == "main-key-not-present" || serialNumber == "sub-key-not-present")
	{
		returnObj.Set("regNotFound", true);
		shouldRemoveRegistry = true;
		ofs << "serial number not present" << endl;
		ofs.close();
	}
	else
	{
		// validating local values
		ofs << "serialnumber present : " << endl;
		ofs.close();

		bool areSimilar = areContentsSimilar(appDataFilePath);
		// cout << endl
		// 	 << "Are contents similar : " << areSimilar << endl;
		if (areSimilar == true)
		{
			returnObj.Set("areContentsSimilar", true);
			bool areValid = areDatesValid();
			if (areValid == true)
			{
				// cout << endl
				// 	 << "Are dates valid : " << areValid << endl;
				returnObj.Set("areDatesValid", true);
			}
			else
			{
				returnObj.Set("areDatesValid", false);
				shouldRemoveRegistry = true;
			}
		}
		else
		{
			returnObj.Set("areContentsSimilar", false);
			shouldRemoveRegistry = true;
		}
	}
	ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came to ispreprocesslicensevalid function present in preprocess-validate.h" << endl;
	DWORD dwSens;
	if (IsNetworkAlive(&dwSens) == TRUE)
	{

		ofs << "internet connected" << endl;
		std::string validateMode = "validate-with-serial-number";
		Napi::Object serverValidationResponse = ServerValidation(Decrypt(serialNumber), basepath, env, validateMode, serverPath);
		std::string serverPhpResponse = serverValidationResponse.Get("ServerPhpResponse").ToString();
		RSJresource phpResponseObj(serverPhpResponse);

		std::string isValid = phpResponseObj["isValid"].as<std::string>();
		// ! when connected to a network but no internet;
		// ! isValid returns "" (empty string)
		// ! in such case -> fallback to localValidation
		if (isValid == "")
		{
		}
		else
		{
			returnObj.Set("ServerValidation", isValid);
		}
		ofs << "response of server validation is : " << isValid << endl;
		// std::cout<<response;
		// returnObj.Set("ServerValidation",response);
	}
	else
	{
		ofs << "Internet not connected : " << endl;
	}

	//  ? Content Imported from preprocess() function in preprocess.cc
	// ofs.close();
	// ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
	ofs << "came out of preproprocess validateh" << endl;
	bool isLicenseValid = true;

	std::string areContentsSimilar = returnObj.Get("areContentsSimilar").ToString();
	std::string areDatesValid = returnObj.Get("areDatesValid").ToString();
	ofs << "are Dates Valid : " << areDatesValid << endl;
	ofs << "are Contents similar : " << areContentsSimilar << endl;

	if (areContentsSimilar != "true" || areDatesValid != "true")
	{
		isLicenseValid = false;
	}
	bool hasServerResponse = returnObj.Has("ServerValidation");
	ofs << "has server resposne : " << hasServerResponse << endl;

	if (hasServerResponse == true)
	{
		std::string isServerValidStr = returnObj.Get("ServerValidation").ToString();
		if (isServerValidStr != "true")
		{
			isLicenseValid = false;
			ofs << "server response is NOT true " << endl;
		}
		else
		{
			ofs << "server response IS true " << endl;
		}
	}
	// cout << endl
	// 	 << "Validating License : is Valid ->" << isLicenseValid;
	ofs << "is license valid :  " << isLicenseValid << endl;

	if (isLicenseValid == true)
	{
		ofs << "license valid is true - before calling validation cb " << endl;
	}
	else
	{
		// ! License Invalid "KILL WHOLE PROCESS"
		ofs.open("D:\\new-processing-log.txt", std::ofstream::out | std::ofstream::app);
		ofs << "license is not valid - returning control to js" << endl;

		exit(0);
	}
	// ? Content Imported from preproces()
	// cout << endl
	// 	 << "shouldRemoveRegistry : " << shouldRemoveRegistry << endl;
	if (shouldRemoveRegistry == true)
	{
		removeRegistryContents();
	}
	return returnObj;
}