#include <iostream>
#include <direct.h>
#include <string>
#include <array>
#include <node.h>
#include <windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <napi.h>
#include "commandFunctions.h"
#include <psapi.h>
#include <errno.h>
HANDLE hChildProcess; //NULL
HANDLE hStdIn;        // Handle to parents std input. NULL
BOOL bRunThread;      // TRUE
int noOfCommands = 0;
int commandCounter = 0;
std::string(*strCommands);
int screen_count = 1;
int camera_count = 1;

HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
HANDLE hInputWriteTmp, hInputRead, hInputWrite;
HANDLE hErrorWrite;
HANDLE hThread;
DWORD ThreadId;
char *RunCommandFileBuffer(std::string cmdx, Napi::Env env)
{

	// int duration = durationx;
	LPTSTR cmd = const_cast<char *>((cmdx).c_str());

	BOOL ok = TRUE;
	HANDLE hStdInPipeRead = NULL;
	HANDLE hStdInPipeWrite = NULL;
	HANDLE hStdOutPipeRead = NULL;
	HANDLE hStdOutPipeWrite = NULL;

	// Create two pipes.
	SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), NULL, TRUE};
	ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
	if (ok == FALSE)
		return "null";
	ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
	if (ok == FALSE)
		return "null";

	// Create the process.
	STARTUPINFO si = {};
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdError = hStdOutPipeWrite;
	si.hStdOutput = hStdOutPipeWrite;
	si.hStdInput = hStdInPipeRead;
	PROCESS_INFORMATION pi = {};
	LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
	LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
	LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
	LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
	BOOL bInheritHandles = TRUE;
	DWORD dwCreationFlags = 0;
	LPVOID lpEnvironment = NULL;
	LPCWSTR lpCurrentDirectory = NULL;
	// ok = CreateProcess(
	//     lpApplicationName,
	//     lpCommandLine,
	//     lpProcessAttributes,
	//     lpThreadAttribute,
	//     bInheritHandles,
	//     dwCreationFlags,
	//     lpEnvironment,
	//     lpCurrentDirectory,
	//     &si,
	//     &pi);
	ok = CreateProcess(NULL, cmd, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	if (ok == FALSE)
		return "null";

	// Close pipes we do not need.
	CloseHandle(hStdOutPipeWrite);
	CloseHandle(hStdInPipeRead);

	// The main loop for reading output from the DIR command.
	char buf[1024 + 1] = {};
	DWORD dwRead = 0;
	DWORD dwAvail = 0;

	ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
	while (ok == TRUE)
	{
		buf[dwRead] = '\0';
		OutputDebugStringA(buf);
		puts(buf);
		ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
	}

	// Clean up and exit.
	CloseHandle(hStdOutPipeRead);
	CloseHandle(hStdInPipeWrite);
	DWORD dwExitCode = 0;
	GetExitCodeProcess(pi.hProcess, &dwExitCode);

	return buf;
	// return 0;
}
// function formatScrollinText(formatScrollinText) {
// 	let updatedText = '';
// 	// const symbols = ['~','`','!','@','#','$','%','^','&','*','(',')','_','+','-','=','\\','|',';',':','\'','"',',','<','.','>','/','?'];
// 	const symbols = ['%'];
// 	for (const index in formatScrollinText) {
// 		if(symbols.includes(formatScrollinText[index])) {
// 			updatedText += ('\\' + formatScrollinText[index]);
// 		} else {
// 			updatedText += formatScrollinText[index];
// 		}
// 	}
// 	return updatedText;
// }

std::string getBrightnessFilter(Napi::Object currentRec) {
    std::string brightnessFilter = "";
    if(currentRec.Has("adjustBrightnessValues")) {
        cout << endl << "adjust brightness object present" << endl;
        Napi::Object brightnessValuesObj = currentRec.Get("adjustBrightnessValues").ToObject();
        std::string adjustBrightnessFlag = brightnessValuesObj.Get("enableAdjustBrightness").ToString();
        cout << endl << "adjust brightness object present : adjustBrightnessFlag" << adjustBrightnessFlag << endl;

        if(adjustBrightnessFlag == "true") {
            std::string gamma = brightnessValuesObj.Get("gamma").ToString();
            std::string saturation = brightnessValuesObj.Get("saturation").ToString();
            std::string brightness = brightnessValuesObj.Get("brightness").ToString();
            std::string contrast = brightnessValuesObj.Get("contrast").ToString();

            brightnessFilter = ",eq=gamma=" + gamma + ":saturation=" + saturation + ":brightness=" + brightness + ":contrast=" + contrast;
        }
    }
    return brightnessFilter;
}
std::string ReplaceAll(std::string str, const std::string &from, const std::string &to);
Napi::Object getVideoFilter(Napi::Object config, Napi::Object currentRec, Napi::Env env, std::string configFolderPath, std::string basepath, std::string previewStartTime);
std::string getCameraRotation(std::string cameraFilePath, std::string basepath,  Napi::Env env) {
    std::string ffprobeWithBasePath = basepath + "src\\assets\\exe\\ffprobe.exe";
    std::string cmd = ffprobeWithBasePath + " -loglevel error -select_streams v:0 -show_entries stream_tags=rotate -of default=nw=1:nk=1 -i " + cameraFilePath;
	std::string response = RunCommandFileBuffer(cmd, env);
    std::string rotationFormula = "null";
    if(response == "") {
        // cout << endl << "NO ROTATION WAS FOUND IN VIDEO" <<endl;
    } else {
        // cout << endl << "ffprobe command result : " << response <<endl;
        // cout << endl << "ffprobe command result : " << response.length() <<endl;
        // NOTE -> possible rotation values are 90, 180, 270
        if(response.find("90") != std::string::npos) {
            rotationFormula = "PI/2";
        } else if(response.find("180") != std::string::npos) {
            rotationFormula = "PI";
        } else if(response.find("270") != std::string::npos) {
            rotationFormula = "3*PI/2";
        } else {
            // cout << endl << "response did not match anywhere" << endl;
        }
    }
    return rotationFormula;
}
Napi::Object previewprocess(const Napi::CallbackInfo &info)
{
    const Napi::Env env = info.Env();
    Napi::Object config = info[0].ToObject(); //consists the information about recorded files
    Napi::Object currentRec = info[1].ToObject();
    Napi::Function cb = info[2].As<Napi::Function>();

    Napi::Object tmp = Napi::Object::New(env);
    tmp.Set("commandIndex", "Hello");
    cb.Call(env.Global(), {tmp});

    std::vector<std::string> cmd;
    std::string configFolderPath = config.Get(Napi::String::New(env, "configFolderPath")).As<Napi::String>().As<Napi::String>().Utf8Value();

    std::string outPutfileName = "false";

    std::string basepath = info[3].ToString();
    std::string previewStartTime = info[4].ToString();
    std::string previewStartTimeTmp = previewStartTime;
    std::string ffplayWithBasePath = basepath + "src\\assets\\exe\\ffplay.exe";

    std::string cameraFilePath = '"' + configFolderPath + "\\\\" + (currentRec.Get(Napi::String::New(env, "cpath")).As<Napi::String>().Utf8Value() + '"');
    cmd.push_back(ffplayWithBasePath);

    if(currentRec.Has("cameraCutStartTimeSeconds")) {
        std::string cameraStartTime = currentRec.Get("cameraCutStartTimeSeconds").ToString();
        // ! IMPORTANT NOTE
        // ? if screen cut start time is greater than 0
        // ? and seek point is less than screen cut start time
        // ? seek point needs to be updated to screen cut start time
        // ? otherwise it creates issues
        // ! issue -> audio audible before video starts and video speed is affected also
        int cameraStartTimeInt = stoi(cameraStartTime);
        int previewStartTimeInt = stoi(previewStartTime);
        previewStartTimeInt += cameraStartTimeInt;
        previewStartTimeTmp = to_string(previewStartTimeInt);
    }
    cmd.push_back("-ss");
    cmd.push_back(previewStartTimeTmp);

    cmd.push_back("-i");
    cmd.push_back(cameraFilePath);

    Napi::Object videoFilterObj = getVideoFilter(config, currentRec, env, configFolderPath, basepath, previewStartTime);
    std::string videoFilter = videoFilterObj.Get("filter").ToString();
    // ! TRIM caused playback issues with files converted to MP4 from MTS
    // ! trim was added to set start and end of the video
    // ? start of the video can be set using -ss and seekpoint
    // ? removing trim would not stop the video at end time
    // std::string cameraAudioTrimFilter = videoFilterObj.Get("cameraAudioTrimFilter").ToString();

    cmd.push_back("-vf");
    cmd.push_back("\" " + videoFilter + "\"");
    cmd.push_back("-x");
    cmd.push_back("720"); //16:9 ratio viewport
    cmd.push_back("-y");
    cmd.push_back("405");
    cmd.push_back("-autoexit");
    // cmd.push_back(cameraAudioTrimFilter);
    cmd.push_back("-noautorotate");

    strCommands = new std::string[1];
    strCommands[0] = createCommand(cmd);
    std::cout << "\nCommands in preprocess :\n"
              << strCommands[0];

    const char *command = strCommands[0].c_str();
    // std::cout << "preview code exec complete" << command;
	std::string response = RunCommandFileBuffer(command, env);

    // system(command);
	Napi::Object returnObj = Napi::Object::New(env);
    returnObj.Set("command",command);
    cout << endl << "preview command is : " << command << endl;
    return returnObj;
}
std::string getScrollingTextFilter(Napi::Object scrollingTextObj, Napi::Object currentRec, Napi::Object config, Napi::Env env, std::string configFolderPath) {


    std::string outputWidth = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
    std::string outputHeight = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

    int outputWidthInt = stoi(outputWidth);
    int outputHeightInt = stoi(outputHeight);

    std::string fixedTextWidthRawPx = scrollingTextObj.Get("fixedTextWidthRawPx").ToString();
    std::string topLocationRawPx = scrollingTextObj.Get("topLocationRawPx").ToString();

    std::string fixedTextWidthPx = scrollingTextObj.Get("fixedTextWidthPx").ToString();
    std::string topLocationPx = scrollingTextObj.Get("topLocationPx").ToString();


    std::string fontsize = scrollingTextObj.Get("fontSize").ToString();
    int fontSizeInt = std::stoi(fontsize);
    fontSizeInt = fontSizeInt * 4;
    fontsize =  to_string(fontSizeInt);
    // ! ask why +10 to neha, and write reason here
    int temp = std::stoi(fontsize);
    // NOTE -> +10 makes incorrect location
    // temp = temp + 10;
    std::string padding = "10";

    std::string height =  fontsize;
    // cout << endl << "fixedTextWidthRawPx" << fixedTextWidthRawPx;
    // cout << endl << "topLocationRawPx" << topLocationRawPx;

    // cout << endl << "fixedTextWidthPx" << fixedTextWidthPx;
    // cout << endl << "topLocationPx" << topLocationPx;

    // cout << endl << "fontsize" << fontsize;
    // cout << endl << "height" << height;

    std::string totaltime = scrollingTextObj.Get("scrollTextRunningTimeSec").ToString();
    std::string totaltimeraw = totaltime;
    int totaltimeInt = std::stoi(totaltime);
    std::string starttime = scrollingTextObj.Get("scrollTextStartTimeSec").ToString();
    int starttimeInt = std::stoi(starttime);


    totaltimeInt = (starttimeInt + totaltimeInt);
    totaltime = to_string(totaltimeInt);

    // cout << endl << "totaltime" << totaltime;
    // cout << endl << "starttime" << starttime;

    std::string fixedTextColor = scrollingTextObj.Get("fixedTextColor").ToString();
    std::string fixedText = scrollingTextObj.Get("fixedText").ToString();
    std::string fixedTextBackColor = scrollingTextObj.Get("fixedTextBackColor").ToString();

    // cout << endl << "fixedTextColor" << fixedTextColor;
    // cout << endl << "fixedText" << fixedText;
    // cout << endl << "fixedTextBackColor" << fixedTextBackColor;

    std::string scrollTextColor = scrollingTextObj.Get("scrollTextColor").ToString();
    std::string scrollingText = scrollingTextObj.Get("scrollingText").ToString();
    std::string scrollTextBackColor = scrollingTextObj.Get("scrollTextBackColor").ToString();

    // TODO -> create temp files to write scrolling and fixed text
    std::string fixedTextContentFilePath = configFolderPath + "\\\\\\\\fixedTextContentTemp.txt";
    std::string scrollingTextContentFilePath = configFolderPath + "\\\\\\\\scrollingTextContentTemp.txt";

    std::string fixedTextContentFilePathForFFMPEG = ReplaceAll(fixedTextContentFilePath, ":", "\\\\:");
    std::string scrollingTextContentFilePathForFFMPEG = ReplaceAll(scrollingTextContentFilePath, ":", "\\\\:");


    fstream fixedTextContentFileObj;
    fstream scrollingTextContentFileObj;

    fixedTextContentFileObj.open(fixedTextContentFilePath.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc);
    scrollingTextContentFileObj.open(scrollingTextContentFilePath.c_str(), std::fstream::in | std::fstream::out | std::fstream::trunc);

    // ? sanitize texts for ffmpeg
    fixedText = ReplaceAll(fixedText, "%", "\\%");
    scrollingText = ReplaceAll(scrollingText, "%", "\\%");

    // ? write text to files
    fixedTextContentFileObj << fixedText;
    scrollingTextContentFileObj << scrollingText;

    // ? close files
    fixedTextContentFileObj.close();
    scrollingTextContentFileObj.close();
    // cout << endl << "scrollingText" << scrollingText;
    // cout << endl << "scrollTextColor" << scrollTextColor;
    // cout << endl << "scrollTextBackColor" << scrollTextBackColor;

    // cout << endl << "outputWidthInt" << outputWidthInt;
    // cout << endl << "outputHeightInt" << outputHeightInt;
    std::string box1 = "drawbox=x=0:y="+topLocationPx+":"+"w="+outputWidth+":h="+height+"+10:color="+
                        scrollTextBackColor+"@1.0:t=fill:enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    // std::string box2 = "drawbox=x=0:y="+topLocationPx+":"+"w="+fixedTextWidthPx+"*2.8:h="+height+"+10:color="+
    // fixedTextBackColor+"@1.0:t=fill:enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    // ? backup command
    // std::string filter1 = "drawtext=fontfile=dosis.ttf:text="+scrollingText+":fontcolor="+scrollTextColor+"@1.0:fontsize="+fontsize+":"+
    //              "y="+ topLocationPx+":x=w-tw/10*mod(t\\,"+totaltime+"):"+
    //              "enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    std::string filter1 = "drawtext=fontfile=dosis.ttf:textfile="+scrollingTextContentFilePathForFFMPEG +":fontcolor="+scrollTextColor+"@1.0:fontsize="+fontsize+":"+
                 "y="+ topLocationPx+"+10:x=w-mod(max(t-"+starttime+"\\,0)*(w+tw)/"+totaltimeraw+"\\,(w+tw))"+
                 ":enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";



    std::string name1="drawtext=fontfile=dosis.ttf:textfile="+fixedTextContentFilePathForFFMPEG+":fontcolor="+fixedTextColor+"@1.0:fontsize="+fontsize+ ":"+
               "x=5:y="+topLocationPx+
                "+10:box=1:boxcolor="+fixedTextBackColor+":boxborderw=10:enable=gt(mod(t\\,"+totaltime+")\\,"+starttime+")";

    std::string final2 = "," + box1+"," +filter1+","+name1;

    std::string filter = final2;
    // std::string filter = "";
    return filter;
}
Napi::Object sanitizeForFFMPEG(int outputWidth, int outputHeight,int width,int height,int left,int top, Napi::Env env) {
    Napi::Object dimensions = Napi::Object::New(env);
    // cout << endl << "old width : " << width;
    // cout << endl << "old height : " << height;
    // cout << endl << "old left : " << left;
    // cout << endl << "old top : " << top;
    if (width + left > outputWidth || (width % 2 != 0 && left != 0)) {
		left--;
		width++;
	}
	if (height + top > outputHeight || (height % 2 != 0 && top != 0)) {
		top--;
		height++;
	}
    // cout << endl << "new width : " << width;
    // cout << endl << "new height : " << height;
    // cout << endl << "new left : " << left;
    // cout << endl << "new top : " << top << endl << endl;
    dimensions.Set("width",width);
    dimensions.Set("height",height);
    dimensions.Set("left",left);
    dimensions.Set("top",top);

    return dimensions;
}
Napi::Object getVideoFilter(Napi::Object config, Napi::Object currentRec, Napi::Env env, std::string configFolderPath, std::string basepath, std::string previewStartTime)
{
    Napi::Object filterObj = Napi::Object::New(env);

    std::string filter = "";
    std::string type = currentRec.Get(Napi::String::New(env, "type")).As<Napi::String>().ToString();
    std::string backImgPath = config.Get(Napi::String::New(env, "backImgPath")).As<Napi::String>().ToString();
    // cout << endl
    //      << "backImgPath" << backImgPath;
    std::string cwidth = currentRec.Get(Napi::String::New(env, "cwidth")).As<Napi::String>().ToString();
    std::string cheight = currentRec.Get(Napi::String::New(env, "cheight")).As<Napi::String>().ToString();
    std::string ctop = currentRec.Get(Napi::String::New(env, "ctop")).As<Napi::String>().ToString();
    std::string cleft = currentRec.Get(Napi::String::New(env, "cleft")).As<Napi::String>().ToString();
    std::string outputWidth = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
    std::string outputHeight = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();

    int leftInt = stoi(cleft);
    int topInt = stoi(ctop);
    int widthInt = stoi(cwidth);
    int heightInt = stoi(cheight);

    int outputWidthInt = stoi(outputWidth);
    int outputHeightInt = stoi(outputHeight);

    // cout << endl
    //      << "Left + width " << (leftInt + widthInt) << " out of : " << outputWidthInt << endl;
    // cout << endl
    //      << "top + height " << (topInt + heightInt) << " out of : " << outputHeightInt << endl;

	Napi::Object ffmpegSanitizedDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, widthInt, heightInt, leftInt, topInt, env);
    cleft = ffmpegSanitizedDimensions.Get("left").ToString();
    ctop = ffmpegSanitizedDimensions.Get("top").ToString();
    cwidth = ffmpegSanitizedDimensions.Get("width").ToString();
    cheight = ffmpegSanitizedDimensions.Get("height").ToString();

    // cout << endl << "sanitized width : " << cwidth;
    // cout << endl << "sanitized height : " << cheight;
    // cout << endl << "sanitized left : " << cleft;
    // cout << endl << "sanitized top : " << ctop << endl << endl;
    // if ((leftInt + widthInt) > outputWidthInt)
    // {
    //     widthInt--;
    //     cwidth = to_string(widthInt);
    // }
    // if ((topInt + heightInt) > outputHeightInt)
    // {
    //     heightInt--;
    //     cheight = to_string(heightInt);
    // }

    Napi::Object logoCoords = config.Get("logoCoords").ToObject();
    Napi::Array logoKeys = currentRec.GetPropertyNames();
    bool isLogoPresent = false;
    bool isLogoVisibleForCurrentRec = false;


    std::string logoWidth = "";
    std::string logoHeight = "";
    std::string logoLeft = "";
    std::string logoTop = "";
    std::string logoImgPath = "";
    logoImgPath = config.Get("logoImgPath").ToString();
    // std::string cameraAudioTrimFilter = "";

    if (logoImgPath != "false")
    {
        isLogoPresent = true;
        // ! must add logo only if visibility is not false
        // ? null or undefined indicates that user has not specifically
        // ? set it to false
        if(currentRec.Has("isLogoVisible")) {
            std::string logoVisibility = currentRec.Get("isLogoVisible").ToString();
            if(logoVisibility == "false") {
                isLogoPresent = false;
            }
        }
        logoWidth = logoCoords.Get("width").ToString();
        logoHeight = logoCoords.Get("height").ToString();
        logoLeft = logoCoords.Get("left").ToString();
        logoTop = logoCoords.Get("top").ToString();


        int logoLeftInt = stoi(logoLeft);
        int logoTopInt = stoi(logoTop);
        int logoWidthInt = stoi(logoWidth);
        int logoHeightInt = stoi(logoHeight);

        // cout << endl << "Logo coords before sanitization : " ;
        //  cout << endl
        //      << "Logo width : " << logoWidth << endl;
        // cout << endl
        //      << "Logo height : " << logoHeight << endl;
        // cout << endl
        //      << "Logo left : " << logoLeft << endl;
        // cout << endl
        //      << "Logo top : " << logoTop << endl;

        // ! sanitize for ffmpeg
        Napi::Object ffmpegSanitizedLogoDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, logoWidthInt, logoHeightInt, logoLeftInt, logoTopInt, env);
        logoLeft = ffmpegSanitizedLogoDimensions.Get("left").ToString();
        logoTop = ffmpegSanitizedLogoDimensions.Get("top").ToString();
        logoWidth = ffmpegSanitizedLogoDimensions.Get("width").ToString();
        logoHeight = ffmpegSanitizedLogoDimensions.Get("height").ToString();
        // it has width , height,left,top properties
        // cout << endl << "Logo coords after sanitization : " ;

        // cout << endl
        //      << "Logo width : " << logoWidth << endl;
        // cout << endl
        //      << "Logo height : " << logoHeight << endl;
        // cout << endl
        //      << "Logo left : " << logoLeft << endl;
        // cout << endl
        //      << "Logo top : " << logoTop << endl;
    }
    // cout << endl
    //      << "logoImgPath : " << logoImgPath << endl;
    bool transparencyCheck = currentRec.Get(Napi::String::New(env, "transparencyCheck")).ToBoolean();

    if (type.compare("both") == 0)
    {
        // ? init screen params
        std::string swidth = currentRec.Get(Napi::String::New(env, "swidth")).As<Napi::String>().ToString();
        std::string sheight = currentRec.Get(Napi::String::New(env, "sheight")).As<Napi::String>().ToString();
        std::string stop = currentRec.Get(Napi::String::New(env, "stop")).As<Napi::String>().ToString();
        std::string sleft = currentRec.Get(Napi::String::New(env, "sleft")).As<Napi::String>().ToString();

        int sleftInt = stoi(sleft);
        int stopInt = stoi(stop);
        int swidthInt = stoi(swidth);
        int sheightInt = stoi(sheight);

        Napi::Object ffmpegSanitizedDimensions = sanitizeForFFMPEG(outputWidthInt,outputHeightInt, swidthInt, sheightInt, sleftInt, stopInt, env);
        sleft = ffmpegSanitizedDimensions.Get("left").ToString();
        stop = ffmpegSanitizedDimensions.Get("top").ToString();
        swidth = ffmpegSanitizedDimensions.Get("width").ToString();
        sheight = ffmpegSanitizedDimensions.Get("height").ToString();

        // cout << endl << "sanitized screen width : " << swidth;
        // cout << endl << "sanitized screen height : " << sheight;
        // cout << endl << "sanitized screen left : " << sleft;
        // cout << endl << "sanitized screen top : " << stop << endl << endl;

        std::string spath = (configFolderPath + "\\\\\\\\" + currentRec.Get(Napi::String::New(env, "spath")).As<Napi::String>().Utf8Value());
        spath = ReplaceAll(spath, ",", "\\,");

        // cout << endl
        //      << "spath" << spath << endl;
        spath = ReplaceAll(spath, ":", "\\\\:");
        // std::string screenTrimFilter = "";
        if(currentRec.Has("screenCutStartTimeSeconds")) {
            std::string screenStartTime = currentRec.Get("screenCutStartTimeSeconds").ToString();
            std::string screenEndTime = currentRec.Get("screenCutEndTimeSeconds").ToString();
            // screenTrimFilter = ", trim=start=" +screenStartTime + ":end=" +screenEndTime + ",setpts=PTS-STARTPTS";

            // ! IMPORTANT NOTE
            // ? if screen cut start time is greater than 0
            // ? and seek point is less than screen cut start time
            // ? seek point needs to be updated to screen cut start time
            // ? otherwise it creates issues
            // ! issue -> audio audible before video starts and video speed is affected also
            int screenStartTimeInt = stoi(screenStartTime);
            int previewStartTimeInt = stoi(previewStartTime);
            previewStartTimeInt += screenStartTimeInt;
            previewStartTime = to_string(previewStartTimeInt);
        }
        filter += "movie=" + spath + ":seek_point="+previewStartTime  +"[screen];";
        if (isLogoPresent)
        {
            logoImgPath = ReplaceAll(logoImgPath, "\\", "\\\\");
            logoImgPath = ReplaceAll(logoImgPath, ":", "\\\\:");
            logoImgPath = ReplaceAll(logoImgPath, ",", "\\,");

            filter += "movie=" + logoImgPath + "[logo];[logo]scale=" + logoWidth + ":" + logoHeight + " [scaled_logo];";
        }
        // cout << endl
        //      << "filter" << filter << endl;

        // cout << "transparencyCheck : " << transparencyCheck;
        if (backImgPath.compare("false") != 0)
        {
            // std::string backPath = config.Get(Napi::String::New(env,"backImgPath")).As<Napi::String>().Utf8Value();
            // cout << endl
            //      << "backImgPath" << backImgPath;
            // std::string backPath;
            // backImgPath=ReplaceAll(backImgPath,":", "\\\\\\\\:");
            backImgPath = ReplaceAll(backImgPath, "\\", "\\\\");
            backImgPath = ReplaceAll(backImgPath, ":", "\\\\:");
            backImgPath = ReplaceAll(backImgPath, ",", "\\,");

            filter += "movie=" + backImgPath + "[back];";
            // cout << endl
            //      << "backImgPath" << backImgPath;
        }
         // TODO - get crop command for camera , regardless of transparency
        bool isCameraWindowCoordsPresent = currentRec.Has("cameraWindowCoords");
        std::string cameraCropFilter = "";
        if(isCameraWindowCoordsPresent == true) {
            std::string wccWidth = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
            std::string wccHeight = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
            std::string wccScreenX = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
            std::string wccScreenY = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();

            // NOTE -> when videos are rotated , in some cases,
            // ? if rotated by 90deg or 270deg
            // ? we need to swap crop coords to make this work
            std::string cameraFilePath = '"' + configFolderPath + "\\\\" + (currentRec.Get(Napi::String::New(env, "cpath")).As<Napi::String>().Utf8Value() + '"');
            std::string cameraRotation = getCameraRotation(cameraFilePath, basepath, env);
            cout << endl << "Camera rotation value : " << cameraRotation << endl;
            std::string cameraRotationFilter = "";
            bool shouldSwapDimensions = false;
            if(cameraRotation != "null") {
                cameraRotationFilter = ", rotate="+cameraRotation;
                if(cameraRotation == "PI/2" ||cameraRotation == "3*PI/2") {
                    shouldSwapDimensions = true;
                }
            }
            cout << endl << "should swap dimensions : " << shouldSwapDimensions << endl;
            if(shouldSwapDimensions) {
                cameraCropFilter = "crop=" + wccHeight  + ":" + wccWidth + ":" + wccScreenY + ":" + wccScreenX + ",";
            } else {
                cameraCropFilter = "crop=" + wccWidth + ":" + wccHeight + ":" + wccScreenX + ":" + wccScreenY + ",";
            }
        }
        // TODO - set camera trim filter
        // std::string cameraTrimFilter = "";
        if(currentRec.Has("cameraCutStartTimeSeconds")) {
            std::string cameraStartTime = currentRec.Get("cameraCutStartTimeSeconds").ToString();
            std::string cameraEndTime = currentRec.Get("cameraCutEndTimeSeconds").ToString();
            // cameraTrimFilter = "trim=start=" +cameraStartTime + ":end=" +cameraEndTime + ",setpts=PTS-STARTPTS,";
            // NOTE -> removing asetpts, adding it causes vsync issues in mp4 converted from mts
            // cameraAudioTrimFilter = " -af atrim=start=" +cameraStartTime + ":end=" +cameraEndTime + ",asetpts=PTS-STARTPTS";
            // cameraAudioTrimFilter = " -af atrim=start=" +cameraStartTime + ":end=" +cameraEndTime;
        }
        std::string cameraFilePath = '"' + configFolderPath + "\\\\" + (currentRec.Get(Napi::String::New(env, "cpath")).As<Napi::String>().Utf8Value() + '"');
        std::string cameraRotation = getCameraRotation(cameraFilePath, basepath, env);
        cout << endl << "Camera rotation value : " << cameraRotation << endl;
        std::string cameraRotationFilter = "";
        if(cameraRotation != "null") {
            cameraRotationFilter = ", rotate="+cameraRotation;
        }
        // TODO - get brightness values for camera
        std::string brightnessFilter = getBrightnessFilter(currentRec);
        if (transparencyCheck == true)
        {
            std::string colorHex = currentRec.Get(Napi::String::New(env, "colorHex")).As<Napi::String>().ToString();
            std::string transparencySimilarity = currentRec.Get(Napi::String::New(env, "transparencySimilarity")).As<Napi::String>().ToString();
            std::string transparencyOpacity = currentRec.Get(Napi::String::New(env, "transparencyOpacity")).As<Napi::String>().ToString();
            colorHex = ReplaceAll(colorHex, "#", "");
            // cout << endl
            //      << colorHex << "colorHex" << endl;
            filter += "[in]"+cameraCropFilter + "scale=" + cwidth + ":" + cheight + brightnessFilter + ",chromakey=0x" + colorHex + ":" + transparencySimilarity + ":" + transparencyOpacity + cameraRotationFilter +"[camera];";
        }
        else
        {
            filter += "[in]"+cameraCropFilter+"scale=" + cwidth + ":" + cheight + cameraRotationFilter + brightnessFilter +"[camera];";
        }
        // cout << endl
        //      << "filter two" << filter << endl;
        Napi::Array keys = currentRec.GetPropertyNames();
        for (int i = 0; i < keys.Length(); i++)
        {
            std::string key = keys.Get(i).ToString();
            std::string value = currentRec.Get(keys.Get(i)).ToString();
            // cout << key << " => " << value << endl;
        }
        bool fullScreenMode = currentRec.Get(Napi::String::New(env, "fullScreenMode")).ToBoolean();
        // cout << endl
        //      << "filter two" << fullScreenMode << endl;

        // cout << endl << "fullScreenMode" <<fullScreenMode<< endl;
        if (fullScreenMode == true)
            filter += "[screen]scale=" + swidth + ":" + sheight + "[screen_scaled];";
        else
        {
            // TODO -  check if crop exists here or not
            bool isScreenWindowCoordsPresent = currentRec.Has("screenWindowCoords");
            // ! no need here, already declared above
            // std::string swidth = currentRec.Get(Napi::String::New(env, "swidth")).As<Napi::String>().ToString();
            // std::string sheight = currentRec.Get(Napi::String::New(env, "sheight")).As<Napi::String>().ToString();
            if(isScreenWindowCoordsPresent == true) {
                std::string wcWidth = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
                std::string wcHeight = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
                std::string wcScreenX = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
                std::string wcScreenY = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();

                filter += "[screen]crop=" + wcWidth + ":" +
                        wcHeight + ":" +
                        wcScreenX + ":" +
                        wcScreenY + ",scale=" + swidth + ":" + sheight + "[screen_scaled];";
            } else {
                filter += "[screen]scale=" + swidth + ":" + sheight + "[screen_scaled];";
            }

        }
        if (backImgPath.compare("false") != 0)
        {
            // ! no need here , already declared above
            // std::string sleft = currentRec.Get(Napi::String::New(env, "sleft")).As<Napi::String>().ToString();
            // std::string stop = currentRec.Get(Napi::String::New(env, "stop")).As<Napi::String>().ToString();
            filter += "[back]scale=" + outputWidth + ":" + outputHeight + "[back_scaled];";
            filter += "[back_scaled][screen_scaled]overlay=" + sleft + ":" + stop + "[backscreen];";
        }
        else
        {
            // ! no need here, already declared above
            // std::string outputWidth = config.Get(Napi::String::New(env,"outputRes")).ToObject().Get(Napi::String::New(env,"width")).As<Napi::String>().ToString();
            // std::string outputHeight = config.Get(Napi::String::New(env,"outputRes")).ToObject().Get(Napi::String::New(env,"height")).As<Napi::String>().ToString();
            // std::string stop = currentRec.Get(Napi::String::New(env, "stop")).As<Napi::String>().ToString();
            // std::string sleft = currentRec.Get(Napi::String::New(env, "sleft")).As<Napi::String>().ToString();
            std::string paddingColor = config.Get(Napi::String::New(env, "paddingColor")).As<Napi::String>().ToString();
            filter += "[screen_scaled]pad=width=" + outputWidth +
                      ":height=" + outputHeight +
                      ":x=" + sleft +
                      ":y=" + stop +
                      ":color=" + paddingColor + "[backscreen];";
            // filter += "[screen_scaled]pad=width="+config.Get(Napi::String::New(env,"outputRes")).ToObject().Get(Napi::String::New(env,"width").As<Napi::String>().Utf8Value());
        }
        if (isLogoPresent)
        {
            filter += "[backscreen][camera]overlay=" + cleft + ":" + ctop + " [over];[over][scaled_logo]overlay=" + logoLeft + ":" + logoTop;
        }
        else
        {
            filter += "[backscreen][camera]overlay=" + cleft + ":" + ctop;
        }
    }
    else
    {
        Napi::Array keys = currentRec.GetPropertyNames();
        for (int i = 0; i < keys.Length(); i++)
        {
            std::string key = keys.Get(i).ToString();
            std::string value = currentRec.Get(keys.Get(i)).ToString();
            // cout << key << " => " << value << endl;
        }
        if (backImgPath.compare("false") != 0)
        {
            // std::string backPath = config.Get(Napi::String::New(env,"backImgPath")).As<Napi::String>().Utf8Value();

            backImgPath = ReplaceAll(backImgPath, "\\", "\\\\");
            backImgPath = ReplaceAll(backImgPath, ":", "\\\\:");
            backImgPath = ReplaceAll(backImgPath, ",", "\\,");

            filter += "movie=" + backImgPath + "[back];";
        }
        if (isLogoPresent)
        {
            logoImgPath = ReplaceAll(logoImgPath, "\\", "\\\\");
            logoImgPath = ReplaceAll(logoImgPath, ":", "\\\\:");
            logoImgPath = ReplaceAll(logoImgPath, ",", "\\,");

            filter += "movie=" + logoImgPath + "[logo];[logo]scale=" + logoWidth + ":" + logoHeight + " [scaled_logo];";
        }
        std::string cameraTrimFilter = "";
        if(currentRec.Has("cameraCutStartTimeSeconds")) {
            std::string cameraStartTime = currentRec.Get("cameraCutStartTimeSeconds").ToString();
            std::string cameraEndTime = currentRec.Get("cameraCutEndTimeSeconds").ToString();
            // cameraTrimFilter = "trim=start=" +cameraStartTime + ":end=" +cameraEndTime + ",setpts=PTS-STARTPTS,";
            // cameraAudioTrimFilter = " -af atrim=start=" +cameraStartTime + ":end=" +cameraEndTime + ",asetpts=PTS-STARTPTS";
            // NOTE -> removing asetpts, adding it causes vsync issues in mp4 converted from mts
            // cameraAudioTrimFilter = " -af atrim=start=" +cameraStartTime + ":end=" +cameraEndTime;
        }
        if(currentRec.Has("screenCutStartTimeSeconds")) {
            std::string screenStartTime = currentRec.Get("screenCutStartTimeSeconds").ToString();
            std::string screenEndTime = currentRec.Get("screenCutEndTimeSeconds").ToString();
            // cameraTrimFilter = "trim=start=" +screenStartTime + ":end=" +screenEndTime + ",setpts=PTS-STARTPTS,";
            // cameraAudioTrimFilter = " -af atrim=start=" +screenStartTime + ":end=" +screenEndTime + ",asetpts=PTS-STARTPTS";
            // NOTE -> removing asetpts, adding it causes vsync issues in mp4 converted from mts
            // cameraAudioTrimFilter = " -af atrim=start=" +screenStartTime + ":end=" +screenEndTime;
        }

        std::string cameraFilePath = '"' + configFolderPath + "\\\\" + (currentRec.Get(Napi::String::New(env, "cpath")).As<Napi::String>().Utf8Value() + '"');
        std::string cameraRotation = getCameraRotation(cameraFilePath, basepath, env);
        cout << endl << "Camera rotation value : " << cameraRotation << endl;
        std::string cameraRotationFilter = "";
        if(cameraRotation != "null") {
            cameraRotationFilter = ", rotate="+cameraRotation;
        }
        // cout << endl
        //      << "back image path for not both : : " << backImgPath << endl;
        bool fullScreenMode = currentRec.Get(Napi::String::New(env, "fullScreenMode")).ToBoolean();
        // std::string fsMode = currentRec.Get(Napi::String::New(env,"fullScreenMode")).As<Napi::String>().ToString();
        if (type.compare("camera") == 0)
        {
            // ! no need of these already declared above
            // std::string cwidth = currentRec.Get(Napi::String::New(env, "cwidth")).As<Napi::String>().ToString();
            // std::string cheight = currentRec.Get(Napi::String::New(env, "cheight")).As<Napi::String>().ToString();

            // TODO - get brightness values for camera
            std::string brightnessFilter = getBrightnessFilter(currentRec);
            cout << endl << "starting brightness work" << endl;

            cout << endl << "final Brightness filter : " << brightnessFilter;
            // TODO - get crop command for camera , regardless of transparency
            bool isCameraWindowCoordsPresent = currentRec.Has("cameraWindowCoords");
            std::string cameraCropFilter = "";
            if(isCameraWindowCoordsPresent == true) {
                std::string wccWidth = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
                std::string wccHeight = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
                std::string wccScreenX = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
                std::string wccScreenY = currentRec.Get(Napi::String::New(env, "cameraWindowCoords")).ToObject().Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();
                cameraCropFilter = "crop=" + wccWidth + ":" + wccHeight + ":" + wccScreenX + ":" + wccScreenY + ",";
            }

            if (transparencyCheck == true)
            {
                std::string colorHex = currentRec.Get(Napi::String::New(env, "colorHex")).As<Napi::String>().ToString();
                std::string transparencySimilarity = currentRec.Get(Napi::String::New(env, "transparencySimilarity")).As<Napi::String>().ToString();
                std::string transparencyOpacity = currentRec.Get(Napi::String::New(env, "transparencyOpacity")).As<Napi::String>().ToString();
                colorHex = ReplaceAll(colorHex, "#", "");
                // cout << endl
                //      << colorHex << "colorHex" << endl;
                // ! brightness filter must always be before transparency
                filter += "[in]"+ cameraCropFilter  + "scale=" + cwidth + ":" + cheight + brightnessFilter + ",chromakey=0x" + colorHex + ":" + transparencySimilarity + ":" + transparencyOpacity + cameraRotationFilter + "[zero];";
            }
            else
            {
                // ! brightness filter must always be before transparency
                filter += "[in]" + cameraCropFilter  + "scale=" + cwidth + ":" + cheight + brightnessFilter + cameraRotationFilter  + "[zero];";
            }
        }
        else
        {
            // screen and crop
            // ? old way
            // std::string wcWidth = currentRec.Get(Napi::String::New(env, "windowCoords")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
            // std::string wcHeight = currentRec.Get(Napi::String::New(env, "windowCoords")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
            // std::string wcScreenX = currentRec.Get(Napi::String::New(env, "windowCoords")).ToObject().Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
            // std::string wcScreenY = currentRec.Get(Napi::String::New(env, "windowCoords")).ToObject().Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();

            // filter += "[in]crop=" + wcWidth + ":" + wcHeight + ":" + wcScreenX + ":" + wcScreenY + ",scale=" + cwidth + ":" + cheight + "[zero];";

            // ? updated way after allowing camera crop
            bool isScreenWindowCoordsPresent = currentRec.Has("screenWindowCoords");
            if(isScreenWindowCoordsPresent) {
                std::string wcWidth = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
                std::string wcHeight = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
                std::string wcScreenX = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "screenX")).As<Napi::String>().ToString();
                std::string wcScreenY = currentRec.Get(Napi::String::New(env, "screenWindowCoords")).ToObject().Get(Napi::String::New(env, "screenY")).As<Napi::String>().ToString();

                filter += "[in]crop=" + wcWidth + ":" + wcHeight + ":" + wcScreenX + ":" + wcScreenY + ",scale=" + cwidth + ":" + cheight +"[zero];";
            } else {
                filter += "[in]scale=" + cwidth + ":" + cheight +"[zero];";
            }

        }
        std::string outputWidth = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "width")).As<Napi::String>().ToString();
        std::string outputHeight = config.Get(Napi::String::New(env, "outputRes")).ToObject().Get(Napi::String::New(env, "height")).As<Napi::String>().ToString();
        // std::string ctop = currentRec.Get(Napi::String::New(env,"ctop")).As<Napi::String>().ToString();
        // std::string cleft = currentRec.Get(Napi::String::New(env,"cleft")).As<Napi::String>().ToString();
        std::string paddingColor = config.Get(Napi::String::New(env, "paddingColor")).As<Napi::String>().ToString();
        if (backImgPath.compare("false") != 0)
        {
            filter += "[back]scale=" + outputWidth + ":" + outputHeight + "[back_scaled];";
            filter += "[back_scaled][zero]overlay=" + cleft + ":" + ctop;
        }
        else
        {
            filter += "[zero]pad=width=" + outputWidth +
                      ":height=" + outputHeight +
                      ":x=" + cleft +
                      ":y=" + ctop +
                      ":color=" + paddingColor;
        }
        if (isLogoPresent)
        {
            filter += "[over];[over][scaled_logo]overlay=" + logoLeft + ":" + logoTop;
        }
    }

    std::string fpsFilter = "";
    if(config.Has("outputFrameRate")) {
        std::string outputFrameRate = config.Get(Napi::String::New(env, "outputFrameRate")).As<Napi::String>().ToString();
        // std::cout << endl << "outputFrameRate: " <<  outputFrameRate << endl;
        if(outputFrameRate != "auto") {
            // ! we do not need fps in preview.. must not present otherwise it creates issue while seeking and overlaying videos in ffplay
            fpsFilter = "";
            // fpsFilter = ",fps=" + outputFrameRate;
        }
    }
    std::string scrollingTextFilter = "";
    if(currentRec.Has("scrollingTextObj")) {
        Napi::Object scrollingTextObj = currentRec.Get("scrollingTextObj").ToObject();
        std::string scrollingTextCheck = scrollingTextObj.Get(Napi::String::New(env, "scrollingTextCheck")).ToString();
        // cout << endl << "scrollingTextCheck" << scrollingTextCheck << endl;
        // cout << endl << "scrollingTextCheck command result " << (scrollingTextCheck == "true") << endl;
        // cout << endl << "scrollingTextCheck command result 2" << (scrollingTextCheck.compare("true") == 0) << endl;
        if (scrollingTextCheck.compare("true") == 0) {
            // cout << endl << "scrollingTextCheck turned out to be true"  << endl;

            scrollingTextFilter = getScrollingTextFilter(scrollingTextObj,currentRec, config, env, configFolderPath);
        }
    }

    filter += ",setsar=sar=1/1,setdar=dar=16/9" ;
    filter += fpsFilter;
    filter += scrollingTextFilter;
    // std::cout << endl
    //           << "Returning Filter : " << filter << endl;
    filterObj.Set("filter", filter);
    // filterObj.Set("cameraAudioTrimFilter", cameraAudioTrimFilter);

    // return filter;
    return filterObj;
}

std::string ReplaceAll(std::string str, const std::string &from, const std::string &to)
{
    size_t start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
    }
    return str;
}

Napi::Object Init(Napi::Env env, Napi::Object exports)
{
    exports.Set(Napi::String::New(env, "previewprocess"),
                Napi::Function::New(env, previewprocess));

    return exports;
}
NODE_API_MODULE(previewprocess, Init);
