const {desktopCapturer,ipcRenderer,remote}  = require("electron")
var recordingStarted = false;
const electron = require("electron");
    
/*Developer Version*/
    console.log('********** Screen Recorder window Developer Tools **********')
let win = require('electron').remote.getCurrentWindow()

    $("#dragger").mouseenter(function(){
        win.setIgnoreMouseEvents(false);  
    })
    $("#dragger").mouseleave(function(){
        win.setIgnoreMouseEvents(true, {forward: true})
    })
    $("#topContainer").mouseleave(function(){
        win.setIgnoreMouseEvents(false);  
    })
    $("#topContainer").mouseenter(function()
    {
        win.setIgnoreMouseEvents(true, {forward: true})
    })

window.addEventListener('resize',function (){
	
	windowCoords.screenX = window.screenX + 10;
	windowCoords.screenY = window.screenY + 10;
	windowCoords.width = window.innerWidth - 20;
	windowCoords.height = window.innerHeight - 30;
	windowCoords.outerWidth = window.outerWidth;
	windowCoords.outerHeight = window.outerHeight;
    
	ipcRenderer.send('recorder-resized',windowCoords);
	ipcRenderer.send('dragging-recorder');
	$("#topContainer").css('width',window.innerWidth-20);
	$("#topContainer").css('height',window.innerHeight-20);
});
ipcRenderer.on('hide-dragger',(event , data) =>
{
	$('#dragger').hide();
    $('body').css('border','none')
    console.log('dragger hidden')
    //$("#topContainer").off('mouseenter');
})
ipcRenderer.on('show-dragger',(event , data) =>
{
	$("#dragger").show();
    $('body').css('border','1px dashed crimson')
    console.log('dragger show!!!');
})
ipcRenderer.on('toggle-full-screen-mode',(event , fullScreenMode) =>
{
    if(fullScreenMode)
    {
        electron.remote.getCurrentWindow().hide()
    }
    else
    {
        electron.remote.getCurrentWindow().show()    
    }
})
 let wX;
    let wY;
    let dragging = false;
    $('#dragger').mousedown(function (e) {
        ipcRenderer.send('dragging-recorder', 'down')
        dragging = true;
        wX = e.pageX;
        wY = e.pageY;
        console.log('event triggered');
    });

    $(window).mousemove(function (e) {
        e.stopPropagation();
        e.preventDefault();
        if (dragging) {
            var xLoc = e.screenX - wX;
            var yLoc = e.screenY - wY;

            try {
            	// console.log(remote);
                remote.BrowserWindow.getFocusedWindow().setPosition(xLoc, yLoc);
            } catch (err) {
                console.log(err);
            }
        }
    });

    $('#dragger').mouseup(function () {
        dragging = false;
        console.log('have been dragging');
        checkWindowInViewport();
        ipcRenderer.send('dragging-recorder', 'up')
    });

    function checkWindowInViewport(){
        let currentWindow = electron.remote.getCurrentWindow();
        let screenW = electron.screen.getPrimaryDisplay().workAreaSize.width;
        let screenH = electron.screen.getPrimaryDisplay().workAreaSize.height;
        let flag = false;
        let bounds = currentWindow.getBounds();
        console.log(screenW,screenH,bounds);

        console.log('first : ',(bounds.width + bounds.x > screenW));
        console.log('second : ',(bounds.height + bounds.y > screenH));
        if(bounds.x < 0 || bounds.y < 0){
            flag = true;
        } 
        if(bounds.width + bounds.x > screenW || bounds.height + bounds.y > screenH){
           
            flag = true;
        }
        if(flag){
            currentWindow.setPosition(50,50);
        }
    }