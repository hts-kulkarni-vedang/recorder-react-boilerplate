// if(require('electron-squirrel-startup')) return;
const electron = require('electron');
const os = require('os');
const { dialog } = require("electron")
var environmentJs = require('./environment');
    const $ = require('jquery');
    console.log($);
const request = require('request');
const {desktopCapturer, ipcRenderer, ipcMain ,remote ,Menu, globalShortcut} = require('electron')
// Module to control application life.
const path = require('path')
const url = require('url');
const { app, BrowserWindow  } = require('electron')
var addon = require('bindings')('validation');
let pids = [];
var ps = require("process")
var fs            = require('fs')
var rimraf = require('rimraf');
var documentsFolderPath = os.homedir+'/Documents/HiFi_Recorder Files/';
var updateDownloaded = false;
const swal = require('sweetalert2')
const env = require('./environment');
let previewClose = false;
let isDownloading = false;
let mainWindow
let previewWindow=null
let shortcuts = {};
var showPreviewTriggered = false;

var sys = require('util')
var exec = require('child_process').exec;
const child_process = require("child_process");
var paramForNewWindow = null;

   
   
    app.commandLine.appendSwitch('disable-gpu');
    app.commandLine.appendArgument('disable-gpu');
    // DISABLE GPU electron
    app.disableHardwareAcceleration();

    function createWindow() {

        var  status = addon.filePresent(documentsFolderPath);
        var basepath = app.getAppPath('exe');
        console.log(basepath);
        basepath = basepath.replace("app.asar","")
        basepath+= "\\";
        console.log("status : ",status);
        if(status == "false"){
            console.log("status is false");
            createLicensingWindow();
        }
        else
        {
                require('dns').lookup('google.com',function(err) {
                if (err && err.code == "ENOTFOUND") {
                    
                    var isLocalValid = addon.validateLocal(documentsFolderPath);
                    console.log(isLocalValid);
                    if(isLocalValid.LocalValidation == true){
                        console.log("Locally validated!");
                        createRecorderWindow();
                    }else{
                        dialog.showMessageBox({
                            type : "error",
                            title: "Invalid License",
                            message : "We could not verify your license!"
                        });    
                        if(fs.existsSync(documentsFolderPath+'/sample.dat'))
                            fs.unlinkSync(documentsFolderPath+'/sample.dat');
                        createLicensingWindow();
                    }
                } else {
                    serverPath = env.serverPath;
                    newServerValidation();
                }
            });
        }
    }

    function createRecorderWindow () {    
        // Create the browser window.
        console.log("came to create recorder window");
        let dis = electron.screen.getPrimaryDisplay();
        mainWindow = new BrowserWindow({
            alwaysOnTop : false,
            frame : false,
            resizable : false
        });
        mainWindow.setFullScreen(false);

        console.log("showing main window");
        mainWindow.on('show',function(){
           console.log("mainWindow has been shown");
            mainWindow.setSize(800, 600);
        });
        mainWindow.loadURL(url.format({
                pathname: path.join(__dirname, 'index.html'),
                protocol: 'file:',
                slashes: true
            }));
        mainWindow.setSize(800,600);

        mainWindow.on('restore',function(){      
            mainWindow.webContents.send('pause-both-recorders');
        });
            
        mainWindow.on('closed', function () {
            app.quit();
        });
     }
    app.on('ready', ()=>{
        setTimeout(()=>{
            const gotTheLock = app.requestSingleInstanceLock();
            if(gotTheLock){
                registerShortcuts();
                createWindow();    
            }else{
                electron.dialog.showMessageBox({
                    "type" : "error",
                    "message": "Another instance of HiFi Recorder is already running , Please close other instances"
                });   
            }    
        },500);
    });

    app.on('window-all-closed', function () {

      if (process.platform !== 'darwin') {
        app.quit()
      }
    })

    app.on('activate', function () {
      if (mainWindow === null) {
        createWindow()
      }
    })

    function registerShortcuts()
    {
        let playPause,showHideScreen,stopRec;
        let start=49,end=57;
        for(let i=start;i<=end;i++)
        {
            const ret = globalShortcut.register('CommandOrControl+'+String.fromCharCode(i),() => {
                mainWindow.webContents.send('shortcut-ev','play-pause'); 
            });
            if (!ret) 
            {
                
                continue;
            }
            else
            {
                playPause = String.fromCharCode(i);
                start = i;
                break;
            }
        }  
        for(let i=start+1;i<=end;i++)
        {
            const ret = globalShortcut.register('CommandOrControl+'+String.fromCharCode(i),() => {
                showPreviewTriggered = true;
                mainWindow.webContents.send('shortcut-ev','show-hide');        
            })
            if (!ret) 
            {
                
                continue;
            }
            else
            {
                showHideScreen = String.fromCharCode(i);
                start = i;
                break;
            }
        }
        for(let i=start+1;i<=end;i++)
        {
            const ret = globalShortcut.register('CommandOrControl+'+String.fromCharCode(i),() => {
                mainWindow.webContents.send('shortcut-ev','stop');
                
            })
            if (!ret) 
            {
                
                continue;
            }
            else
            {
                stopRec = String.fromCharCode(i);
                start = i;
                break;
            }
        }   
        
        shortcuts = {playPauseS : playPause,showHideScreenS : showHideScreen,stopRecS : stopRec};
    }

    ipcMain.on('stop-rec', (event, sourceId) => {
        if(previewWindow!=null && previewWindow!=undefined){
            previewWindow.webContents.send('reset-timer')
            closePreviewWindow()
        }
    })

    ipcMain.on('close-app',(event,sourceId) => {
        app.quit();
    });
    ipcMain.on('minimize-app',(event,param) => {
        mainWindow.minimize();
    });
    function closePreviewWindow(params) {
        if(previewWindow!=null)
            previewWindow.close()
    }
    ipcMain.on('close-camera-preview',(event,param)=>{
        closePreviewWindow()
    })
    ipcMain.on('open-camera-preview',(event , param) => {
        
        if(param.deviceChanged && previewWindow==null){
            // add swal or remove this
        }
        else if((param.deviceChanged && previewWindow!=null)||(!param.deviceChanged && previewWindow == null)||(param.deviceChanged==undefined && previewWindow!=null)){
            
            if(previewWindow != null && previewWindow != undefined){
                previewWindow.close()
            }
            previewWindow = new BrowserWindow({
                frame : false,
                transparent : false,
                alwaysOnTop : false,
                show : false,
                width : 240,
                height : 180,
                maxWidth : 1600,
                maxHeight : 1600,
                thickFrame : true,
                titleBarStyle : 'hidden',
                resizable : true,
            })
            previewWindow.setPosition(electron.screen.getPrimaryDisplay().size.width-510,50);
            previewWindow.loadURL(url.format({
                pathname : path.join(__dirname,'./preview-camera-window/preview.html'),
                protocol : 'file:',
                slashes : true
            }))
            previewWindow.on('show',function(){
                if(showPreviewTriggered){
                    param.showAfterOpen = true;
                }
                console.log("param for preview :",param);

                previewWindow.webContents.send('open-camera-preview',param);
                param.reload = false;
                // param = {};
            
                mainWindow.webContents.send('preview-ready',true);
                showPreviewTriggered = false;
            })
            previewWindow.on('close',function (){
                mainWindow.webContents.send('preview-ready',false);
                mainWindow.webContents.send('preview-window-closed');
                previewClose = true;
                previewWindow = null;
            });
            previewWindow.on('restore',function (){
                previewClose = false;
            });
            previewWindow.on('ready-to-show',()=>{
                previewWindow.show();
            })
        }
    })
    ipcMain.on('hide-camera-preview',(event , param) => {
        if(previewWindow!=null)
            previewWindow.hide()
    })
    ipcMain.on('minimize-camera-preview',(event , param) => {
        console.log('came to main js minimize camera preview')
        if(previewWindow!=null || previewWindow!=undefined)
            previewWindow.hide()
    })
    ipcMain.on('maximize-camera-preview',(event , param) => {
        console.log('came to main js maximize camera preview');
        if(previewWindow!=null || previewWindow!=undefined)
            previewWindow.show()
    })


    ipcMain.on('preview',(event , param) =>{
        mainWindow.webContents.send('preview',param);
    });
    ipcMain.on('started-rec-preview',(event , param) =>{
      /*Object has been destroyed error when closed preview window and then starting recording*/
        if(previewWindow!=null)
        previewWindow.webContents.send('started-rec-preview',param);
    });
    ipcMain.on('resumed-rec-preview',(event , param) =>{
        if(previewWindow!=null)
        previewWindow.webContents.send('resumed-rec-preview',param);
    });
    ipcMain.on('paused-rec-preview',(event , param) =>{
        if(previewWindow!=null)
        previewWindow.webContents.send('paused-rec-preview',param);
        
    });

    ipcMain.on('get-shortcuts',(event,param) => {
        mainWindow.webContents.send('shortcuts',shortcuts);
    })
    ipcMain.on('timer',(event,param) => {
        if(previewWindow != null){
            previewWindow.webContents.send('timer',param)
        }
    })
    ipcMain.on('shortcut-ev',(event,param) => {
        mainWindow.webContents.send('shortcut-ev',param);
    });
    ipcMain.on('update-current-time',(event,param)=>{
        mainWindow.webContents.send('update-current-time',param);
    })
    ipcMain.on('update-audio-source',(event,param)=>{
        previewWindow.webContents.send('update-audio-source',param);
    });
    ipcMain.on('load_Recorder',(event,param)=>{
        mainWindow.loadFile('index.html');
        // mainWindow.maximize()
        console.log('Verified')
    });
    function createLicensingWindow() { 
        mainWindow = new BrowserWindow({
                width: 400,
                height: 400,
                frame: false,
                resizable : false
        });
        mainWindow.loadFile('validation.html')
        mainWindow.on('closed', function () {
            mainWindow = null
        });

    }
    ipcMain.on('backSettings',(event,param)=>{
        mainWindow.loadURL(url.format({
            pathname : path.join(__dirname,'index.html'),
            protocol : 'file:',
            slashes : true
        }));
        mainWindow.setFullScreen(false);
        mainWindow.setSize(800, 600);

        let bounds = electron.screen.getPrimaryDisplay().bounds;
        let x = bounds.x + ((bounds.width - 800) / 2);
        let y = bounds.y + ((bounds.height - 600) / 2);

        mainWindow.setPosition(x,y);
    });
    function showDialogWithErrorCode(responseObj){
        let text = ""; let flag=1;
        console.log(responseObj);
        if(responseObj.error == environmentJs.errorCodes.KEY_NOT_FOUND){
            text = "Key Not Found";
        }
        if(responseObj.error == environmentJs.errorCodes.LICENSE_NOT_FOUND){
            text = "License Not Found";
        }
        if(responseObj.error == environmentJs.errorCodes.LICENSE_EXPIRED){
            text = "License Expired";
        }
        if(responseObj.error == environmentJs.errorCodes.LICENSE_CANCELLED){
            text = "License Cancelled";
        }
        if(responseObj.error == environmentJs.errorCodes.KEY_ALREADY_USED){
            text = "Key Already Used";
        }
        if(responseObj.error == environmentJs.errorCodes.PC_REGISTERED_WITH_DIFFERENT_USER){
            text = "This PC was registered with different user!,Please contact admin";
        }
        if(responseObj.error == environmentJs.errorCodes.LICENSE_VALID){
            flag=0;
            createRecorderWindow();

        }
        if(flag == 1){
            dialog.showMessageBox({
                type : "error",
                message : text
            });
            if(fs.existsSync(documentsFolderPath+'/sample.dat'))
                fs.unlinkSync(documentsFolderPath+'/sample.dat');
            // app.quit();
            createLicensingWindow();
        }
    }

    function toJSONString(str){
        console.log("came to stringify",str);
        let jsonString = "";
        let start = str.indexOf("{");
        let end = str.lastIndexOf("}");
        console.log('start index : ',start);
        console.log('end index : ',end);
        jsonString = str.substr(start,(end-start + 1));
        console.log("returning string : ",jsonString);
        return jsonString;
    }
    function newServerValidation(){
       getSerialNumber().then((serialNumber)=>{
           serialNumber = serialNumber.replace(/^\s+|\s+$/g, '');
           let indexOfNewLine = serialNumber.indexOf("\n");
           serialNumber = serialNumber.substr(indexOfNewLine + 1,serialNumber.length - indexOfNewLine +1);
           console.log("response from getSerialNumber : ",serialNumber);
            // var valid = addon.startUp(basepath, serverPath,documentsFolderPath,status);
            var formData = {
                serialNumber : serialNumber
            };
            request.post({url:env.serverPath+"recorder-validation.php", formData: formData}, function optionalCallback(err, httpResponse, body) {
                if (err) {
                    console.log('upload failed:', err);
                }
                showDialogWithErrorCode(body);

                console.log(body);
            });
       });
    }
    async function getSerialNumber(){
        // var result;
        return new Promise((resolve,reject)=>{
            var result = "";
            var process = child_process.spawn("wmic",["bios","get","SerialNumber"]);
            process.stdout.on("data",(data)=>{
                resolve(data.toString());
            });
            process.stderr.on("data",(data)=>{
                resolve(data.toString());
            });
            process.on("close",(code)=>{
                console.log("process completed with code : ",code," result is : ",result);
                // resolve(result);
            });    
        });
    }