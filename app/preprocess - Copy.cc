#include<iostream>
#include <direct.h>
#include<string>
#include <array>
#include<node.h>
#include<windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include <napi.h>
#include "commandFunctions.h"
#include <psapi.h>
#include <errno.h>

// #pragma comment(lib, "User32.lib")
HANDLE hChildProcess; //NULL
HANDLE hStdIn; // Handle to parents std input. NULL
BOOL bRunThread; // TRUE
int noOfCommands=0; 
int commandCounter = 0;
std::string (*strCommands);
//durations array maps with strCommands in such a way that duration for strCommands[i] th command is duration[i]
int *durations = NULL;
int screen_count=1;
int camera_count=1;

HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
HANDLE hInputWriteTmp,hInputRead,hInputWrite;
HANDLE hErrorWrite;
HANDLE hThread;
DWORD ThreadId;

int RunCommand2(std::string cmdx,int remainingCount,Napi::Function cb,Napi::Env env,int noOfCommands,int currentCount,int durationx){

    int duration = durationx;
    LPTSTR cmd = const_cast<char *>((cmdx).c_str());

    BOOL ok = TRUE;
    HANDLE hStdInPipeRead = NULL;
    HANDLE hStdInPipeWrite = NULL;
    HANDLE hStdOutPipeRead = NULL;
    HANDLE hStdOutPipeWrite = NULL;

    // Create two pipes.
    SECURITY_ATTRIBUTES sa = { sizeof(SECURITY_ATTRIBUTES), NULL, TRUE };
    ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
    if (ok == FALSE) return -1;
    ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
    if (ok == FALSE) return -1;

    // Create the process.
    STARTUPINFO si = { };
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdError = hStdOutPipeWrite;
    si.hStdOutput = hStdOutPipeWrite;
    si.hStdInput = hStdInPipeRead;
    PROCESS_INFORMATION pi = { };
    LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
    LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
    LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
    LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
    BOOL bInheritHandles = TRUE;
    DWORD dwCreationFlags = 0;
    LPVOID lpEnvironment = NULL;
    LPCWSTR lpCurrentDirectory = NULL;
    // ok = CreateProcess(
    //     lpApplicationName,
    //     lpCommandLine,
    //     lpProcessAttributes,
    //     lpThreadAttribute,
    //     bInheritHandles,
    //     dwCreationFlags,
    //     lpEnvironment,
    //     lpCurrentDirectory,
    //     &si,
    //     &pi);
    ok =CreateProcess(NULL,cmd,NULL,NULL,TRUE,CREATE_NO_WINDOW,NULL,NULL,&si,&pi);
    if (ok == FALSE) return -1;

    // Close pipes we do not need.
    CloseHandle(hStdOutPipeWrite);
    CloseHandle(hStdInPipeRead);

    // The main loop for reading output from the DIR command.
    char buf[1024 + 1] = { };
    DWORD dwRead = 0;
    DWORD dwAvail = 0;
    ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    while (ok == TRUE)
    {
        buf[dwRead] = '\0';
        OutputDebugStringA(buf);
        puts(buf);

        Napi::Object obj=Napi::Object::New(env);
        cout << "duration of current file : "<<duration<<endl;
        obj.Set("currentCount",(currentCount + 1));
        obj.Set("remainingCount",(remainingCount));
        obj.Set("totalCount",(noOfCommands));
        obj.Set("duration",(duration));
        obj.Set("output",buf);
        cb.Call(env.Global(),{obj});

        ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    }

    // Clean up and exit.
    CloseHandle(hStdOutPipeRead);
    CloseHandle(hStdInPipeWrite);
    DWORD dwExitCode = 0;
    GetExitCodeProcess(pi.hProcess, &dwExitCode);
    return 0;
}

// std::string getKey(std::string keyx,std::string type,Napi::Object currentObj,std::string keyFor){
std::string getKey(Napi::Env env,std::string keyx,std::string type,Napi::Object currentObject,std::string keyFor){
    Napi::Array keys = currentObject.GetPropertyNames();
    std::string tmpKey;
    for(int i=0;i<keys.Length();i++){
        std::string key = keys.Get(i).ToString();
        // if type = camera & key = cwidth & keyx = width
        if(keyFor.compare("camera") == 0 ){
            tmpKey = "c"+keyx;
        }else{
            tmpKey = "s"+keyx;
        }
        if (key.compare(tmpKey) == 0 && keyx.compare("width") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("height") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("left") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("top") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
    }
}
std::string getFilterComplex(Napi::Env env,Napi::Object currentObj,Napi::Object configObj,std::string subtype){
    std::string filter = "";
    std::string type = currentObj.Get(Napi::String::New(env,"type")).As<Napi::String>().ToString();
    Napi::Object outputRes =  configObj.Get(Napi::String::New(env,"outputRes")).ToObject();
    // Napi::Object settings=configObj.Get(Napi::String::New(env,"settings")).ToObject();
    std::string outputW = outputRes.Get(Napi::String::New(env,"width")).As<Napi::String>().ToString();
    std::string outputH = outputRes.Get(Napi::String::New(env,"height")).As<Napi::String>().ToString();

    // std::string outputW = "1280";
    // std::string outputH = "720";

    // 0. crop if screen or both
    if(type.compare("screen") == 0 || (type.compare("both") == 0 && subtype.compare("screen") == 0)){
        bool fullScreenMode = currentObj.Get(Napi::String::New(env,"fullScreenMode")).ToBoolean();
        if(fullScreenMode == true){
            // no need to crop
        }else{

            Napi::Object cropCoords =  currentObj.Get(Napi::String::New(env,"windowCoords")).ToObject();
            std::string width = cropCoords.Get(Napi::String::New(env,"width")).As<Napi::String>().ToString();
            std::string height = cropCoords.Get(Napi::String::New(env,"height")).As<Napi::String>().ToString();
            std::string screenX = cropCoords.Get(Napi::String::New(env,"screenX")).As<Napi::String>().ToString();
            std::string screenY = cropCoords.Get(Napi::String::New(env,"screenY")).As<Napi::String>().ToString();
            filter += "crop="+width+":"+height+":"+screenX+":"+screenY+",";
        }
    }
//     // 1. scale with 
    std::string width = getKey(env,"width",type,currentObj,subtype);
    std::string height = getKey(env,"height",type,currentObj,subtype);
    std::string left = getKey(env,"left",type,currentObj,subtype);
    std::string top = getKey(env,"top",type,currentObj,subtype);
    filter += "scale="+width+":"+height;

    // 2. pad or add back image

    std::string backImgPath = configObj.Get(Napi::String::New(env,"backImgPath")).As<Napi::String>().ToString();
    if(backImgPath.compare("false") == 0){
        // if backimagenot set
        if(!(type.compare("both") == 0 && subtype.compare("camera") == 0)){
            filter += ",pad=width="+outputW+":height="+outputH+":x="+left+":y="+top+":color=white";
        }
    }else {
        // backimage set
        // in both we add back image only to screen
        if(!(type.compare("both") == 0 && subtype.compare("camera") == 0)){
            filter += "[in];[1]scale="+outputW+":"+outputH+"[back];[back][in]overlay="+left+":"+top;
        }
    }
    
    filter += ",setsar=sar=1/1,setdar=dar=16/9";
    cout << endl << "returning filterComplex : " << filter <<endl;
    return filter;
}
std::vector <std::string> getArgs(Napi::Env env,Napi::Object currentObj,Napi::Object configObj,int i,std::string subtype,int finalMergerLength){
    std::vector <std::string> cmd;

    Napi::Object settings=configObj.Get(Napi::String::New(env,"settings")).ToObject();
    cmd.push_back("ffmpeg");
    std::string type = currentObj.Get(Napi::String::New(env,"type")).As<Napi::String>().ToString();
    std::string configFolderPath = configObj.Get(Napi::String::New(env,"configFolderPath")).As<Napi::String>().ToString();
    // source
    std::string tmpPathKey = "";
    if(subtype.compare("camera") == 0){
        tmpPathKey = "cpath";
    }else{
        tmpPathKey = "spath";
    }
    cout << "subtype" << subtype << " tmpPathKey" << tmpPathKey;
    std::string cpath = currentObj.Get(Napi::String::New(env,tmpPathKey)).As<Napi::String>().ToString();
    cout << "configFolderPath : "<<configFolderPath << " cpath : "<<cpath;
    Napi::Array keys = currentObj.GetPropertyNames();
    for(int i=0;i<keys.Length();i++){
        std::string key = keys.Get(i).ToString();
        std::string value = currentObj.Get(keys.Get(i)).ToString();
        cout << key << " => "<<value<<endl;
    }
    cmd.push_back("-i"); cmd.push_back('"'+configFolderPath+"\\"+cpath+'"');

    std::string backImgPath = configObj.Get(Napi::String::New(env,"backImgPath")).As<Napi::String>().ToString();
    if(backImgPath.compare("false") == 0){
        // if backimage is not set
    }else{
        // if backimage is set
        // in both add back image only to screen
        if(!(type.compare("both") == 0 && subtype.compare("camera") == 0)){
            cmd.push_back("-i"); cmd.push_back('"'+backImgPath+'"');
        }
    }
    std::string filterComplex = getFilterComplex(env,currentObj,configObj,subtype);
    cmd.push_back("-filter_complex"); cmd.push_back(filterComplex); 
    // std::string ouputCameraRecording=configFolderPath+"intermediate/camera_";
    std::string folderForOutput = "\\intermediate\\";
    cout << endl << "***********************" <<endl;
    cout << "finalMergerLength : " << finalMergerLength; 
    cout << "type : " << type; 
    cout << endl << "***********************" <<endl;
    if(finalMergerLength == 1 && ((type.compare("camera") == 0) || (type.compare("screen")) == 0)){
        folderForOutput = "\\output\\";
        cout << endl << "***********************" <<endl;
        cout << "settings output as folder for output";
        cout << endl << "***********************" <<endl;

    }
    std::string out = configFolderPath+folderForOutput+subtype+"_"+std::to_string(i)+".mp4";
    std::string preset = configObj.Get(Napi::String::New(env,"preset")).As<Napi::String>().ToString();
    std::string crf = configObj.Get(Napi::String::New(env,"crf")).As<Napi::String>().ToString();
    
    cmd.push_back("-preset");
    cmd.push_back(preset);
    cmd.push_back("-crf");
    cmd.push_back(crf);
    cmd.push_back("-r");
    cmd.push_back("30");
    // cmd.push_back("-preset"); cmd.push_back(settings.Get(Napi::String::New(env,"preset")).As<Napi::String>().Utf8Value()); 
    // cmd.push_back("-crf"); cmd.push_back(settings.Get(Napi::String::New(env,"crf")).As<Napi::String>().ToString());

    cmd.push_back("-y");cmd.push_back('"'+out+'"');

    return cmd;
}
// std::vector <std::string> getScreenArgs(Napi::Env env,Napi::Object currentObj){
std::vector <std::string> getScreenArgs(Napi::Object currentObj){
    std::vector <std::string> cmd;
    cmd.push_back("ffmpeg");
    return cmd;
}
void runtimeExec(const char* cmd,Napi::Function cb,Napi::Env env,int commandIndex) {
    cout << "came to runtimeExec";
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
        cout <<"result of command:"<< result;
        Napi::Object res= Napi::Object::New(env);
        res.Set("commandIndex",commandIndex);
        res.Set("commandOutput",buffer.data());
        cb.Call(env.Global(),{res});

    }
}
Napi::Object preprocess(const Napi::CallbackInfo& info) {
          
    const Napi::Env env = info.Env();
    // //creating intermidiate directory here
    int check;
    auto configObj = info[0].ToObject();//consists the information about recorded files
    int id=info[3].As<Napi::Number>().Int32Value();
    std::string configFolderPath = info[1].As<Napi::String>().ToString();//path to the folder wich contains recorded files
    std::string ouputCameraRecording=configFolderPath+"intermediate/camera_";
    std::string outputScreenRecording=configFolderPath+"intermediate/screen_";
    std::string outPutfileName=configFolderPath+"errorLog/preprocessStdout.txt";
    Napi::Object outputRes =  configObj.Get(Napi::String::New(env,"outputRes")).ToObject();
    Napi::Object settings=configObj.Get(Napi::String::New(env,"settings")).ToObject();
    outputRes.Get(Napi::String::New(env,"width"))=settings.Get(Napi::String::New(env,"width")).As<Napi::String>().ToString();
    outputRes.Get(Napi::String::New(env,"height"))=settings.Get(Napi::String::New(env,"height")).As<Napi::String>().ToString();

    cout << endl << "only object conversion" << endl;
    // return outputRes;

    setId_fileName(id,outPutfileName);
    check=CreateDirectory((configFolderPath+"errorLog").c_str(),NULL);
    if(!check){
        if (GetLastError() == ERROR_ALREADY_EXISTS) {
            // directory already exists
            check=1;
        } 
    }
    if (check){                            
        check=CreateDirectory((configFolderPath+"/intermediate").c_str(),NULL);
        if(!check){
                if (GetLastError() == ERROR_ALREADY_EXISTS) {
                // directory already exists
                check=1;
            } 
        }
        if(check){  //  std::cout<<"Directory created\n"; 
            

            Napi::Function cb = info[2].As<Napi::Function>();
            cout << "error is not with cb";
            Napi::HandleScope scope(env);
            
            std::vector <std::string> _args_camera;
            std::vector <std::string> _args_screen;

            Napi::Array finalMerger=configObj.Get(Napi::String::New(env,"finalMerger")).As<Napi::Array>();//finalMerger array is inside configobj and contians object for every recorded file 
     
            for (unsigned int i = 0; i < finalMerger.Length(); i++) {
                Napi::Object currentObj = (finalMerger.Get(i)).ToObject();
                std::string type = currentObj.Get((Napi::String::New(env,"type"))).As<Napi::String>().ToString();//type of recorded file eg camera,screen,both
                if(type.compare("both") == 0){
                    noOfCommands = noOfCommands +2; //type both contains two recorded files 1 screen and 1 camera   
                }
                else{
                    noOfCommands = noOfCommands +1;
                }
            }
            strCommands = new std::string[noOfCommands];
            durations = new int[noOfCommands];
            for (unsigned int i = 0; i < finalMerger.Length(); i++){
                _args_camera.clear();
                _args_screen.clear();
                Napi::Object intermidiate= Napi::Object::New(env);

                Napi::Object currentObj = (finalMerger.Get(i)).ToObject();
                std::string type = currentObj.Get(Napi::String::New(env,"type")).As<Napi::String>().ToString();
                int finalMergerLength = finalMerger.Length();
                _args_camera = getArgs(env,currentObj,configObj,i,"camera",finalMergerLength);
                int startTime = currentObj.Get(Napi::String::New(env,"startTime")).As<Napi::Number>().Int32Value();
                int endTime = currentObj.Get(Napi::String::New(env,"endTime")).As<Napi::Number>().Int32Value();
                
                cout << endl << "startTime : " << startTime << endl;
                cout << endl << "endTime : " << endTime << endl;

                int duration = (endTime - startTime);
                cout << endl << "duration : " << duration << endl;

                if(type.compare("both") == 0){
                    _args_screen = getArgs(env,currentObj,configObj,i,"screen",finalMergerLength);
                    durations[commandCounter] = duration;
                    strCommands[commandCounter++]= createCommand(_args_screen);
                    int last = _args_screen.size();
                    std::string lastPath = _args_screen[last - 1];
                    intermidiate.Set(Napi::String::New(env,"spath"), lastPath);
                }
                durations[commandCounter] = duration;
                strCommands[commandCounter++]= createCommand(_args_camera);

                // attatch outputs to intermidiate object
                int last = _args_camera.size();
                std::string lastPath = _args_camera[last - 1];
                intermidiate.Set(Napi::String::New(env,"cpath"), lastPath);
                (((configObj.Get(Napi::String::New(env,"finalMerger")).ToObject()).Get(i)).ToObject()).Set(Napi::String::New(env,"intermidiate"),intermidiate);                
            }  

            std::cout<<"\nCommands in preprocess :\n";
            int res;
            cout << "Durations array : "<< endl;
            for(int i=0;i< noOfCommands;i++){
                cout<< "duration of ith : " << durations[i] <<endl;
                cout << endl << strCommands[i] <<endl;
            }
            for(int i=0;i< noOfCommands;i++){
                // runtimeExec(strCommands[i].c_str(),cb,env,i);
                std::cout <<"**\n** Command no : **"<<i<<"**\n Command : **" << strCommands[i]<<"**\n";        
                res = RunCommand2(strCommands[i],(noOfCommands - i),cb,env,noOfCommands,i,durations[i]);
            }
            
            Napi::Value str=Napi::String:: New(env,"preprocess-completedd\n");
            cb.Call(env.Global(),{str});
            //  Napi::Object::Delete(str);
            Napi::Object obj=Napi::Object::New(env);
            obj.Delete(str);
            // cb.Call(env.Global(),{str});
            //  str.free();
            std::cout<<"preprocess-complete\n";
            strCommands=NULL;
            env.Null();

            return configObj;
        }
        else {
            std::cout<<"Unable to create directory\n"; 
            DisplayError("DirectoryProblem");
        }        
    }
    else{  
        std::cout<<"Unable to create directory\n"; 
        DisplayError("DirectoryProblem");
    }
}
Napi::Object Init(Napi::Env env, Napi::Object exports) {
  exports.Set(Napi::String::New(env, "preprocess"),
              Napi::Function::New(env, preprocess));
  return exports;
}
NODE_API_MODULE(preprocess, Init);


