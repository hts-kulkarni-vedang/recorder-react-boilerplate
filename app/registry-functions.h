#include <napi.h>
#include <windows.h>
void writeRegistry(){
	HKEY hKey;
	LPCTSTR sk = TEXT(mainKey.c_str());

	LONG openRes = RegOpenKeyEx(HKEY_CURRENT_USER, sk, 0, KEY_ALL_ACCESS , &hKey);

	if (openRes==ERROR_SUCCESS) {
	    printf("Success opening key.");
	} else {
	    printf("Error opening key.");
	}

	LPCTSTR value = TEXT(subKey.c_str());
	LPCTSTR data = TEXT("11111111111");

	LONG setRes = RegSetValueEx(hKey, value, 0, REG_SZ, (LPBYTE)data, _tcslen(data) * sizeof(TCHAR));		

	if (setRes == ERROR_SUCCESS) {
	    printf("Success writing to Registry.");
	} else {
	    printf("Error writing to Registry.");
	}
	cout << setRes << endl;

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS) {
	    printf("Success closing key.");
	} else {
	    printf("Error closing key.");
	}
}