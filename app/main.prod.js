const electron = require('electron');
const os = require('os');
const { dialog } = require("electron")
var environmentJs = require('./environment');
const { ipcMain, globalShortcut } = require('electron')
const path = require('path')
const url = require('url');
const { app, BrowserWindow } = require('electron')
var addon = require('bindings')('validation');
var fs = require('fs')
var documentsFolderPath = os.homedir + '/Documents/HiFi_Recorder Files/';
const env = require('./environment');
let mainWindow = null;
let starterWindow = null;
let shortcuts = {};
const log = require('electron-log');
const { autoUpdater } = require("electron-updater");
autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');
var cmd = process.argv[1];
if (cmd == '--squirrel-firstrun') {
    // Running for the first time.
}
function createWindow() {
    var basepath = app.getAppPath('exe');
    basepath = basepath.replace("app.asar", "")
    basepath += "\\";
    var appDataPath = app.getPath('userData');
    require('dns').lookup('google.com', function (err) {
        if (err && err.code == "ENOTFOUND") {
            validateOfflineHelper(documentsFolderPath, basepath, appDataPath);
        } else {
            serverPath = env.serverPath;
            // ! NEED CHECK -  this function was set to be removed;
            // ! TODO - check what happens when you comment this function
            // ! before validating auto insert license data locally
            // let resetResult = addon.resetLocalLicenseData(basepath);
            var valid = addon.validateOnline(basepath, serverPath, appDataPath);
            // NOTE -> handle response when http response is not correct
            // ? in such case key 'error is set in ServerValidation.ServerPhpResponse'
            let shouldValidateLocally = false;
            // dialog.showMessageBox({
            //     type : "error",
            //     message : "Response from server : "+valid.ServerValidation.ServerPhpResponse
            // });
            // console.log('valid.ServerValidation.ServerPhpResponse: ', valid.ServerValidation.ServerPhpResponse);
            if (valid.ServerValidation && valid.ServerValidation.ServerPhpResponse) {
                valid.ServerValidation.ServerPhpResponse = cleanString(valid.ServerValidation.ServerPhpResponse);
                if (IsJsonString(cleanString(valid.ServerValidation.ServerPhpResponse))) {
                    const errorResponse = JSON.parse(cleanString(valid.ServerValidation.ServerPhpResponse));
                    const error = errorResponse.error;
                    if (error === 'timeout-error' || error === 'http-request-error') {
                        shouldValidateLocally = true;
                    }
                }else {
                    console.log('not a json string');
                }
            }
            if (shouldValidateLocally === true) {
                validateOfflineHelper(documentsFolderPath, basepath, appDataPath);
            } else {
                showDialogWithErrorCode(valid);
            }
        }
    });
}
function createRecorderWindow() {
    // Create the browser window.
    // 
    // let dis = electron.screen.getPrimaryDisplay();
    mainWindow = new BrowserWindow({
        alwaysOnTop: false,
        frame: false,
        resizable: false,
        webPreferences: {
            nodeIntegrationInWorker: true
        }
    });
    mainWindow.setFullScreen(false);
    mainWindow.on('show', function () {
        mainWindow.setSize(800, 600);
    });
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.setSize(800, 600);
    mainWindow.on('restore', function () {
        mainWindow.webContents.send('pause-both-recorders');
    });
    mainWindow.on('closed', function () {
        app.quit();
    });
}
app.on('ready', () => {
    setTimeout(() => {
        const gotTheLock = app.requestSingleInstanceLock();
        if (gotTheLock) {
            registerShortcuts();
            createWindow();
            createStarterWindow();
        } else {
            electron.dialog.showMessageBox({
                "type": "error",
                "message": "Another instance of HiFi Recorder is already running , Please close other instances"
            });
        }
    }, 500);
});
app.on('window-all-closed', function () {
    if (process.platform !== 'darwin') {
        app.quit();
    }
})
app.on('activate', function () {
    if (mainWindow === null) {
        createWindow()
    }
})
function registerShortcuts() {
    let playPause, showHideScreen, stopRec;
    let start = 49, end = 57;
    for (let i = start; i <= end; i++) {
        const ret = globalShortcut.register('CommandOrControl+' + String.fromCharCode(i), () => {
            let isStarterWindowVisible = false;
            if (starterWindow) {
                if (starterWindow.isVisible()) {
                    isStarterWindowVisible = true;
                }
            }
            if (!isStarterWindowVisible) {
                mainWindow.webContents.send('shortcut-ev', 'play-pause');
            }
        });
        if (!ret) {
            continue;
        }
        else {
            playPause = String.fromCharCode(i);
            start = i;
            break;
        }
    }
    for (let i = start + 1; i <= end; i++) {
        const ret = globalShortcut.register('CommandOrControl+' + String.fromCharCode(i), () => {
            showPreviewTriggered = true;
            mainWindow.webContents.send('shortcut-ev', 'show-hide');
        })
        if (!ret) {
            continue;
        }
        else {
            showHideScreen = String.fromCharCode(i);
            start = i;
            break;
        }
    }
    for (let i = start + 1; i <= end; i++) {
        const ret = globalShortcut.register('CommandOrControl+' + String.fromCharCode(i), () => {
            let isStarterWindowVisible = false;
            if (starterWindow) {
                if (starterWindow.isVisible()) {
                    isStarterWindowVisible = true;
                }
            }
            if (!isStarterWindowVisible) {
                mainWindow.webContents.send('shortcut-ev', 'stop');
            }
        })
        if (!ret) {
            continue;
        }
        else {
            stopRec = String.fromCharCode(i);
            start = i;
            break;
        }
    }
    shortcuts = { playPauseS: playPause, showHideScreenS: showHideScreen, stopRecS: stopRec };
}
ipcMain.on('close-app', (event, sourceId) => {
    app.quit();
});
ipcMain.on('minimize-app', (event, param) => {
    mainWindow.minimize();
});
ipcMain.on('request-auto-updater-status', (event, param) => {
    mainWindow.webContents.send("request-auto-updater-status", autoUpdaterStatus);
});
ipcMain.on('close-camera-preview', (event, param) => {
    closePreviewWindow()
})
ipcMain.on('get-shortcuts', (event, param) => {
    mainWindow.webContents.send('shortcuts', shortcuts);
})
ipcMain.on('shortcut-ev', (event, param) => {
    mainWindow.webContents.send('shortcut-ev', param);
});
ipcMain.on('update-audio-source', (event, param) => {
    previewWindow.webContents.send('update-audio-source', param);
});
ipcMain.on('load_Recorder', (event, param) => {
    mainWindow.loadFile('index.html');
});
ipcMain.on('show-template-list', (event, param) => {
    mainWindow.loadFile('template-list-window/template-list-window.html');
});
ipcMain.on('open-recorder-settings', (event, param) => {
    console.log('came to open recorder settings');
    mainWindow.loadFile('settings/settings.html');
});
ipcMain.on('starter-window-open-close', (event, param) => {
    if (param == "open") {
        if (starterWindow) {
            starterWindow.show();
            starterWindow.webContents.send("start-timer");
        }
    } else {
        if (starterWindow) {
            mainWindow.webContents.send("starter-window-open-close", "close");
            starterWindow.webContents.send("end-timer");
            starterWindow.hide();
        }
    }
});
ipcMain.on('set-taskbar-progress', (event, param) => {
    // param is object with mode and progress keys
    if (param.mode === 'indeterminate') {
        mainWindow.setProgressBar(1);
    } else {
        mainWindow.setProgressBar(param.progress, { mode: param.mode });
    }
});
function createLicensingWindow() {
    mainWindow = new BrowserWindow({
        width: 400,
        height: 600,
        frame: false,
        resizable: false
    });
    mainWindow.loadFile('validation.html');
    // mainWindow.loadFile('index.html')
    mainWindow.on('closed', function () {
        mainWindow = null
    });
}
ipcMain.on('backSettings', (event, param) => {
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file:',
        slashes: true
    }));
    mainWindow.setFullScreen(false);
    mainWindow.setSize(800, 600);
    let bounds = electron.screen.getPrimaryDisplay().bounds;
    let x = bounds.x + ((bounds.width - 800) / 2);
    let y = bounds.y + ((bounds.height - 600) / 2);
    mainWindow.setPosition(x, y);
});
ipcMain.on('start-check-for-updates', (event, param) => {
    log.info('checking for updates now ...');
    // autoUpdater.checkForUpdatesAndNotify();
    autoUpdater.checkForUpdates();
});
function cleanString(input) {
    output = input.replace(/[^a-z0-9{}'":,]/gi,'-');
    return output;
}
function IsJsonString(str) {
    console.log('str: ', str);
    try {
        JSON.parse(str);
    } catch (e) {
        console.log('e: ', e);
        return false;
    }
    return true;
}
function showDialogWithErrorCode(validationObj) {
    let text = ""; let flag = 1;
    let serverResponse;
    let error;
    // try{
    if (validationObj.ServerValidation) {
        // dialog.showMessageBox({
        //     type : "error",
        //     message : "Response from server : "+validationObj.ServerValidation.ServerPhpResponse
        // });
        if (validationObj.ServerValidation.ServerPhpResponse == "") {
        } else {
            console.log('validationObj.ServerValidation.ServerPhpResponse: ', validationObj.ServerValidation.ServerPhpResponse);
            if (IsJsonString(validationObj.ServerValidation.ServerPhpResponse)) {
                serverResponse = JSON.parse(validationObj.ServerValidation.ServerPhpResponse);
                error = serverResponse.error;
            } else {
                // ! response contains error from http connection; fallback to localValidation
            }
        }
    }
    if (validationObj.regNotFound == true) {
        // ? local licensing data not found for this machine;
        text = "Error Validating Installation"
    } else if (validationObj.areContentsSimilar == true && validationObj.areDatesValid == true) {
        if(serverResponse) {
            if (serverResponse.isValid == true) {
                flag = 0;
            }
        } else {
            // ? if server reponse is undefined then rely on local validation
            flag = 0;
        }
    }
    if (error == environmentJs.errorCodes.KEY_NOT_FOUND) {
        text = "Key Not Found";
    }
    if (error == environmentJs.errorCodes.LICENSE_NOT_FOUND) {
        text = "License Not Found";
    }
    if (error == environmentJs.errorCodes.LICENSE_EXPIRED) {
        text = "License Expired";
    }
    if (error == environmentJs.errorCodes.LICENSE_CANCELLED) {
        text = "License Cancelled";
    }
    if (error == environmentJs.errorCodes.KEY_ALREADY_USED) {
        text = "Key Already Used";
    }
    if (error == environmentJs.errorCodes.PC_REGISTERED_WITH_DIFFERENT_USER) {
        text = "This PC was registered with different user!,Please contact admin";
    }
    // if(error == environmentJs.errorCodes.HTTP_RESPONSE_ERROR){
    //     text = "HiFi Recorder could not connect with server, Please try again or Contact Admin";
    // }
    if (flag == 1) {
        dialog.showMessageBox({
            type: "error",
            message: text
        });
        createLicensingWindow();
    } else if (flag == 0) {
        createRecorderWindow();
    }
}
function createStarterWindow() {
    starterWindow = new BrowserWindow({});
    starterWindow.hide();
    starterWindow.setOpacity(0.9);
    starterWindow.setPosition(0, 0);
    starterWindow.loadFile("./starter-window/starter-window-index.html");
}
autoUpdater.on('checking-for-update', () => {
    sendStatusToWindow({ code: 'checking-for-update', text: 'Checking for update...' });
})
autoUpdater.on('update-available', (info) => {
    sendStatusToWindow({ code: 'update-available', text: 'Update available, will be downloaded in the background, You can continue recording' });
})
autoUpdater.on('update-not-available', (info) => {
    sendStatusToWindow({ code: 'update-not-available', text: 'Latest Version Already Installed' });
})
autoUpdater.on('error', (err) => {
    sendStatusToWindow({ code: 'update-error', text: 'Error while updating, Please try later!', err });
})
// autoUpdater.on('download-progress', (progressObj) => {
//     let log_message = "Download speed: " + progressObj.bytesPerSecond;
//     log_message = log_message + ' - Downloaded ' + progressObj.percent + '%';
//     log_message = log_message + ' (' + progressObj.transferred + "/" + progressObj.total + ')';
//     sendStatusToWindow(log_message);
// })
autoUpdater.on('update-downloaded', (info) => {
    sendStatusToWindow({ code: 'update-downloaded', text: 'Update Downloaded' });
});
autoUpdater.on('checking-for-update', () => {
    sendStatusToWindow({ code: 'checking-for-update', text: 'Checking for Update now' });
});
autoUpdater.on('update-available', (info) => {
    sendStatusToWindow({ code: 'update-available', text: 'Update available, will be downloaded in the background, You can continue recording' });
});
autoUpdater.on('update-not-available', (info) => {
    sendStatusToWindow({ code: 'update-not-available', text: 'Latest version already installed' });
});
autoUpdater.on('error', (err) => {
    sendStatusToWindow({ code: 'update-error', text: 'Error while updating, Please try later!', err });
});
autoUpdater.on('download-progress', (progressObj) => {
    const obj = {
        code: 'download-progress',
        text: progressObj.percent.toFixed(2) + '% Downloaded'
    }
    sendStatusToWindow(obj);
});
autoUpdater.on('update-downloaded', (info) => {
    sendStatusToWindow({ code: 'update-downloaded', text: 'Update Downloaded' });
});
function sendStatusToWindow(text) {
    log.info(text);
    mainWindow.webContents.send('message', text);
}
ipcMain.on('install-update-now', (eve, data) => {
    autoUpdater.quitAndInstall();
})
function validateOfflineHelper(documentsFolderPath, basepath, appDataPath) {
    var isLocalValid = addon.validateOffline(documentsFolderPath, basepath, appDataPath);
    if (isLocalValid.areContentsSimilar == true && isLocalValid.areDatesValid == true) {
        createRecorderWindow();
    } else {
        dialog.showMessageBox({
            type: "error",
            title: "Invalid License",
            message: "We could not verify your license!"
        });
        if (fs.existsSync(documentsFolderPath + '/sample.dat'))
            fs.unlinkSync(documentsFolderPath + '/sample.dat');
        createLicensingWindow();
    }
}