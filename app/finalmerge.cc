#include <napi.h>
#include <iostream>
#include <string>
#include <array>
#include <windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "commandFunctions.h"
std::string *strCommands;
HANDLE hChildProcess_f = NULL; //NULL
HANDLE hStdIn_f = NULL;		   // Handle to parents std input. NULL
BOOL bRunThread_f;			   // TRUE
int commandCounter_f = 0;
int noOfFinalMergeCommands = 1;
#pragma comment(lib, "User32.lib")


char * RunCommandFileBuffer(std::string cmdx,Napi::Env env,Napi::Function cb){

    // int duration = durationx;
    LPTSTR cmd = const_cast<char *>((cmdx).c_str());

    BOOL ok = TRUE;
    HANDLE hStdInPipeRead = NULL;
    HANDLE hStdInPipeWrite = NULL;
    HANDLE hStdOutPipeRead = NULL;
    HANDLE hStdOutPipeWrite = NULL;

    // Create two pipes.
    SECURITY_ATTRIBUTES sa = { sizeof(SECURITY_ATTRIBUTES), NULL, TRUE };
    ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
    if (ok == FALSE) return "null";
    ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
    if (ok == FALSE) return "null";

    // Create the process.
    STARTUPINFO si = { };
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdError = hStdOutPipeWrite;
    si.hStdOutput = hStdOutPipeWrite;
    si.hStdInput = hStdInPipeRead;
    PROCESS_INFORMATION pi = { };
    LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
    LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
    LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
    LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
    BOOL bInheritHandles = TRUE;
    DWORD dwCreationFlags = 0;
    LPVOID lpEnvironment = NULL;
    LPCWSTR lpCurrentDirectory = NULL;
    // ok = CreateProcess(
    //     lpApplicationName,
    //     lpCommandLine,
    //     lpProcessAttributes,
    //     lpThreadAttribute,
    //     bInheritHandles,
    //     dwCreationFlags,
    //     lpEnvironment,
    //     lpCurrentDirectory,
    //     &si,
    //     &pi);
    ok =CreateProcess(NULL,cmd,NULL,NULL,TRUE,CREATE_NO_WINDOW,NULL,NULL,&si,&pi);
    if (ok == FALSE) return "null";

    // Close pipes we do not need.
    CloseHandle(hStdOutPipeWrite);
    CloseHandle(hStdInPipeRead);

    // The main loop for reading output from the DIR command.
    char buf[1024 + 1] = { };
    DWORD dwRead = 0;
    DWORD dwAvail = 0;

    ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    while (ok == TRUE){
        buf[dwRead] = '\0';
        OutputDebugStringA(buf);
        cout << endl << "Output of final merge command : " <<buf;
		puts(buf);
		cb.Call(env.Global(), {Napi::String::New(env, buf)});
        ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    }

    // Clean up and exit.
    CloseHandle(hStdOutPipeRead);
    CloseHandle(hStdInPipeWrite);
    DWORD dwExitCode = 0;
    GetExitCodeProcess(pi.hProcess, &dwExitCode);

	return buf;
    // return 0;
}
std::string ReplaceAll(std::string str, const std::string &from, const std::string &to)
{
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
	}
	return str;
}
void finalmerge(const Napi::CallbackInfo &info)
{

	std::cout << "\n\ncame to finally merge\n\n";
	//  int check;
	// check=CreateDirectory("errorLog",NULL);
	// if (!check)
	//     std::cout<<"Directory created\n";
	// else
	// {  std::cout<<"Unable to create directory\n";
	// DisplayError("DirectoryProblem");
	// }
	Napi::Env env = info.Env();
	auto configObj = info[0].ToObject();
	std::string configFolderPath = info[1].As<Napi::String>().ToString();
	std::string optname = info[2].As<Napi::String>().ToString();
	Napi::Function cb = info[3].As<Napi::Function>();
	int id = info[4].As<Napi::Number>().Int32Value();

    std::string basepath = info[5].ToString();
    basepath += "src\\assets\\exe\\ffmpeg";
	std::vector<std::string> _args_camera;
	strCommands = new std::string[noOfFinalMergeCommands];
	std::string mergingListDotTxt = configFolderPath + "mergingList.txt";
	mergingListDotTxt = ReplaceAll(mergingListDotTxt, "\\\\", "\\");
	mergingListDotTxt = ReplaceAll(mergingListDotTxt, "/", "\\");

	cout << "merging list path name : " << mergingListDotTxt;

	_args_camera.push_back(basepath);
	_args_camera.push_back("-f");
	_args_camera.push_back("concat");
	_args_camera.push_back("-safe");
	_args_camera.push_back("0");
	_args_camera.push_back("-i");
	_args_camera.push_back('"' + mergingListDotTxt + '"');
	_args_camera.push_back("-codec");
	_args_camera.push_back("copy");
	_args_camera.push_back("-y");
	_args_camera.push_back('"' + configFolderPath + optname + '"');
	strCommands[commandCounter_f++] = createCommand(_args_camera);
	std::cout << "\nCommands in final merge  :\n";
	for (int i = 0; i < commandCounter_f; i++)
	{
		std::cout << "**\n** Command no : **" << i << "**\n Command : **" << strCommands[i] << "**\n";
	}
	std::string commandToRun = strCommands[0];
	std::string outputRunCommandFileBuffer = RunCommandFileBuffer(commandToRun,env,cb);
	
	Napi::Object res = Napi::Object::New(env);
	res.Set("status", "complete");
	res.Set("outputFileName", configFolderPath + optname);
	cb.Call(env.Global(), {res});

	std::cout << "final-merge-complete";
}

Napi::Object Init(Napi::Env env, Napi::Object exports)
{
	return Napi::Function::New(env, finalmerge);
}
NODE_API_MODULE(finalmerge, Init)
