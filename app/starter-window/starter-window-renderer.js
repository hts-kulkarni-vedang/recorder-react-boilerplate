const electron = require("electron");
const { ipcRenderer } = require("electron");
let currentWindow;
let counter = 3;

ipcRenderer.on("start-timer", (event, param) => {
	currentWindow.setFullScreen(true);
	let interval = setInterval(function () {
		$(".countdown-timer").text(counter);
		$(".countdown-title").text("Recording Starts in ...");
		counter--;
		if (counter == -1) {
			clearInterval(interval);
			$(".countdown-title").text("Starting Recorder ...");
			$(".countdown-timer").text("3");
			currentWindow.setFullScreen(false);
			ipcRenderer.send("starter-window-open-close", "close");
			counter = 3;
		}
	}, 1000);
});
ipcRenderer.on("end-timer", (event, param) => {
	counter = 3;
	$(".countdown-timer").text(counter);
	$(".countdown-title").text("Recording Starts in ...");
});

$(document).ready(function () {
	counter = 3;
	$(".countdown-timer").text(counter);
	$(".countdown-title").text("Recording Starts in ...");
	currentWindow = electron.remote.getCurrentWindow();
	console.log(currentWindow.getSize());
	// let bounds = electron.remote.screen.getPrimaryDisplay().bounds;
	$("html,body").css("width", "100%");
	$("html,body").css("height", "100%");
});

