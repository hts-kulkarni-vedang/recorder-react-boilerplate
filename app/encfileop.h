#include<iostream>
#include<fstream>
#include "encrypt.h"
#include <string>
#include <algorithm>
#include <iterator>
#include<ctime>
#include <sstream>
#include <direct.h>
#include "commandFunctions.h"
#include "RSJparser.tcc"
#include <cstdlib>
#include<napi.h>
#include <array>

#include <ctype.h>
#include <psapi.h>
#include <errno.h>
#include <cstdlib>
#include <wininet.h>
#include <tchar.h>
#include <Sensapi.h>
#include <cstdio>
#include <memory>
#include <stdexcept>



std::string homeDir;
std::string appDataPath;

/*
------------------------------------------------------------------------------------------------------------------------
Parameters stored in enc file:
startday,startmonth,startyear:-Date when user first enters key.
endday,endmonth,endyear:-Last date at which key will remain valid.
lastvalidday,lastvalidmonth,lastvalidyear:-Date of last successful date verification.
------------------------------------------------------------------------------------------------------------------------
Functions for file operations:
Data in files is stored in key-value pairs.
Parameters given above are the keys.
writetofile(string key,string value):-Stores both key and value in encrypted format.
readfromfile(string key):-Retrieves value of the corresponding key.
deletefromfile(string key):-Deletes key-value pair from key.
editfromfile(string key,string newvalue):-Changes value of key to newvalue.
calcEnd():-This function takes the startday and dayssubscribed and calculates the enddate and it stores in the file.
saveFile():-This function creates a file according to file_name and saves the startdate and dayssubscribed.
verifyLocal():- This function uses enddate,currdate and lastvaliddate to verify the time period.
-------------------------------------------------------------------------------------------------------------------------
*/



// std::string exec(const char* cmd) {
//     std::array<char, 128> buffer;
//     std::string result;
//     std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);
//     if (!pipe) {
//         throw std::runtime_error("popen() failed!");
//     }
//     while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
//         result += buffer.data();
//     }
//     return result;
// }
// std::string ReplaceString(std::string subject, const std::string& search,
//                           const std::string& replace) {
//     size_t pos = 0;
//     while((pos = subject.find(search, pos)) != std::string::npos) {
//          subject.replace(pos, search.length(), replace);
//          pos += replace.length();
//     }
//     return subject;
// }
template <class Container>
void split3(const std::string& str, Container& cont,
              char delim = ' ')
{
    std::size_t current, previous = 0;
    current = str.find(delim);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}

std::string HTS_KEY = "HAPPYTOSERVE";
std::string Encrypt(std::string strtoenc)
{
    
  std::string msg = strtoenc;
 	std::string encrypted_msg = encrypt(msg, HTS_KEY);
  return encrypted_msg;
}
std::string Decrypt(std::string strtodec)
{
    std::string msg = strtodec;
  	std::string decrypted_msg = decrypt(msg, HTS_KEY);
    return decrypted_msg;
}

char file_name[] = "sample.dat";
void writetofile(std::string& key,std::string& value){
    std::ofstream fout;
    fout.open((homeDir+"\\"+file_name).c_str(),ios::app);
    fout<<Encrypt(key)<<":"<<Encrypt(value)<<"\n";
    fout.close();
}
void setHomeDir(std::string homeDirx){
  homeDir= homeDirx;
  // cout << endl << "Home dir in set function set to : " << homeDir << endl;
}
string readfromfile(std::string& paramtoread){
  // cout << endl << "Home dir in readfromfile function set to : " << homeDir << endl;

  std::vector<std::string> words;
  std::ifstream infile;
  // cout<< "path to read : "<< (homeDir+"\\"+file_name).c_str();
  infile.open((homeDir+file_name).c_str());
  string ans="";
  if(infile.good()){
    // cout << endl<<"came to read param : "<< paramtoread<<endl;
    string sLine,compstring;
     while (std::getline(infile, sLine))
      {
        // cout << endl << "A line :" << sLine<<endl;
          split3(sLine,words,':');
          // cout << "words at 0 : " << words.at(0);
          compstring=Decrypt(words.at(0));
          // cout << "decrypted : " << compstring;
          if(compstring.compare(paramtoread)==0){
            // cout << endl << "read from file : "<<words.at(1);
            // cout << endl << "decrypted read from file : "<<Decrypt(words.at(1));
            ans=Decrypt(words.at(1));
            break;
          }
          else{
            words.clear();
            continue;
          }
      }
    infile.close();
  }else{
    cout << "dat file not found";
    // ans = "dat file not found";
  }
  // cout  <<endl<< "before returning->"<<ans<<"<-" <<endl;
  return ans;
}
void deletefromfile(string paramtodelete){
  std::vector<std::string> words;
  std::ifstream infile;
  std::ofstream outfile;
  infile.open((homeDir+"\\"+file_name).c_str());
  outfile.open((homeDir+"\\"+"temp.dat").c_str());
  string sLine,compstring,ans;
   while (std::getline(infile, sLine))
    {
        split3(sLine,words,' ');
        compstring=Decrypt(words.at(0));
        words.clear();
        if(compstring.compare(paramtodelete)==0){
          continue;
        }
        else{
          outfile<<sLine<<"\n";
          continue;
        }
    }
  infile.close();
  outfile.close();
  // cout << "now deleting file";
  // remove((homeDir+"\\"+file_name).c_str());
  rename((homeDir+"\\"+"temp.dat").c_str(),(homeDir+"\\"+file_name).c_str());
}
void editfromfile(string paramtoedit,string newvalue){
  deletefromfile(paramtoedit);
  writetofile(paramtoedit,newvalue);
}

void saveFile(string datefromserver,string homeDirx){
    RSJresource my_resource (datefromserver);

    std::string serialNumber = my_resource["serialNumber"].as<std::string>();
    std::string isSerialNumberInvalid = my_resource["isSerialNumberInvalid"].as<std::string>();
    homeDir = homeDirx;
    time_t t = time(NULL);
	  tm* timePtr = localtime(&t);
    std::vector<std::string> date;                                                           

    std::vector<std::string> enddate;

    split3(my_resource["startDate"].as<std::string>(),date,'-');
    split3(my_resource["endDate"].as<std::string>(),enddate,'-');

    std::string lastday="lastvalidday",lastmonth="lastvalidmonth",lastyear="lastvalidyear";
    std::string endday="endday",endmonth="endmonth",endyear="endyear",serialNumberStr="serialNumber",isSerialNumberInvalidStr="isSerialNumberInvalid";
    int startdaynum = stoi(date.at(0));
    int startmonthnum = stoi(date.at(1));
    int startyearnum = stoi(date.at(2));
    string startday="startday",startmonth="startmonth",startyear="startyear";

    writetofile(startday,to_string(startdaynum));
    writetofile(startmonth,to_string(startmonthnum));
    writetofile(startyear,to_string(startyearnum));

    writetofile(lastday,to_string(startdaynum));
    writetofile(lastmonth,to_string(startmonthnum));   //Add 1 to tm_mon because it start calculating month from 0.
    writetofile(lastyear,to_string(startyearnum));  //Add 1900 to tm_year because it start calculating year from 1900.    

    writetofile(endday,enddate.at(0));
    writetofile(endmonth,enddate.at(1));
    writetofile(endyear,enddate.at(2));
    writetofile(serialNumberStr,serialNumber);
    writetofile(isSerialNumberInvalidStr,isSerialNumberInvalid);

    cout << endl << "writing serial number to file : "  << serialNumber << endl;
}

bool verifyLocal(std::string basepath){
  // cout << "came to verifyLocal";
  time_t t = time(NULL);
	tm* timePtr = localtime(&t);
  string paramtoread="lastvalidday";
  int lastday=stoi(readfromfile(paramtoread));
  paramtoread="lastvalidmonth";
  int lastmonth=stoi(readfromfile(paramtoread));
  paramtoread="lastvalidyear";
  int lastyear=stoi(readfromfile(paramtoread));
  // cout<< "after readingfrom file";


  int currday = timePtr->tm_mday;
  int currmonth = timePtr->tm_mon + 1;
  int curryear = timePtr->tm_year + 1900;

  std::string eday="endday",emonth="endmonth",eyear="endyear";
  std::string serialNumberStr = "serialNumber";
  std::string isSerialNumberInvalidStr = "isSerialNumberInvalidStr";
  int endday=stoi(readfromfile(eday));
  int endmonth=stoi(readfromfile(emonth));
  int endyear=stoi(readfromfile(eyear));
  std::string serialNumber = readfromfile((serialNumberStr));
  std::string isSerialNumberInvalid = readfromfile((isSerialNumberInvalidStr));

  std::string sday="startday",smonth="startmonth",syear="startyear";
  int startday=stoi(readfromfile(sday));
  int startmonth=stoi(readfromfile(smonth));
  int startyear=stoi(readfromfile(syear));
  
  if(curryear>endyear){
    return false;
  }
  else if(currmonth>endmonth&&curryear==endyear){
    return false;
  }
  else if(curryear==endyear&&currmonth==endmonth&&currday>endday){
    return false;
  }
  else {
    if(curryear<lastyear){
    return false;
   }
    else if(currmonth<lastmonth&&curryear==lastyear){
      return false;
    }
    else if(curryear==lastyear&&currmonth==lastmonth&&currday<lastday){
      return false;
    }
    else{
      paramtoread="lastvalidday";
      editfromfile(paramtoread,to_string(currday));
      paramtoread="lastvalidmonth";
      editfromfile(paramtoread,to_string(currmonth));
      paramtoread="lastvalidyear";
      editfromfile(paramtoread,to_string(curryear));
      return true;
    }
  }
}

string isFilePresent(std::string homeDir)
{
  // std::cout << endl << "searching for file : " << (homeDir+"\\"+file_name).c_str() << endl;
  std::ifstream infile((homeDir+"\\"+file_name).c_str());
  // std::cout << infile.good() << " is the result";
    if(infile.good()){
      return "true";
    }
    else{
      return "false"; 
    }
}