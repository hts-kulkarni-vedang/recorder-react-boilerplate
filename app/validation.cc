#include <napi.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include "commandFunctions_validate.h"
#include "RSJparser.tcc"
#include "encfileop.h"
#include "dirent.h"
#include <filesystem>
// for thread async promise functions
#include <future>
#include <thread>
#include <utility>

// for regex
#include <regex>

// for last modified time formatting
#include <iomanip>
#include <algorithm>

// wait for WMIC_TIMEOUT/1000 seconds for response to come from WMIC
#define WMIC_TIMEOUT 10000
#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

// for getting file information
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#ifndef WIN32
#include <unistd.h>
#endif
#ifdef WIN32
#define stat _stat
#endif

#include <chrono>

std::string mainKey = "HRECKEY";
// states for stopTimerFlag
// 0 - timer not started
// 1 - timer stopped from command output
// 2 - timer stopped because command did not send response
// 3 - timer terminated
int stopTimerFlag = 0;
std::promise<std::string> globalPromise;

HANDLE hChildProcess; //NULL
HANDLE hStdIn;		  // Handle to parents std input. NULL
BOOL bRunThread;
std::string(*strCommands);

int noOfCommands = 1;
int commandCounter = 1;

HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
HANDLE hInputWriteTmp, hInputRead, hInputWrite;
HANDLE hErrorWrite;
HANDLE hThread;
DWORD ThreadId;

/**
* Executes command and returns continuous output by listening to stdout
* @param {string} - command - command to run
* @param {Napi::Env} - env - to construct local objects
*/
std::string RunCommand(std::string cmd, Napi::Env env)
{
	strCommands = new std::string[noOfCommands];
	strCommands[0] = cmd;
	std::string outPutfileName = homeDir + "\\log.txt"; // A file is generated to store output from server.

	/*Security certficates*/
	SECURITY_ATTRIBUTES sa;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;
	/*CreatePipe() returns read handle and write handle for the pipe. A process creates a pipe
	just before it forks one or more child processes. The pipe is then used for communication
	either between the parent or child processes, or between two sibling processes*/
	if (!CreatePipe(&hOutputReadTmp, &hOutputWrite, &sa, 0))
		DisplayError("CreatePipe");

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!DuplicateHandle(GetCurrentProcess(), hOutputWrite,
		GetCurrentProcess(), &hErrorWrite, 0,
		TRUE, DUPLICATE_SAME_ACCESS))
		DisplayError("DuplicateHandle");

	// Create the child input pipe.
	if (!CreatePipe(&hInputRead, &hInputWriteTmp, &sa, 0))
		DisplayError("CreatePipe");

	// Create new output read handle and the input write handles. Set
	// the Properties to FALSE. Otherwise, the child inherits the
	// properties and, as a result, non-closeable handles to the pipes
	// are created.
	if (!DuplicateHandle(GetCurrentProcess(), hOutputReadTmp,
		GetCurrentProcess(),
						 &hOutputRead, // Address of new handle.
						 0, FALSE,	 // Make it uninheritable.
						 DUPLICATE_SAME_ACCESS))
		DisplayError("DupliateHandle");

	if (!DuplicateHandle(GetCurrentProcess(), hInputWriteTmp,
		GetCurrentProcess(),
						 &hInputWrite, // Address of new handle.
						 0, FALSE,	 // Make it uninheritable.
						 DUPLICATE_SAME_ACCESS))
		DisplayError("DupliateHandle");

	// Close inheritable copies of the handles you do not want to be
	// inherited.
	if (!CloseHandle(hOutputReadTmp))
		DisplayError("CloseHandle");
	if (!CloseHandle(hInputWriteTmp))
		DisplayError("CloseHandle");
	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ((hStdIn = GetStdHandle(STD_INPUT_HANDLE)) ==
		INVALID_HANDLE_VALUE)
		DisplayError("GetStdHandle");

	//for creating process

	PrepAndLaunchRedirectedChild(hOutputWrite, hInputRead, hErrorWrite, strCommands, commandCounter, hChildProcess, env);

	// Close pipe handles (do not continue to modify the parent).
	// You need to make sure that no handles to the write end of the
	// output pipe are maintained in this process or else the pipe will
	// not close when the child process exits and the ReadFile will hang.
	if (!CloseHandle(hOutputWrite))
		DisplayError("CloseHandle");
	if (!CloseHandle(hInputRead))
		DisplayError("CloseHandle");
	if (!CloseHandle(hErrorWrite))
		DisplayError("CloseHandle");

	//Launch the thread that gets the input and sends it to the child.
	//DWORD getAndSendInputThread;
	SetAndSendInputThread(hStdIn, bRunThread);

	hThread = CreateThread(NULL, 0, &GetAndSendInputThread,
		(LPVOID)&hInputWrite, 0, &ThreadId);
	if (hThread == NULL)
		DisplayError("CreateThreadnn");

	std::string responseText = ReadAndHandleOutput(env, hOutputRead, outPutfileName);
	// Redirection is complete
	// Tell the thread to exit and wait for thread to die.

	bRunThread = FALSE;

	SetAndSendInputThread(hStdIn, bRunThread); //setting bRunthread's value

	DWORD waitResponse = WaitForSingleObject(hThread, 5000);

	if (waitResponse == WAIT_FAILED)
		DisplayError("WaitForSingleObject");

	if (!CloseHandle(hStdIn))
		DisplayError("CloseHandle");
	SetAndSendInputThread(hStdIn, bRunThread);

	if (!CloseHandle(hOutputRead))
		DisplayError("CloseHandle");
	if (!CloseHandle(hInputWrite))
		DisplayError("CloseHandle");

	return responseText;
}
char *RunCommandFileBuffer(std::string cmdx, Napi::Env env)
{

	// int duration = durationx;
	LPTSTR cmd = const_cast<char *>((cmdx).c_str());

	BOOL ok = TRUE;
	HANDLE hStdInPipeRead = NULL;
	HANDLE hStdInPipeWrite = NULL;
	HANDLE hStdOutPipeRead = NULL;
	HANDLE hStdOutPipeWrite = NULL;

	// Create two pipes.
	SECURITY_ATTRIBUTES sa = {sizeof(SECURITY_ATTRIBUTES), NULL, TRUE};
	ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
	if (ok == FALSE)
		return "null";
	ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
	if (ok == FALSE)
		return "null";

	// Create the process.
	STARTUPINFO si = {};
	si.cb = sizeof(STARTUPINFO);
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdError = hStdOutPipeWrite;
	si.hStdOutput = hStdOutPipeWrite;
	si.hStdInput = hStdInPipeRead;
	PROCESS_INFORMATION pi = {};
	LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
	LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
	LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
	LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
	BOOL bInheritHandles = TRUE;
	DWORD dwCreationFlags = 0;
	LPVOID lpEnvironment = NULL;
	LPCWSTR lpCurrentDirectory = NULL;
	// ok = CreateProcess(
	//     lpApplicationName,
	//     lpCommandLine,
	//     lpProcessAttributes,
	//     lpThreadAttribute,
	//     bInheritHandles,
	//     dwCreationFlags,
	//     lpEnvironment,
	//     lpCurrentDirectory,
	//     &si,
	//     &pi);
	ok = CreateProcess(NULL, cmd, NULL, NULL, TRUE, CREATE_NO_WINDOW, NULL, NULL, &si, &pi);
	if (ok == FALSE)
		return "null";

	// Close pipes we do not need.
	CloseHandle(hStdOutPipeWrite);
	CloseHandle(hStdInPipeRead);

	// The main loop for reading output from the DIR command.
	char buf[1024 + 1] = {};
	DWORD dwRead = 0;
	DWORD dwAvail = 0;

	ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
	while (ok == TRUE)
	{
		buf[dwRead] = '\0';
		OutputDebugStringA(buf);
		puts(buf);
		ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
	}

	// Clean up and exit.
	CloseHandle(hStdOutPipeRead);
	CloseHandle(hStdInPipeWrite);
	DWORD dwExitCode = 0;
	GetExitCodeProcess(pi.hProcess, &dwExitCode);

	return buf;
	// return 0;
}
bool compareLicenseDatIntegrity(std::string appDataFilePath) {
	struct stat result;
	if(stat(appDataFilePath.c_str(), &result)==0)
	{
		auto modified_at = result.st_mtime;
		auto created_at = result.st_ctime;
		// std::string created_at = result.st_ctime;
		cout << endl << "License File Info :";
		cout << endl << "Last modified at : " << modified_at;
		cout << endl << "File created at : " << created_at;

    auto diff = created_at - modified_at;
    cout << endl << "difference between times is : " << diff;

    if(diff >= 20 || diff >= -20) {
      cout << endl <<  "license was  NOT manipulated";
      return false;
    } else {
      cout << endl <<  "probably the license was manipulated";
      return true;
    }

    // NOTE - old logic.. sometimes it took more than 1 second to write the data and create the fiel
    // NOTE - then even the file was not modified by the user ; because the modified and created time became diff
    // NOTE - this logic got broken
		// if(modified_at == created_at) {
		// 	cout << "file modified time and created at time is SIMILAR";
		// 	return false;
		// } else if(modified_at < created_at) {
		// 	cout << "Modified At is less than created at";
		// 	return true;
		// } else if(modified_at > created_at) {
		// 	cout << "Modified At is greater than created at";
		// 	return true;
		// } else {
		// 	return true;
		// }

	}
}
std::string getObjectStr(std::string s)
{
	// std::cout << "Check contetnts : "<<endl;
	std::string result;
	std::size_t found = s.find_last_of('}');
	// cout << "last index of } is : " << found;
	for (std::string::size_type i = 0; i < s.size(); i++)
	{
		// std::cout << s[i] << "->";
		result += s[i];
		if (s[i] == '}' && i == found)
		{
			// cout << "end found";
			break;
		}
	}
	// cout << endl <<"trimmed result is : " <<result <<endl;
	return result;
}
/**************** Registry Functions Start ******************/
void createMainKey()
{
	HKEY hKey = NULL;
	const char *key = "HRECKEY";
	long j = RegCreateKeyEx(HKEY_CURRENT_USER, key, 0L, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hKey, NULL);
	if (ERROR_SUCCESS != j)
		cout << "Error: Could not create registry key " << key << endl
	<< "\tERROR: " << j << endl;
	// else
		// cout << "Success: Key created" << endl;
}
/**
* Deletes all subkeys under mainkey
* ! UPDATE -> do not remove registry contents
* NOTE -> incorrect data will be compared with licenseInfo.dat for validity
*/
std::string removeRegistryContents()
{
	// HKEY hKey;
	// LPCTSTR sk = TEXT(mainKey.c_str());
	// LONG openRes = RegOpenKeyEx(HKEY_CURRENT_USER, sk, 0, KEY_ALL_ACCESS, &hKey);
	// std::string response = "";
	// if (openRes == ERROR_SUCCESS)
	// {
	// 	printf("Success opening main key for deletion.");
	// 	response = "Success opening main key for deletion.";
	// 	LONG l = RegDeleteTreeA(hKey, NULL);
	// 	if (l == ERROR_SUCCESS)
	// 	{
	// 		printf("Success deleting the registry tree.");
	// 		response = "Success deleting the registry tree.";
	// 	}
	// 	else
	// 	{
	// 		printf("Error while deleting Registry Tree.");
	// 		response = "Error while deleting Registry Tree.";
	// 	}
	// }
	// else
	// {
	// 	printf("Error opening key for RegDeleteTreeA.");
	// 	response = "Error opening key for RegDeleteTreeA.";
	// }
	// return response;
	return "false";
}
void writeToRegistry(std::string subKey, std::string subKeyValue)
{
	HKEY hKey;
	LPCTSTR sk = TEXT(mainKey.c_str());
	LONG openRes = RegOpenKeyEx(HKEY_CURRENT_USER, sk, 0, KEY_ALL_ACCESS, &hKey);
	if (openRes == ERROR_SUCCESS)
	{
		// printf("Success opening key.");
	}
	else
	{
		// printf("Error opening key.");
	}
	LPCTSTR value = TEXT(subKey.c_str());
	LPCTSTR data = TEXT(subKeyValue.c_str());

	LONG setRes = RegSetValueEx(hKey, value, 0, REG_SZ, (LPBYTE)data, _tcslen(data) * sizeof(TCHAR));

	if (setRes == ERROR_SUCCESS)
	{
		// printf("Success writing to Registry.");
	}
	else
	{
		// printf("Error writing to Registry.");
	}
	// cout << setRes << endl;

	LONG closeOut = RegCloseKey(hKey);
	if (closeOut == ERROR_SUCCESS)
	{
		// printf("Success closing key.");
	}
	else
	{
		// printf("Error closing key.");
	}
}
std::string readFromRegistry(std::string subKey)
{
	HKEY hk;
	DWORD dwData;
	HKEY hKey;
	LONG returnStatus;
	DWORD dwType = REG_DWORD;
	DWORD dwSize = sizeof(DWORD) * 2;

	HKEY OpenResult;

	LPCSTR pValue = subKey.c_str();
	DWORD flags = RRF_RT_ANY;

	//Allocationg memory for a DWORD value.
	// REG_SZ dataType;

	WCHAR value[255];
	PVOID pvData = value;

	DWORD size = sizeof(value); // not 8192;

	std::string returnValue = "";

	// Notice that it's SOFTWARE instead of \\SOFTWARE:
	long n = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT(mainKey.c_str()), 0, KEY_QUERY_VALUE, &hk);
	if (n == ERROR_SUCCESS)
	{
		// cout << "HRECKEY is now open" << endl;
		n = RegGetValueA(OpenResult, NULL, pValue, flags, NULL, pvData, &size);

		// cout << endl << "type of result is : " << dataType <<endl ;
		if (n != ERROR_SUCCESS)
		{
			// wprintf(L"Error getting value. Code: %x\n", n);
			returnValue = "sub-key-not-present";
		}
		else
		{
			returnValue = (char *)pvData;
		}
		RegCloseKey(OpenResult); // don't forget !
		return returnValue;
	}
	else
	{
		cout << "HRECKEY not present " << n << endl;
		returnValue = "main-key-not-present";
		return returnValue;
	}
}
std::string filetime_to_string(FILETIME ftime) // ISO format, time zone designator Z == zero (UTC)
{
	SYSTEMTIME utc;
	::FileTimeToSystemTime(std::addressof(ftime), std::addressof(utc));

	std::ostringstream stm;
	const auto w2 = std::setw(2);
	stm << std::setfill('0') << std::setw(4) << utc.wYear << '-' << w2 << utc.wMonth
	<< '-' << w2 << utc.wDay << ' ' << w2 << utc.wYear << ' ' << w2 << utc.wHour
	<< ':' << w2 << utc.wMinute << ':' << w2 << utc.wSecond << 'Z';

	return stm.str();
}

std::string getLastModifiedTime()
{
	std::string time = "";
	HKEY hKey;
	LONG dwRegOPenKey = RegOpenKeyEx(HKEY_CURRENT_USER, TEXT(mainKey.c_str()), 0, KEY_READ, &hKey);
	if (dwRegOPenKey == ERROR_SUCCESS)
	{
		// printf("RegOpenKeyEx succeeded, error code %d\n", GetLastError());
		// QueryKey(hKey);
		TCHAR achKey[MAX_KEY_LENGTH];		 // buffer for subkey name
		DWORD cbName;						 // size of name string
		TCHAR achClass[MAX_PATH] = TEXT(""); // buffer for class name
		DWORD cchClassName = MAX_PATH;		 // size of class string
		DWORD cSubKeys = 0;					 // number of subkeys
		DWORD cbMaxSubKey;					 // longest subkey size
		DWORD cchMaxClass;					 // longest class string
		DWORD cValues;						 // number of values for key
		DWORD cchMaxValue;					 // longest value name
		DWORD cbMaxValueData;				 // longest value data
		DWORD cbSecurityDescriptor;			 // size of security descriptor
		FILETIME ftLastWriteTime;			 // last write time

		DWORD i, retCode;

		TCHAR achValue[MAX_VALUE_NAME];
		DWORD cchValue = MAX_VALUE_NAME;

		// Get the class name and the value count.
		retCode = RegQueryInfoKey(
			hKey,				   // key handle
			achClass,			   // buffer for class name
			&cchClassName,		   // size of class string
			NULL,				   // reserved
			&cSubKeys,			   // number of subkeys
			&cbMaxSubKey,		   // longest subkey size
			&cchMaxClass,		   // longest class string
			&cValues,			   // number of values for this key
			&cchMaxValue,		   // longest value name
			&cbMaxValueData,	   // longest value data
			&cbSecurityDescriptor, // security descriptor
			&ftLastWriteTime);	 // last write time

		// cout << endl
		// << "Last modified at : " << filetime_to_string(ftLastWriteTime) << endl;
		time = filetime_to_string(ftLastWriteTime);
	}
	else
	{
		// printf("RegOpenKeyEx failed, error code %d\n", dwRegOPenKey);
		time = "time-not-found";
	}
	RegCloseKey(hKey);
	return time;
}
// Returns a string of random alphabets of
// length n.
string printRandomString(int n)
{
	// Use current time as seed for random generator
	srand(time(0));
	const int MAX = 26;
	char alphabet[MAX] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g',
	'h', 'i', 'j', 'k', 'l', 'm', 'n',
	'o', 'p', 'q', 'r', 's', 't', 'u',
	'v', 'w', 'x', 'y', 'z' };

	string res = "";
	for (int i = 0; i < n; i++)
		res = res + alphabet[rand() % MAX];

	return res;
}
/**************** Registry Functions End ******************/

/**
* Generates serialnumber either from wmic or hts-serialnumber-utility
* inserts the serialnumber into registry using helper function writeToRegistry();
* @returns {string} generatedSerialnumber - string format of generated serialNumber
*/
std::string GenerateSerialNumber(std::string basepath, Napi::Env env)
{
	std::string finalSerialNumber = "command-did-not-respond";
	std::future<string> future = std::async(std::launch::async, [env]() {
		std::string cmd = "wmic bios get serialnumber";
		std::string response = RunCommandFileBuffer(cmd, env);
		// cout << endl << "serialNumberStr from wmic : " << response;
		response = regex_replace(response, regex("SerialNumber"), "");
		response = regex_replace(response, regex("\n"), "");
		response.erase(response.find_last_not_of(" \n\r\t") + 1);
		// cout << "serialNumberStr from wmic after reaplacing :->" << response << "<-";
		// for (int i = 0; i < response.size(); i++)
		// {
		// 	cout << "char " << i << " th" << response[i] << "<-" << endl;
		// }
		return response;
	});
	// std::cout << "waiting...\n";
	int timeout = 0;
	BOOL commandReturned = false;
	std::future_status status;
	do
	{
		status = future.wait_for(std::chrono::seconds(1));
		if (status == std::future_status::deferred)
		{
			// std::cout << "deferred\n";
		}
		else if (status == std::future_status::timeout)
		{
			// std::cout << timeout << " second timeout passed !" << endl;
			timeout++;
			if (timeout == 10)
			{
				commandReturned = false;
				break;
			}
		}
		else if (status == std::future_status::ready)
		{
			// std::cout << "ready!\n";
			commandReturned = true;
		}
	} while (status != std::future_status::ready);

	if (commandReturned == true)
	{
		// std::cout << "result is " << future.get() << '\n';
		finalSerialNumber = future.get();
	}
	else
	{
		// cout << "Command could not return value within time";
		finalSerialNumber = "wmic-did-not-respond";
		std::string tmpCmd = basepath + "src\\assets\\exe\\kill-process-name.exe wmic.exe";
		system(tmpCmd.c_str());
	}
	// cout << endl
	// << "Final serial number after promise is : " << endl;
	// cout << finalSerialNumber << endl;
	std::string len = "length of serial number" + finalSerialNumber.length();

	/**
	* ! At this stage finalSerialNumber will either contain
	* ? a) valid serialnumber from wmic
	* ? b) invalid serialnumber from wmic (e.g. "SerialNumber or to be filled by O.E.M or wmic is not a command")
	* ? c) "wmic-did-not-respond"
	* ? d) "System Serial Number"
 	*/
	std::string response;
	std::size_t wmicNotFound = finalSerialNumber.find("recognized");
	std::size_t otherError = finalSerialNumber.find("disabled");

	// cout << endl << "not recognized as result : " << wmicNotFound << endl;
	// cout << endl << "other error result : " << otherError << endl;
	// ! IMPORTANT NOTE -
	// ? on some devices - finalSerialNumber value is directly null;

	if (wmicNotFound != std::string::npos || finalSerialNumber == "wmic-did-not-respond" || otherError != std::string::npos || finalSerialNumber == "null")
	{
		std::string tmp = "NULL String found";
		if(finalSerialNumber == "null") {
			// MessageBox(0,tmp.c_str() , tmp.c_str(), MB_OK);
		}
		// ? this indicates that the substring 'not recognized as' was present
		// ? in the finalSerialNumber string
		// cout << endl << "came to generate random number";
		finalSerialNumber = printRandomString(10);
		// cout << endl << "random number is : " << finalSerialNumber;

		// cout << endl
		// << "Error while getting  serial number ; so the serial number was generated randomly" << endl;
		// MessageBox(0, "Newly generated serial number is after this MessageBox", "blah", MB_OK);
		// MessageBox(0, finalSerialNumber.c_str(),"blah",MB_OK);
		return finalSerialNumber;
	}
	else
	{
		// cout << endl << "Serial Number if did not work .";
		// cout << endl << "came to get serialnumber from memory chip ";
		// cout << endl << "in such scenario the serial number is : " << finalSerialNumber;
		std::replace(finalSerialNumber.begin(), finalSerialNumber.end(), ' ', '*'); // replace all ' ' to '*'
		std::size_t found = finalSerialNumber.find("*");

		// * found in serialnumber which means it contains spaces ; i.e. it is not a valid serialnumber
		if (found != std::string::npos)
		{
			// std::cout << "first '*' found at: " << found << '\n';
			// serialnumber is invalid
			// NOTE -> take serial number from memorychip (RAM)
			// ! RAM serial number cannot be considered because
			// ! a lot of users have 00000000 as RAM serial number
			// NOTE -> if 00000000 occured then use random string
			std::string tmpCmd = "wmic memorychip get serialnumber";
			std::string response = RunCommandFileBuffer(tmpCmd, env);
			// cout << endl
			// << "serialNumberStr from wmic memorychip : " << response;
			response = regex_replace(response, regex("SerialNumber"), "");
			response = regex_replace(response, regex("\n"), "");
			response.erase(response.find_last_not_of(" \n\r\t") + 1);
			// cout << "serialNumberStr from wmic memorychip after reaplacing :->" << response << "<-";
			// cout << "returning final serialNumber memorychip from generate serial number : " << response;

			if(response == "00000000") {
				// ? IF SERIAL NUMBER IS 00000000 -: then generate a random string
				// cout << endl << "came to generate random number in else";
				finalSerialNumber = printRandomString(10);
				// cout << endl << "random number is : " << finalSerialNumber;
				return finalSerialNumber;
			} else {
				return response;
			}
		}
		else
		{
			// valid serialNumber found
			return finalSerialNumber;
		}
	}
}

/**
* validates user license by making http request with key and or serialnumber
* @param {string} keyOrSerialNumber - when validateMode is "validate-with-key" this contains key , when validateMode is "validate-with-key" this contains serialNumber
* @param {string} basepath - path to where recorder is installed
* @param {Napi::Env} env - environment for Napi
* @param {string} validateMode - either contains "validate-with-key" or "validate-with-serial-number"
*/
Napi::Object ServerValidation(std::string keyOrSerialNumber, std::string basepath, Napi::Env env, std::string validateMode, std::string serverPath)
{
	Napi::Object returnObj = Napi::Object::New(env);

	std::string serialNumber = "";
	// serialnumber not present in registry
	if (validateMode == "validate-with-key")
	{
		// NOTE -> should i read from licenseInfo.dat
		serialNumber = GenerateSerialNumber(basepath, env);
		std::string key = keyOrSerialNumber;
		// std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "recorder-validation-2-0-0.php" + " activation " + key + " " + serialNumber;
		std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "validation/recorder-validation" + " activation " + key + " " + serialNumber;
		// ? enclose command in qoutes ; for the directory structure might contain spaces
		// cmd = "\"" + cmd + "\"";
		// cout << endl
		// << "executing command : " << cmd;
		std::string res = getObjectStr(RunCommand(cmd, env));
		// cout << endl
		// << "server response : " << res << endl;
		// MessageBox(0,"command for validation with key" , cmd.c_str(), MB_OK);
		// MessageBox(0,cmd.c_str() , cmd.c_str(), MB_OK);

		// MessageBox(0,"response from server" , cmd.c_str(), MB_OK);
		// MessageBox(0,res.c_str() , cmd.c_str(), MB_OK);
		if (res == "")
		{
			res = "{error : 'server-response-empty-while-key-activation'}";
		}
		returnObj.Set("ServerPhpResponse", res);
		returnObj.Set("command", cmd);
	}
	else if (validateMode == "validate-with-serial-number")
	{
		std::string toRead = "serialNumber";
		std::string encryptedToRead = Encrypt(toRead);
		std::string valueReadFromRegistry = readFromRegistry(encryptedToRead);
		std::string decryptedSerialNumber = Decrypt(valueReadFromRegistry);
		// cout << endl << "To read : " << toRead << endl;
		// cout << endl << "Encrypted To read : " << encryptedToRead << endl;
		// cout << endl << "value read from registry : " << valueReadFromRegistry << endl;
		// cout << endl << "decrypted SerialNumber : " << decryptedSerialNumber << endl;
		serialNumber = decryptedSerialNumber;
		// cout << endl << "SerialNumber read " << serialNumber << endl;
		std::string lastModifiedLocalTime = getLastModifiedTime();

		// ? enclose command in qoutes ; for the directory structure might contain spaces
		std::string cmdStart = basepath + "src\\assets\\exe\\http.exe";
		cmdStart = "\"" + cmdStart + "\"";
		// std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "recorder-validation-2-0-0.php" + " validation " + serialNumber + " " + lastModifiedLocalTime;
		std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "validation/recorder-validation" + " validation " + serialNumber + " " + lastModifiedLocalTime;

		// cout << endl << "command for http : " << cmd << endl;
		std::string cmdOutput = RunCommand(cmd, env);
		std::string res = getObjectStr(cmdOutput);
		// cout << endl << "server response : " << res << endl;
		// MessageBox(0,"command for validation with serial numver" , cmd.c_str(), MB_OK);
		// MessageBox(0,cmd.c_str() , cmd.c_str(), MB_OK);

		// MessageBox(0,"response from server" , cmd.c_str(), MB_OK);
		// MessageBox(0,res.c_str() , cmd.c_str(), MB_OK);
		if (res == "")
		{
			res = "{error : 'server-response-empty-while-serial-validation'}";
		}
		returnObj.Set("ServerPhpResponse", res);
		returnObj.Set("command", cmd);
		returnObj.Set("commandOutput", res);
		returnObj.Set("serialNumber", serialNumber);
	}
	// cout << endl << "SerialNumber generated  : " << serialNumber;
	return returnObj;
}
/**
* validates user license by making http request with key and or serialnumber
* @param {string} keyOrSerialNumber - when validateMode is "validate-with-key" this contains key , when validateMode is "validate-with-key" this contains serialNumber
* @param {string} basepath - path to where recorder is installed
* @param {Napi::Env} env - environment for Napi
* @param {string} validateMode - either contains "validate-with-key" or "validate-with-serial-number"
*/
void writeResponseToRegistry(RSJresource responseObj)
{
	std::string serialNumber = responseObj["serialNumber"].as<std::string>();
	std::string isLifetimeValid = responseObj["isLifetimeValid"].as<std::string>();
	// cout << endl << "came to write server response to registry ; serialnumber is " << serialNumber;

	// cout << endl << "writing serialNumber to registry : " << serialNumber << endl;
	std::vector<std::string> date;
	std::vector<std::string> enddate;

	// NOTE -> patch to handle date as ISO strings
	std::vector<std::string> tmpDatesVector, tmpEndDatesVector;
	std::string startDateTemp = responseObj["startDate"].as<std::string>();
	split3(startDateTemp, tmpDatesVector, 'T');
	startDateTemp = tmpDatesVector.at(0);

	std::string endDateTemp = responseObj["endDate"].as<std::string>();
	split3(endDateTemp, tmpEndDatesVector, 'T');
	endDateTemp = tmpEndDatesVector.at(0);

	split3(startDateTemp, date, '-');
	split3(endDateTemp, enddate, '-');
	// NOTE -> patch end

	// ! original
	// split3(responseObj["startDate"].as<std::string>(), date, '-');
	// split3(responseObj["endDate"].as<std::string>(), enddate, '-');
	// ! end original

	std::string lastday = "lastvalidday", lastmonth = "lastvalidmonth", lastyear = "lastvalidyear";
	std::string endday = "endday", endmonth = "endmonth", endyear = "endyear", serialNumberStr = "serialNumber", isSerialNumberInvalidStr = "isSerialNumberInvalid";
	std::string isLifetimeValidStr = "isLifetimeValid";

	// ! original
	// int startdaynum = stoi(date.at(0));
	// int startmonthnum = stoi(date.at(1));
	// int startyearnum = stoi(date.at(2));
	// ! end original

	// NOTE -> date as ISO string patch
	int startdaynum = stoi(date.at(2));
	int startmonthnum = stoi(date.at(1));
	int startyearnum = stoi(date.at(0));
	// NOTE -> patch end
	string startday = "startday", startmonth = "startmonth", startyear = "startyear";

	createMainKey();

	writeToRegistry(Encrypt(startday), Encrypt(to_string(startdaynum)));
	writeToRegistry(Encrypt(startmonth), Encrypt(to_string(startmonthnum)));
	writeToRegistry(Encrypt(startyear), Encrypt(to_string(startyearnum)));

	writeToRegistry(Encrypt(lastday), Encrypt(to_string(startdaynum)));
	writeToRegistry(Encrypt(lastmonth), Encrypt(to_string(startmonthnum))); //Add 1 to tm_mon because it start calculating month from 0.
	writeToRegistry(Encrypt(lastyear), Encrypt(to_string(startyearnum)));   //Add 1900 to tm_year because it start calculating year from 1900.

	writeToRegistry(Encrypt(endday), Encrypt(enddate.at(2)));
	writeToRegistry(Encrypt(endmonth), Encrypt(enddate.at(1)));
	writeToRegistry(Encrypt(endyear), Encrypt(enddate.at(0)));

	writeToRegistry(Encrypt(serialNumberStr), Encrypt(serialNumber));

	// cout << endl << "writing islifetimevalid to registry : " << endl;
	// cout << endl << "raw value : " << isLifetimeValid << endl;
	// cout << endl << "encrypted key :  : " << Encrypt(isLifetimeValidStr) << endl;
	// cout << endl << "encrypted value :  : " << Encrypt(isLifetimeValid) << endl;
	writeToRegistry(Encrypt(isLifetimeValidStr), Encrypt(isLifetimeValid));
}
/**
* reads provided key from licenseInfo.dat file present in appdata folder
* @param {string} encrypted key to read
* @returns {string} encrypted value associated with encrypted key in appdata folder licenseinfo.dat file
*/
std::string readFromAppDataFile(std::string encryptedKey, std::string appDataFilePath)
{
	std::string readValue = "";
	std::string tmpValue = "";
	std::ifstream infile;
	std::vector<std::string> words;

	infile.open(appDataFilePath.c_str());
	if (infile.good())
	{
		string readLine = "";
		while (std::getline(infile, readLine))
		{
			split3(readLine, words, ':');
			tmpValue = words.at(0);
			if (tmpValue == encryptedKey)
			{
				readValue = words.at(1);
				break;
			}
			words.clear();
		}
		infile.close();
	}
	else
	{
		readValue = "file-not-present";
	}
	return readValue;
}
/**
* Writes key-value pair encrypted data to licenseInfo.dat file prensent in AppData Folder
* @param {string} - key - must be previously encrypted
* @param {string} - value - must be previously encrypted
*
*/
void writeToAppDataFile(std::string key, std::string value, std::string appDataFilePath)
{
	std::ofstream fout;
	fout.open((appDataFilePath).c_str(), ios::app);
	fout << key << ":" << value << endl;
	fout.close();
}
/***
* Empties the licenseInfo.dat file if exists
**/
void emptyAppDataFileIfExists(std::string appDataFilePath)
{
	std::ofstream out;
	out.open(appDataFilePath.c_str(), std::ofstream::out | std::ofstream::trunc);
	out.close();
	// cout << endl << "File created and contents emptied at : " << appDataFilePath;
}
/**
* Wrapper for writing server validation response to licenseInfo.dat file in AppData
* @param {RSJresource[json]} - serverResponse - json object containing server response of activation
*/
void writeResponseToAppDataWrapper(RSJresource responseObj, std::string appDataFilePath, std::string tempAppDataFilePath, std::string basepath, std::string serverPath, std::string key, Napi::Env env, std::string appDataFolderPath) {


	std::string serialNumber = responseObj["serialNumber"].as<std::string>();
	// cout << endl << "writing serialNumber to registry : " << serialNumber << endl;
	std::vector<std::string> date;
	std::vector<std::string> enddate;

	// NOTE -> patch to handle date as ISO strings
	std::vector<std::string> tmpDatesVector, tmpEndDatesVector;
	std::string startDateTemp = responseObj["startDate"].as<std::string>();
	split3(startDateTemp, tmpDatesVector, 'T');
	startDateTemp = tmpDatesVector.at(0);
	// cout << startDateTemp;
	std::string endDateTemp = responseObj["endDate"].as<std::string>();
	split3(endDateTemp, tmpEndDatesVector, 'T');
	endDateTemp = tmpEndDatesVector.at(0);
	// cout << "respoonse end date : " << endDateTemp;
	split3(startDateTemp, date, '-');
	split3(endDateTemp, enddate, '-');
	// NOTE -> patch end

	// ! original
	// split3(responseObj["startDate"].as<std::string>(), date, '-');
	// split3(responseObj["endDate"].as<std::string>(), enddate, '-');
	// ! end original

	std::string lastday = "lastvalidday", lastmonth = "lastvalidmonth", lastyear = "lastvalidyear";
	std::string endday = "endday", endmonth = "endmonth", endyear = "endyear", serialNumberStr = "serialNumber", isSerialNumberInvalidStr = "isSerialNumberInvalid";
	std::string keyString = "key";
	// ! original
	// int startdaynum = stoi(date.at(0));
	// int startmonthnum = stoi(date.at(1));
	// int startyearnum = stoi(date.at(2));
	// ! end original

	// NOTE -> date as ISO string patch
	int startdaynum = stoi(date.at(2));
	int startmonthnum = stoi(date.at(1));
	int startyearnum = stoi(date.at(0));
	// NOTE -> patch end
	string startday = "startday", startmonth = "startmonth", startyear = "startyear";

	emptyAppDataFileIfExists(tempAppDataFilePath);


	cout << endl << "App data file path while writing : " << tempAppDataFilePath;
	writeToAppDataFile(Encrypt(startday), Encrypt(to_string(startdaynum)), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(startmonth), Encrypt(to_string(startmonthnum)), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(startyear), Encrypt(to_string(startyearnum)), tempAppDataFilePath);

	writeToAppDataFile(Encrypt(lastday), Encrypt(to_string(startdaynum)), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(lastmonth), Encrypt(to_string(startmonthnum)), tempAppDataFilePath); //Add 1 to tm_mon because it start calculating month from 0.
	writeToAppDataFile(Encrypt(lastyear), Encrypt(to_string(startyearnum)), tempAppDataFilePath);   //Add 1900 to tm_year because it start calculating year from 1900.

	writeToAppDataFile(Encrypt(endday), Encrypt(enddate.at(2)), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(endmonth), Encrypt(enddate.at(1)), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(endyear), Encrypt(enddate.at(0)), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(serialNumberStr), Encrypt(serialNumber), tempAppDataFilePath);
	writeToAppDataFile(Encrypt(keyString), Encrypt(key), tempAppDataFilePath);

	// cout << endl << "end day written to appdatafile as : " << enddate.at(2) << endl;
	// cout << endl << "end month written to appdatafile as : " << enddate.at(1) << endl;
	// cout << endl << "end year written to appdatafile as : " << enddate.at(0) << endl;



	// NOTE -> after writing contents to appDataFile
	// NOTE -> set modified and created time in database
	struct stat result;
	if(stat(tempAppDataFilePath.c_str(), &result)==0)
	{
		auto modified_at = result.st_mtime;
		struct tm *tm;
		char buf[200];
		tm = localtime(&modified_at);
		strftime(buf, sizeof(buf), "%d.%m.%Y-%H:%M:%S", tm);
		std::string modified_at_str = buf;
		// cout << endl << "string rep of time : " << modified_at_str;


		auto created_at = result.st_mtime;
		struct tm *tm1;
		char buf1[200];
		tm1 = localtime(&created_at);
		strftime(buf1, sizeof(buf1), "%d.%m.%Y-%H:%M:%S", tm1);
		std::string created_at_str = buf1;
		// cout << endl << "string rep of time : " << created_at_str;

		// TODO -> now update this time to server
		std::string tmpKey = key;
		tmpKey.erase(std::remove(tmpKey.begin(), tmpKey.end(), '-'), tmpKey.end());

		// ? enclose command in qoutes ; for the directory structure might contain spaces
		std::string cmdStart = basepath + "src\\assets\\exe\\http.exe";
		cmdStart = "\"" + cmdStart + "\"";
		std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "validation/update-modified-time" + " update-modified-time " + tmpKey + " " + created_at_str + " " + modified_at_str;

		// cout << endl << "command for http : " << cmd << endl;
		std::string cmdOutput = RunCommand(cmd, env);
		std::string res = getObjectStr(cmdOutput);
		// cout << endl << "server response for updating modified time for current client is : " << res << endl;


		// TODO - read object
		RSJresource responseObj(res);
		std::string hasError = responseObj["error"].as<std::string>();
		// cout << endl << "Response to update modified time from server : " << hasError;
		if(hasError == "false") {
			// TODO - remove all other files with name licenseInfo
			std::vector<std::string> licenseInfoFiles;
			DIR *dir;
			struct dirent *ent;
			// TODO -> create vector of files with name like licenseInfo
			if ((dir = opendir (appDataFolderPath.c_str())) != NULL) {
				/* print all the files and directories within directory */
				while ((ent = readdir (dir)) != NULL) {
					// printf ("%s\n", ent->d_name);
					std::string actualFileName = ent->d_name;
					std::string licenseFileNameLike = "licenseInfo";
					if (actualFileName.find(licenseFileNameLike) != std::string::npos) {
						// std::cout << "a license file found!" << endl;
						licenseInfoFiles.push_back(actualFileName);
					}
				}
				// cout << endl << "out of the loop; vector generated";
				closedir (dir);
			} else {
				/* could not open directory */
				// perror ("");
			}
			for(int i=0;i< licenseInfoFiles.size();i++) {
				std::string filename = appDataFolderPath + "\\" + licenseInfoFiles.at(i);
				if(filename != tempAppDataFilePath) {
					remove(filename.c_str());
				}
			}
		} else {
			// ? something went wrong while updating time
			// TODO -> newly generated file must be removed
			cout << endl << "error occured while updating in server";
			remove(tempAppDataFilePath.c_str());
		}
	}
}
void setModifiedTimeInDB(std::string key, std::string basepath, std::string serverPath, std::string time)
{
	// std::string cmd = (basepath + "src\\assets\\exe\\setLastModifiedTime.exe" + " " + serverPath + "setLastModifiedTime.php " + key + " " + time);
	// cout << endl
	// << "running command : " << cmd << endl;

	// system(cmd.c_str());
}
void removeLicenseFilesIfMultiple(std::string appDataFolderPath) {
	std::vector<std::string> licenseInfoFiles;
	DIR *dir;
	struct dirent *ent;
	// TODO -> create vector of files with name like licenseInfo
	if ((dir = opendir (appDataFolderPath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			// printf ("%s\n", ent->d_name);
			std::string actualFileName = ent->d_name;
			std::string licenseFileNameLike = "licenseInfo";
			if (actualFileName.find(licenseFileNameLike) != std::string::npos) {
				// std::cout << "a license file found!" << endl;
				licenseInfoFiles.push_back(actualFileName);
			}
		}
		// cout << endl << "out of the loop; vector generated";
		closedir (dir);
	} else {
		/* could not open directory */
		// perror ("");
	}
	if(licenseInfoFiles.size() > 1) {
		for(int i=0;i< licenseInfoFiles.size();i++) {
			// cout << endl << "licenseInfo file : " << licenseInfoFiles.at(i);
			std::string filename = appDataFolderPath + "\\" + licenseInfoFiles.at(i);
			// cout << endl << "match not found while removing : " << filename;
			remove(filename.c_str());
		}
	}
}
std::string getAppDataFilePath(std::string appDataFolderPath) {
	std::vector<std::string> licenseInfoFiles;
	DIR *dir;
	struct dirent *ent;
	// TODO -> create vector of files with name like licenseInfo
	if ((dir = opendir (appDataFolderPath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			// printf ("%s\n", ent->d_name);
			std::string actualFileName = ent->d_name;
			std::string licenseFileNameLike = "licenseInfo";
			if (actualFileName.find(licenseFileNameLike) != std::string::npos) {
				// std::cout << "a license file found!" << endl;
				licenseInfoFiles.push_back(actualFileName);
			}
		}
		// cout << endl << "out of the loop; vector generated";
		closedir (dir);
	} else {
		/* could not open directory */
		// perror ("");
		return "error";
	}
    // for (const auto & entry : fs::directory_iterator(appDataFolderPath)) {
    //     std::string actualFileName = entry.path();
	// 	std::string licenseFileNameLike = "licenseInfo";
	// 	if (actualFileName.find(licenseFileNameLike) != std::string::npos) {
	// 		std::cout << "a license file found!" << endl;
	// 		licenseInfoFiles.push_back(actualFileName);
	// 	}
	// }
	bool shouldRemoveOtherLicenseFiles = false;
	std::string newTempFileName = "licenseInfo.dat";

	if(licenseInfoFiles.size() == 0) {
		newTempFileName = "licenseInfo.dat";
	} else if(licenseInfoFiles.size() == 1) {
		newTempFileName = licenseInfoFiles.at(0);
	} else {
		newTempFileName = "error";
	}
	return newTempFileName;

}
std::string getTempAppDataFilePath(std::string appDataFolderPath) {
	std::vector<string> licenseInfoFiles;
	// TODO -> create vector of files with name like licenseInfo
	DIR *dir;
	struct dirent *ent;
	// TODO -> create vector of files with name like licenseInfo
	if ((dir = opendir (appDataFolderPath.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir (dir)) != NULL) {
			// printf ("%s\n", ent->d_name);
			std::string actualFileName = ent->d_name;
			std::string licenseFileNameLike = "licenseInfo";
			if (actualFileName.find(licenseFileNameLike) != std::string::npos) {
				// std::cout << "a license file found!" << endl;
				licenseInfoFiles.push_back(ent->d_name);
			}
		}
		// cout << endl << "out of loop to read dir";
		closedir (dir);
	} else {
		/* could not open directory */
		// perror ("");
		return "error";
	}

	std::string newTempFileName = "licenseInfo.dat";
	if(licenseInfoFiles.size() == 0) {
		std::string timestamp = to_string(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count());
		newTempFileName = "licenseInfo-" + timestamp + ".dat";
	} else if(licenseInfoFiles.size() == 1) {
		// TODO -> generate file by timestamp
		// auto unixtime_in_ms = duration_cast<milliseconds>(high_resolution_clock::now().time_since_epoch()).count();

		std::string timestamp = to_string(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count());
		newTempFileName = "licenseInfo-" + timestamp + ".dat";
	} else {
		newTempFileName = "error";
	}
	// cout << endl << "returning licenseInfo.dat from getTempAppDataFilePath";
	return newTempFileName;
}
Napi::Object ValidateWithKey(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();

	std::string key = info[0].As<Napi::String>().ToString();
	std::string basepath = info[1].As<Napi::String>().ToString();
	// std::cout << endl << "key: " << key << endl;
	std::string serverPath = info[2].As<Napi::String>().ToString();
	homeDir = info[3].As<Napi::String>().ToString();
	appDataPath = info[4].As<Napi::String>().ToString();


	// std::string tempAppDataPath = info[4].As<Napi::String>().ToString();
	// tempAppDataPath += + "\\licenseInfo-temp.dat";

	removeLicenseFilesIfMultiple(appDataPath);
	std::string appDataFolderPath = appDataPath;
	std::string tempAppDataPath = (appDataPath + "\\" + getTempAppDataFilePath(appDataPath));
	appDataPath = (appDataPath + "\\" +getAppDataFilePath(appDataPath));
	cout << endl << "new appdata file path in VALIDATE WITH KEY : " << appDataPath;
	cout << endl << "new temp appdata file path VALIDATE WITH KEY: " << tempAppDataPath;
	// appDataPath += "\\licenseInfo.dat";

	Napi::Object returnObj = Napi::Object::New(env);
	std::string validateMode = "validate-with-key";

	returnObj = ServerValidation(key, basepath, env, validateMode, serverPath);
	std::string serverResponse = returnObj.Get("ServerPhpResponse").As<Napi::String>().ToString();

	// cout << endl << "response from server : " << serverResponse << endl;
	RSJresource responseObj(serverResponse);
	std::string hasError = responseObj["error"].as<std::string>();
	// cout << endl << "hasError in validate with key: " << hasError;

	if(hasError == "null") {
		std::string isLicenseValid = responseObj["isValid"].as<std::string>();
		// cout << endl << isLicenseValid << endl;
		// MessageBox(0,"Came to validate with key - is license valid : " , "", MB_OK);
		// MessageBox(0,serverResponse.c_str() , "", MB_OK);

		if (isLicenseValid == "true")
		{
			writeResponseToRegistry(responseObj);
			writeResponseToAppDataWrapper(responseObj, appDataPath, tempAppDataPath, basepath, serverPath, key, env, appDataFolderPath);
			// std::string modifiedTime = getLastModifiedTime();
			// std::replace(modifiedTime.begin(), modifiedTime.end(), ' ', '*'); // replace all ' ' to '*'
			// setModifiedTimeInDB(key, basepath, serverPath, modifiedTime);

		}
		else
		{
			removeRegistryContents();
		}
	}

	return returnObj;
}
Napi::Object ValidateWithSerialNumber(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	Napi::Object returnObj = Napi::Object::New(env);
	std::string validateMode = "validate-with-serial-number";
	std::string toRead = "serialNumber";
	std::string serialNumber = Decrypt(readFromRegistry(Encrypt(toRead)));
	std::string basepath = info[1].As<Napi::String>().ToString();
	// std::cout << endl << "key: " << key << endl ;
	std::string serverPath = info[2].As<Napi::String>().ToString();
	returnObj = ServerValidation(serialNumber, basepath, env, validateMode, serverPath);
	// MessageBox(0,"Came to validate with serial number : " , "", MB_OK);
	// MessageBox(0,serialNumber.c_str() , "", MB_OK);
	return returnObj;
}
/**
* check if dates in reg are valid
*
*/
bool areDatesValid()
{
	// ! if lifetime is set to true - always return true
	string startDayKey = "startday", startMonthKey = "startmonth", startYearKey = "startyear";
	std::string lastDayKey = "lastvalidday", lastMonthKey = "lastvalidmonth", lastYearKey = "lastvalidyear";
	std::string endDayKey = "endday", endMonthKey = "endmonth", endYearKey = "endyear";

	std::string isLifetimeValidStr = "isLifetimeValid";
	// cout << endl << "Encrypted isLifetimeValid : " << Encrypt(isLifetimeValidStr);
	// cout << endl << "After reading from registry : " << readFromRegistry(Encrypt(isLifetimeValidStr));
	std::string isLifetimeValid = Decrypt(readFromRegistry(Encrypt(isLifetimeValidStr)));

	//s start day month year
	int startDayReg = stoi(Decrypt(readFromRegistry(Encrypt(startDayKey))));
	int startMonthReg = stoi(Decrypt(readFromRegistry(Encrypt(startMonthKey))));
	int startYearReg = stoi(Decrypt(readFromRegistry(Encrypt(startYearKey))));
	// end day month year
	int endDayReg = stoi(Decrypt(readFromRegistry(Encrypt(endDayKey))));
	int endMonthReg = stoi(Decrypt(readFromRegistry(Encrypt(endMonthKey))));
	int endYearReg = stoi(Decrypt(readFromRegistry(Encrypt(endYearKey))));
	// last day month year
	int lastDayReg = stoi(Decrypt(readFromRegistry(Encrypt(lastDayKey))));
	int lastMonthReg = stoi(Decrypt(readFromRegistry(Encrypt(lastMonthKey))));
	int lastYearReg = stoi(Decrypt(readFromRegistry(Encrypt(lastYearKey))));

	time_t t = time(NULL);
	tm *timePtr = localtime(&t);

	int currday = timePtr->tm_mday;
	int currmonth = timePtr->tm_mon + 1;
	int curryear = timePtr->tm_year + 1900;

	// cout << endl << "is this valid for lifetime : " << isLifetimeValid << endl;
	if (isLifetimeValid == "true")
	{
		// std::string lastday="lastvalidday";
		// writeToRegistry(Encrypt(lastday),Encrypt(to_string(currday)));
		// std::string lastmonth="lastvalidmonth";
		// writeToRegistry(Encrypt(lastmonth),Encrypt(to_string(currmonth)));
		// std::string lastyear="lastvalidyear";
		// writeToRegistry(Encrypt(lastyear),Encrypt(to_string(curryear)));
		return true;
	}
	else
	{
		// cout << endl << "Not Valid for Lifetime : " << endl;

		if (curryear > endYearReg)
		{
			return false;
		}
		else if (currmonth > endMonthReg && curryear == endYearReg)
		{
			return false;
		}
		else if (curryear == endYearReg && currmonth == endMonthReg && currday > endDayReg)
		{
			return false;
		}
		else
		{
			if (curryear < lastYearReg)
			{
				return false;
			}
			else if (currmonth < lastMonthReg && curryear == lastYearReg)
			{
				return false;
			}
			else if (curryear == lastYearReg && currmonth == lastMonthReg && currday < lastDayReg)
			{
				return false;
			}
			else
			{
				std::string lastday = "lastvalidday";
				writeToRegistry(Encrypt(lastday), Encrypt(to_string(currday)));
				std::string lastmonth = "lastvalidmonth";
				writeToRegistry(Encrypt(lastmonth), Encrypt(to_string(currmonth)));
				std::string lastyear = "lastvalidyear";
				writeToRegistry(Encrypt(lastyear), Encrypt(to_string(curryear)));
				return true;
			}
		}
	}
}
/**
* Compares contents in licenseInfo.dat present in appdata folder with contents from registry
* @return - true - if contents are similar
* @return - false - if contents are not similar
*
*/
bool areContentsSimilar(std::string appDataFilePath)
{
	// keys in file
	bool areSimilar = true;
	string startDayKey = "startday", startMonthKey = "startmonth", startYearKey = "startyear";
	std::string lastDayKey = "lastvalidday", lastMonthKey = "lastvalidmonth", lastYearKey = "lastvalidyear";
	std::string endDayKey = "endday", endMonthKey = "endmonth", endYearKey = "endyear", serialNumberKey = "serialNumber";

	// Read Values from Registry
	// serialnumber
	std::string serialNumberReg = Decrypt(readFromRegistry(Encrypt(serialNumberKey)));
	// start day month year
	std::string startDayReg = Decrypt(readFromRegistry(Encrypt(startDayKey)));
	std::string startMonthReg = Decrypt(readFromRegistry(Encrypt(startMonthKey)));
	std::string startYearReg = Decrypt(readFromRegistry(Encrypt(startYearKey)));
	// end day month year
	std::string endDayReg = Decrypt(readFromRegistry(Encrypt(endDayKey)));
	std::string endMonthReg = Decrypt(readFromRegistry(Encrypt(endMonthKey)));
	std::string endYearReg = Decrypt(readFromRegistry(Encrypt(endYearKey)));

	// ! Do not compare last days because last days are going to be updated

	// cout << endl << "appdata file path while comparing is : " << appDataFilePath;

	std::string serialNumberFile = Decrypt(readFromAppDataFile(Encrypt(serialNumberKey), appDataFilePath));

	std::string startDayFile = Decrypt(readFromAppDataFile(Encrypt(startDayKey), appDataFilePath));
	std::string startMonthFile = Decrypt(readFromAppDataFile(Encrypt(startMonthKey), appDataFilePath));
	std::string startYearFile = Decrypt(readFromAppDataFile(Encrypt(startYearKey), appDataFilePath));
	// end day month year
	std::string endDayFile = Decrypt(readFromAppDataFile(Encrypt(endDayKey), appDataFilePath));
	std::string endMonthFile = Decrypt(readFromAppDataFile(Encrypt(endMonthKey), appDataFilePath));
	std::string endYearFile = Decrypt(readFromAppDataFile(Encrypt(endYearKey), appDataFilePath));
	// // ! compare without decrypt
	// std::string startDayFile = (readFromAppDataFile((startDayKey), appDataFilePath));
	// std::string startMonthFile = (readFromAppDataFile((startMonthKey), appDataFilePath));
	// std::string startYearFile = (readFromAppDataFile((startYearKey), appDataFilePath));
	// // end day month year
	// std::string endDayFile = (readFromAppDataFile((endDayKey), appDataFilePath));
	// std::string endMonthFile = (readFromAppDataFile((endMonthKey), appDataFilePath));
	// std::string endYearFile = (readFromAppDataFile((endYearKey), appDataFilePath));

	// Now compare
	// cout << "SerialNumber compare : " << serialNumberReg << " : " << serialNumberFile << endl;

	// cout << "startDay compare : " << startDayReg << " : " << startDayFile << endl;
	// cout << "startMonth compare : " << startMonthReg << " : " << startMonthFile << endl;
	// cout << "startYear compare : " << startYearReg << " : " << startYearFile << endl;

	// cout << "endDay compare : " << endDayReg << " : " << endDayFile << endl;
	// cout << "endMonth compare : " << endMonthReg << " : " << endMonthFile << endl;
	// cout << "endYear compare : " << endYearReg << " : " << endYearFile << endl;

	if (serialNumberReg != serialNumberFile)
		areSimilar = false;

	if (startDayReg != startDayFile)
		areSimilar = false;
	if (startMonthReg != startMonthFile)
		areSimilar = false;
	if (startYearReg != startYearFile)
		areSimilar = false;

	if (endDayReg != endDayFile)
		areSimilar = false;
	if (endMonthReg != endMonthFile)
		areSimilar = false;
	if (endYearReg != endYearFile)
		areSimilar = false;

	return areSimilar;
}
/**
* Compares contents from registry with contents from appdatafile
* Checks if dates are valid in both registry and appdatafile
* @param {string} - appDataPath - path to appData folder
*
*/
Napi::Object ValidateOffline(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	Napi::Object returnObj = Napi::Object::New(env);
	bool shouldRemoveRegistry = false;
	std::string appDataFilePath = info[2].As<Napi::String>().ToString();
	appDataFilePath = appDataFilePath + "\\" + getAppDataFilePath(appDataFilePath);
	// std::string appDataFilePath = info[2].As<Napi::String>().ToString();
	// appDataFilePath += "\\licenseInfo.dat";


	// NOTE -> NEW
	// ? check if licenseInfo.dat was manipulated

	bool licenseWasManipulated = compareLicenseDatIntegrity(appDataFilePath);
	cout << endl << "was license manipulated ? : " << licenseWasManipulated << endl;
	if(licenseWasManipulated == true) {
		returnObj.Set("areContentsSimilar", false);
		returnObj.Set("areDatesValid", false);
	} else {

		// cout << endl << "was license manipulated : " << licenseWasManipulated;
		bool areSimilar = areContentsSimilar(appDataFilePath);
		// cout << endl << "Are contents similar : " << areSimilar << endl;
		if (areSimilar == true)
		{
			returnObj.Set("areContentsSimilar", true);
			bool areValid = areDatesValid();
			if (areValid == true)
			{
				returnObj.Set("areDatesValid", true);
			}
			else
			{
				returnObj.Set("areDatesValid", false);
				shouldRemoveRegistry = true;
			}
		}
		else
		{
			returnObj.Set("areContentsSimilar", false);
			shouldRemoveRegistry = true;
		}
		if (shouldRemoveRegistry == true)
		{
			removeRegistryContents();
		}
	}
	return returnObj;
}
/**
* Compares contents from registry with contents from appdatafile
* Checks if dates are valid in both registry and appdatafile
* Compares if registry date modified time is equal to registry modified time present in database
* @param {string} - appDataPath - path to appData folder
*/
Napi::Object ValidateOnline(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	Napi::Object returnObj = Napi::Object::New(env);
	bool shouldRemoveRegistry = false;
	std::string toRead = "serialNumber";
	std::string serialNumber = readFromRegistry(Encrypt(toRead));


	std::string basepath = info[0].As<Napi::String>().ToString();
	std::string serverPath = info[1].As<Napi::String>().ToString();
	std::string appDataFilePath = info[2].As<Napi::String>().ToString();

	removeLicenseFilesIfMultiple(appDataFilePath);
	std::string appDataFolderPath = appDataFilePath;
	std::string tempAppDataPath = (appDataFilePath + "\\" + getTempAppDataFilePath(appDataFilePath));
	std::string appDataFileName = getAppDataFilePath(appDataFilePath);
	appDataFilePath = appDataFilePath + "\\" + appDataFileName;
	// appDataFilePath += "\\licenseInfo.dat";
	cout << endl << "Generated AppData file path is  in VALIDATE ONLINE: " << appDataFilePath;
	cout << endl << "Generated Temp AppData file path is  in VALIDATE ONLINE: " << tempAppDataPath;


	// NOTE -> NEW
	// ? check if licenseInfo.dat was manipulated

	bool licenseWasManipulated = compareLicenseDatIntegrity(appDataFilePath);
	cout << endl << "was license manipulated : " << licenseWasManipulated;

	// NOTE -> case to be noted here ->
	// ? for our current clients, they have licenseInfo.dat
	// ? allow if the appDataFiileName is licenseInfo.dat
	// ? after the response it will be auto updated to licenseInfo-1.dat
	if(appDataFileName == "licenseInfo.dat") {
		licenseWasManipulated = false;
	}

	if(licenseWasManipulated == true) {
		returnObj.Set("regNotFound", true);
	} else if (serialNumber == "main-key-not-present" || serialNumber == "sub-key-not-present") {
		returnObj.Set("regNotFound", true);
		shouldRemoveRegistry = true;
	}
	else {
		// validate values from server
		std::string validateMode = "validate-with-serial-number";
		Napi::Object serverValidationResponse = ServerValidation(Decrypt(serialNumber), basepath, env, validateMode, serverPath);

		// write response values from server to registry -
		// to ensure that when validity is edited at server side - it gets replicated locally as well
		std::string serverResponse = serverValidationResponse.Get("ServerPhpResponse").As<Napi::String>().ToString();
		RSJresource responseObj(serverResponse);

		// ! NOTE -> responseObj might contain key 'error' , when this happens
		// ! you issue was with server, this does not mean the license is not valid
		// ? in such case show such error, do not remove license information
		std::string hasError = responseObj["error"].as<std::string>();
		// cout << endl << "hasError : " << hasError;
		if(hasError != "null") {
			shouldRemoveRegistry = false;
			returnObj.Set("ServerValidation", serverValidationResponse);
			bool areSimilar = areContentsSimilar(appDataFilePath);
			// cout << endl << "Are contents similar : " << areSimilar << endl;
			if (areSimilar == true)
			{
				returnObj.Set("areContentsSimilar", true);
				bool areValid = areDatesValid();
				// cout << endl << "Are dates valid : " << areValid << endl;
				if (areValid == true)
				{
					returnObj.Set("areDatesValid", true);
				}
				else
				{
					returnObj.Set("areDatesValid", false);
					shouldRemoveRegistry = true;
				}
			}
			else
			{
				returnObj.Set("areContentsSimilar", false);
				shouldRemoveRegistry = true;
			}
		} else {
			std::string isLicenseValid = responseObj["isValid"].as<std::string>();
			// cout << endl << "is my license valid ? " << isLicenseValid << endl;
			if (isLicenseValid == "true")
			{
				// cout << endl << "License is valid now write to registry and appdatafile" << endl;
				writeResponseToRegistry(responseObj);
				std::string key = responseObj["key"].as<std::string>();
				writeResponseToAppDataWrapper(responseObj, appDataPath, tempAppDataPath, basepath, serverPath, key, env, appDataFolderPath);

				// ? NOTE if server responds then we change the licenseInfo file
				// ? thus now the appDataFilePath should be updated for the same reason
				appDataFilePath = tempAppDataPath;

				// writeResponseToAppDataWrapper(responseObj, appDataFilePath);
				// std::string modifiedTime = getLastModifiedTime();
				// std::replace(modifiedTime.begin(), modifiedTime.end(), ' ', '*'); // replace all ' ' to '*'
				// setModifiedTimeInDB(key,basepath,serverPath,modifiedTime);
			}
			else
			{
				removeRegistryContents();
			}

			returnObj.Set("ServerValidation", serverValidationResponse);
			bool areSimilar = areContentsSimilar(appDataFilePath);
			// cout << endl << "Are contents similar : " << areSimilar << endl;
			if (areSimilar == true)
			{
				returnObj.Set("areContentsSimilar", true);
				bool areValid = areDatesValid();
				// cout << endl << "Are dates valid : " << areValid << endl;
				if (areValid == true)
				{
					returnObj.Set("areDatesValid", true);
				}
				else
				{
					returnObj.Set("areDatesValid", false);
					shouldRemoveRegistry = true;
				}
			}
			else
			{
				returnObj.Set("areContentsSimilar", false);
				shouldRemoveRegistry = true;
			}
		}

	}
	// cout << endl << "Should remove registry ? " << shouldRemoveRegistry << endl;
	if (shouldRemoveRegistry == true)
	{
		removeRegistryContents();
	}
	return returnObj;
}

Napi::Object setSerialNumberInRegistry(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	Napi::Object returnObj = Napi::Object::New(env);
	std::string basepath = info[0].As<Napi::String>().ToString();

	createMainKey();

	std::string toRead = "serialNumber";
	std::string serialNumber = Decrypt(readFromRegistry(Encrypt(toRead)));
	// std::cout << "serialNumber read from registry is : " << serialNumber;
	if(serialNumber.length() == 2) {
		// ? this means serial number was not found in registry but empty string got decrypted as [(pound symbol)
		// ? so its length is 2
		// std::string serialNumber = GenerateSerialNumber(basepath, env);
		// ? mask serial number so that we replicate user scenario where user is not
		// ? already registered
		std::string serialNumber = "BLABLABLABLAL";

		std::string serialNumberStr = "serialNumber";
		writeToRegistry(Encrypt(serialNumberStr), Encrypt(serialNumber));
	}

	returnObj.Set("serialNumberSetInRegistry", true);
	return returnObj;
}
Napi::Object generateTrialLicense(const Napi::CallbackInfo &info) {
	Napi::Env env = info.Env();
	std::string email = info[0].As<Napi::String>().ToString();
	std::string name = info[1].As<Napi::String>().ToString();
	std::string lifetimeValidity = "false";
	std::string days = "7";
	std::string months = "0";
	std::string years = "0";
	std::string licenseCount = "1";


	std::string basepath = info[2].As<Napi::String>().ToString();
	std::string serverPath = info[3].As<Napi::String>().ToString();
	std::string toRead = "serialNumber";
	std::string serialNumber = Decrypt(readFromRegistry(Encrypt(toRead)));

	std::string cmd = "\"" + basepath + "src\\assets\\exe\\http.exe\"" + " " + serverPath + "validation/add-new-trial-license" + " get-trial-license " + serialNumber + " "+email + " " + name + " " + lifetimeValidity + " " + days + " " + months + " " + years + " " + licenseCount;
	// ? enclose command in qoutes ; for the directory structure might contain spaces
	// cmd = "\"" + cmd + "\"";
	// cout << endl << "executing command : " << cmd;
	Napi::Object returnObj = Napi::Object::New(env);
	std::string res = getObjectStr(RunCommand(cmd, env));
	returnObj.Set("response", res);
	returnObj.Set("command",cmd);
	return returnObj;
}

Napi::Object Init(Napi::Env env, Napi::Object exports)
{
	exports.Set(Napi::String::New(env, "validateWithKey"),
		Napi::Function::New(env, ValidateWithKey));
	exports.Set(Napi::String::New(env, "validateWithSerialNumber"),
		Napi::Function::New(env, ValidateWithSerialNumber));
	exports.Set(Napi::String::New(env, "validateOffline"),
		Napi::Function::New(env, ValidateOffline));
	exports.Set(Napi::String::New(env, "validateOnline"),
		Napi::Function::New(env, ValidateOnline));
	exports.Set(Napi::String::New(env, "setSerialNumberInRegistry"),
		Napi::Function::New(env, setSerialNumberInRegistry));
	exports.Set(Napi::String::New(env, "generateTrialLicense"),
		Napi::Function::New(env, generateTrialLicense));
	return exports;
}

NODE_API_MODULE(hello, Init)
