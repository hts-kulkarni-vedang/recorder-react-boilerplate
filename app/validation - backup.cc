#include <napi.h>
#include <ctype.h>
#include <iostream>
#include <sstream>
#include <direct.h>
#include "commandFunctions_validate.h"

#include <psapi.h>
#include <errno.h>
#include <array>
#include <windows.h>
#include <iterator>
#include <cstdlib>
#include <stdlib.h>
#include <wininet.h>
#include <tchar.h>
#include <fstream>
#include <Sensapi.h>
#include <cstdio>
#include <memory>
#include <stdexcept>
#include <algorithm>
#include "RSJparser.tcc"
#include <sstream>
#include <regex>

#include <chrono>
#include <thread>
#include <functional>

#pragma comment(lib, "Sensapi.lib")
// states for stopTimerFlag
// 0 - timer not started
// 1 - timer stopped from command output
// 2 - timer stopped because command did not send response
// 3 - timer terminated
int stopTimerFlag = 0;
// wait time for command to respond;
int COMMAND_RUN_WAIT_TIME = 5000;
// std::unique_ptr<FILE, decltype(&_pclose)> globalPipe;
FILE *pipe = NULL;
std::string exec(const char* cmd) {
	std::array<char, 128> buffer;
	std::string result;
	std::unique_ptr<FILE, decltype(&_pclose)> pipe1(_popen(cmd, "r"), _pclose);
	// globalPipe = pipe;
	if (!pipe1) {
		throw std::runtime_error("popen() failed!");
	}
	while (fgets(buffer.data(), buffer.size(), pipe1.get()) != nullptr) {
		result += buffer.data();
	}
	return result;
}
void timer_start(std::function<void(void)> func, unsigned int interval){
    std::thread([func, interval]() {
        if(stopTimerFlag == 0){
            std::this_thread::sleep_for(std::chrono::milliseconds(interval));
            stopTimerFlag = 2;
            func();
        }
    }).detach();
}
void execOnlyUntilSecondsResponse(std::string response){
	cout << endl << "stop timer flag : " << stopTimerFlag << endl;

	if(response == "command-did-not-repond"){
		// _pclose();
		cout << endl << "command did not respond after specific time" << endl;
		if(pipe){
	    	_pclose(pipe);
	    }
	}else if(stopTimerFlag != 3){
		cout << endl << "Command responded with result : " << response << endl;
	}
	// operation completed
	stopTimerFlag = 3;
}
void waitForCommandToRun(){
    std::cout << "command did not respond after " << COMMAND_RUN_WAIT_TIME << " milliseconds" << std::endl;
    stopTimerFlag = 3;
    execOnlyUntilSecondsResponse("command-did-not-repond");
}
FILE * popen2(string command, string type, int & pid){
    pid_t child_pid;
    int fd[2];
    pipe(fd);

    if((child_pid = fork()) == -1){
        perror("fork");
        exit(1);
    }

    /* child process */
    if (child_pid == 0){
        if (type == "r"){
            close(fd[READ]);    //Close the READ end of the pipe since the child's fd is write-only
            dup2(fd[WRITE], 1); //Redirect stdout to pipe
        }
        else{
            close(fd[WRITE]);    //Close the WRITE end of the pipe since the child's fd is read-only
            dup2(fd[READ], 0);   //Redirect stdin to pipe
        }

        setpgid(child_pid, child_pid); //Needed so negative PIDs can kill children of /bin/sh
        execl("C::\\WINDOWS\\SYSTEM32\\CMD.EXE", "cmd.exe" "-c", command.c_str(), NULL);
        exit(0);
    }
    else{
        if (type == "r"){
            close(fd[WRITE]); //Close the WRITE end of the pipe since parent's fd is read-only
        }
        else{
            close(fd[READ]); //Close the READ end of the pipe since parent's fd is write-only
        }
    }

    pid = child_pid;

    if (type == "r"){
        return fdopen(fd[READ], "r");
    }

    return fdopen(fd[WRITE], "w");
}
int pclose2(FILE * fp, pid_t pid){
    int stat;
    fclose(fp);
    while (waitpid(pid, &stat, 0) == -1){
        if (errno != EINTR){
            stat = -1;
            break;
        }
    }
    return stat;
}
void execOnlyUntilSeconds(const char* cmd) {
	char buffer[128];
    std::string result = "";
	// std::unique_ptr<FILE, decltype(&_pclose)> pipe(_popen(cmd, "r"), _pclose);

	pipe = _popen(cmd, "r");

	// start timer after pipe creation
	timer_start(waitForCommandToRun, COMMAND_RUN_WAIT_TIME);
	// if (!pipe) {
	// 	throw std::runtime_error("popen() failed!");
	// }
	// while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
	// 	// if the command responds after limit time ,  do not proceed 
	// 	std::cout << "Got some data : " << endl;
	// 	_pclose(pipe);		
	// 	if(stopTimerFlag == 3){
	// 		break;
	// 	}
	// 	result += buffer.data();
	// }
	if (!pipe) throw std::runtime_error("popen() failed!");
    try {
    	
        while (fgets(buffer, sizeof buffer, pipe) != NULL) {
        	if(stopTimerFlag == 3){
				throw;
			}
            result += buffer;
        }
    } catch (...) {
    	if(pipe){
        	_pclose(pipe);
    	}
        throw;
    }
    if(pipe){
    	_pclose(pipe);
    }

	// if the command responds after limit time ,  do not proceed 
	if(stopTimerFlag != 3){
		execOnlyUntilSecondsResponse(result);
	}
	// return result;
}

std::string ReplaceString(std::string subject, const std::string& search,
	const std::string& replace) {
	size_t pos = 0;
	while((pos = subject.find(search, pos)) != std::string::npos) {
		subject.replace(pos, search.length(), replace);
		pos += replace.length();
	}
	return subject;
}

#include "encfileop.h"
// std::string homeDir;
HANDLE hChildProcess; //NULL
HANDLE hStdIn; // Handle to parents std input. NULL
BOOL bRunThread; 
std::string (*strCommands);

int noOfCommands=1; 
int commandCounter = 1;

HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
HANDLE hInputWriteTmp,hInputRead,hInputWrite;
HANDLE hErrorWrite;
HANDLE hThread;
DWORD ThreadId;


std::string RunCommand(std::string cmd, Napi::Env env);
std::string GetRamSerialNumber(Napi::Env env);
std::string curlPath;
std::string serverPath;

std::string RunCommand(std::string cmd, Napi::Env env)
{
	strCommands = new std::string[noOfCommands];

	strCommands[0] = cmd;

  std::string outPutfileName= homeDir+"\\log.txt";  // A file is generated to store output from server.

  /*Security certficates*/
  SECURITY_ATTRIBUTES sa;
  sa.nLength= sizeof(SECURITY_ATTRIBUTES);
  sa.lpSecurityDescriptor = NULL;
  sa.bInheritHandle = TRUE;
  /*CreatePipe() returns read handle and write handle for the pipe. A process creates a pipe
  just before it forks one or more child processes. The pipe is then used for communication
  either between the parent or child processes, or between two sibling processes*/
  if (!CreatePipe(&hOutputReadTmp,&hOutputWrite,&sa,0))
  	DisplayError("CreatePipe");

  // Create a duplicate of the output write handle for the std error
  // write handle. This is necessary in case the child application
  // closes one of its std output handles.
  if (!DuplicateHandle(GetCurrentProcess(),hOutputWrite,
  	GetCurrentProcess(),&hErrorWrite,0,
  	TRUE,DUPLICATE_SAME_ACCESS))
  	DisplayError("DuplicateHandle");

  // Create the child input pipe.
  if (!CreatePipe(&hInputRead,&hInputWriteTmp,&sa,0))
  	DisplayError("CreatePipe");

  // Create new output read handle and the input write handles. Set
  // the Properties to FALSE. Otherwise, the child inherits the
  // properties and, as a result, non-closeable handles to the pipes
  // are created.
  if (!DuplicateHandle(GetCurrentProcess(),hOutputReadTmp,
  	GetCurrentProcess(),
                       &hOutputRead, // Address of new handle.
                       0,FALSE, // Make it uninheritable.
                       DUPLICATE_SAME_ACCESS))
  	DisplayError("DupliateHandle");

  if (!DuplicateHandle(GetCurrentProcess(),hInputWriteTmp,
  	GetCurrentProcess(),
                           &hInputWrite, // Address of new handle.
                           0,FALSE, // Make it uninheritable.
                           DUPLICATE_SAME_ACCESS))
  	DisplayError("DupliateHandle");


  // Close inheritable copies of the handles you do not want to be
  // inherited.
  if (!CloseHandle(hOutputReadTmp)) DisplayError("CloseHandle");
  if (!CloseHandle(hInputWriteTmp)) DisplayError("CloseHandle");
          // Get std input handle so you can close it and force the ReadFile to
          // fail when you want the input thread to exit.
  if ( (hStdIn = GetStdHandle(STD_INPUT_HANDLE)) ==
  	INVALID_HANDLE_VALUE )
  	DisplayError("GetStdHandle");

   //for creating process  

  PrepAndLaunchRedirectedChild(hOutputWrite,hInputRead,hErrorWrite,strCommands,commandCounter,hChildProcess,env);
  
  // Close pipe handles (do not continue to modify the parent).
  // You need to make sure that no handles to the write end of the
  // output pipe are maintained in this process or else the pipe will
  // not close when the child process exits and the ReadFile will hang.
  if (!CloseHandle(hOutputWrite)) DisplayError("CloseHandle");
  if (!CloseHandle(hInputRead )) DisplayError("CloseHandle");
  if (!CloseHandle(hErrorWrite)) DisplayError("CloseHandle");

        //Launch the thread that gets the input and sends it to the child.
        //DWORD getAndSendInputThread;
  SetAndSendInputThread(hStdIn,bRunThread);

  hThread = CreateThread(NULL,0,&GetAndSendInputThread,
  	(LPVOID)&hInputWrite,0,&ThreadId);
  if (hThread == NULL) 
  	DisplayError("CreateThreadnn");

  //for writing output on command prompt and in files       // Read the child's output.
  // std::cout<<outPutfileName;

  std::string responseText = ReadAndHandleOutput(env,hOutputRead, outPutfileName);
  

  // Redirection is complete
  // Tell the thread to exit and wait for thread to die.

  bRunThread = FALSE;
  
  SetAndSendInputThread(hStdIn,bRunThread);//setting bRunthread's value

  if (WaitForSingleObject(hThread,INFINITE) == WAIT_FAILED)
  	DisplayError("WaitForSingleObject");

  if (!CloseHandle(hStdIn)) DisplayError("CloseHandle");
  SetAndSendInputThread(hStdIn,bRunThread);

  if (!CloseHandle(hOutputRead)) DisplayError("CloseHandle");
  if (!CloseHandle(hInputWrite)) DisplayError("CloseHandle");
  return responseText;

}

//checks if serial number contains spaces and returns memorychip serial number if former is true
std::string checkSerialNumber(std::string serialNumber,Napi::Env env){
	std::size_t vmwareFound = serialNumber.find("VMware");
	std::size_t found = serialNumber.find("*");

	if(vmwareFound!=std::string::npos){
        // do nothing
	}else{
		if (found!=std::string::npos){
			std::cout << "first '*' found at: " << found << '\n';
            // serialnumber is invalid
			serialNumber = GetRamSerialNumber(env);
		}
	}
	return serialNumber;
}
// returns SerialNumber if found
// else returns "serial-number-not-found"
std::string GetSerialNumber(Napi::Env env){
	std::string serialNumber = exec("wmic bios get serialnumber");
  // paresh serialnumber remove this
	std::istringstream tmp(serialNumber);

	std::string final;
	std::string line;    
	int counter = 0;
	while (std::getline(tmp, line)) {

		const std::string nothing = "" ;
		line = std::regex_replace( line, std::regex( "^\\s+" ), nothing ) ;
		line = std::regex_replace( line, std::regex( "\\s+$" ), nothing ) ;
        // std::cout << endl << "Line no - " << counter << "->"  << line << std::endl;
        // std::cout << endl << "Length of lie  no - " << counter << "->"  << line.length() << std::endl;
		if(line.length() != 0 && line != "SerialNumber"){
			final += line;
		}
		counter++;
	}
    std::replace( final.begin(), final.end(), ' ', '*'); // replace all 'x' to 'y'
    std::cout << endl << "returning final ram serialNumber bios : " << final << endl ;

    // if(final.length() == 0){
    //  final = "serial number not found";
    // }
    final = checkSerialNumber(final,env);
    std::cout << endl << "returning final serialNumber bios : " << final << endl ;
    return final;
}
std::string GetRamSerialNumber(Napi::Env env){
	std::string serialNumber = exec("wmic memorychip get serialnumber");
	std::istringstream tmp(serialNumber);

	std::string final;
	std::string line;    
	while (std::getline(tmp, line)) {

		const std::string nothing = "" ;
		line = std::regex_replace( line, std::regex( "^\\s+" ), nothing ) ;
		line = std::regex_replace( line, std::regex( "\\s+$" ), nothing ) ;
        // std::cout << endl << "Line no - " << counter << "->"  << line << std::endl;
        // std::cout << endl << "Length of lie  no - " << counter << "->"  <<  << std::endl;
		if(line.length() != 0 && line != "SerialNumber"){
			final += line;
		}
	}
    std::replace( final.begin(), final.end(), ' ', '*'); // replace all 'x' to 'y'
    
 //  std::cout << endl << "returning final ram serialNumber bios : " << final << endl ;

	// if(final.length() == 0){
	// final = "serial number not found";
	// }
    std::cout << endl << "returning final ram serialNumber bios : " << final << endl ;
    return final;
}

// returns path of curl.exe if present
// if curl path not found returns "file-not-found"
std::string GetCurlPath(std::string basepath)
{
	std::string path;
    // std::cout <<basepath<<"\n";

	if((sizeof(void *) * 8) == 32) {
		path = basepath+ "src\\assets\\exe\\http";
	}
	else{
		path = basepath+ "src\\assets\\exe\\http";
	}
    // cout << path << endl;
	std::ifstream infile((path+".exe").c_str());
    // std::cout << infile.good() << " is the result";
    // if(infile.good()){
    //     // return "true";
    // }
    // else{
    //     path = "file-not-found";
    // }
	return path;
}

std::string getObjectStr(std::string s){
    // std::cout << "Check contetnts : "<<endl;
	std::string result;
	std::size_t found = s.find_last_of('}');
    // cout << "last index of } is : " << found;
	for (std::string::size_type i = 0; i < s.size(); i++) {
        // std::cout << s[i] << "->";
		result+= s[i];
		if(s[i] == '}' && i == found){
            // cout << "end found";
			break;
		}
	}
    // cout << endl <<"trimmed result is : " <<result <<endl;
	return result;
}
// Used while activating using key
Napi::Object ServerValidation(std::string key, std::string basepath,  Napi::Env env)
{
	std::string cmd = "";
	Napi::Object returnObj=Napi::Object::New(env);
	curlPath = GetCurlPath(basepath);
	if(curlPath.compare("file-not-found") == 0){
		returnObj.Set("CurlError","http.exe not found");
	}else{
		std::string serialNumber = GetSerialNumber(env);

		returnObj.Set("SerialNumber",serialNumber);
		cout << endl <<"sending serial number : " << serialNumber << endl;
        // serialNumber = "\"" + serialNumber + "\"";
		serialNumber = ReplaceString(serialNumber," ","*");
        // serialNumber = ReplaceString(serialNumber,"\r","");
        // serialNumber = ReplaceString(serialNumber,"\r","");

		cmd = curlPath+" "+serverPath+"recorder-validation-test.php"+" activation "+key+" "+serialNumber;
		std::string res = getObjectStr(RunCommand(cmd, env));
		cout << endl << "server response : "<< res << endl;
		returnObj.Set("ServerPhpResponse",res);
	}
	return returnObj;
}

// used while validating without key
std::string GlobalValidation(std::string basepath,  Napi::Env env)
{
	std::string cmd = "";

	curlPath = GetCurlPath(basepath);

  // std::string serialNumber = GetSerialNumber(env);
	std::string serialNumberFromDatFileStr = "serialNumber";
	std::string serialNumberFromDatFile = readfromfile(serialNumberFromDatFileStr);  
	cout<< endl  << "sending serialnumber for validation: " <<  serialNumberFromDatFile<<endl;
   // serialNumberFromDatFile = "\"" + serialNumberFromDatFile + "\"";
	serialNumberFromDatFile = ReplaceString(serialNumberFromDatFile," ","*");
        // serialNumberFromDatFile = ReplaceString(serialNumberFromDatFile,"\r","");
        // serialNumberFromDatFile = ReplaceString(serialNumberFromDatFile,"\r","");
	cmd = curlPath+" "+serverPath+"recorder-validation-test.php"+" validation "+serialNumberFromDatFile;

  // cout << "Running command : " << cmd;
	return RunCommand(cmd, env);
}

bool checkLocalSerialNumber(std::string basepath,Napi::Env env){
	bool isValid = false;
	std::string serialNumberStr = "serialNumber";
	std::string isSerialNumberInvalidStr = "isSerialNumberInvalid";

	std::string serialNumberFromDatFile = readfromfile((serialNumberStr));
	std::string isSerialNumberInvalid = readfromfile((isSerialNumberInvalidStr));

	cout << endl<< " serialNumberFromDatFile : " << serialNumberFromDatFile <<endl;
	cout << endl<< " isSerialNumberInvalid : " << isSerialNumberInvalid <<endl;

	if(isSerialNumberInvalid.compare("true")==0){
      	// validate using ramSerialNumber
		std::string ramSerialNumberFromPC = GetRamSerialNumber(env);
		std::string ramSerialNumberStr = "ramSerialNumber";
		std::string ramSerialNumberFromDatFile = readfromfile((ramSerialNumberStr));

		cout << endl<< " ramSerialNumberFromPC : " << ramSerialNumberFromPC <<endl;
		cout << endl<< " ramSerialNumberFromDatFile : " << ramSerialNumberFromDatFile <<endl;


		if(ramSerialNumberFromPC.compare(ramSerialNumberFromDatFile) == 0){
			isValid = true;
		}else{
			isValid = false;
		}
	}else{
      // validate using serialNumber
		std::string serialNumberFromPC = GetSerialNumber(env);
		if(serialNumberFromPC.compare(serialNumberFromDatFile) == 0){
			isValid = true;
		}else{
			isValid = false;
		}
	}
	return isValid;

	return true;
}



// everytime the app starts and internet is connected

Napi::Object StartUpValidation(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	Napi::Object returnObj=Napi::Object::New(env);

	std::string basepath = info[0].As<Napi::String>().ToString();
	std::string isLocalFilePresent = info[3].As<Napi::String>().ToString();
	homeDir = info[2].As<Napi::String>().ToString();

	serverPath = info[1].As<Napi::String>().ToString();

    // cout << "now verifyingglobal()";
	DWORD dwSens;
	if (IsNetworkAlive(&dwSens) == TRUE){
		std::string response = GlobalValidation(basepath, env);

    // std::cout<<response;
		returnObj.Set("ServerValidation",response);
	}
	bool isLocalValid = verifyLocal(basepath);
	cout << endl << "verifyLocal : " << isLocalValid << endl;
	if(isLocalValid){
		isLocalValid = checkLocalSerialNumber(basepath,env);
		cout << endl << "checkLocalSerialNumber : " << isLocalValid << endl;

	}
	returnObj.Set("LocalValidation",isLocalValid);

	return returnObj;
}

void writeRamSerialNumberToDatFile(Napi::Env env){
	std::string ramSerialNumber = GetRamSerialNumber(env);
	cout << endl << "ramSerialNumberFromPC : " << ramSerialNumber << endl;
	std::string ramSerialNumberStr = "ramSerialNumber";
	writetofile(ramSerialNumberStr,ramSerialNumber);
}

Napi::Object InitialValidation(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();

	std::string key = info[0].As<Napi::String>().ToString();  
	std::string basepath = info[1].As<Napi::String>().ToString();
	// std::cout << endl << "key: " << key << endl ;
	serverPath = info[2].As<Napi::String>().ToString();
	homeDir = info[3].As<Napi::String>().ToString();

	Napi::Object returnObj=Napi::Object::New(env);
	returnObj =  ServerValidation(key, basepath, env);
	std::string serverResponse = returnObj.Get("ServerPhpResponse").As<Napi::String>().ToString();

	RSJresource responseObj (serverResponse);
	std::string isLicenseValid = responseObj["isValid"].as<std::string>();
	cout << endl << isLicenseValid << endl;
	if(isLicenseValid == "true"){
		saveFile(serverResponse,homeDir);   
		// writeRamSerialNumberToDatFile(env);
	}else{

	}

	return returnObj;
}

Napi::String CredentialsAvailable(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	std::string documentsFolderPath = info[0].As<Napi::String>().ToString();
	std::string temp = isFilePresent(documentsFolderPath);
	Napi::String str =  Napi::String::New(env, temp);
	return str;
}

Napi::Object ValidateLocal(const Napi::CallbackInfo &info){
	Napi::Env env = info.Env();
	Napi::Object returnObj=Napi::Object::New(env);
	homeDir = info[0].As<Napi::String>().ToString();
	std::string basepath = info[1].As<Napi::String>().ToString();
	bool isLocalValid = verifyLocal(basepath);
	if(isLocalValid){
		isLocalValid = checkLocalSerialNumber(basepath,env);
		if(isLocalValid){
			returnObj.Set("LocalValidation",true);
		}else{
			returnObj.Set("LocalValidation",false);
		}
	}else{
		returnObj.Set("LocalValidation",false);
	}
	return returnObj;
}

Napi::Object DirectSaveFile(const Napi::CallbackInfo &info){
	Napi::Env env = info.Env();
	Napi::Object returnObj=Napi::Object::New(env);
	homeDir = info[0].As<Napi::String>().ToString();
	std::string serverResponse = info[1].As<Napi::String>().ToString();
	saveFile(serverResponse,homeDir);
	returnObj.Set("fileSaved","true");

	return returnObj;
}
Napi::Object TestExecOnlyUntil(const Napi::CallbackInfo &info){
	Napi::Env env = info.Env();
	Napi::Object returnObj=Napi::Object::New(env);
	
	// response is sent to execOnlyUntilSecondsResponse
	execOnlyUntilSeconds("D:\\node-cpp\\registry-functions\\sleep-for-10-sec.exe");
	
	returnObj.Set("tested","true");
	return returnObj;
}
Napi::Object Init(Napi::Env env, Napi::Object exports)
{
	exports.Set(Napi::String::New(env, "validate"),
		Napi::Function::New(env, InitialValidation));
	exports.Set(Napi::String::New(env, "startUp"),
		Napi::Function::New(env, StartUpValidation));
	exports.Set(Napi::String::New(env, "filePresent"),
		Napi::Function::New(env, CredentialsAvailable));
	exports.Set(Napi::String::New(env, "validateLocal"),
		Napi::Function::New(env, ValidateLocal));
	exports.Set(Napi::String::New(env, "directSaveFile"),
		Napi::Function::New(env, DirectSaveFile));
	exports.Set(Napi::String::New(env, "testExecOnlyUntil"),
		Napi::Function::New(env, TestExecOnlyUntil));
	return exports;
}

NODE_API_MODULE(hello, Init)
