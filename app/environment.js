 // License error codes - 
    // -1 => Key Not Found
    // -2 => Serial Number Not Found
    // -3 => License Expired
    // -4 => License Cancelled
    // -5 => Key already used
    // -6 => PC registered with another key
    //  NULL => Valid Date
const os = require("os");
var recorderFilesPath = os.homedir+'/Documents/HiFi_Recorder Files'
var settingsFilePath = recorderFilesPath+'/settings.json';
var settingsTemplatePath = recorderFilesPath+'/template';
var templateBackgroundImagesPath = recorderFilesPath+'/template-background-images/';
const errorCodes = {
	KEY_NOT_FOUND : -1,
	LICENSE_NOT_FOUND : -2,
	LICENSE_EXPIRED : -3,
	LICENSE_CANCELLED : -4,
	KEY_ALREADY_USED : -5,
	PC_REGISTERED_WITH_DIFFERENT_USER : -6,
	LICENSE_VALID : null ,
	HTTP_RESPONSE_ERROR: 'http-request-error'
};
module.exports ={
	recorderFilesPath : recorderFilesPath,
	squirrelUrl : "https://s3.ap-south-1.amazonaws.com/hifirecorder/release/",
	// serverPath : "http://13.58.84.186/recorder/php/",
	// serverPath : "http://localhost/recorder/php/",
	// serverPath : "https://recorder-node-server.herokuapp.com/",
	// serverPath : "http://localhost:3002/",
	serverPath : 'https://hifi-recorder-279715.el.r.appspot.com/',
	settingsFilePath : settingsFilePath,
	settingsTemplatePath: settingsTemplatePath,
	errorCodes : errorCodes,
	waveformPictureHeight: 100,
	pixelsPerSecond: 10,
	freeSpaceLimitGB: 3,
	templateBackgroundImagesPath,
	recorderWebsiteURLConfig: {
		// homePage: 'http://localhost:3000/',
		// loginPage: 'http://localhost:3000/account-sign-in-simple.html',
		// homePageBuyNowSection: 'http://localhost:3000#buy-now-section',
		homePage: 'http://hifirecorder.com/',
		loginPage: 'http://hifirecorder.com/get-free-trial-license.html',
		homePageBuyNowSection: 'http://hifirecorder.com#buy-now-section'
	}
};