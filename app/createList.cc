#include <iostream>
#include <fstream>
#include <napi.h>
// #include <node_api.h>
using namespace std;
void createList(const Napi::CallbackInfo &info)
{
	Napi::Env env = info.Env();
	Napi::Function cb = info[1].As<Napi::Function>();
	// cb.Call(env.Global(), {Napi::String::New(env,"got args")});
	Napi::HandleScope scope(env);
	auto configObj = info[0].ToObject();
	Napi::Array finalMerger = configObj.Get(Napi::String::New(env, "finalMerger")).As<Napi::Array>();
	std::string filename = info[2].ToString();
	ofstream fout;
	std::string path;
	fout.open(filename, std::fstream::out | std::fstream::trunc);

	Napi::Object returnObj = Napi::Object::New(env);

	cout << "came to createList";
	if (!fout){
		std::cout << "file could not open";
		std::string s = "could not open mergingList Path : "+filename;
		cb.Call(env.Global(), {Napi::String::New(env, s)});
	}
	else
	{
		for (unsigned int i = 0; i < finalMerger.Length(); i++)
		{
			Napi::Object currentObj = (finalMerger.Get(i)).ToObject();

			std::string type = currentObj.Get((Napi::String::New(env, "type"))).As<Napi::String>().ToString();
			path = ((currentObj.Get(Napi::String::New(env, "intermidiate")).ToObject()).ToObject()).Get(Napi::String::New(env, "cpath")).ToString();

			// if(type.compare("both") == 0){
			//     path=((currentObj.Get(Napi::String::New(env,"intermidiate")).ToObject()).ToObject()).Get(Napi::String::New(env,"opath")).ToString();
			// }
			// else{
			//     path=((currentObj.Get(Napi::String::New(env,"intermidiate")).ToObject()).ToObject()).Get(Napi::String::New(env,"cpath")).ToString();
			// }
			if (path[0] == '\"')
			{
				path = path.substr(1, (path.length() - 2));
			}
			cb.Call(env.Global(), {Napi::String::New(env, path)});

			// if(path[0]=="\""){
			//     path = "\""+path+"\"";
			// }
			fout << "file '" << path << "'" << endl;
			cout << endl << "path written in fikle" << endl;

		}
	}
	fout.close();
	std::string s = "merging-list-created";
	cb.Call(env.Global(), {Napi::String::New(env, s)});

}

Napi::Object Init(Napi::Env env, Napi::Object exports)
{
	exports.Set(Napi::String::New(env, "createList"),
				Napi::Function::New(env, createList));
	return exports;
}
NODE_API_MODULE(createList, Init);