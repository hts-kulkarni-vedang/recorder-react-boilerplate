const {ipcRenderer ,electron} = require('electron')
const {dialog} = require('electron').remote
var fs            = require('fs');
var child_process = require('child_process');
var path = require('path');
var exec = require('child_process').execSync;
var ncp = require('ncp').ncp;
//var FFMPEG_DIR = '/opt/ffmpeg/bin/';
var FFMPEG_DIR = '';
var ncp = require('ncp').ncp;
var spawn_proc_count = 0;
var FPS     = 30;
var CRF 	= 25;
var PRESET = 'superfast';
var frameTime = 1 / FPS; // TODO not sure this is the best approach, adding one fictitious frame

var cameraFiles = []
var screenFiles = []
var rimraf = require('rimraf');
var doneDuration=0,totalDuration=0
var overlayDurations =[]
var overlayPcount = 0
var mode;

function createBackupFolder(documentsFolderPath)
{
	optname = documentsFolderPath+'/record_combined'+(Math.floor(Date.now() / 1000))+'.mp4';
	//console.log('came to create backup folder',fs.readdirSync('assets/'))
	let folder = optname.split(".mp4")[0]
	fs.mkdirSync(folder)	
	fs.mkdirSync(folder+'/camera/')
	fs.mkdirSync(folder+'/screen/')
	fs.mkdirSync(folder+'/backup/')
	fs.mkdirSync(folder+'/backup/camera')
	fs.mkdirSync(folder+'/backup/screen')

	if(fs.existsSync(documentsFolderPath+'/assets/camera/'))
	{
		let camFiles = fs.readdirSync(documentsFolderPath+'/assets/camera/')	
		for(let i=0;i<camFiles.length;i++)
		{
			fs.copyFileSync(documentsFolderPath+'/assets/camera/'+camFiles[i],folder+'/camera/'+camFiles[i])
			fs.copyFileSync(documentsFolderPath+'/assets/camera/'+camFiles[i],folder+'/backup/camera/'+camFiles[i])
		}
		rimraf.sync(documentsFolderPath+'/assets/camera', {},function () { });
	}
	else
	{
		console.log('could not find assets/camera')
	}
	if(fs.existsSync(documentsFolderPath+'/assets/screen/'))
	{
		let screenFiles = fs.readdirSync(documentsFolderPath+'/assets/screen/')	
		for(let i=0;i<screenFiles.length;i++)
		{
			fs.copyFileSync(documentsFolderPath+'/assets/screen/'+screenFiles[i],folder+'/screen/'+screenFiles[i])	
			fs.copyFileSync(documentsFolderPath+'/assets/screen/'+screenFiles[i],folder+'/backup/screen/'+screenFiles[i])	
		}
		rimraf.sync(documentsFolderPath+'/assets/screen', {},function () { });
	}
	else
	{
		console.log('could not find assets/screen')
	}
	createConfigFile(documentsFolderPath)
	return folder
}

/*function getOutputName()
{
	
	return optname
}*/
function createConfigFile(documentsFolderPath)
{
	var foldername = optname.split(".mp4")[0]
	var config = {}
	changeFinalMergerPaths(documentsFolderPath)
	console.log('Final  merger : ',finalMerger)
	config.data = {
		overlayPos : overlayPos,
		finalMerger : finalMerger,
		transparencyCheck : transparencyCheck,
		transparencyOpacity : transparencyOpacity,
		transparencySimilarity : transparencySimilarity,
		colorHex : colorHex,
		overlayRes : overlayRes,
		preset : PRESET,
		crf : CRF,
		backImgCheck : backImgCheck,
		imgPath : imgPath,
		fullScreenMode : fullScreenMode
	}

	if(fs.existsSync(foldername+'/settings.config'))
	{
		fs.truncateSync(foldername+'/settings.config')
	}
	fs.appendFileSync(foldername+'/settings.config',JSON.stringify(config.data))
	finalMerger = []
}
function changeFinalMergerPaths()
{
	console.log('to change finalMerger : ',finalMerger)
	let folder = optname.split(".mp4")[0]

	for(let i=0;i<finalMerger.length;i++)
	{
		let obj = finalMerger[i]
		if(obj.type == "camera")
		{
			//obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length - 1]
		}
		else if(obj.type == "screen")
		{
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')*/
			obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
		}
		else if(obj.type == "both")
		{
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			obj.spath = obj.spath.replace("./assets/",folder+'/')*/
			obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.spath = obj.spath.split("/assets/")[obj.spath.split("/assets/").length -1]
		}
	}
}
function updateBackupFolder(backupFolderPath,foldername,mainWindow)
{
	console.log(backupFolderPath,foldername)
	//ncp(backupFolderPath,foldername,function(err)
	copyFoldersHelper(backupFolderPath,foldername,function(err)
	{
		if (err) {
			console.log(err);
		}

		rimraf.sync(backupFolderPath)
		backupFolderPath = foldername
		configObjNew = JSON.parse(fs.readFileSync(backupFolderPath+'/settings.config', 'utf8'));
		var oldBackupFolder =  configObjNew.backupFolder;
		console.log(configObjNew,'done processing',mainWindow)
		console.log(enableRecording)
		mainWindow.webContents.session.clearCache(function(){
		//some callback.
			sessionStorage.setItem('videoSource',$('#videoSource').val())
			sessionStorage.setItem('audioSource',$('#audioSource').val())
			mainWindow.reload()
			$('.menu-buttons').show()
			$('#progress-wrap').hide()		
			$('#progress-text').text('')
		});

		fs.unlinkSync(backupFolderPath+'/settings.config')
		fs.writeFileSync(backupFolderPath+'/settings.config',JSON.stringify(configObjNew))
	})
}

var listFilesToCopy = function(source, filesToCopyListed) {
	var results = [];
	fs.readdir(source, function(err, list) {
		//console.log(err,list)
		if (err) return filesToCopyListed(err);
		var i = 0;
		(function next() {
		var file = list[i++];
		if (!file) return filesToCopyListed(null, results);
		//file = source + '/' + file;
		file = source + '\\' + file;
		//console.log(file)
		fs.stat(file, function(err, stat) {
			if (stat && stat.isDirectory()) {
			listFilesToCopy(file, function(err, res) {
				results = results.concat(res);
				next();
			});
			} else {
			results.push(file);
			next();
			}
		});
		})();
	});
};

function copyFoldersHelper(source,dest,cb){	
	console.log(source,dest)
	
	listFilesToCopy(source,function(err,results){
		console.log(err,results)
		fs.mkdirSync(dest+'/camera/')
		fs.mkdirSync(dest+'/screen/')
		fs.mkdirSync(dest+'/tracks/')
		fs.mkdirSync(dest+'/backup/')
		fs.mkdirSync(dest+'/backup/camera/')
		fs.mkdirSync(dest+'/backup/screen/')
		copyFolders(source,results,dest,cb)
	})
}
function copyFolders(source,results,dest,cb){
	var filesCopied = 0
	for(let i=0;i<results.length;i++){
		var sourceFile = results[i]
		var destFile = dest+(results[i].replace(source,''))
		console.log('source file : ',sourceFile,'destfile : ',destFile)	
		fs.copyFile(sourceFile,destFile,function(){
			console.log('file copied')
			filesCopied++
			if(filesCopied == results.length){
				// $('.menu-buttons').show()
				// $('#progress-wrap').hide()		
				// $('#progress-text').text('')
				cb()
			}
		})
	}
}
function generateTrackImages(backupFolderPath,cb)
{
	var configObj = JSON.parse(fs.readFileSync(backupFolderPath+'/settings.config', 'utf8'));
	console.log('configObj : ',configObj)
	var pCount = 0
	var color = '#ffffff'
	createTracksFolder(backupFolderPath)
	for(var i=0;i<configObj.finalMerger.length;i++)
	{
		var obj =  configObj.finalMerger[i]

		obj.waveform = '/tracks/track-'+i+'.png'
		var outputFile=backupFolderPath+'/tracks/track-'+i+'.png'
		var width = (obj.endTime - obj.startTime)*10
		var _args = [
			'ffmpeg','-i',backupFolderPath+'/'+obj.cpath,
			'-filter_complex','aformat=channel_layouts=mono,showwavespic=s='+width+'x70:colors='+color,'-frames:v','1','-y',outputFile
		];
		var cmd = _args.shift();
		var proc= child_process.spawn(cmd,_args);
		pCount++
		proc.stderr.on('data',function(data) {
			console.log(data.toString());
		});
		proc.on('close',function(data) {
			pCount -- 
			console.log(pCount+' processess remaining')
			if(pCount == 0)
			{
				cb()
				fs.unlinkSync(backupFolderPath+'/settings.config')
				fs.writeFileSync(backupFolderPath+'/settings.config',JSON.stringify(configObj))
			}
		});
	}
}
function createTracksFolder(backupFolderPath)
{
	if(fs.existsSync(backupFolderPath+'/tracks'))
	{
		rimraf.sync(backupFolderPath+'/tracks')
	}
	fs.mkdirSync(backupFolderPath+'/tracks')
}

module.exports = {
	createBackupFolder : createBackupFolder,
	createConfigFile : createConfigFile,
	updateBackupFolder : updateBackupFolder,
	generateTrackImages : generateTrackImages
};

window.onerror = function(errMsg,url,lineNum)
{
	console.log('error occured at ',lineNum)
};