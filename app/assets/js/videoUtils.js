const { ipcRenderer, electron } = require('electron')
const { dialog } = require('electron').remote
var fs = require('fs');
var child_process = require('child_process');
var path = require('path');
var os = require('os')
var exec = require('child_process').execSync;
//var FFMPEG_DIR = '/opt/ffmpeg/bin/';
var FFMPEG_DIR = '';
var spawn_proc_count = 0;
var FPS = 30;
var CRF = 25;
var PRESET = 'superfast';
var frameTime = 1 / FPS; // TODO not sure this is the best approach, adding one fictitious frame
//var errLogstream
var cameraFiles = []
var screenFiles = []
var rimraf = require('rimraf');
var doneDuration = 0, totalDuration = 0
var overlayDurations = []
var overlayPcount = 0
var mode;
function copyErrorFilesAndCreateSettingsConfig(documentsFolderPath, folder) {
	changeFinalMergerPaths(folder) // Changing paths for pushing data in settings file 
}
function createConfigFile(foldername) {
	var config = {}
    // relative path saved in settings file so that it can be used on any PC . (username would not be added to path)

	// ! moving file write operation in renderer-new-ui.js so that file is updated while recording is done
	// ! ensures if power is lost while recording ; then the recordings are kept safe
	// config.data = {
	// 	overlayPos: null,//remove if unused
	// 	finalMerger: finalMerger,
	// 	transparencyCheck: transparencyCheck,//remove if unused
	// 	transparencyOpacity: transparencyOpacity,//remove if unused
	// 	transparencySimilarity: transparencySimilarity,//remove if unused
	// 	colorHex: colorHex,//remove if unused
	// 	overlayRes: null,//remove if unused
	// 	preset: PRESET,
	// 	crf: CRF,
	// 	backImgCheck: backImgCheck,
	// 	imgPath: null,//remove if unused
	// 	// fullScreenMode: fullScreenMode
	// }
	// if (fs.existsSync(foldername + '/settings.config')) {
	// 	fs.truncateSync(foldername + '/settings.config')  // creates empty file if it exists 
	// } 
	// fs.appendFileSync(foldername + '/settings.config', JSON.stringify(config.data))
	finalMerger = [];
	return config;
}
function changeFinalMergerPaths(foldername) {
	console.log('to change finalMerger : ', finalMerger)
	let folder = optname.split(".mp4")[0]
	for (let i = 0; i < finalMerger.length; i++) {
		let obj = finalMerger[i]

		if (obj.type == "camera") {
			//obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			//obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length - 1]
			obj.cpath = 'camera' + obj.cpath.split('camera')[obj.cpath.split('camera').length - 1];
		}
		else if (obj.type == "screen") {
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')*/
			// obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.cpath = 'screen' + obj.cpath.split('screen')[obj.cpath.split('screen').length - 1];
		}
		else if (obj.type == "both") {
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			obj.spath = obj.spath.replace("./assets/",folder+'/')*/
			//obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.cpath = 'camera' + obj.cpath.split('camera')[obj.cpath.split('camera').length - 1];
			obj.spath = 'screen' + obj.spath.split('screen')[obj.spath.split('screen').length - 1];
			//obj.spath = obj.spath.split("/assets/")[obj.spath.split("/assets/").length -1]
		}
	}
}

function updateBackupFolder(backupFolderPath, foldername, mainWindow) {
	console.log(backupFolderPath, foldername)
	copyFoldersHelper(backupFolderPath, foldername, function (err) {
		if (err) {
			console.log(err);
		}
		rimraf.sync(backupFolderPath)
		backupFolderPath = foldername
		configObjNew = JSON.parse(fs.readFileSync(backupFolderPath + '/settings.config', 'utf8'));
		var oldBackupFolder = configObjNew.backupFolder;
		console.log(configObjNew, 'done processing', mainWindow)
		console.log(enableRecording)
		mainWindow.webContents.session.clearCache(function () {
			//some callback.
			mainWindow.reload()
			$('.menu-buttons').show()
			$('#progress-wrap').hide()
			$('#progress-text').text('')
		});
		fs.unlinkSync(backupFolderPath + '/settings.config')
		fs.writeFileSync(backupFolderPath + '/settings.config', JSON.stringify(configObjNew))
	})
}
var listFilesToCopy = function (source, filesToCopyListed) {
	var results = [];
	fs.readdir(source, function (err, list) {
		//console.log(err,list)
		if (err) return filesToCopyListed(err);
		var i = 0;
		(function next() {
			var file = list[i++];
			if (!file) return filesToCopyListed(null, results);
			//file = source + '/' + file;
			file = source + '\\' + file;
			//console.log(file)
			fs.stat(file, function (err, stat) {
				if (stat && stat.isDirectory()) {
					listFilesToCopy(file, function (err, res) {
						results = results.concat(res);
						next();
					});
				} else {
					results.push(file);
					next();
				}
			});
		})();
	});
};
function copyFoldersHelper(source, dest, cb) {
	console.log(source, dest)
	listFilesToCopy(source, function (err, results) {
		console.log(err, results)
		if (!fs.existsSync(dest + '/camera/')) {
			fs.mkdirSync(dest + '/camera/')
		}
		if (!fs.existsSync(dest + '/screen/')) {
			fs.mkdirSync(dest + '/screen/')
		}
		if (!fs.existsSync(dest + '/tracks/')) {
			fs.mkdirSync(dest + '/tracks/')
		}
		if (!fs.existsSync(dest + '/backup/')) {
			fs.mkdirSync(dest + '/backup/')
		}
		if (!fs.existsSync(dest + '/backup/camera/')) {
			fs.mkdirSync(dest + '/backup/camera/')
		}
		if (!fs.existsSync(dest + '/backup/screen/')) {
			fs.mkdirSync(dest + '/backup/screen/')
		}
		copyFolders(source, results, dest, cb)
	})
}
function copyFolders(source, results, dest, cb) {
	var filesCopied = 0
	for (let i = 0; i < results.length; i++) {
		var sourceFile = results[i]
		var destFile = dest + (results[i].replace(source, ''))
		console.log('source file : ', sourceFile, 'destfile : ', destFile)
		fs.copyFile(sourceFile, destFile, function () {
			console.log('file copied')
			filesCopied++
			if (filesCopied == results.length) {
				// $('.menu-buttons').show()
				// $('#progress-wrap').hide()		
				// $('#progress-text').text('')
				cb()
			}
		})
	}
}
// function moved to editor
// function generateTrackImages(backupFolderPath,cb)
// {
// 	var configObj = JSON.parse(fs.readFileSync(backupFolderPath+'/settings.config', 'utf8'));
// 	console.log('configObj : ',configObj)
// 	var pCount = 0
// 	var color = '#ffffff'
// 	createTracksFolder(backupFolderPath)
// 	for(var i=0;i<configObj.finalMerger.length;i++)
// 	{
// 		var obj =  configObj.finalMerger[i]
// 		obj.waveform = '/tracks/track-'+i+'.png'
// 		var outputFile=backupFolderPath+'/tracks/track-'+i+'.png'
// 		var width = (obj.endTime - obj.startTime)*10
// 		var _args = [
// 			'ffmpeg','-i',backupFolderPath+'/'+obj.cpath,
// 			'-filter_complex','aformat=channel_layouts=mono,showwavespic=s='+width+'x70:colors='+color,'-frames:v','1','-y',outputFile
// 		];
// 		var cmd = _args.shift();
// 		var proc= child_process.spawn(cmd,_args);
// 		pCount++
// 		proc.stderr.on('data',function(data) {
// 			console.log(data.toString());
// 		});
// 		proc.on('close',function(data) {
// 			pCount -- 
// 			console.log(pCount+' processess remaining')
// 			if(pCount == 0)
// 			{
// 				cb()
// 				fs.unlinkSync(backupFolderPath+'/settings.config')
// 				fs.writeFileSync(backupFolderPath+'/settings.config',JSON.stringify(configObj))
// 			}
// 		});
// 	}
// }
function createTracksFolder(backupFolderPath) {
	if (fs.existsSync(backupFolderPath + '/tracks')) {
		rimraf.sync(backupFolderPath + '/tracks')
	}
	if (!fs.existsSync(backupFolderPath + '/tracks')) {
		fs.mkdirSync(backupFolderPath + '/tracks')
	}
}
module.exports = {
	copyErrorFilesAndCreateSettingsConfig: copyErrorFilesAndCreateSettingsConfig,
	createConfigFile: createConfigFile,
	updateBackupFolder: updateBackupFolder,
	changeFinalMergerPaths : changeFinalMergerPaths
};
