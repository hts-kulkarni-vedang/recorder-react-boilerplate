
#include <napi.h>
#include<iostream>
#include<string>
#include <array>
#include<windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "commandFunctions.h"

HANDLE hChildProcess_o = NULL; //NULL
HANDLE hStdIn_o = NULL; // Handle to parents std input. NULL
BOOL bRunThread_o; // TRUE
std::string errorLog="overlayLogStderr.txt";
std::string *overlayCommands ;
int overlay_count=0;
int noOfOverlayCommands=0; 
int overlayCommandCounter = 0;
#pragma comment(lib, "User32.lib")

int RunCommand2(std::string cmdx,int remainingCount,Napi::Function cb,Napi::Env env,int totalOverlayDur,int previousCompletedDur){
    LPTSTR cmd = const_cast<char *>((cmdx).c_str());

    BOOL ok = TRUE;
    HANDLE hStdInPipeRead = NULL;
    HANDLE hStdInPipeWrite = NULL;
    HANDLE hStdOutPipeRead = NULL;
    HANDLE hStdOutPipeWrite = NULL;

    // Create two pipes.
    SECURITY_ATTRIBUTES sa = { sizeof(SECURITY_ATTRIBUTES), NULL, TRUE };
    ok = CreatePipe(&hStdInPipeRead, &hStdInPipeWrite, &sa, 0);
    if (ok == FALSE) return -1;
    ok = CreatePipe(&hStdOutPipeRead, &hStdOutPipeWrite, &sa, 0);
    if (ok == FALSE) return -1;

    // Create the process.
    STARTUPINFO si = { };
    si.cb = sizeof(STARTUPINFO);
    si.dwFlags = STARTF_USESTDHANDLES;
    si.hStdError = hStdOutPipeWrite;
    si.hStdOutput = hStdOutPipeWrite;
    si.hStdInput = hStdInPipeRead;
    PROCESS_INFORMATION pi = { };
    LPCWSTR lpApplicationName = L"C:\\Windows\\System32\\cmd.exe";
    LPWSTR lpCommandLine = (LPWSTR)L"C:\\Windows\\System32\\cmd.exe /c dir";
    LPSECURITY_ATTRIBUTES lpProcessAttributes = NULL;
    LPSECURITY_ATTRIBUTES lpThreadAttribute = NULL;
    BOOL bInheritHandles = TRUE;
    DWORD dwCreationFlags = 0;
    LPVOID lpEnvironment = NULL;
    LPCWSTR lpCurrentDirectory = NULL;
    // ok = CreateProcess(
    //     lpApplicationName,
    //     lpCommandLine,
    //     lpProcessAttributes,
    //     lpThreadAttribute,
    //     bInheritHandles,
    //     dwCreationFlags,
    //     lpEnvironment,
    //     lpCurrentDirectory,
    //     &si,
    //     &pi);
    ok =CreateProcess(NULL,cmd,NULL,NULL,TRUE,CREATE_NO_WINDOW,NULL,NULL,&si,&pi);
    if (ok == FALSE) return -1;

    // Close pipes we do not need.
    CloseHandle(hStdOutPipeWrite);
    CloseHandle(hStdInPipeRead);

    // The main loop for reading output from the DIR command.
    char buf[1024 + 1] = { };
    DWORD dwRead = 0;
    DWORD dwAvail = 0;
    ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    while (ok == TRUE)
    {
        buf[dwRead] = '\0';
        OutputDebugStringA(buf);
        puts(buf);
        cout << "output of command : " <<remainingCount<< endl;
        Napi::Object obj=Napi::Object::New(env);

        obj.Set("remainingCount",(remainingCount));
        obj.Set("output",buf);
        obj.Set("totalOverlayDur",totalOverlayDur);
        obj.Set("previousCompletedDur",previousCompletedDur);
        cb.Call(env.Global(),{obj});

        ok = ReadFile(hStdOutPipeRead, buf, 1024, &dwRead, NULL);
    }

    // Clean up and exit.
    CloseHandle(hStdOutPipeRead);
    CloseHandle(hStdInPipeWrite);
    DWORD dwExitCode = 0;
    GetExitCodeProcess(pi.hProcess, &dwExitCode);
    return 0;
}
std::string getKey(Napi::Env env,std::string keyx,std::string type,Napi::Object currentObject,std::string keyFor){
    Napi::Array keys = currentObject.GetPropertyNames();
    std::string tmpKey;
    for(int i=0;i<keys.Length();i++){
        std::string key = keys.Get(i).ToString();
        // if type = camera & key = cwidth & keyx = width
        if(keyFor.compare("camera") == 0 ){
            tmpKey = "c"+keyx;
        }else{
            tmpKey = "s"+keyx;
        }
        if (key.compare(tmpKey) == 0 && keyx.compare("width") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("height") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("left") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
        else if (key.compare(tmpKey) == 0 && keyx.compare("top") == 0){
            return currentObject.Get(Napi::String::New(env,key)).As<Napi::String>().ToString();
        }
    }
}
Napi::Object Overlay(const Napi::CallbackInfo& info) {
    std::cout<<"came to overlay\n";
              
    Napi::Env env = info.Env();
    int id=info[4].As<Napi::Number>().Int32Value();
    auto configObj = info[0].ToObject();
    std::string configFolderPath = info[1].ToString();
   
    std::string pos = info[2].ToString();
    Napi::Array finalMerger=configObj.Get(Napi::String::New(env,"finalMerger")).As<Napi::Array>();
    Napi::Function cb = info[3].As<Napi::Function>();


    std::vector <std::string> _args_camera;
    std::vector <std::string> _args_screen;

    for (unsigned int i = 0; i < finalMerger.Length(); i++) {
        Napi::Object currentObj = (finalMerger.Get(i)).ToObject();
        std::string type = currentObj.Get(Napi::String::New(env,"type")).As<Napi::String>().ToString();
        if(type.compare("both") == 0)
        {
            noOfOverlayCommands = noOfOverlayCommands + 1;
        }
    }
    overlayCommands = new std::string[noOfOverlayCommands];
    std::string outputFile=configFolderPath+"intermediate\\overlay_";
    std::string outPutfileName=configFolderPath+"errorLog\\overlayLogStdout.txt";
    setId_fileName(id,outPutfileName);
    if(noOfOverlayCommands > 1){
        outputFile = configFolderPath+"intermediate\\overlay_";
    }else{
        outputFile = configFolderPath+"output\\overlay_";
    }
    Napi::Object outputRes =  configObj.Get(Napi::String::New(env,"outputRes")).ToObject();
    std::string outputW = outputRes.Get(Napi::String::New(env,"width")).As<Napi::String>().ToString();
    std::string outputH = outputRes.Get(Napi::String::New(env,"height")).As<Napi::String>().ToString();
    int totalOverlayDur = 0;
    for (unsigned int i = 0; i < finalMerger.Length(); i++) 
    {
        _args_camera.clear();
        _args_screen.clear();
        Napi::Object currentObj = (finalMerger.Get(i)).ToObject();
        std::string type = currentObj.Get(Napi::String::New(env,"type")).As<Napi::String>().ToString();
        std::string transparencyFlag = currentObj.Get(Napi::String::New(env,"transparencyCheck")).ToString(); 

        if(type.compare("both") == 0){    

            overlay_count++;
            
            int endTime =   currentObj.Get(Napi::String::New(env,"endTime")).ToNumber();
            int startTime = currentObj.Get(Napi::String::New(env,"startTime")).ToNumber();

            totalOverlayDur += (endTime - startTime);
            currentObj.Set("previousCompleted",(endTime - startTime));

            std::string left = getKey(env,"left",type,currentObj,"camera");
            std::string top = getKey(env,"top",type,currentObj,"camera");
            std::string outputPath = outputFile+std::to_string(overlay_count)+".mp4";
            _args_camera.push_back("ffmpeg");
            _args_camera.push_back("-i");_args_camera.push_back((((currentObj.Get(Napi::String::New(env,"intermidiate")).ToObject()).Get(Napi::String::New(env,"cpath"))).As<Napi::String>().Utf8Value()));
            _args_camera.push_back("-i");_args_camera.push_back((((currentObj.Get(Napi::String::New(env,"intermidiate")).ToObject()).Get(Napi::String::New(env,"spath"))).As<Napi::String>().Utf8Value()));
            if(transparencyFlag.compare("true") == 0){
                std::string colorHex = currentObj.Get(Napi::String::New(env,"colorHex")).As<Napi::String>().ToString();
                std::string similarity = currentObj.Get(Napi::String::New(env,"transparencySimilarity")).As<Napi::String>().ToString();
                std::string opacity = currentObj.Get(Napi::String::New(env,"transparencyOpacity")).As<Napi::String>().ToString();
                _args_camera.push_back("-filter_complex");_args_camera.push_back("\"[0]chromakey=0x"+colorHex+":"+similarity+":"+opacity+" [zero];[1]scale="+outputW+":"+outputH+",fps=30[back];[back][zero]overlay="+left+":"+top+":shortest=1\"");
            }
            else{                   
                _args_camera.push_back("-filter_complex");_args_camera.push_back("[1]scale="+outputW+":"+outputH+",fps=30[back];[back][0]overlay="+left+":"+top+":shortest=1");
            }

            std::string preset = configObj.Get(Napi::String::New(env,"preset")).As<Napi::String>().ToString();
            std::string crf = configObj.Get(Napi::String::New(env,"crf")).As<Napi::String>().ToString();
            
                _args_camera.push_back("-preset");
                _args_camera.push_back(preset);
                _args_camera.push_back("-crf");
                _args_camera.push_back(crf);
                _args_camera.push_back("-r");
                _args_camera.push_back("30");
            _args_camera.push_back("-y");_args_camera.push_back('"'+outputPath+'"');

            overlayCommands[overlayCommandCounter++]= createCommand(_args_camera);
            // sets output path in configObj if is Overlay
            ((((configObj.Get(Napi::String::New(env,"finalMerger")).ToObject()).Get(i)).ToObject()).Get(Napi::String::New(env,"intermidiate")).ToObject()).Set(Napi::String::New(env,"opath"),outputPath);
            ((((configObj.Get(Napi::String::New(env,"finalMerger")).ToObject()).Get(i)).ToObject()).Get(Napi::String::New(env,"intermidiate")).ToObject()).Set(Napi::String::New(env,"cpath"),outputPath);

            (((configObj.Get(Napi::String::New(env,"finalMerger")).ToObject()).Get(i)).ToObject()).Set(Napi::String::New(env,"previousCompleted"),(endTime - startTime));
        }

        // int overlayDurations[overlayCommandCounter];
    }
    std::cout<<"\nCommands in overlay :\n";
    int res;
    int previousCompletedDur = 0;
    for (int i = 0; i < overlayCommandCounter; ++i)
    {

        std::cout<<"\n"<<overlayCommands[i]<<"\n"; 
        res = RunCommand2(overlayCommands[i],(overlayCommandCounter - i),cb,env,totalOverlayDur,previousCompletedDur);
        // previousCompletedDur += (((configObj.Get(Napi::String::New(env,"finalMerger")).ToObject()).Get(i)).ToObject().Get("previousCompleted")).ToNumber();
        int x= (((configObj.Get(Napi::String::New(env,"finalMerger")).ToObject()).Get(i)).ToObject().Get("previousCompleted")).ToNumber();
        previousCompletedDur += x;
        cout<< "previously completed: " <<x;
    }   
   

        cb.Call(env.Global(), {Napi::String::New(env,"overlay-complete")});
        std::cout<<"overlay-complete";
        return configObj;
}

Napi::Object Init(Napi::Env env, Napi::Object exports) {
  return Napi::Function::New(env, Overlay);
}

NODE_API_MODULE(overlay, Init)