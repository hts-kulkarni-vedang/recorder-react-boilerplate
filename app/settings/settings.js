const { ipcRenderer, shell } = require('electron');
const electron = require('electron');
const { dialog } = require('electron').remote;
var swal = require('sweetalert2');
var AWS = require('aws-sdk');
var S3 = require('aws-sdk/clients/s3');
let totalFilesWRitten = 0;

var fs = require('fs');
var htmlToImage = require('html-to-image');
var backImage;
var templatePath;
let currentWindow = null;
var logoImgPath = null;
var currentRec = {};
const env = require('../environment');
var settings = {};
var outputRes = {
  width: 1920,
  height: 1080,
};
const { app } = require('electron').remote;
const path = require('path');
var selectedMode = 'both';
var index = null;
let isCurrentTemplateDefault = false;
let cancelTemplateDownload = false;
let bothCameraImageLoaded = false;
let bothScreenImageLoaded = false;
const scrollingTextWrapperElement = `
<div class="scrolling-text-wrapper" id="scrolling-text-wrapper-id-here" mergerindex='merger-index-here'>
	<div class="scrolling-text-content scrolling-text-canvas-content" mergerindex='merger-index-here'>
		<div class="fixed-text-wrap inline-block" mergerindex='merger-index-here'></div><div class="scrolling-text-wrap inline-block" mergerindex='merger-index-here'></div>
	</div>
</div>
`;

ipcRenderer.on('message', (event, status) => {
  // alert('message in settings : ',text);
  // swal.fire({
  // 	title: text,
  // 	toast: true
  // });
  if (
    status.code === 'update-error' ||
    status.code === 'update-not-available'
  ) {
    swal.fire({
      type: 'info',
      title: status.text,
    });
    $('.btn-settings').prop('disabled', false);
    $('.btn-settings').text('Check for Updates');
    hideLoader();
  } else if (status.code === 'update-available') {
    swal.fire({
      type: 'info',
      title: status.text,
    });
    $('.btn-settings').text('Downloading Update Now');
  } else if (status.code === 'update-downloaded') {
    swal
      .fire({
        type: 'success',
        showCancelButton: true,
        confirmButtonText: 'Install Update Now',
        cancelButtonText: 'Install Later',
      })
      .then((res) => {
        $('.btn-settings').prop('disabled', false);
        $('.btn-settings').text('Check for Updates');
        if (res.value) {
          ipcRenderer.send('install-update-now');
        }
      });
    $('.btn-settings').text('Check for Updates');
    hideLoader();
  } else if (status.code === 'download-progress') {
    $('.download-update-status-text').text(status.text);
  } else {
    $('.btn-settings').prop('disabled', true);
    $('.btn-settings').text(status.text);
  }
});
$('.addBackImg').on('click', function () {
  $('#backImage').val('');
  $('#backImage').click();
});
var settingsRead;
var templateIndex = null;
var mode = null;
$(document).ready(function () {
  mode = sessionStorage.getItem('currentSettingsMode');
  initWindow(mode);
  console.log('mode: ', mode);
  if (mode === 'show-settings') {
    $('.preview-layout').hide();
    $('.options').show();
    $('.optionButtons').show();
    $('.wrapper').removeClass('add-edit-template-wrapper');
    $('.wrapper').addClass('settings-list-wrapper');
    $('.properties-panel-wrapper-outer').css('display', 'none');
    // ? patch - if directly coming to settings without selecting template
    // ? then template index variable is not set
    // TODO -> set it to the default template
    var allTemplates = JSON.parse(
      fs.readFileSync(env.settingsFilePath, 'utf8')
    );
    allTemplates.forEach((template, index) => {
      if (template.defaultkey === true) {
        templateIndex = index;
      }
    });
  } else if (mode === 'add-new-template' || mode === 'edit-template') {
    $('.preview-layout').show();
    $('.options').hide();
    $('.optionButtons').hide();
    templateIndex = sessionStorage.getItem('indexoftemplatefolder');
    console.log('templateIndex: ', templateIndex);
    $('.wrapper').addClass('add-edit-template-wrapper');
    $('.wrapper').removeClass('settings-list-wrapper');
    $('.properties-panel-wrapper-outer').css('display', 'inline-block');
  }
  initColorVal();
  $('#recorder-version').text(app.getVersion());
  $('.popover-btn').popover({ animation: true });
  readSettingsFileIfExists();
  attachDraggables();
  attachBackImage();
  loadSettingsIfReadFromFile();
  initVideoScrollTexts();

  if (mode === 'add-new-template' || mode === 'edit-template') {
    var timeout = setInterval(() => {
      console.log('one second passed :');
      if (bothCameraImageLoaded === true && bothScreenImageLoaded === true) {
        clearInterval(timeout);
        $('#bothPreview').click();
      }
    }, 1000);
  }
});
$(document).on('click', '.show-templates-list', () => {
  showTemplatesList();
});
function hideAll() {
  $('#cameraCanvas-wrap').hide();
  $('#screenCanvas-wrap').hide();
  $('#bothcameraCanvas-wrap').hide();
  $('#bothscreenCanvas-wrap').hide();
}
$('#closePreview').on('click', function () {
  $('.preview-layout').hide();
  $('.options').show();
  electron.remote.getCurrentWindow().setSize(800, 600);
  // $('img').show();
  $('.optionButtons').show();
  $('#preview-mode-type-dropdown').text('Select Mode');

  $('.wrapper').removeClass('add-edit-template-wrapper');
  $('.wrapper').addClass('settings-list-wrapper');
});
$('#savePreview').on('click', function () {
  saveSettings();
});
$('#settings-close').on('click', function () {
  ipcRenderer.send('close-app');
});
$('#settings-minimize').on('click', function () {
  ipcRenderer.send('minimize-app');
});
$('.qual-drop-item').on('click', function () {
  qualityChanged(this);
});
$('.stream-drop-item').on('click', function () {
  streamQualityChanged(this);
});
$('.preset-drop-item').on('click', function () {
  presetChanged(this);
});
$('.framerate-drop-item').on('click', function () {
  framerateChanged(this);
});
$('.output-framerate-drop-item').on('click', function () {
  outputFramerateChanged(this);
});
$('.abps-drop-item').on('click', function () {
  audioBitrateChanged(this);
});
$('.vbps-drop-item').on('click', function () {
  videoBitrateChanged(this);
});
$('.fade-effect-drop-item').on('click', function () {
  fadeEffectChanged(this);
});
$('.max-muxing-queue-size-drop-item').on('click', function () {
  maxMuxingQueueSizeChanged(this);
});
function DrawImage(image, canvas) {
  var image = document.getElementById(image);
  var canvas = document.getElementById(canvas);
  var ctx = canvas.getContext('2d');
  console.log(
    'drawing coords ',
    image.width,
    image.height,
    0,
    0,
    canvas.width,
    canvas.height
  );
  ctx.drawImage(
    image,
    0,
    0,
    image.width,
    image.height,
    0,
    0,
    canvas.width,
    canvas.height
  );
}
$('#cameraPreview').on('click', function () {
  hideAll();
  $('#cameraCanvas-wrap').show();
  DrawImage('cameraImg', 'cameraCanvas');
  $('#preview-mode-type-dropdown').text(this.innerText);
  selectedMode = 'camera';
  sessionStorage.setItem('selectedMode', selectedMode);
  initScrollingText();
});
$('#screenPreview').on('click', function () {
  hideAll();
  $('#screenCanvas-wrap').show();
  DrawImage('screenImg', 'screenCanvas');
  $('#preview-mode-type-dropdown').text(this.innerText);
  selectedMode = 'screen';
  sessionStorage.setItem('selectedMode', selectedMode);
  initScrollingText();
});
$(document).on('resize', '.resizable', function () {
  canvasResized(this);
});
$('#bothPreview').on('click', function () {
  hideAll();
  $('#bothcameraCanvas-wrap').show();
  $('#bothscreenCanvas-wrap').show();
  DrawImage('bothCameraImg', 'bothcameraCanvas');
  DrawImage('bothScreenImg', 'bothscreenCanvas');
  $('#preview-mode-type-dropdown').text(this.innerText);
  selectedMode = 'both';
  sessionStorage.setItem('selectedMode', selectedMode);
  initScrollingText();
});
$('#closeSettings').on('click', function () {
  electron.remote.getCurrentWindow().hide();
});
$('#minimizeSettings').on('click', function () {
  electron.remote.getCurrentWindow().minimize();
});
$('#backbtn-image').on('click', function () {
  const mode = sessionStorage.getItem('currentSettingsMode');
  if (mode === 'show-settings') {
    ipcRenderer.send('backSettings');
  } else if (mode === 'add-new-template' || mode === 'edit-template') {
    ipcRenderer.send('show-template-list');
  } else if (mode === 'generalSettingsMode') {
    win = electron.remote.getCurrentWindow();
    win.loadFile('index.html');
  }
});
$('#saveSettings').on('click', function () {
  saveSettings();
});
$(document).on('click', '#btn-add-logo', function () {
  changeLogoImg();
});
$(document).on('click', '#btn-remove-logo', function () {
  removeLogoImg();
});
$(document).on('click', '#cameraCanvas-wrap', function () {
  // ? camera canvas selected
  // selectedCanvas = this;
  // $("#btn-set-aspect-ratio").attr("disabled", false);
});
$(document).on('click', '#screenCanvas-wrap', function () {
  // ? screen canvas selected
  // selectedCanvas = this;
  // $("#btn-set-aspect-ratio").attr("disabled", false);
});
$(document).on('click', '#bothcameraCanvas-wrap', function () {
  // ? both camera canvas selected
  // selectedCanvas = this;
  // $("#btn-set-aspect-ratio").attr("disabled", false);
});
$(document).on('click', '#bothscreenCanvas-wrap', function () {
  // ? both screen canvas selected
  // selectedCanvas = this;
  // $("#btn-set-aspect-ratio").attr("disabled", false);
});
$(document).on('click', '.logo-canvas', function () {
  selectedCanvas = null;
});
$(document).on(
  'click',
  '.btn-download-background-image-templates',
  function () {
    startDownloadingBackgroundTemplates();
  }
);
$(document).on(
  'click',
  '.btn-cancel-download-background-image-templates',
  function () {
    stopDownloadingBackgroundTemplates();
  }
);
$(document).on('click', '#btn-scroll-logo', function (event) {
  openPropertiesPanel('scrolling-text');
});
$(document).on('click', '.scrolling-text-check', function () {
  toggleScrollingText(this.checked);
});
$(document).on(
  'change input',
  `#scrolling-text-font-size-input,
	#scrolling-text-fixed-text-input,
	#scrolling-text-scrolling-text-input,
	#fixed-text-color,
	#fixed-text-back-color,
	#scrolling-text-color,
	#scrolling-text-back-color`,
  function (event) {
    scrollingTextValuesUpdated();
  }
);
$(document).on("click", ".text-color-img,.back-color-img ", function (event) {
	openScrollColorPicker(this);
});
$(document).on('input change', '.scrolling-text-start-time-sec', function (
  event
) {
  scrollTextTimeSecChanged(this);
});
$(document).on('input change', '.scrolling-text-running-time-sec', function (
  event
) {
  scrollTextTimeSecChanged(this);
});
// $(document).on('click', '#btn-set-aspect-ratio', function () {
// 	if (selectedMode == "camera") {
// 		let cameraCanvas = document.getElementById("cameraCanvas-wrap");
// 		setDimensionsInAspectRatio(cameraCanvas);
// 	} else if (selectedMode == "screen") {
// 		let screenCanvas = document.getElementById("screenCanvas-wrap");
// 		setDimensionsInAspectRatio(screenCanvas);
// 	} else if (selectedMode == "both") {
// 		let cameraCanvas = document.getElementById("bothcameraCanvas-wrap");
// 		let screenCanvas = document.getElementById("bothscreenCanvas-wrap");
// 		setDimensionsInAspectRatio(cameraCanvas);
// 		setDimensionsInAspectRatio(screenCanvas);
// 	}
// });
$('input[type="file"]').change(function (e) {
  backImage = e.target.files[0].path;
  var reader = new FileReader();
  reader.onload = function (e) {
    backImage = backImage.replace(/\\/g, '\\\\');
    let pathWithoutSpaces = backImage.replace(/ /g, '%20');
    pathWithoutSpaces = pathWithoutSpaces.replace(/\(/g, '\\(');
    pathWithoutSpaces = pathWithoutSpaces.replace(/\)/g, '\\)');
    $('#preview').css('background-image', 'url(' + pathWithoutSpaces + ')');
    $('#preview').css('background-size', '100% 100%');
    $('#preview').css('background-repeat', 'norepeat');
  };
  reader.readAsDataURL(e.target.files[0]);
});
$('.removeBackImg').on('click', function () {
  $('#preview').css('background-image', 'none');
  backImage = 'false';
});
$('.btn-settings').on('click', function () {
  // swal.fire({
  // 	type:'info',
  // 	title:'Cheking for Updates',
  // 	toast:true,
  // 	position: 'top-right'
  // }).then(() => {
  // });
  ipcRenderer.send('start-check-for-updates');
  showLoader();
});
$(document).on('resize', function (event) {
  const width = $(event.target).width();
  console.log('width: ', width);
  const height = $(event.target).height();
  console.log('height: ', height);
  const recType = event.target.getAttribute('rectype');
  const subType = event.target.getAttribute('type');
  if (recType === 'camera') {
    DrawImage('cameraImg', 'cameraCanvas');
  } else if (recType === 'screen') {
    DrawImage('screenImg', 'screenCanvas');
  } else if (recType === 'both' && subType === 'camera') {
    $('#bothcameraCanvas').prop('width', width);
    $('#bothcameraCanvas').prop('height', height);
    DrawImage('bothCameraImg', 'bothcameraCanvas');
  } else if (recType === 'both' && subType === 'screen') {
    $('#bothscreenCanvas').prop('width', width);
    $('#bothscreenCanvas').prop('height', height);
    DrawImage('bothScreenImg', 'bothscreenCanvas');
  }
});
// $(document).on('load', '.wait-for-image-load', function() {
// 	console.log('this.id: ', this.id);
// 	console.log('image loaded of type: ', type);

// });
$('.wait-for-image-load')
  .one('load', function () {
    // do stuff
    if (this.id === 'bothCameraImg') {
      bothCameraImageLoaded = true;
    } else if (this.id === 'bothScreenImg') {
      bothScreenImageLoaded = true;
    }
    console.log('image laoded : ', this.id);
  })
  .each(function () {
    if (this.complete) {
      // $(this).load(); // For jQuery < 3.0
      $(this).trigger('load'); // For jQuery >= 3.0
    }
  });
// e.target.value = "";
this.value = '';
$(this).val('');
function canvasResized(elem) {
  let canvasId = elem.id.replace('-wrap', '');
  let canvas = $('#' + canvasId).get(0);
  let parW = $(elem).width();
  let parH = $(elem).height();
  $(canvas).width(parW);
  $(canvas).height(parH);
  // $(canvas).attr('width',parW);
  // $(canvas).attr('height',parH);
  //
}
function getHeight() {
  let quality = $('#quality-dropdown').text();
  // if(quality == 'default'){
  // 	return quality;
  // }
  quality = quality.split('X');
  let height = quality[1];
  // if()
  return height;
}
function getWidth() {
  let quality = $('#quality-dropdown').text();
  // if(quality == 'default'){
  // 	return quality;
  // }
  quality = quality.split('X');
  let width = quality[0];
  return width;
}
function getStreamWidth() {
  let quality = $('#stream-quality-dropdown').text();
  // if(quality == 'default'){
  // 	return quality;
  // }
  quality = quality.split('X');
  let width = quality[0];
  return width;
}
function getStreamHeight() {
  let quality = $('#stream-quality-dropdown').text();
  // if(quality == 'default'){
  // 	return quality;
  // }
  quality = quality.split('X');
  if (quality[0] === 'Auto') {
    quality[1] = 'Auto';
  }
  let height = quality[1];
  return height;
}
function savePreview() {
  settings.preview = {};
  settingsTemp = {};
  let background = {
    top: 0,
    left: 0,
    width: $('#preview').width(),
    height: $('#preview').height(),
  };
  outputRes.height = getHeight();
  outputRes.width = getWidth();
  $('.canvas-wrap').each(function (index, elem) {
    let type = this.getAttribute('type');
    let recType = this.getAttribute('recType');
    let width = getCanvasDimension('width', $(this).width(), background);
    let height = getCanvasDimension('height', $(this).height(), background);
    let left = getCanvasDimension('left', $(this).css('left'), background);
    let top = getCanvasDimension('top', $(this).css('top'), background);
    if (recType == 'both') {
      if (!settings.preview.both) settings.preview.both = {};
      if (type == 'screen') {
        settings.preview.both.swidth = width;
        settings.preview.both.swidthRaw = $(this).width();
        settings.preview.both.sheight = height;
        settings.preview.both.sheightRaw = $(this).height();
        settings.preview.both.sleft = left;
        settings.preview.both.sleftRaw = $(this).css('left');
        settings.preview.both.stop = top;
        settings.preview.both.stopRaw = $(this).css('top');
      } else if (type == 'camera') {
        settings.preview.both.cwidth = width;
        settings.preview.both.cwidthRaw = $(this).width();
        settings.preview.both.cheight = height;
        settings.preview.both.cheightRaw = $(this).height();
        settings.preview.both.cleft = left;
        settings.preview.both.cleftRaw = $(this).css('left');
        settings.preview.both.ctop = top;
        settings.preview.both.ctopRaw = $(this).css('top');
      }
    } else {
      settings.preview[recType] = {};
      settings.preview[recType].cwidth = width;
      settings.preview[recType].cwidthRaw = $(this).width();
      settings.preview[recType].cheight = height;
      settings.preview[recType].cheightRaw = $(this).height();
      settings.preview[recType].cleft = left;
      settings.preview[recType].cleftRaw = $(this).css('left');
      settings.preview[recType].ctop = top;
      settings.preview[recType].ctopRaw = $(this).css('top');
    }
  });
  if ($('.logo-canvas-wrap').length == 1) {
    let logo = $('.logo-canvas-wrap').get(0);
    let width = getCanvasDimension('width', $(logo).width(), background);
    let height = getCanvasDimension('height', $(logo).height(), background);
    let left = getCanvasDimension('left', $(logo).css('left'), background);
    let top = getCanvasDimension('top', $(logo).css('top'), background);
    settings.logoCoords = {
      width: width,
      height: height,
      left: left,
      top: top,
      widthRaw: $(logo).css('width'),
      heightRaw: $(logo).css('height'),
      leftRaw: $(logo).css('left'),
      topRaw: $(logo).css('top'),
    };
  }
}
function getCanvasDimension(type, value, background) {
  // this is css left,top,width,height
  let val;
  if (typeof value == 'string') {
    if (value == 'auto') {
      value = '0px';
    }
    val = parseInt(value.substr(0, value.length - 2));
  } else {
    val = value;
  }
  if (type == 'width' || type == 'height') {
    // convert to %
    val = Math.round((val / background[type]) * 100);
    val = val > 100 ? 100 : val;
    // scale to output resolution
    val = Math.round((val * outputRes[type]) / 100);
    if (val % 2 != 0) {
      val += 1;
    }
  } else if (type == 'left' || type == 'top') {
    // get relative left
    val = isNaN(val) == true ? 0 : val;
    val = parseInt(val - background[type]);
    // convert to %
    if (type == 'left') {
      val = (val / background.width) * 100;
      val = Math.round((val * outputRes.width) / 100);
    } else if (type == 'top') {
      val = (val / background.height) * 100;
      val = Math.round((val * outputRes.height) / 100);
    }
  }
  return val;
}
function saveSettings() {
  $('.dropdownsettingsboth').click();
  savePreview();
  settings.quality = $('#Quality').val();
  settings.outputRes = {};
  settings.outputRes.width = getWidth();
  settings.outputRes.height = getHeight();
  settings.streamRes = {};
  settings.streamRes.width = getStreamWidth();
  settings.streamRes.height = getStreamHeight();
  settings.streamFrameRate = getStreamFrameRate();
  settings.outputFrameRate = getOutputFrameRate();
  settings.audioBitsPerSecond = getAudioBitsPerSecond();
  settings.videoBitsPerSecond = getVideoBitsPerSecond();
  settings.maxMuxingQueueSize = getMaxMuxingQueueSize();
  settings.logoImgPath = logoImgPath;
  settings.preset = $('#preset-dropdown').text().trim();
  if ($('#crf').val() < 51 && $('#crf').val() > 0) {
    settings.crf = parseInt($('#crf').val());
  } else {
    settings.crf = 23;
  }
  settings.type = $('#type').val();
  if (backImage == undefined) settings.backImage = 'false';
  else {
    settings.backImgPath = backImage;
  }
  if (templatePath == undefined) settings.templatePath = 'false';
  else {
    settings.templatePath = templateFolderPath;
  }
  settings.enableFadeEffect = $('#fade-effect-dropdown').text();
  settings.fadeEffectDuration = $('#fade-effect-duration').val();
  if (mode == 'add-new-template') {
    const { value: templateName } = swal
      .fire({
        title: 'Enter Template Name',
        input: 'text',
        showCancelButton: true,
        closeOnCancel: true,
        inputValidator: (value) => {
          if (!value) {
            return 'Please enter template name !';
          }
        },
      })
      .then((res) => {
        if (res.value) {
          saveFile(res);
        }
      });
  } else {
    const res = {
      value: settingsRead.name,
    };
    saveFile(res);
  }
}
function qualityChanged(element) {
  $('#quality-dropdown').text(element.innerText);
}
function streamQualityChanged(element) {
  $('#stream-quality-dropdown').text(element.innerText);
}
function presetChanged(element) {
  $('#preset-dropdown').text(element.innerText);
}
function framerateChanged(element) {
  $('#stream-framerate-dropdown').text(element.innerText);
}
function outputFramerateChanged(element) {
  $('#output-framerate-dropdown').text(element.innerText);
}
function audioBitrateChanged(element) {
  $('#audio-bitrate-dropdown').text(element.innerText);
}
function videoBitrateChanged(element) {
  $('#video-bitrate-dropdown').text(element.innerText);
}
function fadeEffectChanged(element) {
  $('#fade-effect-dropdown').text(element.innerText);
}
function maxMuxingQueueSizeChanged(element) {
  $('#max-muxing-queue-size-dropdown').text(element.innerText);
}
// function handleAutoUpdateEvent(param) {
//
// 	param = JSON.parse(param);
// 	if (param.event == "update-available") {
// 		// show new icon at titlebar
// 		$(".new-image").show();
// 		$(".new-image").attr("src", "assets/images/new.png");
// 	} else if (param.event == "downloading-update") {
// 		$(".new-image,.install-update").hide();
// 		$(".infinite-loader").show();
// 		$(".infinite-loader").text((param.ev.percent).toFixed(0) + "%");
// 		// $(".new-img").attr("src","assets/images/new.png");
// 		isUpdateDownloading = true;
//
// 	} else if (param.event == "update-downloaded") {
// 		isUpdateDownloading = false;
// 		$(".install-update").show();
// 		$(".infinite-loader").hide();
// 		$(".new-image").hide();
// 	}
// }
// function onUpdateAvailableClick() {
// 	swal.fire({
// 		type: "info",
// 		title: "New version of HiFi Recorder is available",
// 		text: "Download Update ?",
// 		showCancelButton: true,
// 		cancelButtonText: "Later",
// 		showConfirmButton: true,
// 		confirmButtonText: "Download Now"
// 	}).then((res) => {
// 		if (res.value) {
// 			ipcRenderer.send("auto-updater", { event: "download-update-now" });
// 		}
// 	});
// }
// function onUpdateDownloadClick() {
// 	swal.fire({
// 		type: "info",
// 		title: "New version of HiFi Recorder has been Downloaded",
// 		text: "Install Update ?",
// 		showCancelButton: true,
// 		cancelButtonText: "Later",
// 		showConfirmButton: true,
// 		confirmButtonText: "Install Now"
// 	}).then((res) => {
// 		if (res.value) {
// 			ipcRenderer.send("auto-updater", { event: "install-update-now" });
// 		}
// 	});
// }
// function checkAutoUpdaterStatus() {
// 	ipcRenderer.send("request-auto-updater-status");
// }
function removeLogoImg() {
  logoImgPath = null;
  $('.logo-canvas-wrap').remove();
}
function changeLogoImg() {
  let logo = dialog.showOpenDialog(currentWindow, {
    filters: [{ name: 'Images', extensions: ['jpg', 'png'] }],
    properties: ['openFile'],
  });
  if (logo) {
    removeLogoImg();
    logoImgPath = logo[0].replace(/\\/g, '\\\\');
    // $("#dummy-back-img").attr('src',logoImgPath);
    // debugger;
    const logoCanvas =
      `
				<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="` +
      logoImgPath +
      `" class='logo-canvas'></div>
			`;
    $('#preview')
      .append(logoCanvas)
      .ready(function () {
        $('.draggable').draggable({
          cursor: 'crosshair',
          containment: '#preview',
          snap: '#preview',
          snapMode: 'inner',
          snapTolerance: 1,
        });
        $('.resizable').resizable({
          containment: '#preview',
          handles: 'e,w,n,s,se,nw,ne,sw',
        });
      });
  }
}
function setDimensionsInAspectRatio(selectedCanvas) {
  //
  let width = $(selectedCanvas).width();
  let height = $(selectedCanvas).height();
  // TODO - reduce width to set in 16:9
  // TODO - 1. set width in multiple of 16
  let newWidth = width,
    newHeight = height;
  if (width % 16 != 0) {
    newWidth = width - (16 - (width % 16));
    $(selectedCanvas).width(newWidth);
    // TODO - now set height as per newWidth
    newHeight = parseInt((newWidth * 9) / 16);
    height = newHeight;
  }
  if (height % 9 != 0) {
    newHeight = height - (9 - (height % 9));
    $(selectedCanvas).height(newHeight);
  }
  $(selectedCanvas).children('canvas').css('width', '100%');
  $(selectedCanvas).children('canvas').css('height', '100%');
  snapIfOutofBounds(selectedCanvas);
}
function snapIfOutofBounds(selectedCanvas) {
  const top = parseInt($(selectedCanvas).css('top').replace('px', ''));
  const height = parseInt($(selectedCanvas).css('height').replace('px', ''));
  const backgroundHeight = $('#preview').height();
  const condition = top + height > backgroundHeight;
  if (condition) {
    // TODO - 1. calculate how much portion is out of the frame
    // TODO - 2. substract out-portion from current top to move it top
    const newTop = top + height - backgroundHeight;
    $(selectedCanvas).css('top', top - newTop + 'px');
  }
  // debugger;
}
function getStreamFrameRate() {
  let value = $('#stream-framerate-dropdown').text();
  value = value.replace(' FPS', '');
  return value;
}
function getOutputFrameRate() {
  let value = $('#output-framerate-dropdown').text();
  value = value.replace(' FPS', '');
  return value;
}
function getAudioBitsPerSecond() {
  let value = $('#audio-bitrate-dropdown').text();
  if (value === 'Auto') {
    // ? returning auto as string
  } else {
    value = parseInt(value, 10);
  }
  return value;
}
function getVideoBitsPerSecond() {
  let value = $('#video-bitrate-dropdown').text();
  if (value === 'Auto') {
    // ? returning auto as string
  } else {
    value = parseInt(value, 10);
  }
  return value;
}
function getMaxMuxingQueueSize() {
  let value = $('#max-muxing-queue-size-dropdown').text();
  if (value === 'Auto') {
    // ? returning auto as string
  } else {
    value = parseInt(value, 10);
  }
  return value;
}
function showLoader() {
  $('.update-loader').css('display', 'inline-block');
}
function hideLoader() {
  $('.update-loader').hide();
}
function showTemplatesList() {
  sessionStorage.setItem('currentSettingsMode', 'generalSettingsMode');
  ipcRenderer.send('show-template-list');
}
function initColorVal() {
  $(function () {
    $('input[type="file"]').change(function () {
      if ($(this).val() != '') {
        $(this).css('color', '#333');
      } else {
        $(this).css('color', 'transparent');
      }
    });
  });
}
function readSettingsFileIfExists() {
  if (fs.existsSync(env.settingsFilePath)) {
    var allTemplates = JSON.parse(
      fs.readFileSync(env.settingsFilePath, 'utf8')
    );
    // ? -1 is passed when we add new template
    if (templateIndex == '-1') {
      templateIndex = allTemplates.length;
    }
    settingsRead = allTemplates[templateIndex];
    if (settingsRead) {
      if (settingsRead.defaultkey === true) {
        isCurrentTemplateDefault = true;
      } else {
        isCurrentTemplateDefault = false;
      }
      if (!settingsRead.streamRes) {
        settingsRead.streamRes = {
          width: 1920,
          height: 1080,
        };
      }
      if (!settingsRead.streamFrameRate) {
        settingsRead.streamFrameRate = '30';
      }
      if (!settingsRead.outputFrameRate) {
        settingsRead.outputFrameRate = 'auto';
      }
      if (!settingsRead.videoBitsPerSecond) {
        settingsRead.videoBitsPerSecond = 2048 * 1000;
      }
      if (!settingsRead.audioBitsPerSecond) {
        settingsRead.audioBitsPerSecond = 128 * 1000;
      }
      if (!settingsRead.maxMuxingQueueSize) {
        settingsRead.maxMuxingQueueSize = 'Auto';
      }
      $('#audio-bitrate-dropdown').text(settingsRead.audioBitsPerSecond);
      $('#video-bitrate-dropdown').text(settingsRead.videoBitsPerSecond);
      if (
        settingsRead.streamRes.width === 'Auto' &&
        settingsRead.streamRes.height === 'Auto'
      ) {
        $('#stream-quality-dropdown').text('Auto');
      } else {
        $('#stream-quality-dropdown').text(
          settingsRead.streamRes.width + 'X' + settingsRead.streamRes.height
        );
      }
      $('#quality-dropdown').text(
        settingsRead.outputRes.width + 'X' + settingsRead.outputRes.height
      );
      $('#preset-dropdown').text(settingsRead.preset);
      $('#stream-framerate-dropdown').text(
        settingsRead.streamFrameRate + ' FPS'
      );
      $('#crf').val(settingsRead.crf);
      $('#type').val(settingsRead.type);
      if (!settingsRead.backImgPath) {
        settingsRead.backImgPath = 'false';
      }
      if (settingsRead.outputFrameRate === 'auto') {
        $('#output-framerate-dropdown').text(settingsRead.outputFrameRate);
      } else {
        $('#output-framerate-dropdown').text(
          settingsRead.outputFrameRate + ' FPS'
        );
      }
      settingsRead.backImgPath = settingsRead.backImgPath.replace(/ /g, '%20');
      if (settingsRead.enableFadeEffect) {
        $('#fade-effect-dropdown').text(settingsRead.enableFadeEffect);
      } else {
        $('#fade-effect-dropdown').text('Disable');
      }
      if (settingsRead.fadeEffectDuration) {
        $('#fade-effect-duration').val(settingsRead.fadeEffectDuration);
      } else {
        $('#fade-effect-duration').val(1);
      }
      if (settingsRead.maxMuxingQueueSize) {
        $('#max-muxing-queue-size-dropdown').text(
          settingsRead.maxMuxingQueueSize
        );
      }
      // backImage=settingsRead.backImgPath.replace(/\\/g,'\\\\');
      backImage = settingsRead.backImgPath;
      if (settingsRead.logoImgPath) {
        removeLogoImg();
        logoImgPath = settingsRead.logoImgPath;
        const logoCanvas =
          `
					<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="` +
          settingsRead.logoImgPath +
          `" class='logo-canvas'></div>
				`;
        $('#preview')
          .append(logoCanvas)
          .ready(function () {
            $('.draggable').draggable({
              cursor: 'crosshair',
              containment: '#preview',
              snap: '#preview',
              snapMode: 'inner',
              snapTolerance: 1,
            });
            $('.resizable').resizable({
              containment: '#preview',
              handles: 'e,w,n,s,se,nw,ne,sw',
            });
          });
      }
    }
  } else {
    isCurrentTemplateDefault = false;
  }
  // if (fs.existsSync(env.settingsFilePath)) {
  // 	settingsRead = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
  // 	// ! this key is not present in older version recordings
  // } else {
  // 	backImage = "false";
  // }
}
function loadSettingsIfReadFromFile() {
  if (settingsRead) {
    $('#cameraCanvas-wrap').css(
      'height',
      settingsRead.preview.camera.cheightRaw
    );
    $('#cameraCanvas-wrap').css('width', settingsRead.preview.camera.cwidthRaw);
    $('#cameraCanvas-wrap').css('top', settingsRead.preview.camera.ctopRaw);
    $('#cameraCanvas-wrap').css('left', settingsRead.preview.camera.cleftRaw);
    $('#screenCanvas-wrap').css(
      'height',
      settingsRead.preview.screen.cheightRaw
    );
    $('#screenCanvas-wrap').css('width', settingsRead.preview.screen.cwidthRaw);
    $('#screenCanvas-wrap').css('top', settingsRead.preview.screen.ctopRaw);
    $('#screenCanvas-wrap').css('left', settingsRead.preview.screen.cleftRaw);
    $('#bothscreenCanvas-wrap').css(
      'height',
      settingsRead.preview.both.sheightRaw
    );
    $('#bothscreenCanvas-wrap').css(
      'width',
      settingsRead.preview.both.swidthRaw
    );
    $('#bothscreenCanvas-wrap').css('top', settingsRead.preview.both.stopRaw);
    $('#bothscreenCanvas-wrap').css('left', settingsRead.preview.both.sleftRaw);
    $('#bothcameraCanvas-wrap').css(
      'height',
      settingsRead.preview.both.cheightRaw
    );
    $('#bothcameraCanvas-wrap').css(
      'width',
      settingsRead.preview.both.cwidthRaw
    );
    $('#bothcameraCanvas-wrap').css('top', settingsRead.preview.both.ctopRaw);
    $('#bothcameraCanvas-wrap').css('left', settingsRead.preview.both.cleftRaw);
    if (settingsRead.logoCoords) {
      $('.logo-canvas-wrap').css('width', settingsRead.logoCoords.widthRaw);
      $('.logo-canvas-wrap').css('height', settingsRead.logoCoords.heightRaw);
      $('.logo-canvas-wrap').css('left', settingsRead.logoCoords.leftRaw);
      $('.logo-canvas-wrap').css('top', settingsRead.logoCoords.topRaw);
    }
  }
}
function attachDraggables() {
  $('.resizable').resizable({
    containment: '#preview',
    handles: 'e,w,n,s,se,nw,ne,sw',
  });
  $('.draggable').draggable({
    cursor: 'crosshair',
    containment: '#preview',
  });
}
function attachBackImage() {
  $('#preview').css('background-image', 'url(' + backImage + ')');
  $('#preview').css('background-size', '100% 100%');
  $('#preview').css('background-repeat', 'norepeat');
}
function initWindow() {
  if (mode === 'show-settings') {
    currentWindow = electron.remote.getCurrentWindow();
    currentWindow.setSize(800, 600);
    let bounds = electron.screen.getPrimaryDisplay().bounds;
    let x = bounds.x + (bounds.width - 800) / 2;
    let y = bounds.y + (bounds.height - 600) / 2;
    currentWindow.setPosition(x, y);
  } else if (mode === 'add-new-template' || mode === 'edit-template') {
    currentWindow = electron.remote.getCurrentWindow();
    currentWindow.maximize();
  }
}

function saveFile(res) {
  var myArray = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
  console.log(currentRec);
  if (selectedMode == 'camera') {
    settings.preview.camera.currentRec = currentRec;
  } else if (selectedMode == 'screen') {
    settings.preview.screen.currentRec = currentRec;
  } else if (selectedMode == 'both') {
    settings.preview.both.currentRec = currentRec;
  }
  myArray[templateIndex] = settings;
  // TODO 1. read the array from actual file
  // TODo 2. using global template index variable add new object to array
  // TODO 3. write the updated array in file
  console.log(mode);
  if (mode === 'add-new-template' || mode === 'edit-template') {
    htmlToImage
      .toPng(document.getElementById('preview'))
      .then(function (dataUrl) {
        saveFileHelper(dataUrl, myArray, res);
      })
      .catch(function (error) {
        throw error;
      });
  } else {
    saveFileHelper(null, myArray, res);
  }
}
function saveFileHelper(dataUrl, myArray, res) {
  if (!fs.existsSync(env.settingsTemplatePath)) {
    fs.mkdirSync(env.settingsTemplatePath);
  }
  const templateFolderPath = path.join(
    env.settingsTemplatePath,
    res.value + '.png'
  );
  if (dataUrl) {
    var base64Data = dataUrl.replace(/^data:image\/png;base64,/, '');
    fs.writeFileSync(templateFolderPath, base64Data, 'base64');
  }
  settings.defaultkey = isCurrentTemplateDefault;
  settings.name = res.value;
  settings.templatePath = templateFolderPath;
  if (res.value === 'Auto Generated Template') {
    settings.templatePath = '../assets/images/auto-generated-template.png';
  }
  fs.writeFile(env.settingsFilePath, JSON.stringify(myArray), 'utf8', (err) => {
    if (err) throw err;
    swal.fire({
      type: 'success',
      toast: true,
      position: 'top-end',
      title: 'Settings Saved!',
      timer: 3000,
    });
  });
}
async function startDownloadingBackgroundTemplates() {
  console.log('need to start background templates download');
  $('#Progress_Status').css('display', 'inline-block');
  $('.btn-cancel-download-background-image-templates').hide();
  $('.btn-download-background-image-templates').prop('disabled', true);
  try {
    AWS.config.setPromisesDependency();
    AWS.config.update({
      accessKeyId: 'AKIAI7TCQNFNFSCMUDFA',
      secretAccessKey: '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',
      region: 'ap-south-1',
    });
    const s3 = new AWS.S3();
    const response = await s3
      .listObjectsV2({
        Bucket: 'hifi-recorder-templates',
      })
      .promise();

    if (!fs.existsSync(env.templateBackgroundImagesPath)) {
      fs.mkdirSync(env.templateBackgroundImagesPath, { recursive: true });
    }
    for (var index = 0; index < response.KeyCount; index++) {
      console.log('cancelTemplateDownload: ', cancelTemplateDownload);
      if (cancelTemplateDownload === true) {
        cancelTemplateDownload = false;
        break;
      } else {
        var key = response.Contents[index].Key;
        var params = {
          Bucket: 'hifi-recorder-templates',
          Key: key,
        };
        var fileExt = key.substr(key.lastIndexOf('/') + 1);
        var file = fs.createWriteStream(
          env.templateBackgroundImagesPath + fileExt
        );
        await s3
          .getObject(params)
          .createReadStream()
          .pipe(file)
          .on('finish', () => {
            Progress(response);
          });
      }
    }
    console.log('for loop completed');
  } catch (e) {
    console.error('e: ', e);
    swal.fire({
      icon: 'error',
      title: 'Something went wrong !',
      text: 'Please try again',
    });
    $('#Progress_Status').hide();
    $('.btn-cancel-download-background-image-templates').hide();
    $('.btn-download-background-image-templates').prop('disabled', false);
    totalFilesWRitten = 0;
  }
}
function Progress(response) {
  totalFilesWRitten++;
  var element = document.getElementById('myprogressBar');
  var percentagecount = Math.round(
    (totalFilesWRitten / response.KeyCount) * 100
  );
  element.style.width = percentagecount + '%';
  element.innerHTML = percentagecount + '%';

  console.log('total Files Written: ', totalFilesWRitten);
  if (totalFilesWRitten === response.KeyCount) {
    console.log('all files written !');
    swal
      .fire({
        title: 'Download Complete',
        type: 'success',
        showCancelButton: true,
        confirmButtonColor: 'blue',
        confirmButtonText: 'Show Downloaded Images',
        cancelButtonText: 'Cancel',
      })
      .then((res) => {
        console.log('should open folder : ', res.value);
        if (res.value) {
          shell.openItem(env.templateBackgroundImagesPath);
        }
        console.log(
          'env.templateBackgroundImagesPath: ',
          env.templateBackgroundImagesPath
        );
      });
    $('#Progress_Status').hide();
    $('.btn-cancel-download-background-image-templates').hide();
    $('.btn-download-background-image-templates').prop('disabled', false);
    stopDownloadingBackgroundTemplates();
  }
}
function stopDownloadingBackgroundTemplates() {
  // cancelTemplateDownload = true;
  totalFilesWRitten = 0;
  $('#Progress_Status').hide();
  $('.btn-cancel-download-background-image-templates').hide();
  var element = document.getElementById('myprogressBar');
  var percentagecount = 0;
  element.style.width = percentagecount + '%';
  element.innerHTML = percentagecount + '%';
  console.log('cancelTemplateDownload: ', cancelTemplateDownload);
  $('.btn-download-background-image-templates').prop('disabled', false);
}
function imageLoaded(type) {}
function openPropertiesPanel(panelItemName) {
  console.log(panelItemName);
  if (panelItemName === 'scrolling-text') {
    handleOpenScrollingTextProperties();
  }
  if (panelItemName === 'nothing') {
    $('.properties-panel-item').hide();
  }
}
function handleOpenScrollingTextProperties() {
  $('.scrolling-text-option').show();
}
function initVideoScrollTexts() {
  // let obj = config.finalMerger[i];
  let aScrollWrapper = scrollingTextWrapperElement;
  aScrollWrapper = aScrollWrapper.replace(/merger-index-here/g, 0);
  aScrollWrapper = aScrollWrapper.replace(
    /scrolling-text-wrapper-id-here/g,
    'scrolling-text-wrapper-' + 0
  );
  $('#preview')
    .append(aScrollWrapper)
    .ready(function (elem, data) {
      // $(".resizable")
      $('.scrolling-text-wrapper').draggable({
        cursor: '-webkit-grab',
        containment: '#preview',
        snap: '#preview',
        snapMode: 'inner',
        snapTolerance: 1,
        drag: function () {
          setScrollingTextTopLocation(this);
        },
      });
    });
}
function setScrollingTextTopLocation(elem) {
  let fixedTextWrapClass = '#scrolling-text-wrapper-' + 0 + ' .fixed-text-wrap';
  const element = $(fixedTextWrapClass).get(0);
  // if (settingsRead.preview[selectedMode].currentRec) {
    if (element) {
      currentRec.scrollingTextObj['topLocationRawPx'] = parseInt(
        $(elem).css('top').replace('px', '')
      );
      currentRec.scrollingTextObj['fixedTextWidthRawPx'] = $(
        fixedTextWrapClass
      ).width();
    }
  // }
}
function toggleScrollingText(isChecked) {
  currentRec.scrollingTextObj.scrollingTextCheck = isChecked;
  if (currentRec.scrollingTextObj.scrollingTextCheck) {
    let scrollingTextWrapperId = '#scrolling-text-wrapper-' + 0;
    $(scrollingTextWrapperId).show();
    // ? this solved problem which made top and left of fixed text to be null in config file
    adjustScrollTextWidths();
  } else {
    let scrollingTextWrapperId = '#scrolling-text-wrapper-' + 0;
    $(scrollingTextWrapperId).hide();
  }
}
function initScrollingTextObj() {
  console.log('in obj');
  currentRec.scrollingTextObj = {
    fixedText: 'HiFi Recorder',
    scrollingText: 'Contact us on www.hifitechnologysolutions.com',
    fontSize: 8,
    fixedTextColor: '#000002',
    fixedTextBackColor: '#fdffff',
    scrollTextColor: '#000002',
    scrollTextBackColor: '#fdffff',
    scrollTextStartTimeSec: 0,
    scrollTextRunningTimeSec: 20,
    scrollingTextCheck: false,
  };
}
function scrollingTextValuesUpdated() {
  const fontSize = $('#scrolling-text-font-size-input').val();
  let fixedText = $('#scrolling-text-fixed-text-input').val();
  let scrollingText = $('#scrolling-text-scrolling-text-input').val();
  const fixedTextColor = $('#fixed-text-color').val();
  const fixedTextBackColor = $('#fixed-text-back-color').val();
  const scrollTextColor = $('#scrolling-text-color').val();
  const scrollTextBackColor = $('#scrolling-text-back-color').val();
  const scrollTextStartTimeSec = $('.scrolling-text-start-time-sec').val();
  const scrollTextRunningTimeSec = $('.scrolling-text-running-time-sec').val();
  currentRec.scrollingTextObj = {
    fixedText: fixedText,
    scrollingText: scrollingText,
    fontSize: fontSize,
    fixedTextColor,
    fixedTextBackColor,
    scrollTextColor,
    scrollTextBackColor,
    scrollTextStartTimeSec: getSecondsFromHMS(scrollTextStartTimeSec).seconds,
    scrollTextRunningTimeSec: getSecondsFromHMS(scrollTextRunningTimeSec).seconds,
    scrollingTextCheck: currentRec.scrollingTextObj.scrollingTextCheck,
  };
  let scrollingTextWrapperId = '#scrolling-text-wrapper-' + 0;
  let scrollingTextWrapClass = scrollingTextWrapperId + ' .scrolling-text-wrap';
  let fixedTextWrapClass = scrollingTextWrapperId + ' .fixed-text-wrap';
  $(fixedTextWrapClass).text(currentRec.scrollingTextObj.fixedText);
  $(scrollingTextWrapClass).text(currentRec.scrollingTextObj.scrollingText);
  $('.scrolling-text-canvas-content').css( 'fontSize', currentRec.scrollingTextObj.fontSize + 'px' );
  $('#fixed-text-color').val(currentRec.scrollingTextObj.fixedTextColor);
  $('#fixed-text-back-color').val( currentRec.scrollingTextObj.fixedTextBackColor );
  $('#scrolling-text-color').val(currentRec.scrollingTextObj.scrollTextColor);
  $('#scrolling-text-back-color').val( currentRec.scrollingTextObj.scrollTextBackColor );
  $(fixedTextWrapClass).css( 'color', currentRec.scrollingTextObj.fixedTextColor );
  $(scrollingTextWrapClass).css( 'color', currentRec.scrollingTextObj.scrollTextColor );
  $(fixedTextWrapClass).css( 'background', currentRec.scrollingTextObj.fixedTextBackColor );
  $(scrollingTextWrapClass).css( 'background', currentRec.scrollingTextObj.scrollTextBackColor );
  $('.scrolling-text-start-time-sec').val( secondsToHms(currentRec.scrollingTextObj.scrollTextStartTimeSec) );
  $('.scrolling-text-running-time-sec').val( secondsToHms(currentRec.scrollingTextObj.scrollTextRunningTimeSec) );
  adjustScrollTextWidths();
  // ? fix to solve bug of topLocationPx value gets removed from
  // ? scrolling text object because of reassignment
  setScrollingTextTopLocation($(scrollingTextWrapperId));
}
function adjustScrollTextWidths() {
  let scrollingTextWrapperId = '#scrolling-text-wrapper-' + 0;
  const element = $(scrollingTextWrapperId).get(0);
  if (element) {
    const totalWidth = parseInt(
      $(scrollingTextWrapperId).css('width').replace('px', ''),
      10
    );
    const fixedTextWidth = parseInt(
      $(scrollingTextWrapperId)
        .find('.fixed-text-wrap')
        .css('width')
        .replace('px', ''),
      10
    );
    scrollingTextWidth = totalWidth - fixedTextWidth - 20 - 1 + 'px';
    $(scrollingTextWrapperId)
      .find('.scrolling-text-wrap')
      .css('width', scrollingTextWidth);
  }
}
function getSecondsFromHMS(hms) {
  const a = hms.split(':'); // split it at the colons
  const time = { seconds: 0, milliseconds: 0, floatSeconds: 0 };
  // minutes are worth 60 seconds. Hours are worth 60 minutes.
  const seconds = +a[0] * 60 * 60 + +a[1] * 60 + +a[2];
  if (isNaN(seconds)) {
    time.seconds = NaN;
  } else {
    time.seconds = seconds;
  }
  time.milliseconds = a[3];
  time.floatSeconds = parseFloat(time.seconds + '.' + time.milliseconds);
  return time;
}
function secondsToHms(timeInSeconds) {
  const pad = function (num, size) {
      return ('000' + num).slice(size * -1);
    },
    time = parseFloat(timeInSeconds).toFixed(3),
    hours = Math.floor(time / 60 / 60),
    minutes = Math.floor(time / 60) % 60,
    seconds = Math.floor(time - minutes * 60);
  const milli = ((timeInSeconds - parseInt(timeInSeconds)) * 1000).toFixed(0);
  return (
    pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2) + ':' + milli
  );
}
function initScrollingText() {
  // ? hides all scrolling-text-wrapper
  $('.scrolling-text-wrapper').each((index, wrapper) => {
    $(wrapper).hide();
  });
  let scrollingTextWrapperId = '#scrolling-text-wrapper-' + 0;
  let scrollingTextWrapClass = scrollingTextWrapperId + ' .scrolling-text-wrap';
  let fixedTextWrapClass = scrollingTextWrapperId + ' .fixed-text-wrap';
  // ? if already exists

  if (! settingsRead || (!settingsRead.preview[selectedMode].currentRec)) {
    initScrollingTextObj();
    // $(scrollingTextWrapperId).hide();
  } else {
    currentRec = settingsRead.preview[selectedMode].currentRec;

  }
  if (currentRec.scrollingTextObj.scrollingTextCheck === true) {
    $(scrollingTextWrapperId).show();
  } else {
    $(scrollingTextWrapperId).hide();
  }
  if (currentRec.scrollingTextObj.topLocationRawPx) {
    $(scrollingTextWrapperId).css( 'top', currentRec.scrollingTextObj.topLocationRawPx + 'px' );
  } else {
    // ? set to some default location at the bottom maybe
    const elemHeight = $('#scrolling-text-wrapper-0').height();
    const previewHeight = $('#preview').height();
    const relativeTop = previewHeight - elemHeight - 6;
    $(scrollingTextWrapperId).css('top', relativeTop + 'px');
  }

  $('.scrolling-text-check').prop( 'checked', currentRec.scrollingTextObj.scrollingTextCheck );
  $('#scrolling-text-font-size-input').val( currentRec.scrollingTextObj.fontSize );
  $('#scrolling-text-fixed-text-input').val( currentRec.scrollingTextObj.fixedText );
  $('#scrolling-text-scrolling-text-input').val( currentRec.scrollingTextObj.scrollingText );
  $(fixedTextWrapClass).text(currentRec.scrollingTextObj.fixedText);
  $(scrollingTextWrapClass).text(currentRec.scrollingTextObj.scrollingText);
  $('.scrolling-text-canvas-content').css( 'fontSize', currentRec.scrollingTextObj.fontSize + 'px' );
  $('#fixed-text-color').val(currentRec.scrollingTextObj.fixedTextColor);
  $('#fixed-text-back-color').val( currentRec.scrollingTextObj.fixedTextBackColor );
  $('#scrolling-text-color').val(currentRec.scrollingTextObj.scrollTextColor);
  $('#scrolling-text-back-color').val( currentRec.scrollingTextObj.scrollTextBackColor );
  $(fixedTextWrapClass).css( 'color', currentRec.scrollingTextObj.fixedTextColor );
  $(scrollingTextWrapClass).css( 'color', currentRec.scrollingTextObj.scrollTextColor );
  $(fixedTextWrapClass).css( 'background', currentRec.scrollingTextObj.fixedTextBackColor );
  $(scrollingTextWrapClass).css( 'background', currentRec.scrollingTextObj.scrollTextBackColor );
  $('.scrolling-text-start-time-sec').val( secondsToHms(currentRec.scrollingTextObj.scrollTextStartTimeSec) );
  $('.scrolling-text-running-time-sec').val( secondsToHms(currentRec.scrollingTextObj.scrollTextRunningTimeSec) );
  adjustScrollTextWidths();
  setScrollingTextTopLocation($(scrollingTextWrapperId));
}

function openScrollColorPicker(elem) {
	const type = elem.getAttribute('type');
	switch (type) {
		case 'fix-text':
			$('#fixed-text-color').click();
			break;
		case 'fix-back':
			$('#fixed-text-back-color').click();
			break;
		case 'scroll-text':
			$('#scrolling-text-color').click();
			break;
		case 'scroll-back':
			$('#scrolling-text-back-color').click();
			break;
		default:
			break;
	}
}