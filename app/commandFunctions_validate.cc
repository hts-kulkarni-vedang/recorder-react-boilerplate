#include<napi.h>
// #include <node_api.h>
#include<windows.h>
#include<string>
#include "commandFunctions_validate.h"
#include <iostream> 
#include <fstream>

// for string trim functions
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
using namespace std;

HANDLE hStdIn_c; // Handle to parents std input. NULL
BOOL bRunThread_c;
int id;
std::string fileName;
DWORD currentGlobalProcessId = NULL;

HANDLE currentGlobalChildProcess = NULL;

void setId_fileName(int id_s,std::string filename)
{
    id=id_s;
    fileName=filename;
}
// trim from start
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}
void DisplayError(char *pszAPI)
{
    LPVOID lpvMessageBuffer;
    CHAR szPrintBuffer[512];
    DWORD nCharsWritten;
    // std::ofstream fout;
    errno = 0;
    cout << GetLastError();
    //std::replace( fileName[0], fileName[fileName.length], " ", "\x20");
    //fopen(str_replace(" ", "\x20", $filename), "r");
    // fout.open( "random.txt",std::fstream::app);
    // if (fout.fail()) 
    // {
    //     std::cout << "Couldn't open the file!" << endl; 
    // }
    // else
    // {
        FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER|FORMAT_MESSAGE_FROM_SYSTEM,
        NULL, GetLastError(),
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpvMessageBuffer, 0, NULL);
        wsprintf(szPrintBuffer,
        "ERROR: API    = %s.\n   error code = %d.\n   message    = %s.\n",
            pszAPI, GetLastError(), (char *)lpvMessageBuffer);
            
        WriteConsole(GetStdHandle(STD_OUTPUT_HANDLE),szPrintBuffer,
                  lstrlen(szPrintBuffer),&nCharsWritten,NULL);

        // fout<< "\n********************************************************\nERROR: API= "<<pszAPI
        // <<"\n error code ="<<GetLastError()
        // <<"\n message= "<<(char *)lpvMessageBuffer
        // <<"\n***********************************************\n";
        // fout.close();
    // }
    
    std::cout<<"Error %d \n"<< errno;

    LocalFree(lpvMessageBuffer);
    // HANDLE hd;
    // hd= GetCurrentProcess();
    // int id_h;
    //     id_h=  GetProcessId(hd);
    //     if(id!=id_h)
    // TerminateProcess(hd,GetLastError());
    //ExitProcess(GetLastError());
}


DWORD WINAPI GetAndSendInputThread(LPVOID lpvThreadParam)
{
    CHAR read_buff[256];
    DWORD nBytesRead,nBytesWrote;
    HANDLE hPipeWrite = (HANDLE)lpvThreadParam;

    // Get input from our console and send it to child through the pipe.
    while (bRunThread_c)
    {
        if(!ReadConsole(hStdIn_c,read_buff,1,&nBytesRead,NULL))
        {       
            DisplayError("ReadConsole");
            //   cb.Call(env.Global(), { Napi::String:: New(env,"preprocess-complete") },errorObj); }

            read_buff[nBytesRead] = '\0'; // Follow input with a NULL.

            if (!WriteFile(hPipeWrite,read_buff,nBytesRead,&nBytesWrote,NULL))
            {
                if (GetLastError() == ERROR_NO_DATA)
                    break; // Pipe was closed (normal exit path).
                else
                    DisplayError("WriteFile");
            }
        }
    }
  
    return 1;
}


void SetAndSendInputThread( HANDLE hStdIn_p,BOOL bRunThread_p)
{
    hStdIn_c=hStdIn_p; // Handle to parents std input. NULL
    bRunThread_c=bRunThread_p;
}


std::string ReadAndHandleOutput(Napi::Env env,HANDLE hPipeRead,std::string fileName)
{
    Napi::HandleScope scope(env);
    char lpBuffer[200];
    DWORD nBytesRead;
    // ofstream fout;
    std::string response;

    // fout.open("D:\\log.txt",std::fstream::out);
    // if(!fout)
    // {
    //     DisplayError("error opening log file");
    // }
    // else
    // {
        while(TRUE)
        {                   
            if (!ReadFile(hPipeRead,lpBuffer,sizeof(lpBuffer),
                      &nBytesRead,NULL) || !nBytesRead)
            {       
                if (GetLastError() == ERROR_BROKEN_PIPE)
                {            
                    break; // pipe done - normal exit path.
                }
                else
                {          
                    DisplayError("ReadFile"); // Something bad happened.
                }
            }        
            //fout<< lpBuffer;

            // cout << endl << "lpBuffer : " << lpBuffer;
            // fout << lpBuffer;
            // cout << endl << "trimmed lpBuffer : " << trim(lpBuffer);
            // cout << endl << "lpBuffer : " << strlen(lpBuffer);
            std::string s = lpBuffer;
            trim(s);
            // cout << endl << "length of trimmed lpBuffer : " << s;

            response += s;                 
        }


        /*this block will remove any linebreaks from the response.*/
        string::size_type pos = 0; 
        while ( ( pos = response.find ("\r\n",pos) ) != string::npos )
        {
            response.erase ( pos, 2 );
        }
        
        response.erase(remove_if(response.begin(), response.end(), isspace), response.end());

        // fout<<response;
        // fout.close();
    // }

    /*Creating a NAPI string to return it to the javascript file.*/
    // return Napi::String::New(env, response);

    return response;
}



void PrepAndLaunchRedirectedChild(HANDLE hChildStdOut,
                                    HANDLE hChildStdIn,
                                    HANDLE hChildStdErr,
                                    std::string *strCommands,
                                    int commandCounter,
                                    HANDLE hChildProcess,
                                    Napi::Env env)
{
    // std::cout<<"\ninside prep";
    const int *NumberOfProcesses = &commandCounter;

    STARTUPINFO *si = new STARTUPINFO[*NumberOfProcesses];
    PROCESS_INFORMATION *pi = new PROCESS_INFORMATION[*NumberOfProcesses];
    
     
    for(int i=0;i<commandCounter;i++)
    {
        ZeroMemory(&si[i],sizeof(si[i]));
        si[i].cb = sizeof(si);
        si[i].dwFlags = STARTF_USESTDHANDLES;
        si[i].hStdOutput = hChildStdOut;
        si[i].hStdInput  = hChildStdIn;
        si[i].hStdError  = hChildStdErr;
        LPTSTR cmd = const_cast<char *>((strCommands[i]).c_str());

        if (!CreateProcess(NULL,cmd,NULL,NULL,TRUE,CREATE_NO_WINDOW,NULL,NULL,&si[i],&pi[i]))
            DisplayError("Inside CreateProcess");
        //if (!CreateProcess(NULL,cmd,NULL,NULL,TRUE,CREATE_NO_WINDOW,NULL,NULL,&si[i],&pi[i]))

        // Set global child process handle to cause threads to exit.
        hChildProcess = pi[i].hProcess;
        currentGlobalChildProcess = pi[i].hProcess;
        currentGlobalProcessId = pi[i].dwProcessId;

        cout << endl << "handle to created process : " << currentGlobalChildProcess << endl;

        // Close any unnecessary handles.
        if (!CloseHandle(pi[i].hThread)) 
            DisplayError("CloseHandle");
    }
}
