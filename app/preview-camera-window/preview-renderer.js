const { ipcRenderer, remote } = require("electron")
const electron = require("electron");
var fs = require('fs')
var os = require('os')
var documentsFolderPath = os.homedir + '/Documents/HiFi_Recorder Files'
let secs = 0;
let mins = 0;
let hours = 0;
let interval = null;
var videoSelect;
var video = document.getElementById('preview-video');
/*var audioContent*/
let videoConstraint_g
var previewStream = []

//var errLogstream
/*Developer Version*/
$(document).ready(function () {
	// errLogstream = fs.createWriteStream(documentsFolderPath + '/assets/' + 'preview-error-log.txt');
	// fs.truncate(documentsFolderPath + '/assets/' + 'preview-error-log.txt', function () {
	// 	errLogstream.write('Preview error Log File')
	// })
	// alert("doc rready");

	var param = sessionStorage.getItem('preview-param')
	var hms = sessionStorage.getItem('currentTime');
	console.log("reloaded param : ",param);
	if (hms) {
		hours = parseInt(hms.split(':')[0])
		mins = parseInt(hms.split(':')[1])
		secs = parseInt(hms.split(':')[2])
		$('#hrs').text(hours)
		$('#mins').text(mins)
		$('#secs').text(secs)
	}
	if (param != '') {
		param = JSON.parse(param)
		startPreview(param)
	}
	else {
		$('#mic-name', 'Please re-connect Mic')
	}
})
ipcRenderer.on('open-camera-preview', (event, param) => {
	console.log("inopen -open-camera-preview : ",param)
	var h = $("#hours").text();
	var m = $("#mins").text();
	var s = $("#secs").text();
	sessionStorage.setItem('preview-param', JSON.stringify(param))
	sessionStorage.setItem('currentTime', h + ':' + m + ':' + s)
	if (param.reload) {
		//debugger
		window.location.reload()
	}
	else {
		startPreview(param)
	}

});
function startPreview(param) {
	if (param != null) {
		videoSelect = param.selectedDevice;
		$('#mic-name').text(JSON.parse(param.selectedAudioDevice).label);
		console.log("param is : ",param);
		if(param.showAfterOpen == false){
			electron.remote.getCurrentWindow().hide();	
			ipcRenderer.send('send-show-hide-to-main-window');
		}
		initDevices(param);
	}
	$("#preview-title").show();
}
ipcRenderer.on('preview-shortcut', (event, param) => {
	if (param == 'play-pause') {
		$("#preview-play-pause").click()
	}
	else if (param == 'stop') {
		$("#preview-stop").click()
	}
})
ipcRenderer.on('stop-from-preview', (event, param) => {
	$("#preview-stop").click()
})
ipcRenderer.on('timer', (event, param) => {
	if (param == 'reset') {
		resetTimer();
	}
	else if (param == 'resume') {
		resumeTimer();
	}
	else if (param == 'pause') {
		pauseTimer();
	}
})
function initDevices(param) {
	console.log(param);
	let audioSource = JSON.parse(param.selectedAudioDevice);
	let videoSource = JSON.parse(param.selectedVideoDevice);
	console.log(audioSource,videoSource);
	const c_constraints = {
		audio: true,
		video: {
			deviceId: videoSource.deviceId,
			width: { min: 640, ideal: 1920 },
			height: { min: 400, ideal: 1080 },
			framerate: {ideal: 30}
		}
	}
	navigator.mediaDevices.getUserMedia(c_constraints)
		.then((stream) => {
			c_handleStream(stream)
		})
		.catch((e) => c_handleError(e));
}
function errorCallback(error) {
}
function c_handleStream(stream) {
	var videoTracks = stream.getVideoTracks();
	video.srcObject = stream
	previewStream.push(stream)
}
function c_handleError(err) {
	alert(err.toString())
	//ipcRenderer.send('devices-error')
}
function playPause() {
	if (recFlag) {
		$("#play-img").attr('src', '../assets/images/play.png');
		$("#preview-stop").hide();
		ipcRenderer.send('preview', 'pause-rec');
		recFlag = false;
		//pauseTimer()
	}
	else {
		$("#play-img").attr('src', '../assets/images/pause.png');
		$("#preview-stop").show();
		ipcRenderer.send('preview', 'start-rec');
		recFlag = true;
		//resumeTimer()
	}
}
function stop() {
	$("#play-img").attr('src', '../assets/images/play.png');
	ipcRenderer.send('preview', 'stop-rec');
	recFlag = false;
	$("#preview-stop").hide();
	clearInterval(interval);
	interval = null
	hours = 0;
	secs = 0;
	mins = 0;
	$("#hours").text(hours);
	$("#mins").text(mins);
	$("#secs").text(secs);
}
function resumeTimer() {
	interval = setInterval(function () {
		secs++;
		if (secs == 60) {
			secs = 0;
			mins++;
		}
		if (mins == 60) {
			mins = 0;
			hours++;
		}
		$("#hours").text(hours);
		$("#mins").text(mins);
		$("#secs").text(secs);
		ipcRenderer.send('update-current-time', hours + 'h:' + mins + 'm:' + secs + 's')
	}, 1000);
}
function pauseTimer() {
	clearInterval(interval)
}
function resetTimer() {
	clearInterval(interval);
	secs = 0
	mins = 0
	hours = 0
	$("#hours").text(hours);
	$("#mins").text(mins);
	$("#secs").text(secs);
	closePreviewStream(previewStream)
}
function closePreviewStream(streamArr) {
	for (var j = 0; j < streamArr.length; j++) {
		var stream = streamArr[j]
		var aTracks = stream.getAudioTracks()
		for (var i = 0; i < aTracks.length; i++) {
			aTracks[i].stop()
			aTracks[i] = null
		}
		var vTracks = stream.getVideoTracks()
		for (var i = 0; i < vTracks.length; i++) {
			vTracks[i].stop()
			vTracks[i] = null
		}
		stream = null
	}
}
ipcRenderer.on('close-log-streams', (event, param) => {
	//errLogstream.end()
})
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
	// errLogstream.write('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
	// 	+ ' Column: ' + column + ' StackTrace: ' + errorObj.toString() + '\n');
	alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
		+ ' Column: ' + column + ' StackTrace: ' + errorObj.toString() + '\n');
}