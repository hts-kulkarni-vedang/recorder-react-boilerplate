#include <napi.h>
#include <iostream>
#include <sstream>
#include <direct.h>
#include <string>
#include "commandFunctions_validate.h"
#include "encfileop.h"
#include <psapi.h>
#include <errno.h>
#include <array>
#include<windows.h>
#include <algorithm>
#include <iterator>
#include <string>
#include <cstdlib>
#include <wininet.h>
#include <tchar.h>
#include <Sensapi.h>
#pragma comment(lib, "Sensapi.lib")



HANDLE hChildProcess; //NULL
HANDLE hStdIn; // Handle to parents std input. NULL
BOOL bRunThread; 
std::string (*strCommands);

int noOfCommands=1; 
int commandCounter = 1;

HANDLE hOutputReadTmp,hOutputRead,hOutputWrite;
HANDLE hInputWriteTmp,hInputRead,hInputWrite;
HANDLE hErrorWrite;
HANDLE hThread;
DWORD ThreadId;


std::string RunCommand(std::string cmd, Napi::Env env);

std::string curlPath;
std::string serverPath;

std::string RunCommand(std::string cmd, Napi::Env env)
{
  strCommands = new std::string[noOfCommands];

  strCommands[0] = cmd;

  std::string outPutfileName= "log.txt";  // A file is generated to store output from server.

  /*Security certficates*/
  SECURITY_ATTRIBUTES sa;
  sa.nLength= sizeof(SECURITY_ATTRIBUTES);
  sa.lpSecurityDescriptor = NULL;
  sa.bInheritHandle = TRUE;
  /*CreatePipe() returns read handle and write handle for the pipe. A process creates a pipe
  just before it forks one or more child processes. The pipe is then used for communication
  either between the parent or child processes, or between two sibling processes*/
  if (!CreatePipe(&hOutputReadTmp,&hOutputWrite,&sa,0))
      DisplayError("CreatePipe");

  // Create a duplicate of the output write handle for the std error
  // write handle. This is necessary in case the child application
  // closes one of its std output handles.
  if (!DuplicateHandle(GetCurrentProcess(),hOutputWrite,
                      GetCurrentProcess(),&hErrorWrite,0,
                       TRUE,DUPLICATE_SAME_ACCESS))
      DisplayError("DuplicateHandle");

  // Create the child input pipe.
  if (!CreatePipe(&hInputRead,&hInputWriteTmp,&sa,0))
      DisplayError("CreatePipe");

  // Create new output read handle and the input write handles. Set
  // the Properties to FALSE. Otherwise, the child inherits the
  // properties and, as a result, non-closeable handles to the pipes
  // are created.
  if (!DuplicateHandle(GetCurrentProcess(),hOutputReadTmp,
                       GetCurrentProcess(),
                       &hOutputRead, // Address of new handle.
                       0,FALSE, // Make it uninheritable.
                       DUPLICATE_SAME_ACCESS))
      DisplayError("DupliateHandle");

  if (!DuplicateHandle(GetCurrentProcess(),hInputWriteTmp,
                           GetCurrentProcess(),
                           &hInputWrite, // Address of new handle.
                           0,FALSE, // Make it uninheritable.
                           DUPLICATE_SAME_ACCESS))
      DisplayError("DupliateHandle");
        

  // Close inheritable copies of the handles you do not want to be
  // inherited.
  if (!CloseHandle(hOutputReadTmp)) DisplayError("CloseHandle");
       if (!CloseHandle(hInputWriteTmp)) DisplayError("CloseHandle");
          // Get std input handle so you can close it and force the ReadFile to
          // fail when you want the input thread to exit.
          if ( (hStdIn = GetStdHandle(STD_INPUT_HANDLE)) ==
                                            INVALID_HANDLE_VALUE )
              DisplayError("GetStdHandle");

   //for creating process  

  PrepAndLaunchRedirectedChild(hOutputWrite,hInputRead,hErrorWrite,strCommands,commandCounter,hChildProcess,env);
  
  // Close pipe handles (do not continue to modify the parent).
  // You need to make sure that no handles to the write end of the
  // output pipe are maintained in this process or else the pipe will
  // not close when the child process exits and the ReadFile will hang.
  if (!CloseHandle(hOutputWrite)) DisplayError("CloseHandle");
     if (!CloseHandle(hInputRead )) DisplayError("CloseHandle");
        if (!CloseHandle(hErrorWrite)) DisplayError("CloseHandle");

        //Launch the thread that gets the input and sends it to the child.
        //DWORD getAndSendInputThread;
            SetAndSendInputThread(hStdIn,bRunThread);
 
  hThread = CreateThread(NULL,0,&GetAndSendInputThread,
                              (LPVOID)&hInputWrite,0,&ThreadId);
  if (hThread == NULL) 
      DisplayError("CreateThreadnn");

  //for writing output on command prompt and in files       // Read the child's output.
  // std::cout<<outPutfileName;

  std::string responseText = ReadAndHandleOutput(env,hOutputRead, outPutfileName);
  

  // Redirection is complete
  // Tell the thread to exit and wait for thread to die.

  bRunThread = FALSE;
  
  SetAndSendInputThread(hStdIn,bRunThread);//setting bRunthread's value

  if (WaitForSingleObject(hThread,INFINITE) == WAIT_FAILED)
      DisplayError("WaitForSingleObject");

  if (!CloseHandle(hStdIn)) DisplayError("CloseHandle");
      SetAndSendInputThread(hStdIn,bRunThread);

  if (!CloseHandle(hOutputRead)) DisplayError("CloseHandle");
      if (!CloseHandle(hInputWrite)) DisplayError("CloseHandle");
         return responseText;

}


std::string GetSerialNumber(Napi::Env env)
{
    system("wmic bios get serialnumber > sn.txt");
    wchar_t sn[16];
    FILE* fp = fopen("sn.txt","r, ccs=UTF-8");
    fgetws(sn,16,fp); 
    fgetws(sn,16,fp); 
    
    fclose(fp);
    remove("sn.txt");  
    wstring ws(sn);
    std::string str(ws.begin(),ws.end());
    
    return str;
    
}

std::string GetCurlPath(std::string basepath)
{
  std::string path;
    std::cout <<basepath<<"\n";

  if((sizeof(void *) * 8) == 32) 
  {
    path = basepath+ "/src/assets/exe/curl32";
  }
  else
  {
    path = basepath+ "/src/assets/exe/curl64";
  }

  return path;
}



std::string ServerValidation(std::string key, std::string basepath,  Napi::Env env)
{
  std::string cmd = "";
  curlPath = GetCurlPath(basepath);
  std::string serialNumber = GetSerialNumber(env);
  cmd = curlPath+" -sb -H -d key="+key+"&serialNumber="+serialNumber+" -H 'Content-Type: application/x-www-form-urlencoded' -X POST "+serverPath+"keycheck.php";
  return RunCommand(cmd, env);
}

std::string GlobalValidation(std::string basepath,  Napi::Env env)
{
  std::string cmd = "";
    
  curlPath = GetCurlPath(basepath);

  std::string serialNumber = GetSerialNumber(env);
  //std::string key = "1";

  cmd = curlPath+" -sb -H -d serialNumber="+serialNumber+" -H 'Content-Type: application/x-www-form-urlencoded' -X POST "+serverPath+"keycheck.php";

  return RunCommand(cmd, env);
}





Napi::String StartUpValidation(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
    
    std::string basepath = info[0].As<Napi::String>().ToString();

    serverPath = info[2].As<Napi::String>().ToString();


  DWORD dwSens;
  if (IsNetworkAlive(&dwSens) == TRUE)
  {
    std::string response = GlobalValidation(basepath, env);

    std::cout<<response;

    if( response == "false")
    {
      return Napi::String::New(env, "false");
    }
  }

  if(verifyLocal())
  {
    return Napi::String::New(env, "true");
  }
  else
  {
    return Napi::String::New(env, "false");
  }

}



Napi::String InitialValidation(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
    
  std::string key = info[0].As<Napi::String>().ToString();
    
  std::string basepath = info[1].As<Napi::String>().ToString();

  serverPath = info[2].As<Napi::String>().ToString();

  std::string response =  ServerValidation(key, basepath, env);

  std::vector<std::string> parameters;  

  split3(response, parameters, '*');


  if(parameters.at(0).compare("Valid") == 0)
  {
    cout << "is valid";
      saveFile(response);        
  }
    
  return Napi::String::New(env, parameters.at(0));
}

Napi::String CredentialsAvailable(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  std::string temp = isFilePresent();
  Napi::String str =  Napi::String::New(env, temp);
  return str;
}


Napi::Object Init(Napi::Env env, Napi::Object exports)
{
  exports.Set(Napi::String::New(env, "validate"),
              Napi::Function::New(env, InitialValidation));
  exports.Set(Napi::String::New(env, "startUp"),
              Napi::Function::New(env, StartUpValidation));
  exports.Set(Napi::String::New(env, "filePresent"),
              Napi::Function::New(env, CredentialsAvailable));
  return exports;
}

NODE_API_MODULE(hello, Init)
