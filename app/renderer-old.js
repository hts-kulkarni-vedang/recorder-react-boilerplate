const { desktopCapturer, ipcRenderer, remote, shell } = require("electron")
const electron = require("electron");
const swal = require('sweetalert2')
const { dialog } = require("electron").remote
const env = require("./environment");
var fs = require('fs')
var videoUtils = require('./assets/js/videoUtils')
var s_recording = false
var c_recording = false
var s_recorder
var updateDownloaded = false;
var c_recorder
let appVersion = require('electron').remote.app.getVersion();
var screenStream
var writeStreamCam, writeStreamScr, camStreamFlag = false, scrStreamFlag = false
var cameraRecorderf
var camCount = 0;
var isDownloading = null;
var cameraPreview = false;
var errorFilesCreated = false;
//var errLogstream;
//var memLogStream;
var extension = '.mp4'
var os = require('os')
var screenCount = 0
var preSendCount = 0
var camName = "camera" //randomString()
var videoObj = document.getElementById("camera-video")
var cameraRec = document.getElementById("camera-rec")
var screenRec = document.getElementById("screen-rec")
var stop_rec = document.getElementById("stop-rec")
var videoSelect = document.querySelector('select#videoSource')
var audioSelect = document.querySelector('select#audioSource')
const screenResolution = electron.screen.getPrimaryDisplay();
let inHome = true;
let microAudioStream;
var os = require('os')
var documentsFolderPath = os.homedir + '/Documents/HiFi_Recorder Files/' + 'record_combined' + (Math.floor(Date.now() / 1000));
let toggleFilePath = '/camera/';
let showHideIcon = false;
var currentTime = "", startTime = ""
let videoSourceList = []
let audioSourceList = []
var s_fileReader = null
var c_fileReader = null
var rimraf = require('rimraf');
var isPreviewReady = false;
// ! ensure it is same as in key_transfer.js

if (!fs.existsSync(os.homedir + '/Documents/HiFi_Recorder Files')) {
	fs.mkdirSync(os.homedir + '/Documents/HiFi_Recorder Files')
}
if (!fs.existsSync(os.homedir + '/Documents/HiFi_Recorder Files/assets')) {
	fs.mkdirSync(os.homedir + '/Documents/HiFi_Recorder Files/assets')
}

var logInterval = undefined
/*Developer Version*/
$(document).ready(function () {
	refreshSources();
	if(isPreviewReady == false){
		$('#play-pause-img').attr('src','assets/images/play-disabled.png');
	}
	askForUpdate(false);
});
document.getElementById('settingsImg').addEventListener('click', function () {		
	if(checkIfRecording()){
		swal({
			type : "warning"
		});
	}else{
		ipcRenderer.send('settings');
	}
});
document.getElementById('refreshButton').addEventListener('click', function () {
	videoSelect.innerHTML = "";
	audioSelect.innerHTML = "";
	audioSourceList = []
	videoSourceList = []
	refreshSources();
});
function refreshSources() {
	navigator.mediaDevices.enumerateDevices()
		.then(gotDevices)
		.catch(errorCallback);
}
ipcRenderer.on('recorder-resized-reply', (event, windowCoordsx) => {
	windowCoords = windowCoordsx;
	//
})
ipcRenderer.on('updateDownload',(event,data)=>{
	$('.new-image').css('display','block');
	updateDownloaded = true;	
})
// ipcRenderer.on('did-finish-loading',(event,param)=>{
// 	console.log('loading.....');
// })
ipcRenderer.on('pause-both-recorders', (event, data) => {
	pauseBothRecorders();
})
ipcRenderer.on('shortcuts', (event, param) => {
	console.log(param);
	if(param.playPauseS == undefined){
		param.playPauseS = 'reload';
	}
	$("#play-pause-cut").text("Ctrl + " + param.playPauseS);
	$("#show-hide-cut").text("Ctrl + " + param.showHideScreenS);
	$("#stop-cut").text("Ctrl + " + param.stopRecS);
	alert("what"+param,playPauseS);
})
ipcRenderer.send('get-shortcuts');
ipcRenderer.on('shortcut-ev', (event, param) => {
	//
	if (!inHome) {
		if (param == 'play-pause') {
			if (currentRecording == 'camera') {
				//ipcRenderer.send('preview-shortcut','play-pause');
				$("#play-pause").click();
			}
			else if (currentRecording == 'screen' || currentRecording == 'both') {
				if (electron.remote.getCurrentWindow().isMinimized()) {
					electron.remote.getCurrentWindow().show()
				}
				else {
					$("#play-pause").click();
				}
				//
			}
		}
		else if (param == 'show-hide')//only for previewWindow
		{
			console.log('camereaPreview value : ',cameraPreview)
			/*if(electron.remote.getCurrentWindow().isMinimized())
			{
				electron.remote.getCurrentWindow().show()
			}*/
			if (cameraPreview == true) {
				ipcRenderer.send('minimize-camera-preview');
				cameraPreview = false;
			}
			else {
				//ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedDevice: videoSelect.options[videoSelect.selectedIndex].innerText, selectedAudioDevice: audioSelect.options[audioSelect.selectedIndex].innerText, reload: false });
				ipcRenderer.send('maximize-camera-preview')
				cameraPreview = true;
			}
		}
		else if (param == 'stop') {
			if (currentRecording == 'camera') {
				if (electron.remote.getCurrentWindow().isMinimized()) {
					electron.remote.getCurrentWindow().show()
				}
				$("#stop").click()
				recordingflag = false;
			}
			else if (currentRecording == 'screen' || currentRecording == 'both') {
				if (electron.remote.getCurrentWindow().isMinimized()) {
					electron.remote.getCurrentWindow().show()
				}
				$("#stop").click()
			}
		}
	}
})
ipcRenderer.on('init-cam-screen-count', (event, param) => {
	camCount = 0
	screenCount = 0
	preSendCount = 0
})
ipcRenderer.on('devices-error', (event, param) => {
	//errLogstream.write('\n' + param + '\n')
	backToHome()
})
ipcRenderer.on('update-current-time', (event, param) => {
	/*time updated every second*/
	currentTime = param
	//
})
ipcRenderer.on('preview-ready', (event, param) => {
	console.log('isPreviewReady : ',param)
	isPreviewReady = param
	if(isPreviewReady) {
		$('#play-pause-img').attr('src','assets/images/play.png');
	} else{
		$('#play-pause-img').attr('src','assets/images/play-disabled.png');
	}
})

document.getElementById('close').addEventListener('click', function () {
	console.log('is downloading',isDownloading);
	
	if(isDownloading == true){
		swal.fire({
			type : 'warning',
			title : 'Update Download Pending !',
			text : 'New version download is in progress !, Do you want to quit and cancel update ?',
			showCancelButton : true,
			confirmButtonText : 'Continue Download',
			cancelButtonText : 'Cancel Update and Quit'
		}).then((res)=>{
			console.log(res);
			if(res.value){

			}else{
				ipcRenderer.send('close-app');
			}
		});
	}else{
		ipcRenderer.send('close-app');
	}
})
document.getElementById('minimize').addEventListener('click', function () {
	ipcRenderer.send('minimize-app');
})
document.getElementById('camera').addEventListener('click', function () {
	initCheck('camera');	
})
document.getElementById('screen').addEventListener('click', function () {
	initCheck('screen');
})
document.getElementById('both').addEventListener('click', function () {
	initCheck('both');	
})
document.getElementById('play-pause').addEventListener('click', function () {
	if (videoSource.childElementCount == 0) {
		backToHome()
		$("#refreshButton").click()
	}
	else {
		playPause();
	}
})
document.getElementById('stop').addEventListener('click', function () {
	stopRecFinal()
	recordingflag = false;
})
document.getElementById('overlay-width').addEventListener('change', function (event) {
	setOverlayRes();
})
document.getElementById('overlay-height').addEventListener('change', function (event) {
	setOverlayRes();
})
document.getElementById('overlay-pos').addEventListener('change', function (event) {
	overlayPos = $("#overlay-pos").find(":selected")[0].id
})
$(".open-existing").on("click", function () {
	if(checkIfRecording()){
		swal({
			type : 'warning',
			title : 'Please stop recording'
		});
	}else{
		dialog.showOpenDialog({
			browserWindow : electron.remote.getCurrentWindow(),
			title : 'Select File',
			buttonLabel : 'Select File',
			filters : [
				{	name : 'Configuration' , extensions : ['CONFIG']	}
			],
			properties: ['openFile']
		},function(filename)
		{
			if(filename != undefined){
				ipcRenderer.send('close-recorder');
				ipcRenderer.send('close-camera-preview');
		    	let obj = JSON.parse(fs.readFileSync(filename[0], 'utf8'));
		    	obj.configFolderPath = filename[0];
		    	obj.documentsFolderPath = filename[0].replace('settings.config','');
		    	obj.sourceFile = 'preview-output-window/output-preview.html';
		    	ipcRenderer.send('process-load-existing',obj);
				// ipcRenderer.send('main-window-reload');
			}
		});
	}
})
$('#back-img-selector').on('click', function (event) {
	imgPath = dialog.showOpenDialog({
		properties: ['openFile'], filters: [
			{ name: 'Images', extensions: ['jpg', 'png'] }
		]
	})
	imgPath = imgPath[0]
	//
	$("#selected-back-img").text(imgPath.split("\\")[(imgPath.split("\\").length - 1)]);
})
$('#hex-color').on('input', function (ev) {
	let val = $(this).val();
	val = val.toUpperCase();
	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
	colorHex = val;
	if(isOk){
		$(this).removeClass('error-input');
		$(this).addClass('correct-input');
	}else{
		$(this).removeClass('correct-input');
		$(this).addClass('error-input');
	}
	console.log(isOk);
});
$(".check-for-updates").on("click", function () {
    if(updateDownloaded){
        swal({
            title: "Install Update",
            text: "Do you want to quit application and install new version?",
            icon: "info",
            showCancelButton: true,
            focusConfirm: false,
            confirmButtonText:
                'Yes',
            confirmButtonAriaLabel: 'yes',
            cancelButtonText:
                'No',
            cancelButtonAriaLabel: 'no'
        }).then(value => {
			console.log(value);
            if (value.value == true) {
				ipcRenderer.send('quitAndInstall');
            }
        });
    } else{
        swal(
            'No new version available',
            'Your version is up-to-date!',
            'success'
          );
    }
})
$('#transparency-check').on('click', function (ev) {
	transparencyCheck = $(this).prop('checked')
	if ($(this).prop('checked')) {
		$("#hex-color").css('visibility', 'visible')
		$("#tl").css('display', 'none')
		$("#tr").css('display', 'none')
		$("#transparency-options").css('display', 'block')
	}
	else {
		$("#hex-color").css('visibility', 'hidden')
		$("#tl").css('display', 'block')
		$("#tr").css('display', 'block')
		$("#transparency-options").css('display', 'none')
	}
})
$("#plugin").on('click', function () {
	ipcRenderer.send('load-plugin', 'bck-img')
})
$("#similarity-range-slider").on('input change', function () {
	$("#similarity-value").text((this.value) + "%")
	transparencySimilarity = this.value;
})
$("#opacity-range-slider").on('input change', function () {
	$("#opacity-value").text((this.value) + "%")
	transparencyOpacity = ((this.value));
})
$(document).on("change", "#videoSource", function () {
	// ipcRenderer.send('open-camera-preview',{type:'camera',selectedDevice:videoSelect.options[videoSelect.selectedIndex].innerText})	
	// ipcRenderer.send('update-audio-source',{type:'camera',selectedDevice:audioSelect.options[audioSelect.selectedIndex].innerText})	
	ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedDevice: videoSelect.options[videoSelect.selectedIndex].innerText, selectedAudioDevice: audioSelect.options[audioSelect.selectedIndex].innerText, reload: true,deviceChanged : true});
})
$(document).on("change", "#audioSource", function () {
	// ipcRenderer.send('update-audio-source',{type:'camera',selectedDevice:audioSelect.options[audioSelect.selectedIndex].innerText})	
	ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedDevice: videoSelect.options[videoSelect.selectedIndex].innerText, selectedAudioDevice: audioSelect.options[audioSelect.selectedIndex].innerText, reload: true,deviceChanged : true});
})
$(document).on('click', '#full-screen-check', function () {
	fullScreenMode = this.checked;
	if(oldFullScreenMode == undefined){
		oldFullScreenMode = fullScreenMode;
	}
	ipcRenderer.send('toggle-full-screen-mode', this.checked);
	initCheck(currentRecording);
});
function initCheck(mode){
	console.log(mode,isPreviewReady);
	// debugger
	oldRecording = currentRecording
	currentRecording = mode;
	initRecorder();
	if (oldRecording == '')
		oldRecording = currentRecording
}
function initRecorder() {
	// 
	// if ($('#full-screen-check').checked) {
	// 	fullScreenMode = false
	// 	ipcRenderer.send('toggle-full-screen-mode', fullScreenMode)
	// }
	ipcRenderer.send('close-recorder', true)
	$("#camera").parent().show()
	$("#screen").parent().show()
	$("#both").parent().show()
	$(".rec-buttons").show();
	$("#stop img").attr('src', 'assets/images/stop-disabled.png');
	$("#stop-rec-text").html('Stop <br> Recording');
	$("#show-screen-wrap").hide();
	//ipcRenderer.send('close-camera-preview');
	$("#play-pause").parent().show();
	$("#stop").parent().show();
	$(".overlay").hide();
	$(".overlay-width").hide();
	$('.option-full-screen').hide()
	/*Stop on switching mode*/
	if (c_recorder != undefined) {
		if (c_recorder.state != 'inactive')
			c_recorder.stop()
		closeAllStreams(c_recorder)
	}
	if (s_recorder != undefined) {
		if (s_recorder.state != 'inactive')
			s_recorder.stop()
		closeAllStreams(s_recorder)
	}
	pausedFlag = false
	recordingflag = false
	screenRecordingFlag = false
	s_recording = false
	c_recording = false
	/**/
	switch (currentRecording) {
		case 'camera':
			$("#current-rec img").attr('src', 'assets/images/preview-webcam.png');
			$("#rec-text-current").html('Recording<br>camera');
			ipcRenderer.send('close-recorder');
			//
			cameraPreview = true;
			/*hide play and stop buttons for now !
				preview window takes care of it
			*/
			electron.remote.getCurrentWindow().setSize(400, 460)
			$("body").css('height', '600');
			//$("#play-pause").parent().hide();
			//$("#stop").parent().hide();
			$("#camera").parent().hide();
			break;
		case 'screen':
			$("#screen").parent().hide();
			$("#current-rec img").attr('src', 'assets/images/screen recording.png');
			$("#rec-text-current").html('Recording<br>screen');
			$('.option-full-screen').show()
			//ipcRenderer.send('open-camera-preview',{type:'screen',selectedDevice:videoSelect.options[videoSelect.selectedIndex].innerText});
			if (!fullScreenMode)
				ipcRenderer.send('open-recorder');
			cameraPreview = true;
			electron.remote.getCurrentWindow().setSize(400, 540)
			$("body").css('height', '500');
			break;
		case 'both':
			$("#both").parent().hide();
			$("#current-rec img").attr('src', 'assets/images/video and screen recording.png');
			$("#rec-text-current").html('Recording<br>both');
			/*$("#show-screen-wrap").show();*/
			$(".overlay").show();
			$(".overlay-width").show();
			$('.option-full-screen').show()
			$("#show-screen-img").attr('src', './assets/images/show-screen-black.png');
			$("#show-screen-text").html('Show Screen');
			//ipcRenderer.send('open-camera-preview',{type:'both',selectedDevice:videoSelect.options[videoSelect.selectedIndex].innerText});
			if (!fullScreenMode)
				ipcRenderer.send('open-recorder');
			cameraPreview = true;
			//ipcRenderer.send('clear-for-both');
			electron.remote.getCurrentWindow().setSize(400, 540)
			$("body").css('height', '900');
			/*camCount=0;
			screenCount=0;*/
			break;
	}
	ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedDevice: videoSelect.options[videoSelect.selectedIndex].innerText, selectedAudioDevice: audioSelect.options[audioSelect.selectedIndex].innerText, reload: false });
	inHome = false;
}
function backToHome() {
	inHome = true;
	$("#camera").parent().show()
	$("#screen").parent().show()
	$("#both").parent().show()
	$('.option-full-screen').hide()
	if (s_recording == true || c_recording == true) {
		pauseBothRecorders();
		alert('stop recording first');
	}
	else {
		$(".menu-buttons").show();
		$(".rec-buttons").hide();
		$("#play-pause-text").html("Start Recording");
		$("#play-pause-img").attr('src', 'assets/images/play.png');
		ipcRenderer.send('close-recorder');
		//ipcRenderer.send('close-camera-preview');
		cameraPreview = false;
		$(".overlay").hide();
		$(".overlay-width").hide();
		electron.remote.getCurrentWindow().setSize(400, 520)
	}
}
function playPause() {
	console.log('came here to playpause',s_fileReader,c_fileReader,'ispreviewready : ',isPreviewReady);
	ipcRenderer.send('resizable-recorder', false)
	//
	
	if (recordingflag == false)//play
	{
		if(isPreviewReady){
			console.log('can record')
			$('#stop img').attr('src','assets/images/stop.png')
			ipcRenderer.send('minimize-app');
			ipcRenderer.send('timer', 'resume');
			$("#play-pause-text").html("Pause Recording");
			$("#play-pause-img").attr('src', 'assets/images/pause.png');
			recordingflag = true;
			if (pausedFlag == false) {//start
				if (currentRecording == 'screen') {
					startScreenRec();
				}
				else if (currentRecording == 'camera') {
					startCameraRec();
				}
				else {
					/*false for camera and true for screen*/
					toggleFilePath = '/camera/';
					startCameraRec();
					startScreenRec();
					if (currentRecording == 'both')
						$("#show-screen-wrap").show()
				}
			}
			else {//resume
				if (s_recorder != undefined) {
					if (s_recorder.state != 'inactive') {
						setTimeout(function () {
							s_recorder.resume();
						}, 500)
					}
					pausedFlag = false;
				}
				if (c_recorder != undefined) {
					if (c_recorder.state != 'inactive') {
						setTimeout(function () {
							c_recorder.resume();
							ipcRenderer.send('resumed-rec-preview', currentRecording);
						}, 500)
					}
					pausedFlag = false;
				}
			}
		}
		else{
			console.log('please wait')
		}
	}
	else //pause
	{
		ipcRenderer.send('timer', 'pause')
		//
		$("#play-pause-text").html("Resume Recording");
		$("#play-pause-img").attr('src', 'assets/images/play.png');
		//
		ipcRenderer.send('paused-rec-preview', currentRecording)
		ipcRenderer.send('open-menu');
		if (c_recorder != undefined) {
			if (c_recorder.state == 'recording') {
				c_recorder.pause();
				pausedFlag = true;
			}
		}
		if (s_recorder != undefined) {
			if (s_recorder.state == 'recording') {
				s_recorder.pause();
				pausedFlag = true;
			}
		}
		if (currentRecording == 'both') {
			$("#stop img").attr('src', 'assets/images/stop.png');
			$("#stop-rec-text").html('Stop <br> Recording');
		}
		recordingflag = false;
	}
	//d after bugs 
	privateStopFlag = false;
	//	
}
const getMicroAudio = (audioStream) => {
	microAudioStream = audioStream;
	audioStream.onended = () => { /**/ }
	//
	//
	let x_localStream = screenStream;
	let audioTracks = microAudioStream.getAudioTracks();
	x_localStream.addTrack(audioTracks[0]);
	screenStream.addTrack(audioTracks[0]);
    /*
    */
	try {
		s_recorder = new MediaRecorder(screenStream)
	} catch (e) {
		return
	}
	s_recorder.ondataavailable = s_recorderOnDataAvailable
	s_recorder.onstop = () => {
		//screenCount++;
		// if (!fs.existsSync(documentsFolderPath+'/screen/')){
		// 	fs.mkdirSync(documentsFolderPath+'/screen/');
		// }
		scrStreamFlag = true;
		//s_fileReader = null
	}
	s_recorder.start(3000);
	if (!fs.existsSync(documentsFolderPath)) {
		fs.mkdirSync(documentsFolderPath)
	}
	if (!fs.existsSync(documentsFolderPath + '/screen/')) {
		fs.mkdirSync(documentsFolderPath + '/screen');
	}
	if (!errorFilesCreated) {
		createErrorFiles();
		//logMemoryUsage();
		errorFilesCreated = true;
	}
	screenCount++;
	writeStreamScr = fs.createWriteStream(documentsFolderPath + '/screen/rec' + screenCount + extension)
	writeStreamScr.on('finish', function () {
		if (oldRecording == 'both') {
			preSendCheck(documentsFolderPath + '/screen/rec' + screenCount + extension)
		}
		else // only screen
		{
			if (startTime == "") {
				startTime = 0
			}
			endTime = getSecondsFromCurrentTime()
			//finalMerger.push({type : 'screen' , cpath : './assets/screen/rec'+screenCount+extension,startTime : startTime,endTime : endTime})
			if(oldFullScreenMode == undefined){
				oldFullScreenMode = false;
			}
			finalMerger.push({ type: 'screen', cpath: documentsFolderPath + '/screen/rec' + screenCount + extension, windowCoords: windowCoords, startTime: startTime, endTime: endTime , fullScreenMode : oldFullScreenMode})
			if(oldFullScreenMode!=fullScreenMode){
				oldFullScreenMode = fullScreenMode;
			}
			startTime = endTime
		}
		onFileLoaded(oldRecording)
	})
	//ipcRenderer.send('started-rec-preview',currentRecording);
	if (currentRecording == 'both' && c_recorder != undefined) {
		if (c_recorder.state == 'inactive' || c_recorder.state == 'paused') {
			c_recorder.stop();
			closeAllStreams(c_recorder)
			//c_recorder.start();
			/*false for camera and true for screen*/
			startCameraRec(false)
		}
	}
	ipcRenderer.send('minimize-app');
	s_recording = true;
	c_recording = true;
	//
}
function createErrorFiles() {
	// memLogStream = fs.createWriteStream(documentsFolderPath + '/memory-usage-log.txt');
	// fs.truncate(documentsFolderPath + '/memory-usage-log.txt', function () {
	// 	memLogStream.write('HiFi Recorder Memory Log file!\n');
	// 	memLogStream.write('Details of memory used : \n');
	// })
	// //errLogstream = fs.createWriteStream(documentsFolderPath + '/main-error-log.txt');
	// //errLogstream.write('HiFi Recorder Error Log file!\n');
	// fs.truncate(documentsFolderPath + '/main-error-log.txt', function () {
	// })
}
function logMemoryUsage() {
	const { webFrame } = require('electron')
	function getMemory() {
		// `format` omitted  (pads + limits to 15 characters for the output)
		function logMemDetails(x) {
			function toMb(bytes) {
				return (bytes / (1000.0 * 1000)).toFixed(2)
			}
			//memLogStream.write(
			// 	(x[0]) + "---" +
			// 	(x[1].count) + "---" +
			// 	(toMb(x[1].size) + "MB") + "---" +
			// 	(toMb(x[1].liveSize) + "MB") + "\n"
			// )
			console.log(
				(x[0]) + "---" +
				(x[1].count) + "---" +
				(toMb(x[1].size) + "MB") + "---" +
				(toMb(x[1].liveSize) + "MB") + "\n"
			)
		}
		//memLogStream.write("\nObject---Count---size---liveSize\n")
		Object.entries(webFrame.getResourceUsage()).map(logMemDetails)
	}
	logInterval = setInterval(getMemory, 10000)
}
function getSecondsFromCurrentTime() {
	var temp = currentTime.split(':')
	return (parseInt(temp[0].substr(0, temp[0].length - 1)) * 3600 + parseInt(temp[1].substr(0, temp[1].length - 1)) * 60 + parseInt(temp[2].substr(0, temp[2].length - 1)))
}
function getUserMediaError(err) {
	//errLogstream.write('\n' + err + '\n')
	alert("Failure with Audio/Video Sources. Please check connected devices")
}
const s_recorderOnDataAvailable = (event) => {
	if (event.data && event.data.size > 0) {
		//s_recordedChunks.push(event.data)
		if (s_fileReader == null) {
			s_fileReader = new FileReader()
		}
		var s_blob = new Blob([event.data], { type: 'video/mp4' });
		s_fileReader.readAsArrayBuffer(s_blob);
		s_fileReader.onload = function () {
			var s_int8array = new Uint8Array(this.result)
			var s_chunk = Buffer.from(s_int8array);
			// might return falsest	
			writeStreamScr.write(s_chunk);
			s_chunk = null;
			s_blob = null;
			s_int8array = null;
			if (scrStreamFlag == true) {
				writeStreamScr.end();
				scrStreamFlag = false
			}
		}
	}
}
function s_handleStream(stream) {
	screenStream = stream;
	/*navigator.webkitGetUserMedia({ audio: {deviceId : { exact : audioSelect.value}}, video: false },getMicroAudio, getUserMediaError);	*/
	navigator.mediaDevices.getUserMedia(
		{
			audio: {
				mimeType: 'video/mp4',
				deviceId: { exact: audioSelect.value },
				sampleRate: 48000,
				audioBitsPerSecond: 128000,
				channelCount: 1
			},
			video: false
		})
		.then((stream) => getMicroAudio(stream))
		.catch((e) => getUserMediaError(e));
}
function s_handleError(e) {
	//errLogstream.write('\n' + e + '\n')
}
const c_recorderOnDataAvailable = (event) => {
	console.log('came in recorder on data available')
	if (event.data && event.data.size > 0) {
		//c_recordedChunks.push(event.data)
		//var fileReader = new FileReader();
		var c_blob = new Blob([event.data], { type: 'video/mp4' });
		if (c_fileReader == null) {
			c_fileReader = new FileReader()
		}
		c_fileReader.readAsArrayBuffer(c_blob);
		c_fileReader.onload = function () {
			var c_int8array = new Uint8Array(this.result)
			var c_chunk = Buffer.from(c_int8array)
			// might return false
			writeStreamCam.write(c_chunk)
			c_chunk = null;
			c_blob = null;
			c_int8array = null
			if (camStreamFlag == true) {
				writeStreamCam.end();
				camStreamFlag = false
			}
		}
	}
}
const getMicroAudioForCamera = (audioStream) => {
	cameraStream.addTrack(audioStream.getAudioTracks()[0])
	var recordingOptions
	// if (MediaRecorder.isTypeSupported('video/mp4;codecs=h264')) {
	// 	recordingOptions = { mimeType: 'video/mp4;codecs=h264' };
	// } else {
		//recordingOptions = { mimeType: 'video/mp4' };
//	}
	try {
		c_recorder = new MediaRecorder(cameraStream)
	} catch (e) {
		throw e
	}
	c_recorder.ondataavailable = c_recorderOnDataAvailable
	c_recorder.onstop = () => {
		// if (!fs.existsSync(documentsFolderPath+'/camera/')){
		// 	fs.mkdirSync(documentsFolderPath+'/camera');
		// }
		camStreamFlag = true;
		//c_fileReader = null
	}
	c_recorder.start(3000);
	if (!fs.existsSync(documentsFolderPath)) {
		fs.mkdirSync(documentsFolderPath)
	}
	if (!fs.existsSync(documentsFolderPath + '/camera/')) {
		fs.mkdirSync(documentsFolderPath + '/camera');
	}
	if (!errorFilesCreated) {
		createErrorFiles();
		//logMemoryUsage();
		errorFilesCreated = true;
	}
	camCount++
	writeStreamCam = fs.createWriteStream(documentsFolderPath + '/camera/rec' + camCount + extension)
	writeStreamCam.on('finish', function () {
		if (oldRecording == 'both') {
			preSendCheck(documentsFolderPath + '/camera/rec' + camCount + extension)
		}
		else // only camera
		{
			if (startTime == "") {
				startTime = 0
			}
			endTime = getSecondsFromCurrentTime();
			if(oldFullScreenMode == undefined){
				oldFullScreenMode = false;
			}
			finalMerger.push({ type: 'camera', cpath: documentsFolderPath + '/camera/rec' + camCount + extension, startTime: startTime, endTime: endTime })
			startTime = endTime
		}
		onFileLoaded(oldRecording)
	})
	ipcRenderer.send('minimize-app');
	ipcRenderer.send('started-rec-preview', currentRecording);
	c_recording = true
	//
}
function c_handleStream(stream) {
	cameraStream = stream;
	/*navigator.webkitGetUserMedia({ audio: { deviceId : {exact : audioSelect.value}}, video: false },getMicroAudioForCamera, getUserMediaError);	*/
	navigator.mediaDevices.getUserMedia(
		{
			audio: {
				deviceId: { exact: audioSelect.value },
				sampleRate: 48000,
				desiredSampleRate: 48000,
				audioBitsPerSecond: 128000,
				channelCount: 1
			},
			video: false
		})
		.then((stream) => getMicroAudioForCamera(stream))
		.catch((e) => getUserMediaError(e));
}
function c_handleError(e) {
	//errLogstream.write('\n' + e + '\n')
}
function startCameraRec(screenRecordingOption) {
	/*
		(document.getElementsByTagName('body')[0]).classList.twebmle("twebmleDrag");*/
	ipcRenderer.send('start-rec', currentRecording);
	c_recordedChunks = [];
	var videoValue = videoSelect.value;
	let c_constraints;
	c_constraints = {
		audio: false,
		video: { deviceId: { exact: videoValue } }
	}
	navigator.mediaDevices.getUserMedia(c_constraints)
		.then((stream) => c_handleStream(stream))
		.catch((e) => c_handleError(e));
}
function gotDevices(deviceInfos) {
	for (var i = 0; i !== deviceInfos.length; ++i) {
		var deviceInfo = deviceInfos[i];
		var option = document.createElement('option');
		option.value = deviceInfo.deviceId;
		if (deviceInfo.deviceId == "communications" || deviceInfo.deviceId == "default")
			continue;
		if (deviceInfo.kind === 'videoinput') {
			option.text = deviceInfo.label || 'Camera ' + (videoSelect.length + 1);
			videoSelect.appendChild(option);
			videoSourceList.push(option)
		}
		if (deviceInfo.kind === 'audioinput') {
			option.text = deviceInfo.label || 'Mic ' + (audioSelect.length + 1);
			audioSelect.appendChild(option);
			audioSourceList.push(option)
		}
	}
	//ipcRenderer.send('update-audio-source', { type: 'camera', selectedDevice: audioSelect.options[audioSelect.selectedIndex].innerText })
	if (videoSelect.childElementCount == 0 || audioSelect.childElementCount == 0) {
		alert('Video / Audio source not found ! Please connect devices and retry')
		$(".menu-buttons").css('pointer-events', 'none')
	}
	else {
		// $(".menu-buttons").css('pointer-events', 'auto')
	}
	if (sessionStorage.getItem('videoSource') != undefined) {
		$('#videoSource').val(sessionStorage.getItem('videoSource'))
		sessionStorage.removeItem('videoSource')
	}
	if (sessionStorage.getItem('audioSource') != undefined) {
		$('#audioSource').val(sessionStorage.getItem('audioSource'))
		sessionStorage.removeItem('audioSource')
	}
	//errLogstream.write('\n Connected Devices Changed \n'+'\nAudio Sources\n'+(JSON.stringify(audioSourceList))+'\nVideo Sources\n'+(JSON.stringify(videoSourceList)))	
	// if (errLogstream) {
	// 	errLogstream.write('\nDevices Changed To :')
	// 	errLogstream.write('Audio Source : ')
	// 	errLogstream.write(audioSelect.options[audioSelect.selectedIndex].innerText)
	// 	errLogstream.write('Video Source : ')
	// 	errLogstream.write(videoSelect.options[videoSelect.selectedIndex].innerText)
	// }
}
function errorCallback(error) {
//	errLogstream.write('\n' + error + '\n')
}
function stopRecFinal() {
	// console.log(finalMerger);
	if(canStopRec()){
		ipcRenderer.send('closeLogStreams')
		clearInterval(logInterval)
		ipcRenderer.send('timer', 'reset')
		privateStopFlag = true;
		//
		recordingStop = false;
		//required for last case of 'both' object merger
		oldRecording = currentRecording
		if (c_recorder != undefined) {
			if (c_recorder.state == 'recording' || c_recorder.state == 'paused') {
				c_recorder.stop();
				recordingStop = true;
				closeAllStreams(c_recorder)
			}
		}
		if (s_recorder != undefined) {
			if (s_recorder.state == 'recording' || s_recorder.state == 'paused') {
				s_recorder.stop();
				recordingStop = true;
				closeAllStreams(s_recorder)
			}
		}
		c_recording = false;
		s_recording = false;
		//if(recordingStop)
		//{
		$("#play-pause-text").html("Start Recording");
		$("#play-pause-img").attr('src', 'assets/images/play.png');
		recordingflag = false;
		pausedFlag = false;
		showHideIcon = false;
		ipcRenderer.send('stop-rec', null)
		backToHome();
		$(".menu-buttons").hide();
		$("#progress-text").text("Loading Files ..")
		$("#progress-wrap").show();
	}
	else{
		console.log('cannot stop recording');
	}
	console.log()

}
function canStopRec(){
	var flag = true;
	console.log(currentRecording,c_recorder,s_recorder)
	if(currentRecording == 'camera'){
		if(c_recorder == undefined || c_recorder.state == 'inactive'){
			flag = false;
		}
	}
	if(currentRecording == 'screen'){
		if(s_recorder == undefined || s_recorder.state == 'inactive'){
			flag = false;
		}
	}
	if(currentRecording == 'both'){
		if(c_recorder == undefined || c_recorder.state == 'inactive' || s_recorder == undefined || s_recorder.state == 'inactive'){
			flag = false;
		}
	}
	console.log('returning flag : ',flag); 
	return flag;
}
function startScreenRec() {
	s_recordedChunks = [];
	ipcRenderer.send('start-rec', currentRecording);
	if (!s_recording) {
		//
		var mand;
		if (currentRecording == 'both') {
			mand = {
				chromeMediaSource: 'desktop',
			};
		}
		else // mode is screen
		{
			mand = {
				chromeMediaSource: 'desktop'
			}
		}
		desktopCapturer.getSources({ types: ['screen'] }, (error, sources) => {
			if (error) {
				//errLogstream.write('\n' + err + '\n')
			}
			const s_constraints = {
				audio: false,
				video: {
					mandatory: mand
				},
				videoBitsPerSecond: 2500000
			}
			navigator.mediaDevices.getUserMedia(s_constraints)
				.then((stream) => s_handleStream(stream))
				.catch((e) => s_handleError(e))
			return
		});
	}
	else {
		c_recorder.stop();
		s_recorder.stop();
		closeAllStreams(c_recorder)
		closeAllStreams(s_recorder)
		//c_recorder.start(); // instead of this can i use startCameraRec(true)
		toggleFilePath = '/camera/';
		/*false for camera and true for screen*/
		startCameraRec()
		ipcRenderer.send('minimize-app');
		c_recording = true;
		s_recording = false;
	}
}
function setVideoRes(res) {
	$("#vid-width").prop('readonly', true);
	$("#vid-height").prop('readonly', true);
	switch (res) {
		case 'Custom':
			$("#vid-width").prop('readonly', false);
			$("#vid-height").prop('readonly', false);
			$("#vid-width").attr('value', '1366');
			$("#vid-height").attr('value', '768');
			break;
		case 'Auto':
			ipcRenderer.send('set-auto-res');
			$("#vid-width").attr('value', '1920');
			$("#vid-height").attr('value', '1080');
			break;
		case '1080p':
			$("#vid-width").attr('value', '1920');
			$("#vid-height").attr('value', '1080');
			break;
		case '720p':
			$("#vid-width").attr('value', '1280');
			$("#vid-height").attr('value', '720');
			break;
		case '480p':
			$("#vid-width").attr('value', '640');
			$("#vid-height").attr('value', '480');
			break;
		case '360p':
			$("#vid-width").attr('value', '640');
			$("#vid-height").attr('value', '360');
			break;
		case '240p':
			$("#vid-width").attr('value', '320');
			$("#vid-height").attr('value', '240');
			break;
		case '144p':
			$("#vid-width").attr('value', '256');
			$("#vid-height").attr('value', '144');
			break;
	}
	videoRes = $('#vid-width').attr('value') + 'x' + $('#vid-height').attr('value');
	//
}
function setOverlayRes() {
	overlayRes = document.getElementById("overlay-width").value + ":" + document.getElementById("overlay-height").value;
	//
}
function pauseBothRecorders() {
	ipcRenderer.send('timer', 'pause')
	if (c_recorder != undefined) {
		if (c_recorder.state == 'recording') {
			c_recorder.pause();
			$("#play-pause-text").html("Resume Recording");
			$("#play-pause-img").attr('src', 'assets/images/play.png');
			ipcRenderer.send('paused-rec-preview', currentRecording)
			pausedFlag = true;
			recordingflag = false;
			//
		}
	}
	if (s_recorder != undefined) {
		if (s_recorder.state == 'recording') {
			s_recorder.pause();
			$("#play-pause-text").html("Resume Recording");
			$("#play-pause-img").attr('src', 'assets/images/play.png');
			ipcRenderer.send('paused-rec-preview', currentRecording)
			pausedFlag = true;
			recordingflag = false;
			//
		}
	}
}
/*In case of both ;  wait for both files to load then push to finalMerger*/
function preSendCheck(file) {
	preSendCount++;
	if (preSendCount == 2) {
		preSendCount = 0;
		if (startTime == "") {
			startTime = 0
		}
		endTime = getSecondsFromCurrentTime();
		if(oldFullScreenMode == undefined){
			oldFullScreenMode = false;
		}
		finalMerger.push({ type: 'both', cpath: documentsFolderPath + '/camera/rec' + camCount + extension, spath: documentsFolderPath + '/screen/rec' + screenCount + extension, windowCoords: windowCoords, startTime: startTime, endTime: endTime , fullScreenMode : oldFullScreenMode });
		if(oldFullScreenMode!=fullScreenMode){
			oldFullScreenMode = fullScreenMode;
		}
		startTime = endTime
	}
}
function onFileLoaded(type) {
	if (recordingStop && preSendCount == 0) {
	
		console.log(finalMerger);
		/*videoUtilsCons()*/
		//backupFolderPath = __dirname +'\\'+videoUtils.createBackupFolder(documentsFolderPath)
		videoUtils.copyErrorFilesAndCreateSettingsConfig(os.homedir + '/Documents/HiFi_Recorder Files', documentsFolderPath);
		backupFolderPath = documentsFolderPath;//videoUtils.createBackupFolder(documentsFolderPath)
		backupFolderPath = backupFolderPath.replace("\\resources\\app.asar\\", "\\")
		//videoUtils.generateTrackImages(backupFolderPath,function()
		{
			$("#progress-wrap").hide();
			recordingStop = false
			$(".menu-buttons").show();
			swal({
				title: "Recording Complete",
				text: "Video Duration " + currentTime + " Process files now ?",
				icon: "success",
				showCancelButton: true,
				focusConfirm: false,
				confirmButtonText:
					'Process Now',
				confirmButtonAriaLabel: 'Now',
				cancelButtonText:
					'Process Later',
				cancelButtonAriaLabel: 'Later',
				buttons: {
					later: {
						text: "Later",
						value: "later"
					},
					now: {
						text: "Now",
						value: "now",
					}
				},
			}).then((value => {
				if (value.value == true) {
					let obj = {
						process : 'now',
						documentsFolderPath : documentsFolderPath,
						mode : 'preview',
						sourceFile : './preview-output-window/output-preview.html'
					};
					ipcRenderer.send('process-videos', obj);
					
					electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
						electron.remote.getCurrentWindow().reload();
					});
					/*optname is set using functions in videoUtilsCons*/
				}
				else if (value.dismiss == "cancel") {
					swal({
						title: "Project Saved",
						text: "Please Locate files at \n " + backupFolderPath ,
						icon: "info",
						// 		showCancelButton: true,
						focusConfirm: false,
						confirmButtonText:
							'Open Folder',
						confirmButtonAriaLabel: 'Open',
						// cancelButtonText:
						//   'Change Now',
						// cancelButtonAriaLabel: 'change',
						buttons: {
							change: {
								text: "Change Now",
								value: "change"
							},
							open: {
								text: "Open Folder",
								value: "open",
							}
						},
					}).then((value => {
						if (value.dismiss == "cancel") {
							showSaveDialog(backupFolderPath)
						}
						else if (value.value == true) {
							//errLogstream.end()
							electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
								electron.remote.getCurrentWindow().reload()
							});
							shell.showItemInFolder(backupFolderPath)
						} else {
							//errLogstream.end()
							electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
								electron.remote.getCurrentWindow().reload()
							});
						}
					}));
				}
				else {
					// default is later
					electron.remote.getCurrentWindow().webContents.session.clearCache(function () {
						electron.remote.getCurrentWindow().reload()
					});
				}
			}));
		}//)
	}
}
// Function added for patch to avoid recursive folders
// Patch SeqNo 24102018 
function showSaveDialog(backupFolderPath) {
	dialog.showSaveDialog({
		title: 'Save Backup Folder As',
		defaultPath: backupFolderPath,
		buttonLabel: 'Save Backup Folder',
	}, function (foldername) {
		sessionStorage.setItem('videoSource', $('#videoSource').val())
		sessionStorage.setItem('audioSource', $('#audioSource').val())
		backupFolderPath = backupFolderPath.replace(/\//g, "\\");
		if (foldername != undefined && (foldername.substr(0, foldername.lastIndexOf('\\')) != backupFolderPath)) {
			fs.mkdirSync(foldername)
			// disable recording until backup has created
			// Patch SeqNo 24102018
			$('.menu-buttons').hide()
			$('#progress-wrap').show()
			// disabled in videoUtils.copyFolders()
			$('#progress-text').text('Copying Files to New Folder ..')
			videoUtils.updateBackupFolder(backupFolderPath, foldername, electron.remote.getCurrentWindow())
		}
		else if (foldername != undefined && (foldername.substr(0,
			foldername.lastIndexOf('\\')) == backupFolderPath)) {
			alert("cannot copy to default folder; Please select another folder")
			showSaveDialog(backupFolderPath)
		}
		//errLogstream.end()
	})
}
function resetRecorder() {
	backToHome()
}
function closeAllStreams(recorder) {
	recorder.stream.getTracks().forEach(track => track.stop()); // stop each of them
	var tracks = recorder.stream.getTracks()
	for (var i = 0; i < tracks.length; i++) {
		tracks[i].stop()
		tracks[i].enabled = false
	}
	recorder.stream.getAudioTracks()[0].stop()
	var aTracks = recorder.stream.getAudioTracks()
	for (var i = 0; i < aTracks.length; i++) {
		aTracks[i].stop()
	}
	var vTracks = recorder.stream.getVideoTracks()
	for (var i = 0; i < vTracks.length; i++) {
		vTracks[i].stop()
	}
	recorder.stream = null
}
navigator.mediaDevices.ondevicechange = function (event) {
	let audioSourceCount = 0, videoSourceCount = 0
	navigator.mediaDevices.enumerateDevices()
		.then((deviceInfos) => {
			for (var i = 0; i !== deviceInfos.length; ++i) {
				var deviceInfo = deviceInfos[i];
				if (deviceInfo.deviceId == "communications" || deviceInfo.deviceId == "default")
					continue;
				if (deviceInfo.kind === 'videoinput') {
					videoSourceCount++
				}
				if (deviceInfo.kind === 'audioinput') {
					audioSourceCount++
				}
			}
			if (audioSourceList.length != audioSourceCount || videoSourceList.length != videoSourceCount) {
				/**/
				// Treat this as a tranition from one mode to another
				/*pauseBothRecorders()
				electron.remote.getCurrentWindow().show()*/
				pauseBothRecorders()
				electron.remote.getCurrentWindow().show()
				oldRecording = currentRecording
				if (!inHome) {
					//should clear this recording and start a new one
					initRecorder()
					//stopRecFinal()
				}
				$("#refreshButton").click()
				swal({
					title: 'Media Devices Changed',
					text: 'Please verify connected devices before proceeding',
					type: 'warning',
					showCancelButton: false,
					confirmButtonText: 'OK',
				}).then((result) => {
					ipcRenderer.send('open-camera-preview', { type: currentRecording, selectedDevice: videoSelect.options[videoSelect.selectedIndex].innerText, selectedAudioDevice: audioSelect.options[audioSelect.selectedIndex].innerText, reload: true,deviceChanged : true });
					//ipcRenderer.send('update-audio-source',{type:'camera',selectedDevice:audioSelect.options[audioSelect.selectedIndex].innerText})	
				})
			}
		})
		.catch((error) => {
			//errLogstream.write('\n' + err + '\n')
		});
}
function closeAllStreams(recorder) {
	recorder.stream.getAudioTracks()[0].stop()
	var aTracks = recorder.stream.getAudioTracks()
	for (var i = 0; i < aTracks.length; i++) {
		aTracks[i].stop()
	}
	var vTracks = recorder.stream.getVideoTracks()
	for (var i = 0; i < vTracks.length; i++) {
		vTracks[i].stop()
	}
	recorder.stream = null
}
	function askForUpdate(displayUptodateMsg){
		let argv = remote.process.argv;
		// check if this is not squirrel's first run
		if(!argv.includes("--squirrel-firstrun")){
			$.ajax({
				url : env.squirrelUrl+"/RELEASES",
				success : function(res){
					checkForUpdate(res,displayUptodateMsg);
				}
			});
		}		
	}

	function checkForUpdate(res,displayUptodateMsg){
		let serverRes = res.split(/\n/);
		let latestVersion = serverRes[serverRes.length - 1];
		let start = latestVersion.indexOf('HiFi_Recorder-') + ("HiFi_Recorder-".length);
		let end = latestVersion.indexOf('-full.nupkg');
		latestVersion = latestVersion.substring(start,end);
		console.log(latestVersion,appVersion);		
		if(appVersion == latestVersion){
			if(displayUptodateMsg){
				swal.fire({
					type : 'success',
					title : 'Up to Date',
					text : 'Latest version is already installed'
				});
			}
		}else{
			swal.fire({
				type : 'info',
				title : 'Update Available',
				text : 'Do you wish to download latest version ?',
				showCancelButton : true,
				confirmButtonText : 'Download Now',
				cancelButtonText : 'Download Later'
			}).then((res) => {
				if(res.value){
					console.log('need to download update');
					ipcRenderer.send('check-for-update');
				}else{
					console.log('Download Later');
				}
			});
		}
	}
	ipcRenderer.on('is-downloading',(event,param) => {
		isDownloading = param;
		if(param == true){
			$(".new-image").show();
		}else if(param == false){
			$(".new-image").hide();
		}
	});
	function onlineOfflineStatusChange(){
		console.log('Status : ',navigator.onLine);
	}
window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
	//errLogstream.write('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
//		+ ' Column: ' + column + ' StackTrace: ' + errorObj.toString() + '\n')
	alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
		+ ' Column: ' + column + ' StackTrace: ' + errorObj.toString() + '\n')
}
function checkIfRecording(){
	let flag = false;
	if(s_recorder){
		if(s_recorder.state == 'paused' || s_recorder.state == 'recording'){
			flag = true;
		}
	}
	if(c_recorder){
		if(c_recorder.state == 'paused' || c_recorder.state == 'recording'){
			flag = true;
		}
	}
	return flag;
}

window.addEventListener('online',  onlineOfflineStatusChange);	
window.addEventListener('offline',  onlineOfflineStatusChange);	