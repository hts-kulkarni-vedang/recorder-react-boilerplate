#include <napi.h>
#include <iostream>
#include <string>
#include <array>
#include <windows.h>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "commandFunctions.h"
std::string *strCommands;
HANDLE hChildProcess_f = NULL; //NULL
HANDLE hStdIn_f = NULL;		   // Handle to parents std input. NULL
BOOL bRunThread_f;			   // TRUE
int commandCounter_f = 0;
int noOfFinalMergeCommands = 1;
#pragma comment(lib, "User32.lib")

std::string ReplaceAll(std::string str, const std::string &from, const std::string &to)
{
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos)
	{
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
	}
	return str;
}
void finalmerge(const Napi::CallbackInfo &info)
{

	std::cout << "\n\ncame to finally merge\n\n";
	//  int check;
	// check=CreateDirectory("errorLog",NULL);
	// if (!check)
	//     std::cout<<"Directory created\n";
	// else
	// {  std::cout<<"Unable to create directory\n";
	// DisplayError("DirectoryProblem");
	// }
	Napi::Env env = info.Env();

	Napi::Function cb = info[3].As<Napi::Function>();
	auto configObj = info[0].ToObject();
	int id = info[4].As<Napi::Number>().Int32Value();
	std::string configFolderPath = info[1].As<Napi::String>().Utf8Value();
	// std::string outPutfileName = configFolderPath + "errorLog/finalMergeLogStdout.txt";

	// setId_fileName(id, outPutfileName);

	std::string optname = info[2].As<Napi::String>().Utf8Value();

	Napi::HandleScope scope(env);

	std::vector<std::string> _args_camera;
	strCommands = new std::string[noOfFinalMergeCommands];
	std::string mergingListDotTxt = configFolderPath + "mergingList.txt";
	mergingListDotTxt = ReplaceAll(mergingListDotTxt, "\\\\", "\\");
	mergingListDotTxt = ReplaceAll(mergingListDotTxt, "/", "\\");

	cout << "merging list path name : " << mergingListDotTxt;

	_args_camera.push_back("ffmpeg");
	_args_camera.push_back("-f");
	_args_camera.push_back("concat");
	_args_camera.push_back("-safe");
	_args_camera.push_back("0");
	_args_camera.push_back("-i");
	_args_camera.push_back('"' + mergingListDotTxt + '"');
	_args_camera.push_back("-codec");
	_args_camera.push_back("copy");
	_args_camera.push_back("-y");
	_args_camera.push_back('"' + configFolderPath + optname + '"');
	strCommands[commandCounter_f++] = createCommand(_args_camera);
	std::cout << "\nCommands in final merge  :\n";
	for (int i = 0; i < commandCounter_f; i++)
	{
		std::cout << "**\n** Command no : **" << i << "**\n Command : **" << strCommands[i] << "**\n";
	}
	HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
	HANDLE hInputWriteTmp, hInputRead, hInputWrite;
	HANDLE hErrorWrite;
	HANDLE hThread;
	DWORD ThreadId;
	SECURITY_ATTRIBUTES sa;

	// Set up the security attributes struct.
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	// Create the child ou0tput pipe.
	if (!CreatePipe(&hOutputReadTmp, &hOutputWrite, &sa, 0))
		DisplayError("CreatePipe");

	// Create a duplicate of the output write handle for the std error
	// write handle. This is necessary in case the child application
	// closes one of its std output handles.
	if (!DuplicateHandle(GetCurrentProcess(), hOutputWrite,
						 GetCurrentProcess(), &hErrorWrite, 0,
						 TRUE, DUPLICATE_SAME_ACCESS))
		DisplayError("DuplicateHandle");

	// Create the child input pipe.
	if (!CreatePipe(&hInputRead, &hInputWriteTmp, &sa, 0))
		DisplayError("CreatePipe");

	// Create new output read handle and the input write handles. Set
	// the Properties to FALSE. Otherwise, the child inherits the
	// properties and, as a result, non-closeable handles to the pipes
	// are created.
	if (!DuplicateHandle(GetCurrentProcess(), hOutputReadTmp,
						 GetCurrentProcess(),
						 &hOutputRead, // Address of new handle.
						 0, FALSE,	 // Make it uninheritable.
						 DUPLICATE_SAME_ACCESS))
		DisplayError("DupliateHandle");

	if (!DuplicateHandle(GetCurrentProcess(), hInputWriteTmp,
						 GetCurrentProcess(),
						 &hInputWrite, // Address of new handle.
						 0, FALSE,	 // Make it uninheritable.
						 DUPLICATE_SAME_ACCESS))
		DisplayError("DupliateHandle");

	// Close inheritable copies of the handles you do not want to be
	// inherited.
	if (!CloseHandle(hOutputReadTmp))
		DisplayError("CloseHandle");
	if (!CloseHandle(hInputWriteTmp))
		DisplayError("CloseHandle");
	// cb.Call(env.Global(), {Napi::String::New(env,"command complete1")});

	// Get std input handle so you can close it and force the ReadFile to
	// fail when you want the input thread to exit.
	if ((hStdIn_f = GetStdHandle(STD_INPUT_HANDLE)) ==
		INVALID_HANDLE_VALUE)
		DisplayError("GetStdHandle");
	// cb.Call(env.Global(), {Napi::String::New(env,"before prep")});

	PrepAndLaunchRedirectedChild(hOutputWrite, hInputRead, hErrorWrite, strCommands, commandCounter_f, hChildProcess_f, env);
	//  cb.Call(env.Global(), {Napi::String::New(env,"after prep")});

	// Close pipe handles (do not continue to modify the parent).
	// You need to make sure that no handles to the write end of the
	// output pipe are maintained in this process or else the pipe will
	// not close when the child process exits and the ReadFile will hang.
	if (!CloseHandle(hOutputWrite))
		DisplayError("CloseHandle");
	if (!CloseHandle(hInputRead))
		DisplayError("CloseHandle");
	if (!CloseHandle(hErrorWrite))
		DisplayError("CloseHandle");

	SetAndSendInputThread(hStdIn_f, bRunThread_f);
	// Launch the thread that gets the input and sends it to the child.
	hThread = CreateThread(NULL, 0, &GetAndSendInputThread, (LPVOID)&hInputWrite, 0, &ThreadId);
	if (hThread == NULL)
		DisplayError("CreateThread");

	// Read the child's output.
	ReadAndHandleOutput(env, hOutputRead, outPutfileName, cb);
	// Redirection is complete

	// Force the read on the input to return by closing the stdin handle.
	//if (!CloseHandle(hStdIn_f)) DisplayError("CloseHandle");

	// Tell the thread to exit and wait for thread to die.
	bRunThread_f = FALSE;
	SetAndSendInputThread(hStdIn_f, bRunThread_f);
	// if (WaitForSingleObject(hThread,INFINITE) == WAIT_FAILED)
	//     DisplayError("WaitForSingleObject");

	// if (!CloseHandle(hOutputRead)) DisplayError("CloseHandle");
	// if (!CloseHandle(hInputWrite)) DisplayError("CloseHandle");

	Napi::Object res = Napi::Object::New(env);
	res.Set("status", "complete");
	res.Set("outputFileName", configFolderPath + optname);
	cb.Call(env.Global(), {res});

	std::cout << "final-merge-complete";
}

Napi::Object Init(Napi::Env env, Napi::Object exports)
{
	return Napi::Function::New(env, finalmerge);
}
NODE_API_MODULE(finalmerge, Init)
