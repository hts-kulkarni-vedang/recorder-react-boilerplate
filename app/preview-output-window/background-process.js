process.on('message', (msg) => {
    
    if (msg.message === 'generateWaveform') {
        generateWaveform(msg);
    } else if (msg.message === 'mergeVideoFilesProcess') {
        mergeVideoFiles(msg);
    } else if (msg.message === 'convert-to-mp4') {
        convertToMp4(msg);
    }
});

function generateWaveform(msg) {
    try {
        const preprocessBinding = require('bindings')('preprocess');
        const obj = {
            videoFilePath: msg.videoFilePath,
            outputFilePath: msg.outputFilePath,
            waveformPictureWidth: parseInt(msg.waveformPictureWidth),
            waveformPictureHeight: parseInt(msg.waveformPictureHeight),
			basepath: msg.basepath,
        };
        const result = preprocessBinding.generateWaveformFromVideo(obj, () => { });
        
        const message = {
            type: 'result',
            result,
            processName: msg.message
        };
        process.send(message);
    } catch (exception) {
        
        const message = {
            type: 'error',
            exception,
            processName: msg.message
        };
        process.send(message);
    }
}
function mergeVideoFiles(msg) {
    
    try {
        const preprocessBinding = require('bindings')('preprocess');
        // const mergeResult = preprocessBinding.mergeFiles(changedFiles, filePath, basepath, fileExtension.extension, fileType, newFileName, (res) => {
        const obj = {
            changedFiles: msg.changedFiles,
			filePath: msg.filePath,
			basepath: msg.basepath,
			extension: msg.extension,
			fileType: msg.fileType,
            newFileName: msg.newFileName,
            callback: () => {}
        };
        const mergeResult = preprocessBinding.mergeFiles(obj);
        
        const message = {
            type: 'result',
            mergeResult,
            processName: msg.message
        };
        process.send(message);
    } catch (exception) {
        
        const message = {
            type: 'error',
            exception,
            processName: msg.message
        };
        process.send(message);
    }
}
function convertToMp4(msg) {
    
    try {
        const preprocessBinding = require('bindings')('preprocess');
        // const mergeResult = preprocessBinding.mergeFiles(changedFiles, filePath, basepath, fileExtension.extension, fileType, newFileName, (res) => {
        const obj = {
            filepath: msg.filepath,
			basepath: msg.basepath,
            newFileName: msg.newFileName,
            callback: () => {}
        };
        const convertResult = preprocessBinding.convertToMp4(obj);
        const message = {
            type: 'result',
            convertResult,
            processName: msg.message
        };
        process.send(message);
    } catch (exception) {
        
        const message = {
            type: 'error',
            exception,
            processName: msg.message
        };
        process.send(message);
    }
}