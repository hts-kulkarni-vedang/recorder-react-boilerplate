const {ipcRenderer,shell} = require("electron");
const {dialog} = require("electron").remote;
const electron = require("electron");
const fs = require("fs");
const outputRes = {width : 1280,height : 720};
const paddingColor = "#ffffff"; //def for white
const previewJs = require('./preview.js');
const url = require('url');
const path = require('path');
var os =  require("os");
const documentsFolderPath = os.homedir+'/Documents/HiFi_Recorder Files';
const env = require("../environment");
var win = null;
var config = null;
var currentRec = null;
var currentFinalMergerIndex = null;
var backImgPath = "false";
var logoImgPath = "false";
var paramObj = {};
var child_process = require('child_process');
var swal = require("sweetalert2");
var selectedCanvas=null;
var currentCropCanvas = null;
var replaceBackSlashFlag = true;
var transparencyOpacity =0;
var transparencySimilarity = 0.1;
var colorHex = 'C1BBBB';
var jCropObj= null;
var selectedCanvasHTML = "";
const aRec = `
	<button id="id-here" class="rec-item" final-merger-index="final-merger-index-here">text-here</button>
`;
const newCanvas = `
	<div class='draggable resizable canvas-wrap' id='canvas-id-here-wrap' mergerindex='merger-index-here' type='rec-type-here' subtype="type-here"><canvas id='canvas-id-here' rectype='rec-type-here' type="type-here"></canvas></div>
`;

const app = require("electron").remote.app;

// ipcRenderer.on('process-videos',(event,param)=>{
// 	console.log(param);
// 	initPreview(param);
// });
ipcRenderer.on('auto-updater', (event, param) => {
	handleAutoUpdateEvent(param);
});
ipcRenderer.on('request-auto-updater-status', (event, param) => {
	// handleAutoUpdateEvent(param);
	console.log(param,event);
	switch(param){
		case "checking-for-update" : 
			break;
		case "update-available" : 
			$(".new-image").show();
			$(".infinite-loader").hide();
			$(".install-update").hide();
			break;
		case "update-not-available" : 
			break;
		case "error" : 
			break;
		case "download-progress" : 
			$(".new-image").hide();
			$(".infinite-loader").show();
			$(".install-update").hide();

			break;
		case "update-downloaded" : 
			$(".new-image").hide();
			$(".infinite-loader").hide();
			$(".install-update").show();
			break;
	}
});

$(document).ready(function(){
	win = electron.remote.getCurrentWindow();
	win.maximize();
	checkAutoUpdaterStatus();
	replaceBackSlashFlag = true;
	// $('#canvas-back-img').width($('#preview').width());
	// $('#canvas-back-img').height($('#preview').height());
	let prevWidth = $('#preview').width();
	let prevHeight = $('#preview').height();

	$('#canvas-back-img').attr('width',prevWidth - 100);
	$('#canvas-back-img').attr('height',prevHeight - 50);

	console.log(win.getSize());
	let winHeight = win.getSize()[1];
	let headerHeight = $("header").height();
	let recListTitleHeight = $(".recording-list-title").height();
	// let recListMargin = parseInt($(".recording-list").css('margin').replace("px","")) * 2;
	$(".side-nav").height(winHeight - headerHeight - 20);
	let sideNavHeight = $(".side-nav").height();
	$(".recording-list").height(sideNavHeight - recListTitleHeight - 20);

	console.log('document is ready',sessionStorage);

	loadDefaultTransparency();
	// initPreview(false);
	let param= {
		configFolderPath : (sessionStorage.getItem("configFolderPath"))
	}
	initPreview(param);
});
$(document).on('click','#close',function(){
	win.close();
});
$(document).on('click','#minimize',function(){
	win.minimize();
});
$(document).on('click','.rec-item',function(){
	let finalMergerIndex = parseInt(this.getAttribute('final-merger-index'));
	currentFinalMergerIndex =finalMergerIndex;
	currentRec = config.finalMerger[finalMergerIndex];
	toggleTransparencyOptions(finalMergerIndex);
	if(currentRec.type == "screen"){
		$("#btn-open-crop-mode").show();
		currentCropCanvas = $("#canvas-camera-"+finalMergerIndex+'-wrap');
	}else if(currentRec.type == "both"){
		$("#btn-open-crop-mode").show();
		currentCropCanvas = $("#canvas-screen-"+finalMergerIndex+'-wrap');
	}else{
		$("#btn-open-crop-mode").hide();
		currentCropCanvas = null;
	}
	showInCanvas(finalMergerIndex);
	$(".rec-item").removeClass("active-rec-item");
	$("#btn-crop").addClass("disabled");
	$(this).addClass("active-rec-item");
	replaceBackSlashFlag = false;

});
$(document).on('click','#btn-process-now',function(){
	processNow();
});
$(document).on('click','#btn-show-preview',function(){
	showPreview();
});
$(document).on('click','#btn-change-back-img',function(){
	changeBackImg();
});
$(document).on('click','#btn-remove-back-img',function(){
	removeBackImg();
});
$(document).on('resize','.resizable',function(){
	canvasResized(this);
});
$(document).on('click','#btn-save-settings',function(){
	saveSettings();
});
$(document).on('click','.transparency-check',function(){
	toggleTransparency(this);
});
$("#similarity-range-slider").on('input change',function(){
	console.log(currentRec);
	$("#similarity-value").text((this.value)+"%")
	transparencySimilarity = (parseInt(this.value) / 100).toFixed(2);
	currentRec.transparencySimilarity = (parseInt(this.value) / 100).toFixed(2);
})
$("#opacity-range-slider").on('input change',function(){
	$("#opacity-value").text((this.value)+"%")
	transparencyOpacity = (parseInt(this.value) / 100).toFixed(2);
	currentRec.transparencyOpacity = (parseInt(this.value) / 100).toFixed(2);
});
$('#hex-color').on('input',function(ev){
	let val = $(this).val();
	val = val.toUpperCase();
	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
	colorHex = val;
	if(isOk){
		$(this).removeClass('error-input');
		$(this).addClass('correct-input');
		$(".hex-color-correct-incorrect").text("");
		config.colorHex = val.replace("#","");
		currentRec.colorHex = val.replace("#","");
	}else{
		$(this).removeClass('correct-input');
		$(this).addClass('error-input');
		$(".hex-color-correct-incorrect").text("Incorrect Color !");
		currentRec.colorHex = val.replace("#","");
	}
});
function isHexOk(){
	let val = $('#hex-color').val();
	val = val.toUpperCase();
	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
	colorHex = val;
	if(isOk){
		$('#hex-color').removeClass('error-input');
		$('#hex-color').addClass('correct-input');
	}else{
		$('#hex-color').removeClass('correct-input');
		$('#hex-color').addClass('error-input');
	}
}
$(document).on('click','.load-default',function(){
	loadDefaultTransparency(this);
});
$(document).on('click','.save-as-default',function(){
	saveAsDefault(this);
});
$(document).on('click','.canvas-wrap',function(){
	canvasSelected(this);
});
$(document).on('click','#btn-crop',function(){
	initCropMode(this);
});
$(document).on('click','#btn-release-crop',function(){
	cancelCropMode(this);
});
$(document).on('click','.btn-confirm-crop',function(){
	confirmCrop(this);
});
$(document).on('click','#btn-open-crop-mode',function(){
	console.log('came to open crop mode')
	openCropMode();
});
$(document).on('click','#btn-close-crop-mode,.cancel-crop-btn',function(){
	closeCropMode();
});
$(document).on('click','.back-btn',function(){
	backToHome();
});
$(document).on('click','#btn-add-logo',function(){
	changeLogoImg();
});
$(document).on('click','#btn-remove-logo',function(){
	removeLogoImg();
});
$(document).on("click",".new-image",function(){
	onUpdateAvailableClick();
});
$(document).on("click",".install-update",function(){
	onUpdateDownloadClick();
});
$(document).on("click","#btn-auto-resize",function(){
	autoResizeCurrentCanvas();
});

function initPreview(param){
	paramObj = param;
	let windowParam = sessionStorage.getItem("windowParam");
	if(windowParam){
		config = JSON.parse(windowParam).config;
		console.log("config loaded from windowParam",config);
	}else{
		config = JSON.parse(fs.readFileSync(param.configFolderPath+'\\settings.config', 'utf8'));
		console.log("config loaded from config file");
	}
	sessionStorage.removeItem("windowParam");
	config.outputRes = outputRes; // remove if unused
	config.paddingColor = paddingColor; // remove if unused
	config.configFolderPath = param.configFolderPath.replace(/\\/g,'\\\\\\\\');
	config.backImgPath = "false"; // to be used in c++ ;
	console.log(config.finalMerger);
	changeFinalMergerPaths();
	showRecordings();
	initVideos();
}
function changeFinalMergerPaths() {
	console.log('to change finalMerger : ', config.finalMerger)
	// let folder = optname.split(".mp4")[0]
	for (let i = 0; i < config.finalMerger.length; i++) {
		let obj = config.finalMerger[i]

		if (obj.type == "camera") {
			//obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			//obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length - 1]
			obj.cpath = 'camera' + obj.cpath.split('camera')[obj.cpath.split('camera').length - 1];
		}
		else if (obj.type == "screen") {
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')*/
			// obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.cpath = 'screen' + obj.cpath.split('screen')[obj.cpath.split('screen').length - 1];
		}
		else if (obj.type == "both") {
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			obj.spath = obj.spath.replace("./assets/",folder+'/')*/
			//obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.cpath = 'camera' + obj.cpath.split('camera')[obj.cpath.split('camera').length - 1];
			obj.spath = 'screen' + obj.spath.split('screen')[obj.spath.split('screen').length - 1];
			//obj.spath = obj.spath.split("/assets/")[obj.spath.split("/assets/").length -1]
		}
	}
}
function showRecordings(){
	$('.recording-list-items').empty();
	for(let i=0;i<config.finalMerger.length;i++){
		let rec = aRec;
		rec = rec.replace(/id-here/g,"rec-item-"+i);
		rec = rec.replace(/text-here/g,(i+1)+"  -  "+config.finalMerger[i].type);
		rec = rec.replace(/final-merger-index-here/g,i);
		$('.recording-list-items').append(rec).ready(function(){
			if(config.finalMerger.length > 0){
				$('.rec-item').get(0).click(); // if any problem in future move this inside setTimeout below
			}
		});
	}
	// setTimeout(()=>{
		
	// },1000);
}
function showInCanvas(index){
	currentRec = config.finalMerger[index];
	$('.canvas-wrap').hide();
	$('.canvas-wrap').removeClass('inline-block');
	$('#canvas-camera-'+index+'-wrap').show();
	$('#canvas-camera-'+index+'-wrap').addClass('inline-block');
	if(currentRec.type == 'both'){
		$('#canvas-screen-'+index+'-wrap').show();
		$('#canvas-screen-'+index+'-wrap').addClass('inline-block');
	}
}
function initVideos(){
	for(let i=0;i<config.finalMerger.length;i++){
		let item = config.finalMerger[i];
		if(item.type == 'both'){
			let screenVideo = addVideo(index=i,type='screen',src = paramObj.configFolderPath+'\\'+item.spath);
			let screenCanvas = addCanvas(index = i,type = 'screen',width = screenVideo.width,screenVideo.height,recType = item.type);//remove width and height if unused
			screenCanvas.setAttribute('windowCoords',JSON.stringify(item.windowCoords));
			if(item.fullScreenMode){
				screenCanvas.setAttribute('fullScreenMode',"true");	
			}else{
				screenCanvas.setAttribute('fullScreenMode',"false");	
			}
			getVideoFrames(screenVideo,screenCanvas);
		}
		let camVideo = addVideo(index=i,type='camera',src = paramObj.configFolderPath+'\\'+item.cpath);
		let camCanvas = addCanvas(index = i,type = 'camera',width = camVideo.width,camVideo.height,recType = item.type);
		if(item.windowCoords && item.type=='screen'){
			camCanvas.setAttribute('windowCoords',JSON.stringify(item.windowCoords));
		}
		if(item.fullScreenMode){
			camCanvas.setAttribute('fullScreenMode',"true");	
		}else{
			camCanvas.setAttribute('fullScreenMode',"false");	
		}
		getVideoFrames(camVideo,camCanvas);
	}
	if(fs.existsSync(env.settingsFilePath)){
		console.log('loading settings');
		loadSettings();
	}else{	
		console.log('settings not found'); //create settings file here
	}
	console.log(config);
}
function getVideoFrames(video,canvas){
	let promise = video.play();
	if (promise !== undefined) {
        promise.then(function () {
            // start Automatic playback;
            (function loop() {
                if (!video.paused && !video.ended) {
                    drawOnCanvas(video, canvas)
                    setTimeout(loop, 1000 / 30); // drawing at 30fps
                }
            })();
            setTimeout(function () {
                video.currentTime = 0
                video.pause()
            }, 1000);
        }).catch(function (error) {
            console.log(error)
        });
    }

    //why this?? if not used remove
    video.addEventListener('play', function () {
        if (this.readyState > 0) {
            var $this = this; //cache
            (function loop() {
                if (!$this.paused && !$this.ended) {
                    drawOnCanvas($this, canvas)
                    setTimeout(loop, 1000 / 30); // drawing at 30fps
                }
            })();
        }
    })
}
function processNow(){
	if(checkCameraHasBackground() == true){
		saveSettings();
		console.log(config);
		checkFFMPEG().then((res) => {
			console.log(config);	
			// debugger
			config.backImgPath = config.backImgPath.replace(/%20/g," ")
			config.configFolderPath = config.configFolderPath.replace(/\\\\\\\\/g,'\\\\');
			fs.writeFileSync(config.configFolderPath+'\\settings.config',JSON.stringify(config),'utf-8');
			let obj = {
				process : 'later',
				configFolderPath : config.configFolderPath.replace('\\\\\\\\','\\\\'),
				mode : 'process',
				sourceFile : '../process-window/index.html'
			};
			localStorage.setItem('configFromPreview',JSON.stringify(config));
			//config = JSON.stringify(config);
		// win.unmaximize();
		// win.setSize(100,100);
			win.loadURL(url.format({
				pathname : path.join(__dirname,obj.sourceFile),
				protocol : 'file:',
				slashes : true
			}));

		}).catch((err)=>{
			swal.fire({
				type : 'error',
				title : 'Installation Error',
				text : 'One or more dependencies were not found!'
			}).then((res)=>{
				shell.openExternal("https://ffmpeg.org/");
			});
		});
	}else if(currentRec.type == "camera"){
		swal.fire({
			type : "info",
			title : "Background image not set",
			text : "Please select background image for using transparency in camera only mode"
		});	
	}
	// ipcRenderer.send('process-videos',obj);
}
function checkCameraHasBackground(){
	let flag = true;
	for(let i=0;i<config.finalMerger.length;i++){
		let iRec = config.finalMerger[i];
		if(iRec.type == "camera" && iRec.transparencyCheck == true && config.backImgPath == "false"){
			flag = false;
		}
	}
	return flag;
}
function showPreview(){
	if(currentRec){
		if(currentRec.transparencyCheck == true && config.backImgPath == "false" && currentRec.type == "camera"){
			swal.fire({
				type : "info",
				title : "Background image not set",
				text : "Please select background image for using transparency in camera only mode"
			});
		}else{
			saveSettings();
			checkFFMPEG().then((res) => {
					console.log(config);
					// debugger
					// TODO - if config.backImgpah has %20 then change it to spaces [c++ requirement]
					config.backImgPath = config.backImgPath.replace(/%20/g," ")
					//debugger
					var basepath = app.getAppPath('exe');
					console.log(basepath);
					basepath = basepath.replace("app.asar","")
					basepath = basepath.replace(/\\/g,"\\\\");
					basepath+= "\\";

					const output = preparePreviewCommand(config,currentRec,replaceBackSlashFlag,basepath);
					console.log("output from preview command : ",output);
				}).catch((err)=>{
					console.error(err);
					swal.fire({
						type : 'error',
						title : "Installation Error",
						text : 'Some of dependencies were not found on this PC!'
					}).then((res)=>{
						// shell.openExternal("https://ffmpeg.org/");
					});
				});
			replaceBackSlashFlag = false;
		}
	}else{
		alert('recording not selected');
	}
}
function changeBackImg(){
	backImgPath = dialog.showOpenDialog(win,{
		filters: [
			{ name: 'Images', extensions: ['jpg', 'png'] },
		],
		properties : ['openFile']
	});
	console.log(backImgPath);
	if(backImgPath){
		backImgPath = backImgPath[0].replace(/\\/g,'\\\\');
		// $("#dummy-back-img").attr('src',backImgPath);
		let preview = $('#preview').get(0);
		let tmpBack = (backImgPath.replace(/ /g,"%20"));
		tmpBack = (tmpBack.replace(/\(/g,"\\("));
		tmpBack = (tmpBack.replace(/\)/g,"\\)"));
		preview.style.backgroundImage = 'url('+tmpBack+')';
		preview.style.backgroundRepeat = 'no-repeat';
		preview.style.backgroundSize = '100% 100%';
		config.backImgPath = backImgPath;

		let img = document.createElement("img");
		img.src = backImgPath;
		var canvas = document.createElement('canvas');
		canvas.width = 200;
		canvas.height = 200;
		var pixelData = canvas.getContext('2d').getImageData(10, 10, 1, 1).data;
		console.log(pixelData,img,canvas);
	}
}
/**
* @param elem - video or audio element
* @param type	  - can be any of (camera,screen,both,background)
* @param canvas-   canvas element to draw on
*/
function drawOnCanvas(elem,canvas){
	let ctx = canvas.getContext("2d");
	ctx.imageSmoothingEnabled = false; //for image quality
	let coords = canvas.getAttribute('windowCoords');	
	let type = canvas.getAttribute('rectype');	
	let subtype = canvas.getAttribute('type');	
	// if()
	if(type =="screen" || subtype =="screen"){
		// console.log("type: ",type,"subtype : ",subtype);
		// console.log("coords: ",coords);
		if(coords == "null" || coords == "{}"){
			let originalVideoRes = {
				width : elem.videoWidth,
				height : elem.videoHeight,
				screenX: 0,
				screenY : 0
			}
			canvas.setAttribute("windowCoords",JSON.stringify(originalVideoRes));
			coords = JSON.stringify(originalVideoRes);
		}
	}
	// console.log(coords);
	// debugger
	if(coords){
		if(coords!="null"){
			coords = JSON.parse(coords);
	        ctx.drawImage(elem, coords.screenX, coords.screenY, coords.width, coords.height, 0, 0, canvas.width, canvas.height);
			// console.log("drawing with coords");
		}else{
			ctx.drawImage(elem, 0,0,canvas.width, canvas.height)
			// console.log("drawing full");
		}
		
	}else{
	    ctx.drawImage(elem, 0,0,canvas.width, canvas.height)
		// console.log("drawing full");
	}	
}
function videoTimeUpdate(elem){
	// console.log(elem);
}
function addCanvas(index,type,width,height,recType){ //remove width and height if unused
	let aCanvas = newCanvas;
	aCanvas = aCanvas.replace(/canvas-id-here/g,'canvas-'+type+'-'+index+'');
	aCanvas = aCanvas.replace(/rec-type-here/g,recType);
	aCanvas = aCanvas.replace(/type-here/g,type);
	aCanvas = aCanvas.replace(/merger-index-here/g,index);
	$('#preview').append(aCanvas).ready(function(){
		// $(".resizable")
	    $(".draggable").draggable({
	        cursor: "crosshair",
	        containment: "#preview",
	        snap : "#preview",
	        snapMode : "inner",
	        snapTolerance : 1
	    });
	    $(".resizable").resizable({
	        containment: "#preview",
	        handles: 'e,w,n,s,se,nw,ne,sw',
	    });	
	});

	

	let camCanvas = $('#canvas-'+type+'-'+index).get(0);
	camCanvas.setAttribute('width',$('#preview').width()); 
	camCanvas.setAttribute('height',$('#preview').height());

	return camCanvas;
}
function addVideo(index,type,src){
	let currentRec = config.finalMerger[index];
	$('.dummies').append('<video id="video-'+type+'-'+index+'"></video>');
	let camVideo = $('#video-'+type+'-'+index).get(0);
	
	camVideo.src = src;
	camVideo.load();
	camVideo.muted = true;
	console.log(camVideo);
	// if(type == "screen"){
	// 	let keysLen = Object.keys(currentRec.windowCoords);
	// 	if(keysLen == 0){
	// 		// windowCoords is empty; set width,height,screenX,screenY to default values
	// 		currentRec.windowCoords = {
	// 			width : -1,
	// 			height : -1,
	// 			screenX: 0,
	// 			screenY : 0
	// 		};
	// 	}
	// }
	return camVideo;
}
function canvasResized(elem){
	let canvasId = elem.id.replace('-wrap','');
	let canvas = $('#'+canvasId).get(0);
	let parW = $(elem).width();
	let parH = $(elem).height();
	$(canvas).width(parW);
	$(canvas).height(parH);
	// $(canvas).attr('width',parW);
	// $(canvas).attr('height',parH);
	// console.log('should probably resize this : ',canvas,elem.id,canvasId);
}
function saveSettings(){
	console.log(config);
	let background = {
        top: 0,
        left: 0,
        width: $("#preview").width(),
        height: $("#preview").height(),
    }
    console.log(background);
	$('.canvas-wrap').each(function(index,elem){
		// console.log(index,elem);
		console.log('width : ',$(this).width());
		console.log('height : ',$(this).height());
		console.log('left : ',$(this).css('left'));
		console.log('top : ',$(this).css('top'));

		// debugger
		let width = getCanvasDimension('width',$(this).width(),background);
        let height = getCanvasDimension('height',$(this).height(),background);
        let left = getCanvasDimension('left',$(this).css('left'),background);
        let top = getCanvasDimension('top',$(this).css('top'),background);

        // debugger
       	let type = this.getAttribute('type');
        let subtype = this.getAttribute('subtype');
        let mergerIndex = parseInt(this.getAttribute('mergerindex'));
       
	        if(type == "screen" || subtype == "screen"){
	        	let windowCoords = $(this).children("canvas").get(0).getAttribute("windowCoords");
		        windowCoords = (windowCoords) ? JSON.parse(windowCoords) : null;
		        config.finalMerger[mergerIndex].windowCoords = windowCoords;	
	        }
	        if(type == 'both'){
	        	if(subtype == 'screen'){
	        		config.finalMerger[mergerIndex].swidth = width;
	        		config.finalMerger[mergerIndex].sheight = height;
	        		config.finalMerger[mergerIndex].sleft = left;
	        		config.finalMerger[mergerIndex].stop = top;
	        	}else if(subtype == 'camera'){
	        		config.finalMerger[mergerIndex].cwidth = width;
	        		config.finalMerger[mergerIndex].cheight = height;
	        		config.finalMerger[mergerIndex].cleft = left;
	        		config.finalMerger[mergerIndex].ctop = top;
	        	}
	        }else{
	        	console.log(config.finalMerger[mergerIndex],config);
	        	config.finalMerger[mergerIndex].cwidth = width;
	    		config.finalMerger[mergerIndex].cheight = height;
	    		config.finalMerger[mergerIndex].cleft = left;
	    		config.finalMerger[mergerIndex].ctop = top;
	        }
	    
	});
	let logoCanvas = $(".logo-canvas-wrap").get(0);
	config.logoCoords = {};
	if(logoCanvas){
		let width = getCanvasDimension('width',$(logoCanvas).width(),background);
        let height = getCanvasDimension('height',$(logoCanvas).height(),background);
        let left = getCanvasDimension('left',$(logoCanvas).css('left'),background);
        let top = getCanvasDimension('top',$(logoCanvas).css('top'),background);
		
		config.logoImgPath = logoImgPath;
        config.logoCoords= {
        	width: width,
        	height: height,
        	left : left,
        	top : top
        };
	}else{
		config.logoImgPath = "false"; //for cpp
	}
	config.transparencyOpacity = transparencyOpacity;
	config.transparencySimilarity = transparencySimilarity;
	config.colorHex = colorHex;
	// if(config.backImgPath)
	// 	config.backImgPath = backImgPath.replace(/\\/g,'\\\\');
	// console.log(config);
	for(let i=0;i<config.finalMerger.length;i++){
		if(config.finalMerger[i].transparencyCheck){
			if(config.finalMerger[i].transparencyCheck == true){
				if(config.finalMerger[i].colorHex){
					if(config.finalMerger[i].colorHex[0] == "#"){
						config.finalMerger[i].colorHex=config.finalMerger[i].colorHex.replace("#","");
					}
				}
			}
		}
	}
	console.log(config);
	// debugger;
}

// has two extra parameters needed only while cropping
// isForCrop = true/false
// originalVideoRes = {width,height} of original video
function getCanvasDimension(type,value,background,isForCrop,originalVideoRes){
    // this is css left,top,width,height
    let val;
    if(typeof value == 'string'){
    	if(value == 'auto'){
    		value = '0px';
    	}
    	// val = parseInt(value.substr(0, value.length - 2));
    	val = parseInt(value.replace("px",""));
    }else{
    	val = value;
    }
    // debugger
    let backTemp = background[type];
    let outputTemp;
    if(isForCrop){
    	outputTemp = originalVideoRes[type];
    }else{
    	outputTemp = config.outputRes[type];
    }
    if(type == 'left'){
    	backTemp = background['width'];
    	if(isForCrop){
    		outputTemp = originalVideoRes['width'];
    	}else{
    		outputTemp = config.outputRes['width'];
	    }
    }else if(type == 'top'){
    	if(isForCrop){
    		outputTemp = originalVideoRes['height'];
    	}else{
    		outputTemp = config.outputRes['height'];
	    }
    	backTemp = background['height'];
    }
   	val = (val * outputTemp) / backTemp;
   	if((type == 'width' || type == 'height') && parseInt(val %2) != 0){
   		// val --; 
   		val ++; 
   	}
   	val = Math.round(val.toFixed(2));
   	console.log(val);
    return val;
}

function toggleTransparencyOptions(finalMergerIndex){
	let item = config.finalMerger[finalMergerIndex];
	console.log(config,item);
	// debugger;
	if(item.type == "both" || item.type == "camera"){

		$('.transparency-options').show();
		$('.transparency-options').css("width","calc(80% - 20px)");
		$('.transparency-options').css("display","inline-block");
		$(".a-transparency-option").hide();
		$(".example-hex-color").hide();
		$("#hex-color").val("");
		if(item.transparencyCheck){
			$(".a-transparency-option").show();
			$(".example-hex-color").show();
			if(item.transparencyCheck == true){

				$('.transparency-check').prop('checked',true);
				$('.a-transparency-option').show();
				$(".example-hex-color").show();

				$(".default-transparency-options").show();
				if(item.colorHex){
					let hash = ""
					if(item.colorHex[0] != "#"){
						hash = "#";
					}else{
						hash = "";
					}
					$("#hex-color").val(hash+item.colorHex);
				}else{
					item.colorHex = "";
				}
				if(item.transparencyOpacity){
					$("#opacity-value").text((item.transparencyOpacity * 100).toFixed(0)+"%");
					$("#opacity-range-slider").val((item.transparencyOpacity * 100).toFixed(0));
				}else{
					$("#opacity-value").text("0%");
					$("#opacity-range-slider").val(0);
					item.transparencyOpacity = 0;
				}
				//debugger
				if(item.transparencySimilarity){
					$("#similarity-range-slider").val((item.transparencySimilarity * 100).toFixed(0));
					$("#similarity-value").text((item.transparencySimilarity * 100).toFixed(0)+"%");
				}else{
					$("#similarity-range-slider").val(10);
					$("#similarity-value").text(("10%"));
					item.transparencySimilarity = 0.1;
				}
				isHexOk();
			}else{
				$('.transparency-check').prop('checked',false);
				$("#transparency-options-inner").hide();
				$(".default-transparency-options").hide();
				isHexOk();
			}
		}else{
			$("#opacity-value").text("0%");
			$("#opacity-range-slider").val(0);
			item.transparencyOpacity = 0;
			$("#similarity-range-slider").val(10);
			$("#similarity-value").text(("10%"));
			item.transparencySimilarity = 0.1;
			$('.transparency-check').prop('checked',false);
			$("#transparency-options-inner").hide();
			$(".default-transparency-options").hide();
		//	debugger
			isHexOk();
		}
	}else{
		$(".transparency-options").hide();
	}
	
}
function toggleTransparency(elem){
	currentRec.transparencyCheck = elem.checked;
	console.log(currentRec.transparencyCheck,config.backImgPath,currentRec.type);
	if(currentRec.transparencyCheck == true && config.backImgPath == "false" && currentRec.type == "camera"){
		swal.fire({
			type : "info",
			title : "Background image not set",
			text : "Please select background image for using transparency in camera only mode"
		});
		elem.checked= false;
		currentRec.transparencyCheck = false;
	}
	else if(currentRec.transparencyCheck){
		let tmpOpacity  = (currentRec.transparencyOpacity * 100).toFixed(0);
		$("#opacity-value").text((tmpOpacity)+"%");
		$("#opacity-range-slider").val(tmpOpacity);
		//debugger
		let tmpSimilarity  = (currentRec.transparencySimilarity * 100).toFixed(0);
		$("#similarity-value").text((tmpSimilarity)+"%");
		$("#similarity-range-slider").val(tmpSimilarity);
		$("#hex-color").text("#"+currentRec.colorHex);

		$('.a-transparency-option').show();
		$(".example-hex-color").show();

		$('.default-transparency-options-hide').addClass("default-transparency-options-show");
		$('.default-transparency-options').show();
	}else{
		$('.a-transparency-option').hide();
		$('.default-transparency-options').hide();
		$(".example-hex-color").hide();

		$('.default-transparency-options-hide').removeClass("default-transparency-options-show");
	}
}
function loadSettings(){
	let settings=JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));	
	console.log(settings);
	config.outputRes = settings.outputRes;
	config.crf = settings.crf;
	config.preset = settings.preset;
	config.backImgPath = settings.backImgPath.replace(/ /g,'%20');

	let preview = $('#preview').get(0);
	preview.style.backgroundImage = 'url('+config.backImgPath+')';
	preview.style.backgroundRepeat = 'no-repeat';
	preview.style.backgroundSize = '100% 100%';

	console.log($('.canvas-wrap').length);
	$('.canvas-wrap').each(function(){
		console.log(this);
		let rectype = this.getAttribute('type');
		let subtype = this.getAttribute('subtype');
		let width,height,left,top;
		console.log(rectype,subtype);
		if(rectype == 'camera' || rectype == 'screen'){
			width = settings.preview[rectype].cwidthRaw;
			height = settings.preview[rectype].cheightRaw;
			left = settings.preview[rectype].cleftRaw;
			top = settings.preview[rectype].ctopRaw;
		}else if(rectype == 'both'){
			let keyPrefix;
			if(subtype == 'camera'){
				keyPrefix = 'c'
			}else if(subtype == 'screen'){
				keyPrefix = 's';
			}
			console.log(settings);
			width = settings.preview.both[keyPrefix+'widthRaw'];
			height = settings.preview.both[keyPrefix+'heightRaw'];
			left = settings.preview.both[keyPrefix+'leftRaw'];
			top = settings.preview.both[keyPrefix+'topRaw'];
		}	
		$(this).width(width);$(this).find('canvas').width(width); //could you give 100% 100% here??
 		$(this).height(height);$(this).find('canvas').height(height);
		$(this).css('left',left);
		$(this).css('top',top);
		console.log(width,height,left,top);
	});
	if(settings.logoImgPath){
		removeLogoImg();
		logoImgPath = settings.logoImgPath;

			const logoCanvas = `
				<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="`+logoImgPath+`" class='logo-canvas'></div>
			`;
			$("#preview").append(logoCanvas).ready(function(){
				$(".draggable").draggable({
			        cursor: "crosshair",
			        containment: "#preview",
			        snap : "#preview",
			        snapMode : "inner",
			        snapTolerance : 1
			    });
			    $(".resizable").resizable({
			        containment: "#preview",
			        handles: 'e,w,n,s,se,nw,ne,sw',
			    });
				$(".logo-canvas-wrap").css('width',settings.logoCoords.widthRaw);
				$(".logo-canvas-wrap").css('height',settings.logoCoords.heightRaw);
				$(".logo-canvas-wrap").css('left',settings.logoCoords.leftRaw);
				$(".logo-canvas-wrap").css('top',settings.logoCoords.topRaw);
			});
	}
}


function removeBackImg(){
	backImgPath = null;
	let preview = $('#preview').get(0);
	preview.style.background = "white";
	config.backImgPath = "false";	//for cpp
}
function removeLogoImg(){
	logoImgPath = null;
	config.logoImgPath = "false"; //for cpp		
	$(".logo-canvas-wrap").remove();
}
async function checkFFMPEG(){
	var basepath = app.getAppPath('exe');
	console.log(basepath);
	basepath = basepath.replace("app.asar","")
	basepath+= "\\src\\assets\\exe\\ffmpeg";


	return new Promise((resolve,reject)=>{
		try{
			let proc = child_process.spawn(basepath,["--version"]);
			
			proc.on('close',function(status,signal){
				console.log('closed',status,signal);
				resolve({installed : true});
			});	
			proc.on('error',function(err){
				console.log(err);
				reject({installed : false});
			});
		}
		catch(err){
			console.log(err);
		}
	});
}
function loadDefaultTransparency(){
	if(fs.existsSync(documentsFolderPath+'\\transparency-defaults.json')){
		let defaultPreviewSettings = JSON.parse(fs.readFileSync(documentsFolderPath+'\\transparency-defaults.json', 'utf8'));
		console.log(defaultPreviewSettings);
		if(defaultPreviewSettings.defaults){
			$("#hex-color").val(defaultPreviewSettings.defaults.color);
			transparencySimilarity = parseFloat(defaultPreviewSettings.defaults.transparencySimilarity);
			transparencyOpacity = parseFloat(defaultPreviewSettings.defaults.transparencyOpacity);
			colorHex = defaultPreviewSettings.defaults.color;

			if(currentRec){
				currentRec.colorHex = defaultPreviewSettings.defaults.color;
				currentRec.transparencySimilarity = parseFloat(defaultPreviewSettings.defaults.transparencySimilarity);
				currentRec.transparencyOpacity = parseFloat(defaultPreviewSettings.defaults.transparencyOpacity);
			}
			$("#opacity-value").text((transparencyOpacity * 100).toFixed(0)+"%")
			$("#similarity-value").text((transparencySimilarity * 100).toFixed(0)+"%")

			$("#similarity-range-slider").val((transparencySimilarity * 100).toFixed(0));
			$("#opacity-range-slider").val((transparencyOpacity * 100).toFixed(0));
			//debugger;
		}
		isHexOk();
	}else{
		swal.fire({
			type: "info",
			title : "No defaults saved!",
			toast : true,
			position  : "top-right"
		});
	}
}
function saveAsDefault(){
	// save 
	let color = $("#hex-color").val();
	// transparencySimilarity,transparencyOpacity
	
	var transparencySettings = {
		"defaults": {
			color: color,
			transparencySimilarity : transparencySimilarity,
			transparencyOpacity : transparencyOpacity
		}
	};
	if(fs.existsSync(documentsFolderPath+'\\transparency-defaults.json')){
		fs.truncateSync(documentsFolderPath+'\\transparency-defaults.json');
	}
	fs.appendFileSync(documentsFolderPath+'\\transparency-defaults.json',JSON.stringify(transparencySettings),'utf-8');
	swal.fire({
		toast : true,
		position : "top-right",
		text: "transparency settings saved!",
		duration : 4000
	});
}
function canvasSelected(canvas){
	let type = canvas.getAttribute("type");
	let subtype = canvas.getAttribute("subtype")
	if(type == "screen" || subtype == "screen"){
		$("#btn-crop").removeClass("disabled");
		selectedCanvas = canvas;
	}else{	
		$("#btn-crop").addClass("disabled");
	}
	console.log(canvas,type);
	// $(canvas).Jcrop();
}
// function initCropMode(elem){
// 	if(!$(elem).hasClass("disabled")){
// 		console.log(selectedCanvas);
// 		selectedCanvasHTML = $(selectedCanvas).html();
// 		$(selectedCanvas).Jcrop({
// 		},function(){
// 			jCropObj = this;
// 		});
// 	}
// 	else{
// 		console.log("please select screen")
// 	}
// }
function cancelCropMode(elem){
	console.log(elem);
	if(!$(elem).hasClass("disabled")){
		console.log(jCropObj);
		// jCropObj.destroy();
		if ($(selectedCanvas).data('Jcrop')) {
		   $(selectedCanvas).data('Jcrop').destroy();
		}
		console.log("selectedCanvasHTML : ",selectedCanvasHTML);
		// $(selectedCanvas).Jcrop();

	}
	else{
		console.log("please select crop")
	}
}
function openCropMode(){
	saveSettings();
	// TODO - 1. disable the left nav
	$(".side-nav").addClass("disabled");
	$(".preview-mode-wrap").hide();
	$(".crop-mode-wrap").show();

	console.log(backImgPath);
	if(config.backImgPath!="false"){
		let preview = $('.crop-mode-preview').get(0);
		preview.style.backgroundImage = 'url('+(backImgPath.replace(/ /g,"%20"))+')';
		preview.style.backgroundRepeat = 'no-repeat';
		preview.style.backgroundSize = '100% 100%';
	}
	// TODO - 2. append empty div
	addCanvasToCrop();
	console.log(currentRec);
}
function closeCropMode(){
	// 1. disable the left nav
	$(".side-nav").removeClass("disabled");
	$(".preview-mode-wrap").show();
	$(".crop-mode-wrap").hide();
	$("#canvas-crop-wrap").remove();
}
function addCanvasToCrop(){
	console.log(currentCropCanvas);
	let oldWidth = $(currentCropCanvas).width();
	let oldHeight = $(currentCropCanvas).height();
	let oldLeft= $(currentCropCanvas).css("left");
	let oldTop = $(currentCropCanvas).css("top");

	let aCanvas = newCanvas;
	aCanvas = aCanvas.replace(/canvas-id-here/g,'canvas-crop');
	
	$('.crop-mode-preview').empty();
	$('.crop-mode-preview').append(aCanvas);
	let camCanvas = $('#canvas-crop-wrap').get(0);
	// camCanvas.setAttribute('width',$('#preview').width() - 100); 
	// camCanvas.setAttribute('height',$('#preview').height() - 50);
	$(camCanvas).show();
	$(camCanvas).width(oldWidth);
	$(camCanvas).height(oldHeight);
	$(camCanvas).css("left",oldLeft);
	$(camCanvas).css("top",oldTop);
	$(camCanvas).css("background","red");

	// ? here we decide which video to load in canvas
	// TODO - 3. draw on canvas
	let videoId = null;
	if(currentRec.type == "screen"){
		videoId = "video-camera-"+currentFinalMergerIndex;
	}else if(currentRec.type == "both"){
		videoId = "video-screen-"+currentFinalMergerIndex;
	}
	let video = $("#"+videoId).get(0);
	console.log(video,camCanvas);
	let canvas = $("#canvas-crop").get(0);
	$(canvas).width(oldWidth);
	$(canvas).height(oldHeight);
	$(canvas).css("left",oldLeft);
	$(canvas).css("top",oldTop);

	$(canvas).attr("width",oldWidth);
	$(canvas).attr("height",oldHeight);

	getVideoFrames(video,canvas);

	$(canvas).Jcrop({
		onSelect : function(){
			$(".preview-btn").removeClass("disabled");
		}
	},function(){
		jCropObj = this;
	});
}
function confirmCrop(){
	console.log("crop values: ",jCropObj.tellSelect());
	// 2. set fullscreemode attribute to false of actual canvas
	let canvasId = "canvas-";
	if(currentRec.type == "screen"){
		canvasId += "camera-"+currentFinalMergerIndex;
	}else if(currentRec.type == "both"){
		canvasId += "screen-"+currentFinalMergerIndex;
	}
	let canvas = $("#"+canvasId).get(0);
	let videoId = null;
	if(currentRec.type == "screen"){
		videoId = "video-camera-"+currentFinalMergerIndex;
	}else if(currentRec.type == "both"){
		videoId = "video-screen-"+currentFinalMergerIndex;
	}
	let video = $("#"+videoId).get(0);
	let cropCoords = getCropCoords(canvas,video);
	console.log(cropCoords);

	// debugger

	console.log(canvas);
	canvas.setAttribute("windowCoords",JSON.stringify(cropCoords.windowCoords));
	currentRec.windowCoords = cropCoords.windowCoords;
	canvas.setAttribute("fullscreenmode","false");
	
	closeCropMode();
	getVideoFrames(video,canvas);
	console.log(currentRec);
	setNewDimensions(cropCoords.outputCoords);
}
function setNewDimensions(newDimensions){
	let keyPrefix = "";
	if(currentRec.type == "screen"){
		keyPrefix = "c";
	}else if(currentRec.type == "both"){
		keyPrefix = "s";
	}
	currentRec[keyPrefix+"width"] = newDimensions.width;
	currentRec[keyPrefix+"height"] = newDimensions.height;
	currentRec[keyPrefix+"top"] = newDimensions.top;
	currentRec[keyPrefix+"left"] = newDimensions.left;

	currentRec.fullScreenMode = false;
}
function getCropCoords(canvasToCrop,video){
	let selection = jCropObj.tellSelect();
	let background = {
        top: 0,
        left: 0,
        width: $(canvasToCrop).width(),
        height: $(canvasToCrop).height(),
    }
    console.log(background);
	// use getCanvasDimenstion for each width,height,left and top of selection
	// while cropping crop values for canvas should be relative to original resolution of screen;
	// but windowCoords should be relative to outputResolution selected in settings
	let forOutputWidth = getCanvasDimension('width',selection.w,background);
    let forOutputHeight = getCanvasDimension('height',selection.h,background);
    let forOutputLeft = getCanvasDimension('left',selection.x,background);
    let forOutputTop = getCanvasDimension('top',selection.y,background);

    let originalVideoRes = {
    	width: video.videoWidth,
    	height: video.videoHeight
    };
    let forCanvasWidth = getCanvasDimension('width',selection.w,background,true,originalVideoRes);
    let forCanvasHeight = getCanvasDimension('height',selection.h,background,true,originalVideoRes);
    let forCanvasLeft = getCanvasDimension('left',selection.x,background,true,originalVideoRes);
    let forCanvasTop = getCanvasDimension('top',selection.y,background,true,originalVideoRes);

    // debugger
    let ret = {
    	windowCoords  : {
	    	width : forCanvasWidth,
	    	height : forCanvasHeight,
	    	screenX : forCanvasLeft,
	    	screenY : forCanvasTop
	    },
	    outputCoords : {
	    	width : forOutputWidth,
	    	height : forOutputHeight,
	    	left : forOutputLeft,
	    	top : forOutputTop
	    }
    };
    return  ret;
    // console.log("cropped area : ",width,height,left,top);
}
function backToHome(){
	win.unmaximize();
	win.setResizable(true);
	win.setSize(800,600);
	win.loadFile("index.html");
}

function handleAutoUpdateEvent(param){
		console.log(param);
		param = JSON.parse(param);
		if(param.event == "update-available"){
			// show new icon at titlebar
			$(".new-image").show();
			$(".new-image").attr("src","assets/images/new.png");
		}else if(param.event == "downloading-update"){
			$(".new-image,.install-update").hide();
			if($(".infinite-loader").hasClass("inline-block")){

			}else{
				$(".infinite-loader").show();
			}
			$(".infinite-loader").addClass("inline-block");
			$(".infinite-loader").text((param.ev.percent).toFixed(0) + "%");
			// $(".new-img").attr("src","assets/images/new.png");
  			isUpdateDownloading = true;
			console.log("downloading update: ",param);
		}else if(param.event == "update-downloaded"){
			isUpdateDownloading = false;
			$(".install-update").show();
			$(".infinite-loader").hide();
			$(".new-image").hide();
		}
	}
	function onUpdateAvailableClick(){
		swal.fire({
			type : "info",
			title : "New version of HiFi Recorder is available",
			text : "Download Update ?",
			showCancelButton : true,
			cancelButtonText : "Later",
			showConfirmButton : true,
			confirmButtonText : "Download Now"
		}).then((res)=>{
			if(res.value){
				ipcRenderer.send("auto-updater",{event : "download-update-now"});
			}
		});	
	}
	function onUpdateDownloadClick(){
		swal.fire({
			type : "info",
			title : "New version of HiFi Recorder has been Downloaded",
			text : "Install Update ?",
			showCancelButton : true,
			cancelButtonText : "Later",
			showConfirmButton : true,
			confirmButtonText : "Install Now"
		}).then((res)=>{
			if(res.value){
				ipcRenderer.send("auto-updater",{event : "install-update-now"});
			}
		});
	}
	function changeLogoImg(){
		let logo = dialog.showOpenDialog(win,{
			filters: [
				{ name: 'Images', extensions: ['jpg', 'png'] },
			],
			properties : ['openFile']
		});
		console.log(logoImgPath);
		if(logo){
			removeLogoImg();
			logoImgPath = logo[0].replace(/\\/g,'\\\\');
			// $("#dummy-back-img").attr('src',logoImgPath);
		
			config.logoImgPath = logoImgPath;
			// debugger;
			const logoCanvas = `
				<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="`+logoImgPath+`" class='logo-canvas'></div>
			`;
			$("#preview").append(logoCanvas).ready(function(){
				$(".draggable").draggable({
			        cursor: "crosshair",
			        containment: "#preview",
			        snap : "#preview",
			        snapMode : "inner",
			        snapTolerance : 1
			    });
			    $(".resizable").resizable({
			        containment: "#preview",
			        handles: 'e,w,n,s,se,nw,ne,sw',
			    });
			});
		}
	}
	function checkAutoUpdaterStatus(){
		ipcRenderer.send("request-auto-updater-status");
	}
	
	
	function preparePreviewCommand(config,currentRec,replaceBackSlashFlag,basepath){
		console.log(currentRec);
		var previewcc = require('bindings')('previewprocess');
		if(!currentRec.fullScreenMode){
			currentRec.fullScreenMode = false;
		}else{
			currentRec.fullScreenMode = true;
		}
		console.log(config,currentRec);
		console.log(replaceBackSlashFlag)
		if(replaceBackSlashFlag){
			config.backImgPath = config.backImgPath.replace(/\\/g,"\\\\");
		}
		var previewresult=previewcc.previewprocess(config,currentRec,function(msg){
			console.log(msg);
			},basepath);
		
		console.log("result from preview is : ",previewresult);
		// console.log(config,currentRec);
		// let cmd = getPreviewCmd(config,currentRec);
		// runPreviewCmd(cmd);
	}
	function autoResizeCurrentCanvas(){	
		let cameraCanvas = $('#canvas-camera-'+currentFinalMergerIndex+'-wrap').get(0);
		setDimensionsInAspectRatio(cameraCanvas);
		if(currentRec.type == "both"){
			let screenCanvas = $('#canvas-screen-'+currentFinalMergerIndex+'-wrap').get(0);
			setDimensionsInAspectRatio(screenCanvas);
		}
	}
	function setDimensionsInAspectRatio(selectedCanvas) {
		// console.log("selected canvas : ",selectedCanvas);
	
		let width = $(selectedCanvas).width();
		let height = $(selectedCanvas).height();
		console.log("width : ", width);
		console.log("height : ", height);
	
		// TODO - reduce width to set in 16:9
		// TODO - 1. set width in multiple of 16
		let newWidth = width, newHeight = height;
		if (width % 16 != 0) {
			newWidth = width - (16 - (width % 16));
			// TODO - now set height as per newWidth
			newHeight = parseInt((newWidth * 9) / 16);
			height = newHeight;
		}
		if (height % 9 != 0) {
			newHeight = height - (9 - (height % 9));
		}

		$(selectedCanvas).width(newWidth);
		$(selectedCanvas).height(newHeight);

		console.log("new width : ", newWidth);
		console.log("new height : ", newHeight);
		$(selectedCanvas).children("canvas").css("width", "100%");
		$(selectedCanvas).children("canvas").css("height", "100%");
	
		snapIfOutofBounds(selectedCanvas);
	}
	function snapIfOutofBounds(selectedCanvas){
		const top = parseInt($(selectedCanvas).css("top").replace("px",""));
		const height = parseInt($(selectedCanvas).css("height").replace("px",""));
	
		const backgroundHeight = $("#preview").height();
	
		const condition = (top + height > backgroundHeight);
		if(condition){
			// TODO - 1. calculate how much portion is out of the frame
			// TODO - 2. substract out-portion from current top to move it top
			const newTop = (top + height) - backgroundHeight;
			$(selectedCanvas).css("top",(top - newTop) + "px");
		}
		// debugger;
		console.log("top : ",top);
		console.log("height : ",height);
		console.log("background : ",backgroundHeight);
	}
	