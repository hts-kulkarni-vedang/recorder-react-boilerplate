const { ipcRenderer, shell, screen } = require("electron");
const { dialog } = require("electron").remote;
const electron = require("electron");
const fs = require("fs");
const outputRes = { width: 1920, height: 1080 };
const paddingColor = "#ffffff"; //def for white
// const previewJs = require('./preview.js');
const url = require('url');
const path = require('path');
let os = require("os");
const documentsFolderPath = os.homedir + '/Documents/HiFi_Recorder Files';
const env = require("../environment");
let win = null;
let config = null;
let currentRec = null;
let currentFinalMergerIndex = null;
let backImgPath = "false";
let logoImgPath = "false";
let paramObj = {};
let child_process = require('child_process');
let swal = require("sweetalert2");
const $ = require("../assets/js/circle-progress");
let selectedCanvas = null;
let currentCropCanvas = null;
let replaceBackSlashFlag = true;
let transparencyOpacity = 0;
let transparencySimilarity = 0.1;
let colorHex = 'C1BBBB';
let jCropObj = null;
let selectedCanvasHTML = "";
let settings = {};
let currentPlaybackPaused = true;
const aRec = `
	<div class="rec-item-wrapper" final-merger-index="final-merger-index-here">
		<button id="id-here" class="rec-item" final-merger-index="final-merger-index-here"><span class="rec-item-sr-no"> sr-no-here </span> - text-here</button>
		<img title="Settings" class="rec-item-settings-icon" src="../assets/images/settings-grey.png" final-merger-index="final-merger-index-here"/>
		<img title="Drag Up or Down to change position" class="rec-item-reorder-icon" src="../assets/images/double-arrows.png" final-merger-index="final-merger-index-here"/>
	</div>
`;
const newCanvas = `
	<div class='draggable resizable canvas-wrap' id='canvas-id-here-wrap' mergerindex='merger-index-here' type='rec-type-here' subtype="type-here"><video id='canvas-id-here' class='preview-area-video' rectype='rec-type-here' type="type-here"></video></div>
`;
const scrollingTextWrapperElement = `
<div class="scrolling-text-wrapper" id="scrolling-text-wrapper-id-here" mergerindex='merger-index-here'>
	<div class="scrolling-text-content scrolling-text-canvas-content" mergerindex='merger-index-here'>
		<div class="fixed-text-wrap inline-block" mergerindex='merger-index-here'></div><div class="scrolling-text-wrap inline-block" mergerindex='merger-index-here'></div>
	</div>
</div>
`;
const app = require("electron").remote.app;
let generatingWaveformsCounter = 0;
// ? NOTE -> values can be
// ? a) 'create-new-recording' , b) 'add-to-current-recording'
let selectFilesToReprocessMode = 'create-new-recording';
let totalRecordingDuration = 0;
let autotickindex=null;
let canvasResizeStartCoords = {};

$(document).ready(function () {
	initWindow();
	const mode = sessionStorage.getItem('sessionFor');
	if (mode === 'process-config-folder') {
		loadDefaultTransparency();
		// initPreview(false);
		let param = {
			configFolderPath: (sessionStorage.getItem("configFolderPath"))
		}
		initPreview(param);
	} else if (mode === 'reprocess-video') {
		$('.reprocess-videos-section-wrapper').show();
		$('.content').hide();
  }
  setCurrentTemplateName();

});
$(document).on('click', '#close', function () {
	sessionStorage.removeItem('currentConfigObject');
	win.close();
});
$(document).on('click', '#minimize', function () {
	win.minimize();
});
$(document).on("click", ".rec-item-settings-icon", function () {
	openRecItemSettingsModal(this);
});
$(document).on("click", ".close-rec-item-settings-modal", function () {
	closeRecItemSettingsModal(this);
});
$(document).on('click', '.rec-item', function () {
	handleRecItemClicked(this);
});
$(document).on('click', '#btn-process-now', function () {
	processNow();
});
$(document).on('click', '#btn-show-preview', function () {
	showPreview();
});
$(document).on('click', '#btn-change-back-img', function () {
	changeBackImg();
});
$(document).on('click', '#btn-remove-back-img', function () {
	removeBackImg();
});
$(document).on('click', '#btn-save-settings', function () {
	saveSettings();
});
$(document).on('click', '.transparency-check', function () {
	toggleTransparency(this);
});
$(document).on('click', '.scrolling-text-check', function () {
	toggleScrollingText(this.checked);

});
$(document).on('click', '.add-remove-logo-check', function () {
	toggleLogoVisibility(this.checked);
});
$(document).on('click', '.adjust-brightness-check', function () {
	toggleAdjustBrightness(this.checked);
});
$("#similarity-value").on('input', function () {
	handleSimilarityChange(this, changeFromInput = true);
});
$("#opacity-value").on('input', function () {
	handleOpacityChange(this, changeFromInput = true);
});
$("#similarity-range-slider").on('input change', function () {
	handleSimilarityChange(this, changeFromInput = false);
})
$("#opacity-range-slider").on('input change', function () {
	handleOpacityChange(this, changeFromInput = false);
});
$('#hex-color').on('input change', function (ev) {
	let val = $(this).val();
	val = val.toUpperCase().replace(/ /g, '');
	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
	colorHex = val;
	if (isOk) {
		$(this).removeClass('error-input');
		$(this).addClass('correct-input');
		$(".hex-color-correct-incorrect").text("");
		config.colorHex = val.replace("#", "");
		currentRec.colorHex = val.replace("#", "");
	} else {
		$(this).removeClass('correct-input');
		$(this).addClass('error-input');
		$(".hex-color-correct-incorrect").text("Incorrect Color !");
		currentRec.colorHex = val.replace("#", "");
	}
});
function isHexOk() {
	let val = $('#hex-color').val();
	val = val.toUpperCase();
	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
	colorHex = val;
	if (isOk) {
		$('#hex-color').removeClass('error-input');
		$('#hex-color').addClass('correct-input');
	} else {
		$('#hex-color').removeClass('correct-input');
		$('#hex-color').addClass('error-input');
	}
}
$(document).on('click', '.load-default', function () {
	loadDefaultTransparency(this);
});
$(document).on('click', '.save-as-default', function () {
	saveAsDefault(this);
});
$(document).on('click', '.canvas-wrap', function () {
	canvasSelected(this);
});
$(document).on('click', '#btn-crop', function () {
	initCropMode(this);
});
$(document).on('click', '#btn-release-crop', function () {
	cancelCropMode(this);
});
$(document).on('click', '.btn-confirm-crop', function () {
	confirmCrop(this);
});
$(document).on('click', '#btn-open-crop-mode', function () {
	openCropMode();
});
$(document).on('click', '#btn-open-camera-crop-mode', function () {
	openCameraCropMode();
});
$(document).on('click', '#btn-close-crop-mode,.cancel-crop-btn', function () {
	closeCropMode();
});
$(document).on('click', '.back-btn', function () {
	backToHome();
});
$(document).on('click', '#btn-add-logo', function () {
	changeLogoImg();
});
$(document).on('click', '#btn-change-template', function () {
	changeTemplate();
});
$(document).on('click', '#btn-remove-logo', function () {
	removeLogoImg();
});
$(document).on("click", ".new-image", function () {
	onUpdateAvailableClick();
});
$(document).on("click", ".install-update", function () {
	onUpdateDownloadClick();
});
$(document).on("click", "#btn-auto-resize", function () {
	autoResizeCurrentCanvas();
});
$(document).on("click", ".btn-view-camera-file", function () {
	viewFile(this);
});
$(document).on("click", ".btn-view-screen-file", function () {
	viewFile(this);
});
$(document).on("click", ".btn-change-file", function () {
	changeCurrentRecItemFile(this);
});
$(document).on("click", ".btn-rec-item-settings", function () {
	updateCurrentRecItem(this);
});
$(document).on("click", ".change-rec-drop-item", function () {
	changeCurrentRecItemMode(this);
});
$(document).on("click", ".waveform-img", function (event) {
	waveformImageClicked(event, this);
});
$(document).on("input", ".time-input", function (event) {
	timeInputChanged(this);
});
$(document).on("click", ".select-reprocess-file-btn", function (event) {
	selectFilesToReprocessMode = 'create-new-recording';
	selectFilesToReprocess(this);
});
$(document).on("click", "#btn-remove-green-screen", function (event) {
	openPropertiesPanel('green-screen');
});
$(document).on("click", "#btn-add-remove-logo", function (event) {
	openPropertiesPanel('logo-options');
	toggleLogoCanvas();
	toggleLogoCheck();
});
$(document).on("click", ".add-rec-item-btn", function (event) {
	selectFilesToReprocessMode = 'add-to-current-recording';
	selectFilesToReprocess(this);
});
$(document).on("click", ".remove-rec-item-btn", function (event) {
	removeSelectedRecItem();
});
$(document).on("click", "#btn-scrolling-text", function (event) {
	openPropertiesPanel('scrolling-text');
});
$(document).on("click", "#btn-adjust-brightness", function (event) {
	openPropertiesPanel('adjust-brightness');
});
$(document).on("click", ".show-scroll-text-preview", function (event) {
	showScrollTextPreview();
});
$(document).on("input change", "#play-time-range-slider", function (event) {
	updateCurrentPlayingTime(this);
});
$(document).on("mousedown", "#play-time-range-slider", function (event) {
  playTimeRangeSliderMouseDown();
});
$(document).on("mouseup", "#play-time-range-slider", function (event) {
  playTimeRangeSliderMouseUp();
});
$(document).on("click", ".text-color-img,.back-color-img ", function (event) {
	openScrollColorPicker(this);
});
$(document).on("input change", ".scrolling-text-start-time-sec", function (event) {
	scrollTextTimeSecChanged(this);
});
$(document).on("input change", ".scrolling-text-running-time-sec", function (event) {
	scrollTextTimeSecChanged(this);
});
$(document).on("click", "#btn-save-project", function (event) {
	saveProject(showAlert = true);
});
$(document).on("change input",
	`#scrolling-text-font-size-input,
	#scrolling-text-fixed-text-input,
	#scrolling-text-scrolling-text-input,
	#fixed-text-color,
	#fixed-text-back-color,
	#scrolling-text-color,
	#scrolling-text-back-color`
	, function (event) {
		scrollingTextValuesUpdated();
	});
$(document).on("focusout",
	`#scrolling-text-font-size-input,
	#scrolling-text-fixed-text-input,
	#scrolling-text-scrolling-text-input,
	#fixed-text-color,
	#fixed-text-back-color,
	#scrolling-text-color,
	#scrolling-text-back-color`
	, function (event) {
		trimScrollingTextInputs();
		scrollingTextValuesUpdated();
	});

$(document).on("change input", `#adjust-brightness-gamma-input,
#adjust-brightness-saturation-input,
#adjust-brightness-brightness-input,
#adjust-brightness-contrast-input`, function (event) {
	adjustBrightnessValuesUpdated();
});



$(document).on("click", ".btn-reset-adjust-brightness-values", function (event) {
	resetCurrentBrightnessValues();
});
$("#gamma-slider").on('input change', function () {
	handleAdjustBrightnessChanged('gamma', this, changeFromInput = false);
});
$("#saturation-slider").on('input change', function () {
	handleAdjustBrightnessChanged('saturation', this, changeFromInput = false);
});
$("#brightness-slider").on('input change', function () {
	handleAdjustBrightnessChanged('brightness', this, changeFromInput = false);
});
$("#contrast-slider").on('input change', function () {
	handleAdjustBrightnessChanged('contrast', this, changeFromInput = false);
});
$(document).on("click",".play-btn", function () {
  playPauseCurrentCanvasVideosWrapper(this);
});
$(document).on("click",".stop-btn", function () {
  currentPlaybackPaused = true;
	stopCurrentCanvasVideos();
});
$(document).on("focus",".play-time-current-input", function () {
  playTimeInputFocusedIn();
});
$(document).on("focusout",".play-time-current-input", function () {
  playTimeInputFocusedOut();
});
$(document).on("click",".fullscreen-canvas-btn", function () {
  showCanvasInFullscreen(this);
});
function initWindow() {
	win = electron.remote.getCurrentWindow();
	win.setResizable(true);
	win.maximize();
	setTimeout(() => {
		setScreenContentDimensions(win);
		replaceBackSlashFlag = true;
		let prevWidth = $('#preview').width();
		let prevHeight = $('#preview').height();
		$('#canvas-back-img').attr('width', prevWidth - 100);
		$('#canvas-back-img').attr('height', prevHeight - 50);
	}, 2000);
}
function initPreview(param) {
	paramObj = param;
	let windowParam = sessionStorage.getItem("windowParam");
	if (windowParam) {
		config = JSON.parse(windowParam).config;
		let tmp = sessionStorage.getItem('currentConfigObject');
		if (tmp) {
			tmp = JSON.parse(tmp);
			if (tmp.configFolderPath === config.configFolderPath) {
				// ? override config from session -> config from file
				config = tmp;
			}
		}
	} else {
		config = JSON.parse(fs.readFileSync(param.configFolderPath + '\\settings.config', 'utf8'));
		let tmp = sessionStorage.getItem('currentConfigObject');
		if (tmp) {
			tmp = JSON.parse(tmp);
			if (tmp.configFolderPath === config.configFolderPath) {
				// ? override config from session -> config from file
				config = tmp;
			}
		}
	}
	sessionStorage.removeItem("windowParam");
	config.outputRes = outputRes; // remove if unused
	config.paddingColor = paddingColor; // remove if unused
	config.configFolderPath = param.configFolderPath.replace(/\\/g, '\\\\\\\\');
	changeFinalMergerPaths();
	showLoader();
	initVideos().then(() => {
		showRecordings();
		handleBackImgPathFromSettings(settings);
		handleLogoImagePathFromSettings(settings);
		checkIfAllFilesExist().then((allOk) => {
			if(allOk === true) {
				initVideoWaveforms();
				initVideoScrollTexts();
				hideLoader();
			}
		});
		hideLoader();
	}).catch((err) => {
		hideLoader();
		swal.fire({
			type: 'error',
			icon: 'error',
			title: 'something went wrong',
			text: 'Could not load recording, please try again'
		});
		throw err;
	});

}
function changeFinalMergerPaths() {
	// let folder = optname.split(".mp4")[0]
	for (let i = 0; i < config.finalMerger.length; i++) {
		let obj = config.finalMerger[i];
		if (obj.type == "camera") {
			//obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			//obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length - 1]
			obj.cpath = 'camera' + obj.cpath.split('camera')[obj.cpath.split('camera').length - 1];
		}
		else if (obj.type == "screen") {
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')*/
			// obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.cpath = 'screen' + obj.cpath.split('screen')[obj.cpath.split('screen').length - 1];
		}
		else if (obj.type == "both") {
			/*obj.cpath = obj.cpath.replace("./assets/",folder+'/')
			obj.spath = obj.spath.replace("./assets/",folder+'/')*/
			//obj.cpath = obj.cpath.split("/assets/")[obj.cpath.split("/assets/").length -1]
			obj.cpath = 'camera' + obj.cpath.split('camera')[obj.cpath.split('camera').length - 1];
			obj.spath = 'screen' + obj.spath.split('screen')[obj.spath.split('screen').length - 1];
			//obj.spath = obj.spath.split("/assets/")[obj.spath.split("/assets/").length -1]
		}
	}
}
function showRecordings() {
	$('.recording-list-items').empty();
	let recordingsLoaded = 0;
	if(config.finalMerger.length == 0) {
		swal.fire({
			icon: 'error',
			type: 'error',
			title: 'Something went wrong',
			text: 'Could not load recordings , please try again !'
		});
	}
	for (let i = 0; i < config.finalMerger.length; i++) {
		let rec = aRec;
		rec = rec.replace(/id-here/g, "rec-item-" + i);
		rec = rec.replace(/sr-no-here/g, (i + 1));
		rec = rec.replace(/text-here/g, config.finalMerger[i].type);
		rec = rec.replace(/final-merger-index-here/g, i);
		$('.recording-list-items').append(rec).ready(function () {
			recordingsLoaded++;
			if (config.finalMerger.length > 0 && recordingsLoaded === 1) {
				const elem = $('.rec-item').get(0);
				handleRecItemClicked(elem);
			}
			if (recordingsLoaded === config.finalMerger.length) {
				addSortableToRecList();
			}
		});
	}
	$('.total-recording-count').text(config.finalMerger.length);
}
function showInCanvas(index) {
	currentRec = config.finalMerger[index];
	$('.canvas-wrap').hide();
	$('.canvas-wrap').removeClass('inline-block');
	$('#canvas-camera-' + index + '-wrap').show();
	$('#canvas-camera-' + index + '-wrap').addClass('inline-block');
	if (currentRec.type == 'both') {
		$('#canvas-screen-' + index + '-wrap').show();
		$('#canvas-screen-' + index + '-wrap').addClass('inline-block');
	}
}
function initVideos() {
	return new Promise(async (resolve, reject) => {
		if (fs.existsSync(env.settingsFilePath)) {
			loadSettings();
		}
		for (let i = 0; i < config.finalMerger.length; i++) {
			let item = config.finalMerger[i];
			if (item.type == 'both') {
				// let screenVideo = addVideo(index = i, type = 'screen', src = paramObj.configFolderPath + '\\' + item.spath);
				let screenCanvas = addCanvas(index = i, type = 'screen', width = 0, height = 0, recType = item.type, src = paramObj.configFolderPath + '\\' + item.spath);//remove width and height if unused
				screenCanvas.setAttribute('windowCoords', JSON.stringify(item.windowCoords));
				if (item.screenWindowCoords) {
					screenCanvas.setAttribute('windowCoords', JSON.stringify(item.screenWindowCoords));
				}
				if (item.fullScreenMode) {
					screenCanvas.setAttribute('fullScreenMode', "true");
				} else {
					screenCanvas.setAttribute('fullScreenMode', "false");
				}
				resizeCanvasBySettings();
				// await getVideoFrames(screenVideo, screenCanvas);
			}
			// let camVideo = addVideo(index = i, type = 'camera', src = paramObj.configFolderPath + '\\' + item.cpath);
			let camCanvas = addCanvas(index = i, type = 'camera', width = 0, height = 0, recType = item.type,src = paramObj.configFolderPath + '\\' + item.cpath);
			// ? allows crop of camera
			camCanvas.setAttribute('windowCoords', JSON.stringify(item.windowCoords));
			if (item.cameraWindowCoords) {
				camCanvas.setAttribute('windowCoords', JSON.stringify(item.cameraWindowCoords));
			}
			// if (item.windowCoords && item.type == 'screen') {
			// }
			if (item.fullScreenMode) {
				camCanvas.setAttribute('fullScreenMode', "true");
			} else {
				camCanvas.setAttribute('fullScreenMode', "false");
			}
			resizeCanvasBySettings();
			// await getVideoFrames(camVideo, camCanvas);
		}
		resolve();
	});

}
// function getVideoFrames(video, canvas) {
// 	return new Promise((resolve, reject) => {
// 		let promise = video.play();
// 		if (promise !== undefined) {
// 			promise.then(function () {
// 				// start Automatic playback;
// 				(function loop() {
// 					if (!video.paused && !video.ended) {
// 						drawOnCanvas(video, canvas)
// 						setTimeout(loop, 1000 / 30); // drawing at 30fps
// 					}
// 				})();
// 				setTimeout(function () {
// 					video.currentTime = 0
// 					video.pause();
// 					resolve();
// 				}, 1000);
// 			}).catch(function (error) {
// 				reject()
// 			});
// 		} else {
// 			reject();
// 		}
// 	});
// }
function processNow() {
	let areAllRecsValid = true;
	for (let index = 0; index < config.finalMerger.length; index++) {
		const recItem = config.finalMerger[index];
		const isValid = validatePreview(recItem);
		if (!isValid.valid) {
			swal.fire({
				type: "info",
				title: isValid.title + ' in recording #' + (index + 1),
				text: isValid.text
			});
			hideLoader();
			areAllRecsValid = false;
			break;
		}
	}
	if (areAllRecsValid === false) {

	} else {
		if (checkCameraHasBackground() == true) {
			saveSettings();
			setFinalMergerSequence();
			checkIfAllFilesExist().then((allOk) => {
				if (allOk === true) {
					checkFFMPEG().then((res) => {
						// debugger
						config.backImgPath = config.backImgPath.replace(/%20/g, " ");
						// NOTE PATCH -> ffmpeg reports %20 in path ; thus no such directory
						config.logoImgPath = config.logoImgPath.replace(/%20/g, " ");
						config.configFolderPath = config.configFolderPath.replace(/\\\\\\\\/g, '\\\\');
						saveProject();
						let obj = {
							process: 'later',
							configFolderPath: config.configFolderPath.replace('\\\\\\\\', '\\\\'),
							mode: 'process',
							sourceFile: '../process-window/index.html'
						};
						localStorage.setItem('configFromPreview', JSON.stringify(config));
						//config = JSON.stringify(config);
						// win.unmaximize();
						// win.setSize(100,100);
						win.loadURL(url.format({
							pathname: path.join(__dirname, obj.sourceFile),
							protocol: 'file:',
							slashes: true
						}));
					}).catch((err) => {
						swal.fire({
							type: 'error',
							title: 'Installation Error',
							text: 'One or more dependencies were not found!'
						}).then((res) => {
							shell.openExternal("https://ffmpeg.org/");
						});
					});
				}
			});
		} else if (currentRec.type == "camera") {
			swal.fire({
				type: "info",
				title: "Background image not set",
				text: "Please select background image for using transparency in camera only mode"
			});
		}
	}
}
function checkCameraHasBackground() {
	let flag = true;
	for (let i = 0; i < config.finalMerger.length; i++) {
		let iRec = config.finalMerger[i];
		if (iRec.type == "camera" && iRec.transparencyCheck == true && config.backImgPath == "false") {
			flag = false;
		}
	}
	return flag;
}
function showPreview() {
	showLoader();
	if (currentRec) {
		const isValid = validatePreview(currentRec);
		if (!isValid.valid) {
			swal.fire({
				type: "info",
				title: isValid.title,
				text: isValid.text
			});
			hideLoader();
		} else {
			saveSettings();
			checkIfAllFilesExist().then((allOk) => {
				if (allOk === true) {
					checkFFMPEG().then((res) => {
						// TODO - if config.backImgpah has %20 then change it to spaces [c++ requirement]
						config.backImgPath = config.backImgPath.replace(/%20/g, " ");
						// TODO - if config.logoImgPath has %20 then change it to spaces [c++ requirement]
						config.logoImgPath = config.logoImgPath.replace(/%20/g, " ")
						let basepath = app.getAppPath('exe');
						basepath = basepath.replace("app.asar", "")
						basepath = basepath.replace(/\\/g, "\\\\");
						basepath += "\\";
						const output = preparePreviewCommand(config, currentRec, replaceBackSlashFlag, basepath);
						hideLoader();
					}).catch((err) => {
						swal.fire({
							type: 'error',
							title: "Installation Error",
							text: 'Some of dependencies were not found on this PC!'
						}).then((res) => {
							// shell.openExternal("https://ffmpeg.org/");
						});
					});
					replaceBackSlashFlag = false;
				}
			}).catch(() => {
				hideLoader();
			});
		}
	} else {
		alert('recording not selected');
		hideLoader();
	}
}
function changeBackImg() {
	backImgPath = dialog.showOpenDialog(win, {
		filters: [
			{ name: 'Images', extensions: ['jpg', 'png', 'jpeg'] },
		],
		properties: ['openFile']
	});
	if (backImgPath) {
		backImgPath = backImgPath[0].replace(/\\/g, '\\\\');
		// $("#dummy-back-img").attr('src',backImgPath);
		let preview = $('#preview').get(0);
		let tmpBack = (backImgPath.replace(/ /g, "%20"));
		tmpBack = (tmpBack.replace(/\(/g, "\\("));
		tmpBack = (tmpBack.replace(/\)/g, "\\)"));
		// ? image version is needed in case if the image is change while user is editing
		// ? if not used , then earlier version is cached in chrome
		const version = new Date().getTime();
		preview.style.backgroundImage = 'url(' + tmpBack + '?v=' + version + ')';
		preview.style.backgroundRepeat = 'no-repeat';
		preview.style.backgroundSize = '100% 100%';
		config.backImgPath = backImgPath;
		let img = document.createElement("img");
		img.src = backImgPath;
		let canvas = document.createElement('canvas');
		canvas.width = 200;
		canvas.height = 200;
	}
}
/**
* @param elem - video or audio element
* @param type	  - can be any of (camera,screen,both,background)
* @param canvas-   canvas element to draw on
*/
function drawOnCanvas(elem, canvas) {

	let ctx = canvas.getContext("2d");
	ctx.imageSmoothingEnabled = false; //for image quality
	let coords = canvas.getAttribute('windowCoords');
	let type = canvas.getAttribute('rectype');
	let subtype = canvas.getAttribute('type');
	// if()
	// if (type == "screen" || subtype == "screen") {
	//
	//
	if (coords == "null" || coords == "{}" || coords == "undefined") {
		let originalVideoRes = {
			width: elem.videoWidth,
			height: elem.videoHeight,
			screenX: 0,
			screenY: 0
		}
		canvas.setAttribute("windowCoords", JSON.stringify(originalVideoRes));
		coords = JSON.stringify(originalVideoRes);
	}
	// }
	//
	// debugger
	if (coords) {
		if (coords != "null") {
			coords = JSON.parse(coords);
			ctx.drawImage(elem, coords.screenX, coords.screenY, coords.width, coords.height, 0, 0, canvas.width, canvas.height);
			//
		} else {
			ctx.drawImage(elem, 0, 0, canvas.width, canvas.height)
			//
		}
	} else {
		ctx.drawImage(elem, 0, 0, canvas.width, canvas.height)
		//
	}
}
function videoTimeUpdate(elem) {
	//
}
function addCanvas(index, type, width, height, recType, src) { //remove width and height if unused
	let aCanvas = newCanvas;
	aCanvas = aCanvas.replace(/canvas-id-here/g, 'canvas-' + type + '-' + index + '');
	aCanvas = aCanvas.replace(/rec-type-here/g, recType);
	aCanvas = aCanvas.replace(/type-here/g, type);
	aCanvas = aCanvas.replace(/merger-index-here/g, index);
	$('#preview').append(aCanvas).ready(function () {
		// $(".resizable")
		$(".draggable").draggable({
			cursor: "crosshair",
			containment: "#preview",
			snap: "#preview",
			snapMode: "inner",
			snapTolerance: 1
		});
		$(".resizable").resizable({
			containment: "#preview",
			handles: 'e,w,n,s,se,nw,ne,sw',
			start: function(event) {
				canvasResizeStarted(event);
			},
			stop: function(event) {
				canvasResized(event);
			},
			resize: function(event) {
				canvasResized(event);
			}
    	});


		let currentRec = config.finalMerger[index];
		let camVideo = $('#canvas-' + type + '-' + index).get(0);
		camVideo.preload = 'metadata';
		camVideo.onloadedmetadata = function () {
			window.URL.revokeObjectURL(camVideo.src);
			currentRec[type + 'Height'] = camVideo.videoHeight;
			currentRec[type + 'Width'] = camVideo.videoWidth;
			currentRec['videoLoadedDuration'] = camVideo.duration;
			resizeCanvasBySettings();
		}.bind(currentRec);
		camVideo.src = src;
		camVideo.muted = true;
	});
	let camCanvas = $('#canvas-' + type + '-' + index).get(0);
	camCanvas.setAttribute('width', $('#preview').width());
  	camCanvas.setAttribute('height', $('#preview').height());

	return camCanvas;
}

function canvasResized(event) {
  
  const canvas = $(event.target).find('video').get(0);

//   // TODO - calculate width & height
     let wrapperWidth = $(event.target).width();
	 let wrapperHeight = $(event.target).height();

    const previewWidth = $("#preview").width();
  const previewHeight = $("#preview").height();

	 const xFactor = oldInnerCanvasResizeCoordinates.width / oldOuterCanvasResizeCoordinates.width;
     const yFactor = oldInnerCanvasResizeCoordinates.height / oldOuterCanvasResizeCoordinates.height;

     const videoWidth = wrapperWidth * xFactor;
     const videoHeight = wrapperHeight * yFactor;

     $(canvas).width(videoWidth);
     $(canvas).height(videoHeight);

  newOuterCanvasResizeCoordinates = {
    width: $(event.target).width(),
    height: $(event.target).height(),
    left: $(event.target).css('left').replace("px",""),
    top: $(event.target).css('top').replace("px","")
  };
  if(newOuterCanvasResizeCoordinates.left == "auto") {
    newOuterCanvasResizeCoordinates.left = 0;
  }
  if(newOuterCanvasResizeCoordinates.top == "auto") {
    newOuterCanvasResizeCoordinates.top = 0;
  }
  newOuterCanvasResizeCoordinates.left = parseInt(newOuterCanvasResizeCoordinates.left);
  newOuterCanvasResizeCoordinates.top = parseInt(newOuterCanvasResizeCoordinates.top);

//   // TODO - calculate left & top
//   // ? calculate distance
   // let distance = oldOuterCanvasResizeCoordinates.left - oldInnerCanvasResizeCoordinates.left;
   let videoLeft = (videoWidth * Math.abs(oldInnerCanvasResizeCoordinates.left)) / oldInnerCanvasResizeCoordinates.width ;
    // let videoLeft = (  newDistance - newOuterCanvasResizeCoordinates.left);

//   // ? calculate distance
//  distance = oldOuterCanvasResizeCoordinates.top - oldInnerCanvasResizeCoordinates.top;
//  newDistance = ( distance * videoHeight ) / oldInnerCanvasResizeCoordinates.height;
let videoTop = (videoHeight * Math.abs(oldInnerCanvasResizeCoordinates.top)) / oldInnerCanvasResizeCoordinates.height ;

 $(canvas).css('left','-' + videoLeft + 'px');
 $(canvas).css('top','-' + videoTop + 'px');
}
// ? writes curret settings to local config object
function saveSettings() {
	let background = {
		top: 0,
		left: 0,
		width: $("#preview").width(),
		height: $("#preview").height(),
	}
	$('.canvas-wrap').each(function (index, elem) {
		// debugger
		let width = getCanvasDimension('width', $(this).width(), background);
		let height = getCanvasDimension('height', $(this).height(), background);
		let left = getCanvasDimension('left', $(this).css('left'), background);
		let top = getCanvasDimension('top', $(this).css('top'), background);

		// let ffmpegSanitizedDimensions = sanitizeForFFMPEG(width, height, left, top);
		// width = ffmpegSanitizedDimensions.width;
		// height = ffmpegSanitizedDimensions.height;
		// left = ffmpegSanitizedDimensions.left;
		// top = ffmpegSanitizedDimensions.top;
		// debugger
		let type = this.getAttribute('type');
		let subtype = this.getAttribute('subtype');
		let mergerIndex = parseInt(this.getAttribute('mergerindex'));
		delete config.finalMerger[mergerIndex].windowCoords;
		if (type == "screen" || subtype == "screen") {
			// let windowCoords = $(this).children("video").get(0).getAttribute("windowCoords");
			// windowCoords = (windowCoords) ? JSON.parse(windowCoords) : null;
			// ? creates a issue if cropping
			// ? if again processing, after camera has been cropped,
			// ? camera in canvas has extra trasparent space on right and bottom
			// ? commenting this might help, testing now
			// config.finalMerger[mergerIndex].windowCoords = windowCoords;
		}
		if (type == 'both') {
			if (subtype == 'screen') {
				config.finalMerger[mergerIndex].swidth = width;
				config.finalMerger[mergerIndex].sheight = height;
				config.finalMerger[mergerIndex].sleft = left;
				config.finalMerger[mergerIndex].stop = top;
				// ? also save raw values for future use
				config.finalMerger[mergerIndex].swidthRaw = $(this).width();
				config.finalMerger[mergerIndex].sheightRaw = $(this).height();
				config.finalMerger[mergerIndex].sleftRaw = $(this).css('left');
				config.finalMerger[mergerIndex].stopRaw = $(this).css('top');

				// ? also save inner raw values for future use
				config.finalMerger[mergerIndex].swidthRawInner = $(this).find('video').width();
				config.finalMerger[mergerIndex].sheightRawInner = $(this).find('video').height();
				config.finalMerger[mergerIndex].sleftRawInner = $(this).find('video').css('left');
				config.finalMerger[mergerIndex].stopRawInner = $(this).find('video').css('top');
			} else if (subtype == 'camera') {
				config.finalMerger[mergerIndex].cwidth = width;
				config.finalMerger[mergerIndex].cheight = height;
				config.finalMerger[mergerIndex].cleft = left;
				config.finalMerger[mergerIndex].ctop = top;
				// ? also save raw values for future use
				config.finalMerger[mergerIndex].cwidthRaw = $(this).width();
				config.finalMerger[mergerIndex].cheightRaw = $(this).height();
				config.finalMerger[mergerIndex].cleftRaw = $(this).css('left');
				config.finalMerger[mergerIndex].ctopRaw = $(this).css('top');

				// ? also save raw values for future use
				config.finalMerger[mergerIndex].cwidthRawInner = $(this).find('video').width();
				config.finalMerger[mergerIndex].cheightRawInner = $(this).find('video').height();
				config.finalMerger[mergerIndex].cleftRawInner = $(this).find('video').css('left');
				config.finalMerger[mergerIndex].ctopRawInner = $(this).find('video').css('top');
			}
		} else {
			config.finalMerger[mergerIndex].cwidth = width;
			config.finalMerger[mergerIndex].cheight = height;
			config.finalMerger[mergerIndex].cleft = left;
			config.finalMerger[mergerIndex].ctop = top;
			// ? also save raw values for future use
			config.finalMerger[mergerIndex].cwidthRaw = $(this).width();
			config.finalMerger[mergerIndex].cheightRaw = $(this).height();
			config.finalMerger[mergerIndex].cleftRaw = $(this).css('left');
			config.finalMerger[mergerIndex].ctopRaw = $(this).css('top');
			
			// ? also save raw inner values for future use
			config.finalMerger[mergerIndex].cwidthRawInner = $(this).find('video').width();
			config.finalMerger[mergerIndex].cheightRawInner = $(this).find('video').height();
			config.finalMerger[mergerIndex].cleftRawInner = $(this).find('video').css('left');
			config.finalMerger[mergerIndex].ctopRawInner = $(this).find('video').css('top');
		}
	});
	let logoCanvas = $(".logo-canvas-wrap").get(0);
	config.logoCoords = {};
	if (logoCanvas) {
		let width = getCanvasDimension('width', $(logoCanvas).width(), background);
		let height = getCanvasDimension('height', $(logoCanvas).height(), background);
		let left = getCanvasDimension('left', $(logoCanvas).css('left'), background);
		let top = getCanvasDimension('top', $(logoCanvas).css('top'), background);
		config.logoImgPath = decodeURI(logoImgPath);
		config.logoCoords = {
			width: width,
			height: height,
			left: left,
			top: top,
			widthRaw: $(logoCanvas).width(),
			heightRaw: $(logoCanvas).height(),
			leftRaw: $(logoCanvas).css('left'),
			topRaw: $(logoCanvas).css('top'),
		};
	} else {
		config.logoImgPath = "false"; //for cpp
	}
	config.transparencyOpacity = transparencyOpacity;
	config.transparencySimilarity = transparencySimilarity;
	config.colorHex = colorHex;
	// if(config.backImgPath)
	// 	config.backImgPath = backImgPath.replace(/\\/g,'\\\\');
	//
	for (let i = 0; i < config.finalMerger.length; i++) {
		// ? handle transparency check
		if (config.finalMerger[i].transparencyCheck) {
			if (config.finalMerger[i].transparencyCheck == true) {
				if (config.finalMerger[i].colorHex) {
					if (config.finalMerger[i].colorHex[0] == "#") {
						config.finalMerger[i].colorHex = config.finalMerger[i].colorHex.replace("#", "");
					}
				}
			}
		}
		// ? handle scrolling text check
		if (config.finalMerger[i].scrollingTextObj && config.finalMerger[i].scrollingTextObj.scrollingTextCheck === true) {
			let top = getCanvasDimension('top', config.finalMerger[i].scrollingTextObj['topLocationRawPx'], background);
			let width = getCanvasDimension('width', config.finalMerger[i].scrollingTextObj['fixedTextWidthRawPx'], background);
			if (!isNaN(top)) {
				config.finalMerger[i].scrollingTextObj['topLocationPx'] = top;
			} else {
				config.finalMerger[i].scrollingTextObj['topLocationPx'] = 0;
			}
			if (!isNaN(width)) {
				config.finalMerger[i].scrollingTextObj['fixedTextWidthPx'] = width;
			} else {
				config.finalMerger[i].scrollingTextObj['fixedTextWidthPx'] = 100;
			}
			// NOTE -> handled in cpp
			// // NOTE -> handles special symbols which are needed to be escaped
			// config.finalMerger[i].scrollingTextObj['fixedText'] = formatScrollinText(config.finalMerger[i].scrollingTextObj['fixedText']);
			// config.finalMerger[i].scrollingTextObj['scrollingText'] = formatScrollinText(config.finalMerger[i].scrollingTextObj['scrollingText']);
		}
	}
	// ? add current local config object to session
	sessionStorage.setItem('currentConfigObject', JSON.stringify(config));

}
// has two extra parameters needed only while cropping
// isForCrop = true/false
// originalVideoRes = {width,height} of original video
function getCanvasDimension(type, value, background, isForCrop, originalVideoRes) {
	// this is css left,top,width,height
	let val;
	if (typeof value == 'string') {
		if (value == 'auto') {
			value = '0px';
		}
		// val = parseInt(value.substr(0, value.length - 2));
		val = parseInt(value.replace("px", ""));
	} else {
		val = value;
	}
	// debugger
	let backTemp = background[type];
	let outputTemp;
	if (isForCrop) {
		outputTemp = originalVideoRes[type];
	} else {
		outputTemp = config.outputRes[type];
	}
	if (type == 'left') {
		backTemp = background['width'];
		if (isForCrop) {
			outputTemp = originalVideoRes['width'];
		} else {
			outputTemp = config.outputRes['width'];
		}
	} else if (type == 'top') {
		if (isForCrop) {
			outputTemp = originalVideoRes['height'];
		} else {
			outputTemp = config.outputRes['height'];
		}
		backTemp = background['height'];
	}
	val = (val * outputTemp) / backTemp;
	val = Math.round(val.toFixed(2));
	return val;
}
function toggleTransparencyOptions(finalMergerIndex) {
	let item = config.finalMerger[finalMergerIndex];
	// debugger;
	if (item.type == "both" || item.type == "camera") {
		// ? show side-nav button for removing green screen
		$('#btn-remove-green-screen').show();
	} else {
		$('#btn-remove-green-screen').hide();
		$(".transparency-options").hide();
	}
}
function toggleTransparency(elem) {
	currentRec.transparencyCheck = elem.checked;
	if (currentRec.transparencyCheck == true && config.backImgPath == "false" && currentRec.type == "camera") {
		swal.fire({
			type: "info",
			title: "Background image not set",
			text: "Please select background image for using transparency in camera only mode"
		});
		elem.checked = false;
		currentRec.transparencyCheck = false;
	}
	else if (currentRec.transparencyCheck) {
		let tmpOpacity = (currentRec.transparencyOpacity * 100).toFixed(0);
		$("#opacity-value").val((tmpOpacity) + "%");
		$("#opacity-range-slider").val(tmpOpacity);
		//debugger
		let tmpSimilarity = (currentRec.transparencySimilarity * 100).toFixed(0);
		$("#similarity-value").val((tmpSimilarity) + "%");
		$("#similarity-range-slider").val(tmpSimilarity);
		$("#hex-color").text("#" + currentRec.colorHex);
		$('.a-transparency-option').show();
		$(".example-hex-color").show();
		$('.default-transparency-options-hide').addClass("default-transparency-options-show");
		$('.default-transparency-options').show();
	} else {
		$('.a-transparency-option').hide();
		$('.default-transparency-options').hide();
		$(".example-hex-color").hide();
		$('.default-transparency-options-hide').removeClass("default-transparency-options-show");
	}
}
function handleBackImgPathFromSettings(settings) {
	// NOTE -> Things to note before back image path
	// ? 1. use backImgPath from config if exists
	// ? 1.a) if backImgPath is set from config -> then use it
	// ? 1.b) if backImgPath is set from config, but not present on device ->
	// ?	show error and choose to use from settings.config
	// ? 2) if backImgPath is not set in config ->
	// ? 2.a) use backImgPath from settings if set
	// ? 2.b) if backImgPath from settings is not set, then do nothing
	//** Start */
	// TODO - 1. check if backImgPath is set in config file
	if (config.backImgPath && config.backImgPath !== 'false') {
		let tmpPath = decodeURI(config.backImgPath);
		if (fs.existsSync(tmpPath) === true) {
			// ? file present dont make any changes
			config.backImgPath = config.backImgPath.replace(/ /g, '%20');
		} else {
			// ? file not present ; use file from settings if exist
			if (settings.backImgPath && settings.backImgPath !== 'false') {
				tmpPath = decodeURI(settings.backImgPath);
				if (fs.existsSync(tmpPath) === true) {
					config.backImgPath = settings.backImgPath.replace(/ /g, '%20');
				} else {
					config.backImgPath = 'false';
				}
			} else {
				config.backImgPath = 'false';
			}
		}
	} else {
		// ? file not present ; use file from settings if exist
		if (settings.backImgPath && settings.backImgPath !== 'false') {
			tmpPath = decodeURI(settings.backImgPath);
			if (fs.existsSync(tmpPath) === true) {
				config.backImgPath = settings.backImgPath.replace(/ /g, '%20');
			} else {
				config.backImgPath = 'false';
			}
		} else {
			config.backImgPath = 'false';
		}
	}
	//** End */
	// ? handle back image path from settings
	// config.backImgPath = settings.backImgPath.replace(/ /g, '%20');
	let preview = $('#preview').get(0);
	// ? image version is needed in case if the image is change while user is editing
	// ? if not used , then earlier version is cached in chrome
	const version = new Date().getTime();
	$('#preview').css('background-image', 'url("' + config.backImgPath + '?v=' + version + '")');
	// preview.style.backgroundImage = 'url(' + config.backImgPath + ')';
	preview.style.backgroundRepeat = 'no-repeat';
	preview.style.backgroundSize = '100% 100%';
}
function handleLogoImagePathFromSettings(settings) {
	// NOTE -> Things to note before back image path
	// ? 1. use logoImgPath from config if exists
	// ? 1.a) if logoImgPath is set from config -> then use it
	// ? 1.b) if logoImgPath is set from config, but not present on device ->
	// ?	show error and choose to use from settings.config
	// ? 2) if logoImgPath is not set in config ->
	// ? 2.a) use logoImgPath from settings if set
	// ? 2.b) if logoImgPath from settings is not set, then do nothing
	//** Start */
	// TODO - 1. check if logoImgPath is set in config file
	if (config.logoImgPath && config.logoImgPath !== 'false') {
		let tmpPath = decodeURI(config.logoImgPath);
		if (fs.existsSync(tmpPath) === true) {
			// ? file present dont make any changes
			config.logoImgPath = config.logoImgPath.replace(/ /g, '%20');
		} else {
			// ? file not present ; use file from settings if exist
			if (settings.logoImgPath && settings.logoImgPath !== 'false') {
				tmpPath = decodeURI(settings.logoImgPath);
				if (fs.existsSync(tmpPath) === true) {
					config.logoImgPath = settings.logoImgPath.replace(/ /g, '%20');
					config.logoCoords = settings.logoCoords;
				} else {
					config.logoImgPath = 'false';
				}
			} else {
				config.logoImgPath = 'false';
			}
		}
	} else {
		// ? file not present ; use file from settings if exist
		if (settings.logoImgPath && settings.logoImgPath !== 'false') {
			tmpPath = decodeURI(settings.logoImgPath);
			if (fs.existsSync(tmpPath) === true) {
				config.logoImgPath = settings.logoImgPath.replace(/ /g, '%20');
				config.logoCoords = settings.logoCoords;
			} else {
				config.logoImgPath = 'false';
			}
		} else {
			config.logoImgPath = 'false';
		}
	}
	//** End */
	if (config.logoImgPath != 'false') {
		// ! this causes issue to not show image for logo
		// removeLogoImg();
		logoImgPath = config.logoImgPath;
		const logoCanvas = `
				<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="`+ config.logoImgPath + `" class='logo-canvas'></div>
			`;
		$("#preview").append(logoCanvas).ready(function () {
			$(".draggable").draggable({
				cursor: "crosshair",
				containment: "#preview",
				snap: "#preview",
				snapMode: "inner",
				snapTolerance: 1
			});
			$(".resizable").resizable({
				containment: "#preview",
				handles: 'e,w,n,s,se,nw,ne,sw',
			});
			$(".logo-canvas-wrap").css('width', config.logoCoords.widthRaw);
			$(".logo-canvas-wrap").css('height', config.logoCoords.heightRaw);
			$(".logo-canvas-wrap").css('left', config.logoCoords.leftRaw);
			$(".logo-canvas-wrap").css('top', config.logoCoords.topRaw);
		});
	}
}
// ? loadSettings backup
// function loadSettings() {
// 	let settings = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
// 	config.outputRes = settings.outputRes;
// 	config.crf = settings.crf;
// 	config.preset = settings.preset;
// 	if (settings.outputFrameRate) {
// 		config.outputFrameRate = settings.outputFrameRate;
// 	}
// 	if (settings.enableFadeEffect) {
// 		config.enableFadeEffect = settings.enableFadeEffect;
// 		config.fadeEffectDuration = settings.fadeEffectDuration;
// 	}
// 	handleBackImgPathFromSettings(settings);
// 	$('.canvas-wrap').each(function () {
// 		let rectype = this.getAttribute('type');
// 		let subtype = this.getAttribute('subtype');
// 		let width, height, left, top;
// 		if (rectype == 'camera' || rectype == 'screen') {
// 			width = settings.preview[rectype].cwidthRaw;
// 			height = settings.preview[rectype].cheightRaw;
// 			left = settings.preview[rectype].cleftRaw;
// 			top = settings.preview[rectype].ctopRaw;
// 		} else if (rectype == 'both') {
// 			let keyPrefix;
// 			if (subtype == 'camera') {
// 				keyPrefix = 'c'
// 			} else if (subtype == 'screen') {
// 				keyPrefix = 's';
// 			}
// 			width = settings.preview.both[keyPrefix + 'widthRaw'];
// 			height = settings.preview.both[keyPrefix + 'heightRaw'];
// 			left = settings.preview.both[keyPrefix + 'leftRaw'];
// 			top = settings.preview.both[keyPrefix + 'topRaw'];
// 		}
// 		$(this).width(width); $(this).find('canvas').width(width); //could you give 100% 100% here??
// 		$(this).height(height); $(this).find('canvas').height(height);
// 		$(this).css('left', left);
// 		$(this).css('top', top);
// 	});
// 	if (settings.logoImgPath) {
// 		removeLogoImg();
// 		logoImgPath = settings.logoImgPath;
// 		const logoCanvas = `
// 				<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="`+ logoImgPath + `" class='logo-canvas'></div>
// 			`;
// 		$("#preview").append(logoCanvas).ready(function () {
// 			$(".draggable").draggable({
// 				cursor: "crosshair",
// 				containment: "#preview",
// 				snap: "#preview",
// 				snapMode: "inner",
// 				snapTolerance: 1
// 			});
// 			$(".resizable").resizable({
// 				containment: "#preview",
// 				handles: 'e,w,n,s,se,nw,ne,sw',
// 			});
// 			$(".logo-canvas-wrap").css('width', settings.logoCoords.widthRaw);
// 			$(".logo-canvas-wrap").css('height', settings.logoCoords.heightRaw);
// 			$(".logo-canvas-wrap").css('left', settings.logoCoords.leftRaw);
// 			$(".logo-canvas-wrap").css('top', settings.logoCoords.topRaw);
// 		});
// 	}
// }
function loadSettings() {
  let mode12 = sessionStorage.getItem('selectedMode')
	const tmp = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
	let flag = false;
	let templateIndex = null;
	tmp.forEach((template, index) => {
		if(template.defaultkey === true) {
			flag=true;
			templateIndex = index;
    }
    if(sessionStorage.getItem('previewMode') == 'confirmMode'){
			templateIndex = sessionStorage.getItem('templateIndex');
			settings = tmp[templateIndex];
			flag=true;
			autotickindex= templateIndex;
		}
		else if(sessionStorage.getItem('previewMode')== 'cancelMode'){
      if(sessionStorage.getItem('templateIndex')) {
        templateIndex = sessionStorage.getItem('templateIndex');
        settings = tmp[templateIndex];
        flag=true;
        autotickindex =templateIndex;
      }
		}
	});
	if(flag === true) {
		settings = tmp[templateIndex];
		// settings = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
		config.outputRes = settings.outputRes;
		config.crf = settings.crf;
		config.preset = settings.preset;
		// NOTE -> patch added for issue when outputFrameRate does not get read from settings to config
		if (settings.outputFrameRate) {
			config.outputFrameRate = settings.outputFrameRate;
		}
		if (settings.enableFadeEffect) {
			config.enableFadeEffect = settings.enableFadeEffect;
			config.fadeEffectDuration = settings.fadeEffectDuration;
		}
		if (settings.maxMuxingQueueSize) {
			config.maxMuxingQueueSize = settings.maxMuxingQueueSize;
		} else {
			delete config.maxMuxingQueueSize;
    }
    for(var i = 0; i< config.finalMerger.length ; i++){
			if(config.finalMerger[i].type == "both"){
				if(settings.preview["both"].currentRec){
					config.finalMerger[i].scrollingTextObj = settings.preview["both"].currentRec.scrollingTextObj;
				}
			}
			else if(config.finalMerger[i].type == "camera"){
				if(settings.preview["camera"].currentRec){
					config.finalMerger[i].scrollingTextObj = settings.preview["camera"].currentRec.scrollingTextObj;
				}
			}
			else if(config.finalMerger[i].type == "screen"){
				if(settings.preview["screen"].currentRec){
					config.finalMerger[i].scrollingTextObj = settings.preview["screen"].currentRec.scrollingTextObj;
					}
				}
			}
	}

}
function removeBackImg() {

	swal.fire({
		type: "info",
		title: "Remove Background ?",
		text: "Are you sure ?",
		showCancelButton: true,
		cancelButtonText: "Remove",
		showConfirmButton: true,
		confirmButtonText: "Cancel"
	}).then((res) => {
		if (res.dismiss === "cancel") {
			backImgPath = null;
			let preview = $('#preview').get(0);
			preview.style.background = "white";
			config.backImgPath = "false";	//for cpp			
		}
	});


}
function removeLogoImg() {
	logoImgPath = null;
	config.logoImgPath = "false"; //for cpp
	$(".logo-canvas-wrap").remove();
}
async function checkFFMPEG() {
	let basepath = app.getAppPath('exe');
	basepath = basepath.replace("app.asar", "")
	basepath += "\\src\\assets\\exe\\ffmpeg";
	return new Promise((resolve, reject) => {
		try {
			let proc = child_process.spawn(basepath, ["--version"]);
			proc.on('close', function (status, signal) {
				resolve({ installed: true });
			});
			proc.on('error', function (err) {
				reject({ installed: false });
			});
		}
		catch (err) {
		}
	});
}
function loadDefaultTransparency() {
	if (fs.existsSync(documentsFolderPath + '\\transparency-defaults.json')) {
		let defaultPreviewSettings = JSON.parse(fs.readFileSync(documentsFolderPath + '\\transparency-defaults.json', 'utf8'));
		if (defaultPreviewSettings.defaults) {
			$("#hex-color").val(defaultPreviewSettings.defaults.color);
			transparencySimilarity = parseFloat(defaultPreviewSettings.defaults.transparencySimilarity);
			transparencyOpacity = parseFloat(defaultPreviewSettings.defaults.transparencyOpacity);
			colorHex = defaultPreviewSettings.defaults.color;
			if (currentRec) {

				// ! must remove # if exists in here
				currentRec.colorHex = defaultPreviewSettings.defaults.color.replace('#','');

				currentRec.transparencySimilarity = parseFloat(defaultPreviewSettings.defaults.transparencySimilarity);
				currentRec.transparencyOpacity = parseFloat(defaultPreviewSettings.defaults.transparencyOpacity);
			}
			$("#opacity-value").val((transparencyOpacity * 100).toFixed(0) + "%")
			$("#similarity-value").val((transparencySimilarity * 100).toFixed(0) + "%")
			$("#similarity-range-slider").val((transparencySimilarity * 100).toFixed(0));
			$("#opacity-range-slider").val((transparencyOpacity * 100).toFixed(0));
			//debugger;
		}
		isHexOk();
	} else {
		swal.fire({
			type: "info",
			title: "No defaults saved!",
			toast: true,
			position: "top-right",
			timer: 3000
		});
	}
}
function saveAsDefault() {
	// save
	let color = $("#hex-color").val();
	// transparencySimilarity,transparencyOpacity
	let transparencySettings = {
		"defaults": {
			color: color,
			transparencySimilarity: transparencySimilarity,
			transparencyOpacity: transparencyOpacity
		}
	};
	if (fs.existsSync(documentsFolderPath + '\\transparency-defaults.json')) {
		fs.truncateSync(documentsFolderPath + '\\transparency-defaults.json');
	}
	fs.appendFileSync(documentsFolderPath + '\\transparency-defaults.json', JSON.stringify(transparencySettings), 'utf-8');
	swal.fire({
		toast: true,
		position: "top-right",
		text: "transparency settings saved!",
		duration: 4000
	});
}
function canvasSelected(canvas) {
	let type = canvas.getAttribute("type");
	let subtype = canvas.getAttribute("subtype");
	// ? if is required
	// ? without if, other canvases , (e.g. crop-canvas) can also get selected
	if (type === 'camera' || type === 'screen' || type === 'both') {
		selectedCanvas = canvas;
	}
	// if (type == "screen" || subtype == "screen") {
	// 	$("#btn-crop").removeClass("disabled");
	// } else {
	// 	$("#btn-crop").addClass("disabled");
	// }
	//
	// $(canvas).Jcrop();
}
// function initCropMode(elem){
// 	if(!$(elem).hasClass("disabled")){
//
// 		selectedCanvasHTML = $(selectedCanvas).html();
// 		$(selectedCanvas).Jcrop({
// 		},function(){
// 			jCropObj = this;
// 		});
// 	}
// 	else{
//
// 	}
// }
function cancelCropMode(elem) {
	if (!$(elem).hasClass("disabled")) {
		// jCropObj.destroy();
		if ($(selectedCanvas).data('Jcrop')) {
			$(selectedCanvas).data('Jcrop').destroy();
		}
		// $(selectedCanvas).Jcrop();
	}
	else {
	}
}
/**
 * This function handles camera crop
 * this and openCropMode function can be reduced
 */
function openCameraCropMode() {
	if (currentRec.type === 'camera') {
		let currentCanvasId = 'canvas-camera-' + currentFinalMergerIndex + '-wrap';
		selectedCanvas = $('#' + currentCanvasId).get(0);
	} else if (currentRec.type == 'both') {
		let currentCanvasId = 'canvas-camera-' + currentFinalMergerIndex + '-wrap';
		selectedCanvas = $('#' + currentCanvasId).get(0);
	}
	if (selectedCanvas) {
		saveSettings();
		// TODO - 1. disable the left nav
		$(".side-nav").addClass("disabled");
		$(".options-side-nav-wrapper").addClass("disabled");
		$(".preview-mode-wrap").hide();
		$(".crop-mode-wrap").css('display', 'inline-block');
		if (config.backImgPath != "false") {
			let preview = $('.crop-mode-preview').get(0);
			preview.style.backgroundImage = 'url(' + (backImgPath.replace(/ /g, "%20")) + ')';
			preview.style.backgroundRepeat = 'no-repeat';
			preview.style.backgroundSize = '100% 100%';
		}
		// TODO - 2. append empty div
		addCanvasToCrop();
	} else {
		swal.fire({
			type: "info",
			title: "Recording not selected",
			text: "Please select camera or screen recording for cropping."
		});
	}
}
/**
 * This function by default , handles crop of screen
 */
function openCropMode() {
	if (currentRec.type === 'camera' || currentRec.type === 'screen') {
		let currentCanvasId = 'canvas-camera-' + currentFinalMergerIndex + '-wrap';
		selectedCanvas = $('#' + currentCanvasId).get(0);
	} else if (currentRec.type == 'both') {
		let currentCanvasId = 'canvas-screen-' + currentFinalMergerIndex + '-wrap';
		selectedCanvas = $('#' + currentCanvasId).get(0);
	}
	if (selectedCanvas) {
		saveSettings();
		// TODO - 1. disable the left nav
		$(".side-nav").addClass("disabled");
		$(".options-side-nav-wrapper").addClass("disabled");
		$(".properties-panel-wrapper").addClass("disabled");
		$(".preview-mode-wrap").hide();
		$(".crop-mode-wrap").css('display', 'inline-block');
		if (config.backImgPath != "false") {
			let preview = $('.crop-mode-preview').get(0);
			preview.style.backgroundImage = 'url(' + (backImgPath.replace(/ /g, "%20")) + ')';
			preview.style.backgroundRepeat = 'no-repeat';
			preview.style.backgroundSize = '100% 100%';
		}
		// TODO - 2. append empty div
		addCanvasToCrop();
	} else {
		swal.fire({
			type: "info",
			title: "Recording not selected",
			text: "Please select camera or screen recording for cropping."
		});
	}
}
function closeCropMode() {
	// 1. disable the left nav
	$(".side-nav").removeClass("disabled");
	$(".options-side-nav-wrapper").removeClass("disabled");
	$(".properties-panel-wrapper").removeClass("disabled");
	$(".preview-mode-wrap").show();
	$(".crop-mode-wrap").hide();
	$("#canvas-crop-wrap").remove();
}
function addCanvasToCrop() {
	//
	// let oldWidth = $(selectedCanvas).width();
	// let oldHeight = $(selectedCanvas).height();
	// let oldLeft = $(selectedCanvas).css("left");
	// let oldTop = $(selectedCanvas).css("top");
	// ? to show crop area in full screen
	let oldWidth = $(".crop-mode-preview").width();
	let oldHeight = $(".crop-mode-preview").height();
	let oldLeft = $(".crop-mode-preview").css("left");
	let oldTop = $(".crop-mode-preview").css("top");
	let aCanvas = newCanvas;
	aCanvas = aCanvas.replace(/canvas-id-here/g, 'canvas-crop');
	$('.crop-mode-preview').empty();
	$('.crop-mode-preview').append(aCanvas);
	let camCanvas = $('#canvas-crop-wrap').get(0);
	// camCanvas.setAttribute('width',$('#preview').width() - 100);
	// camCanvas.setAttribute('height',$('#preview').height() - 50);
	$(camCanvas).show();
	$(camCanvas).width(oldWidth);
	$(camCanvas).height(oldHeight);
	$(camCanvas).css("left", oldLeft);
	$(camCanvas).css("top", oldTop);
	// $(camCanvas).css("background", "red");
	// 3. draw on canvas
	let videoId = null;
	// if (currentRec.type == "screen") {
	// 	videoId = "video-camera-" + currentFinalMergerIndex;
	// } else if (currentRec.type == "both") {
	// 	videoId = "video-screen-" + currentFinalMergerIndex;
	// }
	// ? alternate way to do the same, but allowing crop of camera
	videoId = $(selectedCanvas).find('video').get(0).id;
	// videoId = tmpId.replace('canvas', 'video');
	let video = $("#" + videoId).get(0);
	let canvas = $("#canvas-crop").get(0);
	$(canvas).width(oldWidth);
	$(canvas).height(oldHeight);
	$(canvas).css("left", oldLeft);
	$(canvas).css("top", oldTop);
	$(canvas).attr("width", oldWidth);
	$(canvas).attr("height", oldHeight);
	$(canvas).attr("src", video.src);
  // await getVideoFrames(video, canvas);
	let promise = canvas.play();
  if (promise !== undefined) {
    promise.then(function () {
      setTimeout(() => {
        canvas.pause();
        	$(canvas).Jcrop({
            onSelect: function () {
              $(".preview-btn").removeClass("disabled");
            }
          }, function () {
            jCropObj = this;
          });
      },1000);
    });
  }

}
async function confirmCrop() {
	let canvas = $(selectedCanvas).find('video').get(0);
	// let videoId = 'video-';
	let videoId = $(selectedCanvas).find('video').get(0).id;
	// tmpId = tmpId.replace('canvas-', '');
	// videoId += tmpId
	let video = $("#" + videoId).get(0);
  let cropCoords = getCropCoords(canvas, video,selectedCanvas);

	canvas.setAttribute("windowCoords", JSON.stringify(cropCoords.windowCoords));
	// ! this sets a single variable, which we have used in actual crop command
	// TODO - we need two variables, one for each camera and screen
	currentRec.windowCoords = cropCoords.windowCoords;
	let coordsKey = '';
	if (currentRec.type === 'camera') {
		coordsKey = 'cameraWindowCoords';
	} else if (currentRec.type === 'screen') {
		coordsKey = 'screenWindowCoords';
	} else if (currentRec.type === 'both') {
		if (selectedCanvas.getAttribute('subtype') === 'camera') {
			coordsKey = 'cameraWindowCoords';
		} else if (selectedCanvas.getAttribute('subtype') === 'screen') {
			coordsKey = 'screenWindowCoords';
		}
	}
  currentRec[coordsKey] = cropCoords.windowCoords;
	canvas.setAttribute("fullscreenmode", "false");
  closeCropMode();

  updateCanvasAfterCrop(canvas,cropCoords,selectedCanvas);



  // await getVideoFrames(video, canvas);
  let promise = canvas.play();
  if (promise !== undefined) {
    promise.then(function () {
      setTimeout(() => {
        canvas.pause();
      },1000);
    });
  }
	setNewDimensions(cropCoords.outputCoords);
}
function setNewDimensions(newDimensions) {
	let keyPrefix = "";
	if (currentRec.type == "screen") {
		keyPrefix = "c";
	} else if (currentRec.type == "both") {
		keyPrefix = "s";
	}
	currentRec[keyPrefix + "width"] = newDimensions.width;
	currentRec[keyPrefix + "height"] = newDimensions.height;
	currentRec[keyPrefix + "top"] = newDimensions.top;
	currentRec[keyPrefix + "left"] = newDimensions.left;
	currentRec.fullScreenMode = false;
}
function getCropCoords(canvasToCrop, video, selectedCanvas) {
	let selection = jCropObj.tellSelect();
	selection = handleCroppedArea(selection);
	let background = {
		top: 0,
		left: 0,
		width: $(selectedCanvas).width(),
		height: $(selectedCanvas).height(),
	}
	// use getCanvasDimenstion for each width,height,left and top of selection
	// while cropping crop values for canvas should be relative to original resolution of screen;
	// but windowCoords should be relative to outputResolution selected in settings
	let forOutputWidth = getCanvasDimension('width', selection.w, background);
	let forOutputHeight = getCanvasDimension('height', selection.h, background);
	let forOutputLeft = getCanvasDimension('left', selection.x, background);
	let forOutputTop = getCanvasDimension('top', selection.y, background);
	

	// ? this caused issue that cropping mulitple times would show incorrect preview
	let originalVideoRes = {
		width: video.videoWidth,
		height: video.videoHeight
	};
	// let originalVideoRes = {
	// 	width: $(selectedCanvas).width(),
	// 	height: $(selectedCanvas).height()
	// };
	let forCanvasWidth = getCanvasDimension('width', selection.w, background, true, originalVideoRes);
	let forCanvasHeight = getCanvasDimension('height', selection.h, background, true, originalVideoRes);
	let forCanvasLeft = getCanvasDimension('left', selection.x, background, true, originalVideoRes);
	let forCanvasTop = getCanvasDimension('top', selection.y, background, true, originalVideoRes);
	// debugger
	let ret = {
		windowCoords: {
			width: forCanvasWidth,
			height: forCanvasHeight,
			screenX: forCanvasLeft,
			screenY: forCanvasTop
		},
		outputCoords: {
			width: forOutputWidth,
			height: forOutputHeight,
			left: forOutputLeft,
			top: forOutputTop
    },
    selection
	};
	return ret;
	//
}
function backToHome() {
	sessionStorage.removeItem('currentConfigObject');
	win.unmaximize();
	win.setResizable(true);
	win.setSize(800, 600);
	win.loadFile("index.html");
}
function handleAutoUpdateEvent(param) {
	param = JSON.parse(param);
	if (param.event == "update-available") {
		// show new icon at titlebar
		$(".new-image").show();
		$(".new-image").attr("src", "assets/images/new.png");
	} else if (param.event == "downloading-update") {
		$(".new-image,.install-update").hide();
		if ($(".infinite-loader").hasClass("inline-block")) {
		} else {
			$(".infinite-loader").show();
		}
		$(".infinite-loader").addClass("inline-block");
		$(".infinite-loader").text((param.ev.percent).toFixed(0) + "%");
		// $(".new-img").attr("src","assets/images/new.png");
		isUpdateDownloading = true;
	} else if (param.event == "update-downloaded") {
		isUpdateDownloading = false;
		$(".install-update").show();
		$(".infinite-loader").hide();
		$(".new-image").hide();
	}
}
function onUpdateAvailableClick() {
	swal.fire({
		type: "info",
		title: "New version of HiFi Recorder is available",
		text: "Download Update ?",
		showCancelButton: true,
		cancelButtonText: "Later",
		showConfirmButton: true,
		confirmButtonText: "Download Now"
	}).then((res) => {
		if (res.value) {
			ipcRenderer.send("auto-updater", { event: "download-update-now" });
		}
	});
}
function onUpdateDownloadClick() {
	swal.fire({
		type: "info",
		title: "New version of HiFi Recorder has been Downloaded",
		text: "Install Update ?",
		showCancelButton: true,
		cancelButtonText: "Later",
		showConfirmButton: true,
		confirmButtonText: "Install Now"
	}).then((res) => {
		if (res.value) {
			ipcRenderer.send("auto-updater", { event: "install-update-now" });
		}
	});
}
function changeLogoImg() {
	let logo = dialog.showOpenDialog(win, {
		filters: [
			{ name: 'Images', extensions: ['jpg', 'png'] },
		],
		properties: ['openFile']
	});
	if (logo) {
		removeLogoImg();
		logoImgPath = logo[0].replace(/\\/g, '\\\\');
		config.logoImgPath = logoImgPath;
		const logoCanvas = `
				<div class='draggable resizable logo-canvas-wrap' id='logo-canvas-wrap' type='logo-canvas'><img src="`+ logoImgPath + `" class='logo-canvas'></div>
			`;
		$("#preview").append(logoCanvas).ready(function () {
			$(".draggable").draggable({
				cursor: "crosshair",
				containment: "#preview",
				snap: "#preview",
				snapMode: "inner",
				snapTolerance: 1
			});
			$(".resizable").resizable({
				containment: "#preview",
				handles: 'e,w,n,s,se,nw,ne,sw',
			});
		});
		// ? if isLogoVisible key is not present or is set to true
		// ? then set to true and check the checkbox
		if ((!currentRec.isLogoVisible) || (currentRec.isLogoVisible === true)) {
			currentRec.isLogoVisible = true;
			$('.add-remove-logo-check').prop('checked', true);
		}
		for (let index = 0; index < config.finalMerger.length; index++) {
			const recItem = config.finalMerger[index];
			if (!recItem.isLogoVisible) {
				recItem.isLogoVisible = true;
			}
		}
	}
}
function checkAutoUpdaterStatus() {
	// ipcRenderer.send("request-auto-updater-status");
}
function preparePreviewCommand(config, currentRec, replaceBackSlashFlag, basepath) {
	let previewcc = require('bindings')('previewprocess');
	if (!currentRec.fullScreenMode) {
		currentRec.fullScreenMode = false;
	} else {
		currentRec.fullScreenMode = true;
	}
	if (replaceBackSlashFlag) {
		config.backImgPath = config.backImgPath.replace(/\\/g, "\\\\");
	}
	const currentTime = $('#play-time-range-slider').val();
	let previewresult = previewcc.previewprocess(config, currentRec, function (msg) {

		hideLoader();
	}, basepath, currentTime);
	//
	// let cmd = getPreviewCmd(config,currentRec);
	// runPreviewCmd(cmd);
}
function autoResizeCurrentCanvas() {
	let cameraCanvas = $('#canvas-camera-' + currentFinalMergerIndex + '-wrap').get(0);
	setDimensionsInAspectRatio(cameraCanvas);
	if (currentRec.type == "both") {
		let screenCanvas = $('#canvas-screen-' + currentFinalMergerIndex + '-wrap').get(0);
		setDimensionsInAspectRatio(screenCanvas);
	}
}
function setDimensionsInAspectRatio(selectedCanvas) {
	//
	let width = $(selectedCanvas).width();
	let height = $(selectedCanvas).height();
	// TODO - reduce width to set in 16:9
	// TODO - 1. set width in multiple of 16
	let newWidth = width, newHeight = height;
	if (width % 16 != 0) {
		newWidth = width - (16 - (width % 16));
		// TODO - now set height as per newWidth
		newHeight = parseInt((newWidth * 9) / 16);
		height = newHeight;
	}
	if (height % 9 != 0) {
		newHeight = height - (9 - (height % 9));
	}
	$(selectedCanvas).width(newWidth);
	$(selectedCanvas).height(newHeight);
	$(selectedCanvas).children("video").css("width", "100%");
	$(selectedCanvas).children("video").css("height", "100%");
	snapIfOutofBounds(selectedCanvas);
}
function snapIfOutofBounds(selectedCanvas) {
	const top = parseInt($(selectedCanvas).css("top").replace("px", ""));
	const height = parseInt($(selectedCanvas).css("height").replace("px", ""));
	const backgroundHeight = $("#preview").height();
	const condition = (top + height > backgroundHeight);
	if (condition) {
		// TODO - 1. calculate how much portion is out of the frame
		// TODO - 2. substract out-portion from current top to move it top
		const newTop = (top + height) - backgroundHeight;
		$(selectedCanvas).css("top", (top - newTop) + "px");
	}
	// debugger;
}
/**
 * ? converts fullscreen crop coordinates to original area (before cropping) values
 * ? desc - while cropping we increase the dimensions of canvas to fullscreen
 * ? so the cropepedArea should be converted with respect to small canvas area
 * ? which was selected for cropping
 */
function handleCroppedArea(selection) {
	const newSelection = {};
	// ? selection is relative to size of #crop-preview
	// ? we require selection relative to selectedCanvas
	const selectedCanvasWidth = $(selectedCanvas).width();
	const cropWrapperWidth = $("#canvas-crop-wrap").width();
	newSelection.w = parseInt(((selection.w * selectedCanvasWidth) / cropWrapperWidth), 10);
	const selectedCanvasHeight = $(selectedCanvas).height();
	const cropWrapperHeight = $("#canvas-crop-wrap").height();
	newSelection.h = parseInt(((selection.h * selectedCanvasHeight) / cropWrapperHeight), 10);
	newSelection.x = parseInt(((selection.x * selectedCanvasWidth) / cropWrapperWidth), 10);
	newSelection.y = parseInt(((selection.y * selectedCanvasHeight) / cropWrapperHeight), 10);
	return newSelection;
}
function openRecItemSettingsModal(recItemElement) {
	const finalMergerIndex = parseInt(recItemElement.getAttribute('final-merger-index'));
	currentFinalMergerIndex = finalMergerIndex;
	const currentSettingsRec = config.finalMerger[finalMergerIndex];
	$('#rec-item-settings-modal').show();
	// $('#rec-item-settings-mode-selector').val(currentSettingsRec.type);
	$('#current-rec-dropdown').text(currentSettingsRec.type);
	$('#change-rec-dropdown').text('Select Recording Mode');
	$('#change-rec-dropdown').attr('rectype', currentSettingsRec.type);
	let cameraFilePath = '', simpleCameraFileName = '';
	let screenFilePath = '', simpleScreenFileName = '';
	if (currentSettingsRec.type === 'camera') {
		cameraFilePath = getCurrentCameraFilePath(currentSettingsRec);
		simpleCameraFileName = getSimpleFileName(cameraFilePath);
		$(".btn-change-camera-file").data('relative-file-path', config.finalMerger[finalMergerIndex].cpath);
		$('.camera-section').show();
		$('.screen-section').hide();
	}
	if (currentSettingsRec.type === 'screen') {
		screenFilePath = getCurrentCameraFilePath(currentSettingsRec);
		simpleScreenFileName = getSimpleFileName(screenFilePath);
		$(".btn-change-screen-file").data('relative-file-path', config.finalMerger[finalMergerIndex].cpath);
		$('.camera-section').hide();
		$('.screen-section').show();
	}
	if (currentSettingsRec.type === 'both') {
		cameraFilePath = getCurrentCameraFilePath(currentSettingsRec);
		screenFilePath = getCurrentScreenFilePath(currentSettingsRec);
		simpleCameraFileName = getSimpleFileName(cameraFilePath);
		simpleScreenFileName = getSimpleFileName(screenFilePath);
		$(".btn-change-camera-file").data('relative-file-path', config.finalMerger[finalMergerIndex].cpath);
		$(".btn-change-screen-file").data('relative-file-path', config.finalMerger[finalMergerIndex].spath);
		$('.camera-section').show();
		$('.screen-section').show();
	}
	$(".btn-view-camera-file").data('filepath', cameraFilePath);
	$(".btn-view-screen-file").data('filepath', screenFilePath);
	$('.camera-file-simple-name').text(simpleCameraFileName);
	$('.screen-file-simple-name').text(simpleScreenFileName);
	const time = config.finalMerger[finalMergerIndex].endTime - config.finalMerger[finalMergerIndex].startTime;
	$(".btn-change-camera-file").data('file-duration', time);
	$(".btn-change-screen-file").data('file-duration', time);
	if (config.finalMerger[finalMergerIndex].externalCameraFileDuration) {
		$(".btn-change-camera-file").data('file-duration', config.finalMerger[finalMergerIndex].externalCameraFileDuration);
	}
	if (config.finalMerger[finalMergerIndex].externalScreenFileDuration) {
		$(".btn-change-screen-file").data('file-duration', config.finalMerger[finalMergerIndex].externalScreenFileDuration);
	}
	setCurrentRecStartEndTime(config.finalMerger[finalMergerIndex]);
	initWaveformPins(config.finalMerger[finalMergerIndex]);
	$('.btn-rec-item-settings').data('currentFinalMergerIndex', finalMergerIndex);
	saveSettings();
}
function initWaveformPins(recItem) {
	$('.camera-pins-wrapper').show();
	$('.screen-pins-wrapper').show();
	$('.camera-pins-wrapper-text').text('Select Camera Start and End Time');
	$('.screen-pins-wrapper-text').text('Select Screen Start and End Time');
	// const cameraWaveformPath = $('.camera-waveform-img').attr('src');
	// const screenWaveformPath = $('.screen-waveform-img').attr('src');
	const cameraWaveformPath = recItem.cameraWaveFilePath;
	const screenWaveformPath = recItem.screenWaveFilePath;
	if (cameraWaveformPath === '' || !cameraWaveformPath) {
		// ! NOTE -> 2 px is for border (1px each left and right)
		const px = recItem.cameraWaveFileDuration * env.pixelsPerSecond + 2 + 'px';
		$('.camera-waveform-img').css('width', px);
		$('.camera-waveform-img').css('height', env.waveformPictureHeight + 'px');
	} else {
		$('.camera-waveform-img').attr('src', cameraWaveformPath);
	}
	if (screenWaveformPath === '' || !screenWaveformPath) {
		// $('.screen-pins-wrapper').hide();
		// $('.screen-pins-wrapper-text').text('Could not generate waveform for this file');
		// ! NOTE -> 2 px is for border (1px each left and right)
		const px = recItem.cameraWaveFileDuration * env.pixelsPerSecond + 2 + 'px';
		$('.screen-waveform-img').css('width', px);
		$('.screen-waveform-img').css('height', env.waveformPictureHeight + 'px');
	} else {
		$('.screen-waveform-img').attr('src', screenWaveformPath);
	}
	$('.waveform-pin').each((index, element) => {
		$(element).draggable({
			axis: 'x',
			cursor: "crosshair",
			containment: $(element).parent(),
			snap: $(element).parent(),
			snapMode: "inner",
			snapTolerance: 1,
			drag: (event) => {
				const draggedPinType = $(event.target).attr('timetype');
				const left = $(event.target).css('left');
				waveformPinDragged(left, draggedPinType);
			}
		});
	});
}
function setCurrentRecStartEndTime(recItem) {
	// ? camera start and end time
	if (recItem.cameraCutStartTime) {
		$('.camera-file-start-time').val(recItem.cameraCutStartTime);
		const calculatedLeft = (getSecondsFromHMS(recItem.cameraCutStartTime).seconds * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-camera-start').css('left', calculatedLeft);
	} else {
		$('.camera-file-start-time').val(secondsToHms(recItem.startTime));
		const calculatedLeft = (recItem.startTime * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-camera-start').css('left', calculatedLeft);
	}
	if (recItem.cameraCutEndTime) {
		$('.camera-file-end-time').val(recItem.cameraCutEndTime);
		const calculatedLeft = (getSecondsFromHMS(recItem.cameraCutEndTime).seconds * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-camera-end').css('left', calculatedLeft);
	} else {
		$('.camera-file-end-time').val(secondsToHms(recItem.endTime));
		const calculatedLeft = (recItem.endTime * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-camera-end').css('left', calculatedLeft);
	}
	// ? screen start and end time
	if (recItem.screenCutStartTime) {
		$('.screen-file-start-time').val(recItem.screenCutStartTime);
		const calculatedLeft = (getSecondsFromHMS(recItem.screenCutStartTime).seconds * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-screen-start').css('left', calculatedLeft);
	} else {
		$('.screen-file-start-time').val(secondsToHms(recItem.startTime));
		const calculatedLeft = (recItem.startTime * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-screen-start').css('left', calculatedLeft);
	}
	if (recItem.screenCutEndTime) {
		$('.screen-file-end-time').val(recItem.screenCutEndTime);
		const calculatedLeft = (getSecondsFromHMS(recItem.screenCutEndTime).seconds * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-screen-end').css('left', calculatedLeft);
	} else {
		$('.screen-file-end-time').val(secondsToHms(recItem.endTime));
		const calculatedLeft = (recItem.endTime * env.pixelsPerSecond) + 'px';
		$('.waveform-pin-screen-end').css('left', calculatedLeft);
	}

	// ? camera and screen total duration
	// TODO -> handle things differently if video is external
	let cameraEndTime = recItem.endTime;
	let screenEndTime = recItem.endTime;
	if(recItem.externalCameraFileDuration) {
		cameraEndTime = recItem.externalCameraFileDuration;
	}
	if(recItem.externalScreenFileDuration) {
		screenEndTime = recItem.externalScreenFileDuration;
	}
	$('.camera-file-total-time').val(secondsToHms(cameraEndTime));
	$('.screen-file-total-time').val(secondsToHms(screenEndTime));
}
function getSimpleFileName(filepath) {
	// return path.basename(filepath);
	return filepath;
}
function getCurrentCameraFilePath(currentSettingsRec) {
	let filePath = sessionStorage.getItem('configFolderPath');
	if (filePath && currentSettingsRec.cpath) {
		filePath = path.join(filePath, currentSettingsRec.cpath);
	} else {
		filePath = null;
	}
	return filePath;
}
function getCurrentScreenFilePath(currentSettingsRec) {
	let filePath = sessionStorage.getItem('configFolderPath');
	if (filePath && currentSettingsRec.spath) {
		filePath = path.join(filePath, currentSettingsRec.spath);
	} else {
		filePath = null;
	}
	return filePath;
}
function closeRecItemSettingsModal() {
	$('#rec-item-settings-modal').hide();
}
function viewFile(btnElement) {
	const filePath = $(btnElement).data('filepath');
	if (filePath === '') {
		swal.fire({
			type: 'info',
			title: 'File not selected',
			text: 'Please select a file'
		});
	} else {
		shell.showItemInFolder(filePath);
	}
}
function changeCurrentRecItemFile(changeFileElement) {
	changedFiles = dialog.showOpenDialog(win, {
		filters: [
			{ name: 'Videos', extensions: ['mp4', 'mts', 'avi'] },
		],
		properties: ['openFile', 'multiSelections']
	});
	if (changedFiles) {
		const fileType = $(changeFileElement).attr('filetype');
		const newFileTypeFolderName = path.join(sessionStorage.getItem('configFolderPath'), fileType);
		if (!fs.existsSync(newFileTypeFolderName)) {
			fs.mkdirSync(newFileTypeFolderName);
		}
		if (changedFiles.length > 1) {
			askForMergingFiles(changedFiles, fileType, changeFileElement).then((res) => {
				if (res.value) {
					mergeSelectedFiles(changedFiles, fileType, changeFileElement).then((filePathObject) => {
						generateWaveformHelper(fileType, changeFileElement, filePathObject);
					});
				}
			});
		} else {
			const extension = path.extname(changedFiles[0]);
			if (extension.toLowerCase() === '.mts') {
				const newFileName = new Date().getTime().toString();
				const newFilePath = path.join(sessionStorage.getItem('configFolderPath'), fileType, newFileName + '.mp4');
				convertFileToMp4(changedFiles[0], newFilePath).then((filePath) => {
					const filePathObject = {
						relativeFilePath: (fileType + '/' + newFileName + '.mp4'),
						absoluteFilePath: filePath
					};
					generateWaveformHelper(fileType, changeFileElement, filePathObject);
				}).catch((err) => {
					showSimpleAlert('error', 'Could not convert file');
				});
			} else {
				addNewFileToProject(changeFileElement, changedFiles);
			}
		}
	}
}
function addNewFileToProject(changeFileElement, changedFiles) {
	const fileType = $(changeFileElement).attr('filetype');
	copyNewFileToProject(fileType, changedFiles[0]).then((filePathObject) => {
		generateWaveformHelper(fileType, changeFileElement, filePathObject);
	}).catch((err) => {
		showSimpleAlert('error', 'Could not copy file to folder', 'Please check if file and free space exists in C Drive');
	});
}
function generateWaveformHelper(fileType, changeFileElement, filePathObject) {
	showLoader();
	getFileDuration(filePathObject.absoluteFilePath).then((duration) => {
		$(changeFileElement).data('file-duration', duration);
		if (duration) {
			const videoFilePath = filePathObject.absoluteFilePath;
			const outputFilePath = path.join(paramObj.configFolderPath, 'waveforms', (fileType + '-wave-form-' + new Date().getTime().toString() + '.png'));
			try {
				fs.mkdirSync(path.join(paramObj.configFolderPath, 'waveforms'));
			} catch (exp) {

			}
			hideLoader();
			generatingWaveformsCounter = 1;
			generateWaveformFromVideo(videoFilePath, duration, outputFilePath).then((waveFormPath) => {
				const currentFinalMergerIndex = parseInt($('.btn-rec-item-settings').data('currentFinalMergerIndex'));
				let recItem = config.finalMerger[currentFinalMergerIndex];
				// ? set path in final merger item
				if (fs.existsSync(waveFormPath)) {
					if (fileType === 'camera') {
						recItem.cameraWaveFilePath = waveFormPath;
						recItem.cameraWaveFileDuration = duration;
					} else if (fileType === 'screen') {
						recItem.screenWaveFilePath = waveFormPath;
						recItem.screenWaveFileDuration = duration;
					}
				} else {
					if (fileType === 'camera') {
						recItem.cameraWaveFilePath = null;
						recItem.cameraWaveFileDuration = duration;
					} else if (fileType === 'screen') {
						recItem.screenWaveFilePath = null;
						recItem.screenWaveFileDuration = duration;
					}
				}
				// ? change waveform image
				$('.' + fileType + '-waveform-img').attr('src', waveFormPath + '?v=' + new Date().getTime());
				$('.' + fileType + '-waveform-img').css('width', duration * env.pixelsPerSecond + 'px');
				$('.' + fileType + '-pins-wrapper-text').show();
				$('.' + fileType + '-pins-wrapper').show();
				// ? reset start and end pins
				$('.waveform-pin-' + fileType + '-start').css('left', '0px');
				$('.waveform-pin-' + fileType + '-end').css('left', duration * env.pixelsPerSecond + 'px');
				// ? reset start and end time
				$('.' + fileType + '-file-start-time').val(secondsToHms(0.0));
				$('.' + fileType + '-file-end-time').val(secondsToHms(duration));
			}).catch((err) => {
				hideLoader();
			});
		}
	}).catch((err) => {
		showSimpleAlert('error', 'Duration not set', 'Failed to get duration from selected file , or , selected video format is not supported');
		hideLoader();
	});
	$(changeFileElement).data('relative-file-path', filePathObject.relativeFilePath);
	if (fileType === 'camera') {
		$('.btn-view-camera-file').data('filepath', filePathObject.absoluteFilePath);
		$('.btn-view-camera-file').show();
	} else if (fileType === 'screen') {
		$('.btn-view-screen-file').data('filepath', filePathObject.absoluteFilePath);
		$('.btn-view-screen-file').show();
	}
	if ($(changeFileElement).text() === 'Select File') {
		$(changeFileElement).text('Change File');
	}
}
function askForMergingFiles(changedFiles, fileType, changeFileElement) {
	return new Promise((resolve, reject) => {
		swal.fire({
			type: "info",
			title: "Multiple files were selected, do you wish to merge all these files into a single file ?",
			text: "Merge and Proceed ?",
			showCancelButton: true,
			cancelButtonText: "Cancel",
			showConfirmButton: true,
			confirmButtonText: "Merge Now"
		}).then((res) => {
			resolve(res);
		});
	})
}
function mergeSelectedFiles(changedFiles, fileType, changeFileElement) {
	return new Promise((resolve, reject) => {
		showLoader();
		const fileExtension = getMergingOutputFileExtension(changedFiles);
		if (fileExtension.isSimilar === true) {
			// ? init required params
			let filePath = sessionStorage.getItem('configFolderPath');
			filePath = path.normalize(filePath);
			let basepath = app.getAppPath('exe');
			basepath = basepath.replace("app.asar", "")
			basepath = basepath.replace(/\\/g, "\\\\");
			basepath += "\\";
			if (!fs.existsSync(path.join(filePath, fileType))) {
				fs.mkdirSync(path.join(filePath, fileType));
			}
			const newFileName = new Date().getTime().toString();
			// ? init background process
			const { fork } = require('child_process');
			let mergeVideoFilesProcess = path.join(__dirname, 'background-process.js');
			forked = fork(mergeVideoFilesProcess);
			const msg = {
				message: 'mergeVideoFilesProcess',
				changedFiles,
				filePath,
				basepath,
				extension: fileExtension.extension,
				fileType,
				newFileName
			};
			forked.send(msg);
			forked.on('message', (msg) => {
				if (msg.processName === 'mergeVideoFilesProcess') {
					hideLoader();
					if (msg.type === 'error') {
						reject();
						swal({
							type: 'error',
							title: 'Error',
							text: 'Something went wrong'
						});
					} else if (msg.type === 'result') {
						// ? now we can call generateWaveformHelper
						// NOTE -> it requires object with absolte as well as relativepath of new file
						// ? filePathObject is required for generateWaveformHelper
						// ? which will be called on successfull merging of files
						const filePathObject = {
							relativeFilePath: (fileType + '/' + newFileName + '.' + fileExtension.extension),
							absoluteFilePath: msg.mergeResult.mergedFilePath
						};
						resolve(filePathObject);
					}
					forked.kill('SIGINT');
				}
			});
		} else {
			swal.fire({
				type: 'error',
				title: 'All files must of similar type'
			});
			hideLoader();
			reject();
		}
	});
}
function getMergingOutputFileExtension(changedFiles) {
	let allExtensions = [];
	changedFiles.forEach(files => {
		allExtensions.push(files.split('.')[files.split('.').length - 1]);
	});
	const set = new Set(allExtensions);
	if (set.size === 1) {
		// ! patch
		// NOTE -> this is done because MTS videos cannot be played directly in HTML
		if (allExtensions[0] === 'MTS') {
			allExtensions[0] = 'mp4';
		}
		return { isSimilar: true, extension: allExtensions[0] };
	} else {
		return { isSimilar: false };
	}
}
function updateCurrentRecItem(saveButtonElement) {
	const newMode = $('#change-rec-dropdown').attr('rectype');
	const currentFinalMergerIndex = $(saveButtonElement).data('currentFinalMergerIndex');
	config.finalMerger[currentFinalMergerIndex].type = newMode;
	let fileSelected = true;
	if (newMode === 'camera') {
		const newCameraFile = $(".btn-change-camera-file").data('relative-file-path');
		const newCutStartTime = $('.camera-file-start-time').val();
		const newCutEndTime = $('.camera-file-end-time').val();
		config.finalMerger[currentFinalMergerIndex].cameraCutStartTime = newCutStartTime;
		config.finalMerger[currentFinalMergerIndex].cameraCutEndTime = newCutEndTime;
		config.finalMerger[currentFinalMergerIndex].cameraCutStartTimeSeconds = getSecondsFromHMS(newCutStartTime).floatSeconds;
		config.finalMerger[currentFinalMergerIndex].cameraCutEndTimeSeconds = getSecondsFromHMS(newCutEndTime).floatSeconds;
		config.finalMerger[currentFinalMergerIndex].cpath = newCameraFile;
		config.finalMerger[currentFinalMergerIndex].externalCameraFileDuration = $(".btn-change-camera-file").data('file-duration');
		config.finalMerger[currentFinalMergerIndex].cameraWaveFilePath = $(".camera-waveform-img").attr('src');
		if (!newCameraFile) {
			fileSelected = false;
		}
	}
	if (newMode === 'screen') {
		const newCutStartTime = $('.screen-file-start-time').val();
		const newCutEndTime = $('.screen-file-end-time').val();
		config.finalMerger[currentFinalMergerIndex].screenCutStartTime = newCutStartTime;
		config.finalMerger[currentFinalMergerIndex].screenCutEndTime = newCutEndTime;
		config.finalMerger[currentFinalMergerIndex].screenCutStartTimeSeconds = getSecondsFromHMS(newCutStartTime).floatSeconds;
		config.finalMerger[currentFinalMergerIndex].screenCutEndTimeSeconds = getSecondsFromHMS(newCutEndTime).floatSeconds;
		const newScreenFile = $(".btn-change-screen-file").data('relative-file-path');
		config.finalMerger[currentFinalMergerIndex].exteralScreenFileDuration = $(".btn-change-screen-file").data('file-duration');
		config.finalMerger[currentFinalMergerIndex].cpath = newScreenFile;
		config.finalMerger[currentFinalMergerIndex].screenWaveFilePath = $(".screen-waveform-img").attr('src');
		if (!newScreenFile) {
			fileSelected = false;
		}
	}
	if (newMode === 'both') {
		let newCutStartTime = $('.camera-file-start-time').val();
		let newCutEndTime = $('.camera-file-end-time').val();
		config.finalMerger[currentFinalMergerIndex].cameraCutStartTime = newCutStartTime;
		config.finalMerger[currentFinalMergerIndex].cameraCutEndTime = newCutEndTime;
		config.finalMerger[currentFinalMergerIndex].cameraCutStartTimeSeconds = getSecondsFromHMS(newCutStartTime).floatSeconds;
		config.finalMerger[currentFinalMergerIndex].cameraCutEndTimeSeconds = getSecondsFromHMS(newCutEndTime).floatSeconds;
		newCutStartTime = $('.screen-file-start-time').val();
		newCutEndTime = $('.screen-file-end-time').val();
		config.finalMerger[currentFinalMergerIndex].screenCutStartTime = newCutStartTime;
		config.finalMerger[currentFinalMergerIndex].screenCutEndTime = newCutEndTime;
		config.finalMerger[currentFinalMergerIndex].screenCutStartTimeSeconds = getSecondsFromHMS(newCutStartTime).floatSeconds;
		config.finalMerger[currentFinalMergerIndex].screenCutEndTimeSeconds = getSecondsFromHMS(newCutEndTime).floatSeconds;
		const newCameraFile = $(".btn-change-camera-file").data('relative-file-path');
		const newScreenFile = $(".btn-change-screen-file").data('relative-file-path');
		config.finalMerger[currentFinalMergerIndex].cpath = newCameraFile;
		config.finalMerger[currentFinalMergerIndex].spath = newScreenFile;
		config.finalMerger[currentFinalMergerIndex].externalCameraFileDuration = $(".btn-change-camera-file").data('file-duration');
		config.finalMerger[currentFinalMergerIndex].externalScreenFileDuration = $(".btn-change-screen-file").data('file-duration');
		config.finalMerger[currentFinalMergerIndex].cameraWaveFilePath = $(".camera-waveform-img").attr('src');
		config.finalMerger[currentFinalMergerIndex].screenWaveFilePath = $(".screen-waveform-img").attr('src');
		if (!newCameraFile || !newScreenFile) {
			fileSelected = false;
		}
	}
	const validity = checkCutTimingValidity(config.finalMerger[currentFinalMergerIndex]);
	if (fileSelected === false || validity.isTimeValid === false) {
		let title = 'File not selected';
		let text = 'Please select file before saving';
		if (validity.isTimeValid === false) {
			title = 'Invalid Timings';
			text = validity.timeInvalidReason;
		}
		swal.fire({
			type: 'info',
			title,
			text
		});
	} else {
		saveSettings();
		saveProject();
		closeRecItemSettingsModal();
		window.location.reload();
	}
}
function changeCurrentRecItemMode(selectElement) {
	const newMode = $(selectElement).attr('rectype');
	$('#change-rec-dropdown').text(selectElement.text);
	$('#change-rec-dropdown').attr('rectype', newMode);
	if (newMode === 'camera') {
		$('.camera-section').show();
		$('.screen-section').hide();
	}
	if (newMode === 'screen') {
		$('.camera-section').hide();
		$('.screen-section').show();
	}
	if (newMode === 'both') {
		$('.camera-section').show();
		$('.screen-section').show();
	}
	const cameraFilePath = $('.btn-view-camera-file').data('filepath');
	const screenFilePath = $('.btn-view-screen-file').data('filepath');
	if (cameraFilePath === '') {
		$('.btn-view-camera-file').hide();
		$('.btn-change-camera-file').text('Select File');
	}
	if (screenFilePath === '') {
		$('.btn-view-screen-file').hide();
		$('.btn-change-screen-file').text('Select File');
	}
}
/**
 * Function copies newFile arg and returns object
 * with realtive as well as absolute path to newly copied file
 * @param {} fileType
 * @param {*} newFile
 */
function copyNewFileToProject(fileType, newFile) {
	return new Promise((resolve, reject) => {
		let filePath = sessionStorage.getItem('configFolderPath');
		const extension = newFile.split('.')[newFile.split('.').length - 1];
		if (!fs.existsSync(path.join(filePath, fileType))) {
			fs.mkdirSync(path.join(filePath, fileType));
		}
		// const newFileRelativePath = path.join(fileType, ('external-' + fileType + '-' + new Date().getTime() + '.' + extension));
		const newFileRelativePath = fileType + '/' + ('external-' + new Date().getTime() + '.' + extension);
		if (filePath) {
			filePath = path.join(filePath, newFileRelativePath);
			showLoader();
			fs.copyFile(newFile, filePath, (err) => {
				if (err) {
					reject();
					throw err;
				}
				hideLoader();
				resolve({
					relativeFilePath: newFileRelativePath,
					absoluteFilePath: filePath
				});
			});
		}
	});
}
function showLoader() {
	$('.loader').show();
	// ? disable mouse events while loader is loading
	$('body').css('pointer-events', 'none');
}
function hideLoader() {
	$('body').css('pointer-events', 'all');
	$('.loader').hide();
}
function secondsToHms(timeInSeconds) {
	const pad = function (num, size) { return ('000' + num).slice(size * -1); },
		time = parseFloat(timeInSeconds).toFixed(3),
		hours = Math.floor(time / 60 / 60),
		minutes = Math.floor(time / 60) % 60,
		seconds = Math.floor(time - minutes * 60);
	const milli = ((timeInSeconds - parseInt(timeInSeconds)) * 1000).toFixed(0);
	return pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2) + ':' + milli;
}
async function getFileDuration(filePath) {
	return new Promise((resolve, reject) => {
		let video = document.createElement('video');
		video.preload = 'metadata';
		video.onloadedmetadata = function () {
			window.URL.revokeObjectURL(video.src);

			resolve(video.duration);
		}
		video.onerror = function () {
			window.URL.revokeObjectURL(video.src);
			reject();
		}
		// video.src = URL.createObjectURL(filePath);
		video.src = filePath;
	});
}
function checkCutTimingValidity(currentRecItem) {
	let isTimeValid = true;
	let timeInvalidReason = '';
	const totalDuration = currentRecItem.endTime - currentRecItem.startTime;
	const actualStartTime = 0;
	const actualEndTime = totalDuration;
	if (currentRecItem.type === 'camera') {
		let actualCameraEndTime = actualEndTime;
		const cameraStartSeconds = getSecondsFromHMS(currentRecItem.cameraCutStartTime).seconds;
		const cameraEndSeconds = getSecondsFromHMS(currentRecItem.cameraCutStartTime).seconds;
		const camDuration = (cameraEndSeconds - cameraStartSeconds);
		if (currentRec.externalCameraFileDuration) {
			actualCameraEndTime = currentRec.externalCameraFileDuration;
		}
		if ((camDuration > actualCameraEndTime) || isNaN(camDuration)) {
			isTimeValid = false;
			timeInvalidReason = 'Camera timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualCameraEndTime);
		} else if (cameraStartSeconds >= actualCameraEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Camera timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualCameraEndTime);
		} else if (cameraEndSeconds < cameraStartSeconds || cameraEndSeconds > actualCameraEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Camera timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualCameraEndTime);
		}
	}
	if (currentRecItem.type === 'screen') {
		const screenStartSeconds = getSecondsFromHMS(currentRecItem.screenCutStartTime).seconds;
		const screenEndSeconds = getSecondsFromHMS(currentRecItem.screenCutEndTime).seconds;
		const screenDuration = (screenEndSeconds - screenStartSeconds);
		let actualScreenEndTime = actualEndTime;
		if (currentRec.externalScreenFileDuration) {
			actualScreenEndTime = currentRec.externalScreenFileDuration;
		}
		if ((screenDuration > actualScreenEndTime) || isNaN(screenDuration)) {
			isTimeValid = false;
			timeInvalidReason = 'Screen timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualScreenEndTime);
		} else if (screenStartSeconds >= actualScreenEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Screen timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualScreenEndTime);
		} else if (screenEndSeconds < screenStartSeconds || screenEndSeconds > actualScreenEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Screen timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualScreenEndTime);
		}
	}
	if (currentRecItem.type === 'both') {
		let actualCameraEndTime = actualEndTime;
		let actualScreenEndTime = actualEndTime;
		// NOTE -> if file was added externally
		// ? then we have to use different duration which was specified
		// ? when file was selected for this mode
		// ? can be accessed using a) externalCameraFileDuration and b) externalScreenFileDuration
		if (currentRec.externalCameraFileDuration) {
			actualCameraEndTime = currentRec.externalCameraFileDuration;
		}
		if (currentRec.externalScreenFileDuration) {
			actualScreenEndTime = currentRec.externalScreenFileDuration;
		}
		const cameraStartSeconds = getSecondsFromHMS(currentRecItem.cameraCutStartTime).seconds;
		const cameraEndSeconds = getSecondsFromHMS(currentRecItem.cameraCutEndTime).seconds;
		// NOTE ->
		// ? while comparing we convert to int, so that we dont have any problems while
		// ? checking time is valid or not, float is needed for cutting with milliseconds
		const camDuration = (cameraEndSeconds - cameraStartSeconds);
		const screenStartSeconds = getSecondsFromHMS(currentRecItem.screenCutStartTime).seconds;
		const screenEndSeconds = getSecondsFromHMS(currentRecItem.screenCutEndTime).seconds;
		const screenDuration = parseInt(screenEndSeconds - screenStartSeconds);
		if (cameraStartSeconds >= actualCameraEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Camera start timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualCameraEndTime);
		} else if (cameraEndSeconds < cameraStartSeconds || cameraEndSeconds > actualCameraEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Camera end timing must be greater than start timing and within total duration of actual video, total duration is : ' + secondsToHms(actualCameraEndTime);
		} else if (screenStartSeconds >= actualScreenEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Screen start timing must be within total duration of actual video, total duration is : ' + secondsToHms(actualScreenEndTime);
		} else if (screenEndSeconds < screenStartSeconds || screenEndSeconds > actualScreenEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Screen end timing must be greater than start timing and within total duration of actual video, total duration is : ' + secondsToHms(actualScreenEndTime);
		} else if (camDuration > actualCameraEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Duration of camera file must be less than or equal to total duration :' + secondsToHms(actualCameraEndTime);
		} else if (screenDuration > actualScreenEndTime) {
			isTimeValid = false;
			timeInvalidReason = 'Duration of screen file must be less than or equal to total duration :' + secondsToHms(actualScreenEndTime);
		} else if (camDuration !== screenDuration) {
			isTimeValid = false;
			timeInvalidReason = 'Duration of camera and screen files must be equal';
		}
		if (isNaN(screenDuration) || isNaN(camDuration)) {
			isTimeValid = false;
			timeInvalidReason = 'Duration of camera or screen file must in be proper h:m:s:milli format';
		}
	}
	return { isTimeValid, timeInvalidReason };
}
function getSecondsFromHMS(hms) {
	const a = hms.split(':'); // split it at the colons
	const time = { seconds: 0, milliseconds: 0, floatSeconds: 0 };
	// minutes are worth 60 seconds. Hours are worth 60 minutes.
	const seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
	if (isNaN(seconds)) {
		time.seconds = NaN;
	} else {
		time.seconds = seconds;
	}
	time.milliseconds = a[3];
	time.floatSeconds = parseFloat(time.seconds + '.' + time.milliseconds);
	return time;
}
function initVideoWaveforms() {
	generatingWaveformsCounter = 0;
	for (let i = 0; i < config.finalMerger.length; i++) {
		let item = config.finalMerger[i];
		if (item.type == 'both') {
			if (item.screenWaveFilePath) {
				// ? remove ?v= is present in screenWaveFilePath
				item.screenWaveFilePath = item.screenWaveFilePath.split("?v=")[0];
			}
			if (!(item.screenWaveFilePath && fs.existsSync(item.screenWaveFilePath))) {
				generatingWaveformsCounter++;
				let screenDuration = item.endTime - item.startTime;
				if (item.externalScreenFileDuration) {
					screenDuration = item.externalScreenFileDuration;
				}
				const videoFilePath = path.join(paramObj.configFolderPath, item.spath);
				((recItem) => {
					const outputFilePath = path.join(paramObj.configFolderPath, 'waveforms', ('screen-wave-form-' + new Date().getTime().toString() + '.png'));
					try {
						fs.mkdirSync(path.join(paramObj.configFolderPath, 'waveforms'));
					} catch (exp) {

					}
					generateWaveformFromVideo(videoFilePath, screenDuration, outputFilePath).then((waveFormPath) => {
						recItem.screenWaveFileDuration = screenDuration;
						if (fs.existsSync(waveFormPath)) {
							recItem.screenWaveFilePath = waveFormPath;
						} else {
							recItem.screenWaveFilePath = null;
						}
					}).catch(() => {
						recItem.screenWaveFilePath = null;
						recItem.screenWaveFileDuration = screenDuration;
					});
				})(item);
			}
			if (item.cameraWaveFilePath) {
				// ? remove ?v= is present in cameraWaveFilePath
				item.cameraWaveFilePath = item.cameraWaveFilePath.split("?v=")[0];
			}
			if (!(item.cameraWaveFilePath && fs.existsSync(item.cameraWaveFilePath))) {
				generatingWaveformsCounter++;
				let cameraDuration = item.endTime - item.startTime;
				if (item.externalCameraFileDuration) {
					cameraDuration = item.externalCameraFileDuration;
				}
				const videoFilePath = path.join(paramObj.configFolderPath, item.cpath);
				((recItem) => {
					const outputFilePath = path.join(paramObj.configFolderPath, 'waveforms', ('camera-wave-form-' + new Date().getTime().toString() + '.png'));
					try {
						fs.mkdirSync(path.join(paramObj.configFolderPath, 'waveforms'));
					} catch (exp) {

					}
					generateWaveformFromVideo(videoFilePath, cameraDuration, outputFilePath).then((waveFormPath) => {
						recItem.cameraWaveFileDuration = cameraDuration;
						if (fs.existsSync(waveFormPath)) {
							recItem.cameraWaveFilePath = waveFormPath;
						} else {
							recItem.cameraWaveFilePath = null;
						}
					}).catch((err) => {
						recItem.cameraWaveFileDuration = cameraDuration;
						recItem.cameraWaveFilePath = null;
					});
				})(item);
			}
		} else if (item.type === 'camera') {
			if (item.cameraWaveFilePath) {
				// ? remove ?v= is present in cameraWaveFilePath
				item.cameraWaveFilePath = item.cameraWaveFilePath.split("?v=")[0];
			}
			if (!(item.cameraWaveFilePath && fs.existsSync(item.cameraWaveFilePath))) {
				generatingWaveformsCounter++;
				let cameraDuration = item.endTime - item.startTime;
				if (item.externalCameraFileDuration) {
					cameraDuration = item.externalCameraFileDuration;
				}
				const videoFilePath = path.join(paramObj.configFolderPath, item.cpath);
				((recItem) => {
					const outputFilePath = path.join(paramObj.configFolderPath, 'waveforms', ('camera-wave-form-' + new Date().getTime().toString() + '.png'));
					try {
						fs.mkdirSync(path.join(paramObj.configFolderPath, 'waveforms'));
					} catch (exp) {

					}
					generateWaveformFromVideo(videoFilePath, cameraDuration, outputFilePath).then((waveFormPath) => {
						recItem.cameraWaveFileDuration = cameraDuration;
						if (fs.existsSync(waveFormPath)) {
							recItem.cameraWaveFilePath = waveFormPath;
						} else {
							recItem.cameraWaveFilePath = null;
						}
					}).catch((err) => {
						recItem.cameraWaveFileDuration = cameraDuration;
						recItem.cameraWaveFilePath = null;
					});
				})(item);
			}
		}
		else if (item.type === 'screen') {
			if (item.screenWaveFilePath) {
				// ? remove ?v= is present in screenWaveFilePath
				item.screenWaveFilePath = item.screenWaveFilePath.split("?v=")[0];
			}
			if (!(item.screenWaveFilePath && fs.existsSync(item.screenWaveFilePath))) {
				generatingWaveformsCounter++;
				let screenDuration = item.endTime - item.startTime;
				if (item.externalScreenFileDuration) {
					screenDuration = item.externalScreenFileDuration;
				}
				const videoFilePath = path.join(paramObj.configFolderPath, item.cpath);
				((recItem) => {
					const outputFilePath = path.join(paramObj.configFolderPath, 'waveforms', ('screen-wave-form-' + new Date().getTime().toString() + '.png'));
					try {
						fs.mkdirSync(path.join(paramObj.configFolderPath, 'waveforms'));
					} catch (exp) {

					}
					generateWaveformFromVideo(videoFilePath, screenDuration, outputFilePath).then((waveFormPath) => {
						recItem.screenWaveFileDuration = screenDuration;
						if (fs.existsSync(waveFormPath)) {
							recItem.screenWaveFilePath = waveFormPath;
						} else {
							recItem.screenWaveFilePath = null;
						}
					}).catch(() => {
						recItem.screenWaveFileDuration = screenDuration;
						recItem.screenWaveFilePath = null;
					});
				})(item);
			}
		}
	}
}
function generateWaveformFromVideo(videoFilePath, duration, outputFilePath) {
	showLoader();
	return new Promise((resolve, reject) => {
		const { fork } = require('child_process');//
		let backgroundWaveformProcess = path.join(__dirname, 'background-process.js');
		let basepath = app.getAppPath('exe');
		basepath = basepath.replace("app.asar", "")
		basepath = basepath.replace(/\\/g, "\\\\");
		basepath += "\\";
		forked = fork(backgroundWaveformProcess);
		const msg = {
			message: 'generateWaveform',
			videoFilePath,
			waveformPictureWidth: duration * env.pixelsPerSecond,
			waveformPictureHeight: env.waveformPictureHeight,
			outputFilePath,
			basepath
		};
		forked.send(msg);
		forked.on('message', (msg) => {
			if (msg.processName === 'generateWaveform') {
				generatingWaveformsCounter--;
				if (generatingWaveformsCounter === 0) {
					hideLoader();
				}
				if (msg.type === 'error') {
					reject();
					swal({
						type: 'error',
						title: 'Error',
						text: 'Something went wrong'
					});
				} else {
					resolve(msg.result.outputFilePath);
				}
			}
		});
	});
}
function waveformPinDragged(left, draggedPinType) {
	const seconds = getSecondsFromLeftPx(left);
	const hms = secondsToHms(seconds);
	const isInDuration = isLeftWithinDuration(draggedPinType, left);
	if (isInDuration) {
		switch (draggedPinType) {
			case 'camera-start':
				$('.camera-file-start-time').val(hms);
				break;
			case 'camera-end':
				$('.camera-file-end-time').val(hms);
				break;
			case 'screen-start':
				$('.screen-file-start-time').val(hms);
				break;
			case 'screen-end':
				$('.screen-file-end-time').val(hms);
				break;
			default:
				break;
		}
	} else {
		switch (draggedPinType) {
			case 'camera-start':
				$('.waveform-pin-camera-start').css('left', '0px');
				break;
			case 'camera-end':
				$('.waveform-pin-camera-end').css('left', '0px');
				break;
			case 'screen-start':
				$('.waveform-pin-screen-start').css('left', '0px');
				break;
			case 'screen-end':
				$('.waveform-pin-screen-end').css('left', '0px');
				break;
			default:
				break;
		}
	}
}
function getSecondsFromLeftPx(leftPx) {
	leftPx = leftPx.replace('px', '');
	leftPx = parseInt(leftPx, 10) / (env.pixelsPerSecond);
	return leftPx;
}
function waveformImageClicked(event, imageElement) {
	//
}
function isLeftWithinDuration(elementType, left) {
	let leftWithoutPx = parseInt(left.replace('px', ''));
	let isWithinDuration = true;
	let widthWithoutPx = 0;
	if (elementType === 'camera-start' || elementType === 'camera-end') {
		const width = $('.camera-waveform-img').css('width');
		widthWithoutPx = parseInt(width.replace('px', ''));
	} else if (elementType === 'screen-start' || elementType === 'screen-end') {
		const width = $('.screen-waveform-img').css('width');
		widthWithoutPx = parseInt(width.replace('px', ''));
	}
	if (leftWithoutPx > widthWithoutPx) {
		isWithinDuration = false;
	}
	return isWithinDuration;
}
function timeInputChanged(inputElement) {
	const elementType = $(inputElement).attr('timetype');
	const seconds = getSecondsFromHMS(inputElement.value);
	if (!isNaN(seconds.seconds)) {
		const left = (seconds.seconds * env.pixelsPerSecond) + 'px';
		const isInDuration = isLeftWithinDuration(elementType, left);
		if (isInDuration) {
			switch (elementType) {
				case 'camera-start':
					$('.waveform-pin-camera-start').css('left', left);
					break;
				case 'camera-end':
					$('.waveform-pin-camera-end').css('left', left);
					break;
				case 'screen-start':
					$('.waveform-pin-screen-start').css('left', left);
					break;
				case 'screen-end':
					$('.waveform-pin-screen-end').css('left', left);
					break;
			}
		}
	}
}
function selectFilesToReprocess(selectFileBtn) {
	const fileSelectedType = $(selectFileBtn).attr('rectype');
	let selectedFiles = dialog.showOpenDialog(win, {
		filters: [
			{ name: 'Videos', extensions: ['mp4', 'mkv', 'avi', '3gp', 'mov', 'mts'] },
		],
		properties: ['openFile', 'multiSelections']
	});
	if (selectedFiles) {
		if (selectedFiles.length > 1) {
			askForMergingFiles(selectedFiles, fileSelectedType, null).then((res) => {
				if (res.value) {
					mergeSelectedFiles(selectedFiles, fileSelectedType, null).then((filePathObject) => {
						selectFilesToReprocessHelper(filePathObject.absoluteFilePath, fileSelectedType);
					});
				}
			});
		} else {
			const extension = path.extname(selectedFiles[0]);
			if (extension.toLowerCase() === '.mts' || extension.toLowerCase() === 'mts') {
				const newFileName = new Date().getTime().toString();
				let folderPath = sessionStorage.getItem('configFolderPath');
				// NOTE -> folderpath is not defined in case when creating new recording
				if (!folderPath) {
					folderPath = path.join(env.recorderFilesPath, 'recording_' + new Date().getTime().toString());
					fs.mkdirSync(folderPath);
					fs.mkdirSync(path.join(folderPath, fileSelectedType));
				}
				const newFilePath = path.join(folderPath, fileSelectedType, newFileName + '.mp4');
				convertFileToMp4(selectedFiles[0], newFilePath).then((filePath) => {
					selectFilesToReprocessHelper(filePath, fileSelectedType);
				}).catch((err) => {
					showSimpleAlert('error', 'Could not convert file');
				});
			} else {
				const selectedFile = selectedFiles[0];
				selectFilesToReprocessHelper(selectedFile, fileSelectedType);
			}
		}
	}
}
function selectFilesToReprocessHelper(selectedFile, fileSelectedType) {
	if (selectFilesToReprocessMode === 'create-new-recording') {
		createEmptyRecording(fileSelectedType, selectedFile);
	} else if (selectFilesToReprocessMode === 'add-to-current-recording') {
		// ? hardcode consider this as camera recording,
		// ? because this should not always be a camera or screen or both
		// ? main purpose of this is to add introduction videos
		addEmptyRecordingToCurrentProject('camera', selectedFile);
	}
}
function createEmptyRecording(recordingType, selectedFile) {
	// ? STEP - 1 ->  create config folder
	const newProjectFolderName = 'recording_' + new Date().getTime();
	const folderPath = path.join(documentsFolderPath, newProjectFolderName);
	const selectedFileExtension = selectedFile.split('.')[selectedFile.split('.').length - 1];
	fs.mkdirSync(folderPath);
	if (fs.existsSync(folderPath)) {
		// ? STEP 1.1 -> create camera/screen folder
		fs.mkdirSync(path.join(folderPath, recordingType));
		if (fs.existsSync(path.join(folderPath, recordingType))) {
			// ? STEP 2 -> copy selected file to project folder
			const newFileName = newProjectFolderName + '.' + selectedFileExtension;
			const newFilePath = path.join(folderPath, recordingType, newFileName);
			showLoader();
			fs.copyFile(selectedFile, newFilePath, (err) => {
				if (err) {
					throw err;
				}
				hideLoader();
				if (fs.existsSync(newFilePath)) {
					// ? STEP 3 - create config object
					const relativeFilePath = recordingType + '/' + newFileName;
					const configObj = {
						finalMerger: [{
							type: recordingType,
							cpath: relativeFilePath
						}]
					};
					getFileDuration(newFilePath).then((duration) => {
						if (!isNaN(duration) && duration != Infinity) {
							configObj.finalMerger[0].startTime = 0;
							configObj.finalMerger[0].endTime = duration;
							// ? STEP - 4 - write config object to settings.json file
							const configFilePath = path.join(folderPath, 'settings.config');
							fs.writeFileSync(configFilePath, JSON.stringify(configObj), 'utf-8');
							// ? STEP - 5 - select this recording and reload output process window
							// ? so as to load this as a recording project
							// ! check if commenting sessioStorage.clear() causes any issues
							// sessionStorage.clear();
							sessionStorage.setItem("configFolderPath", folderPath);
							sessionStorage.setItem("sessionFor", "process-config-folder");
							electron.remote.getCurrentWindow().setResizable(true);
							window.location.reload();
						} else {
							swal.fire({
								type: 'error',
								title: 'Cannot Load File',
								text: 'This file appears to be corrupted'
							});
						}
					});
				} else {
					throw 'could not create config folder';
				}
			});
		} else {
			throw 'could not create recording folder folder';
		}
	} else {
		throw 'could not create folder';
	}
}
function checkIfAllFilesExist() {
	return new Promise(async (resolve, reject) => {
		let allOk = true;
		// ? check config folder exists
		if (config.configFolderPath !== 'false') {
			let tmpPath = decodeURI(config.configFolderPath);
			if (fs.existsSync(tmpPath) === false) {
				await showSimpleAlert('error', 'File Error', 'Recording folder does not exist on specified location');
				allOk = false;
			}
		}
		// ? check background image path
		if (config.backImgPath !== 'false') {
			let tmpPath = decodeURI(config.backImgPath);
			if (fs.existsSync(tmpPath) === false) {
				await showSimpleAlert('error', 'File Error', 'Background Image file does not exist in specified location !, Please select background image and try again');
				allOk = false;
			}
		}
		// ? check Logo image path
		if (config.logoImgPath && config.logoImgPath !== 'false') {
			let tmpPath = decodeURI(config.logoImgPath);
			if (fs.existsSync(tmpPath) === false) {
				await showSimpleAlert('error', 'File Error', 'Logo Image file does not exist in specified location !, Please select Logo Image and try again');
				allOk = false;
			}
		}
		// ? check final Merger array
		if (!config.finalMerger || config.finalMerger.length === 0) {
			await showSimpleAlert('error', 'File Error', 'Could not fetch Recording List, the config file is corrupted !');
			allOk = false;
		}
		// ? check final Merger files exist or not
		for (let i = 0; i < config.finalMerger.length; i++) {
			const rec = config.finalMerger[i];
			let indexPrefix = 'th';
			switch ((i + 1)) {
				case 1:
					indexPrefix = 'st'
					break;
				case 2:
					indexPrefix = 'nd'
					break;
				case 3:
					indexPrefix = 'rd'
					break;
				default:
					indexPrefix = 'th'
					break;
			}
			if (rec.type === 'both') {
				const cameraFilePath = getCurrentCameraFilePath(rec);
				const screenFilePath = getCurrentScreenFilePath(rec);
				if (!fs.existsSync(cameraFilePath)) {
					await showSimpleAlert('error', 'File Error', (i + 1) + indexPrefix + ' Camera Recording file does not exist. Please place the file in correct location');
					allOk = false;
				}
				if (!fs.existsSync(screenFilePath)) {
					await showSimpleAlert('error', 'File Error', (i + 1) + indexPrefix + 'th Screen Recording file does not exist. Please place the file in correct location');
					allOk = false;
				}
			} else if (rec.type === 'camera' || rec.type === 'screen') {
				const filePath = getCurrentCameraFilePath(rec);
				if (!fs.existsSync(filePath)) {
					await showSimpleAlert('error', 'File Error', (i + 1) + + indexPrefix + rec.type.toUpperCase() + ' Recording file does not exist. Please place the file in correct location');
					allOk = false;
				}
			}
		}
		resolve(allOk);
	});
}
function showSimpleAlert(type, title, text) {
	return swal({
		type,
		title,
		text
	});
}
function initSideNav() {
	// ? options side nav width should be equal to width of left side nav
	$('#options-side-nav').slideReveal({
		trigger: $(".options-side-nav-trigger"),
		position: 'right',
		width: '20%',
		push: false
	});
}
function openPropertiesPanel(panelItemName) {
	$('.properties-panel-item').hide();
	if (panelItemName === 'green-screen') {
		handleOpenGreenScreenProperties();
	}
	if (panelItemName === 'scrolling-text') {
		handleOpenScrollingTextProperties();
	}
	if (panelItemName === 'logo-options') {
		handleOpenLogoOptions();
	}
	if (panelItemName === 'adjust-brightness') {
		handleOpenBrightnessOptions();
	}
	if (panelItemName === 'nothing') {
		$('.properties-panel-item').hide();
	}
}
function handleOpenGreenScreenProperties() {
	$('.transparency-options').show();
	// ? taken straight from toggleTransparency options
	const item = config.finalMerger[currentFinalMergerIndex];
	$(".a-transparency-option").hide();
	$(".example-hex-color").hide();
	$("#hex-color").val("");
	if (item.transparencyCheck) {
		$(".a-transparency-option").show();
		$(".example-hex-color").show();
		if (item.transparencyCheck == true) {
			$('.transparency-check').prop('checked', true);
			$('.a-transparency-option').show();
			$(".example-hex-color").show();
			$(".default-transparency-options").show();
			if (item.colorHex) {
				let hash = ""
				if (item.colorHex[0] != "#") {
					hash = "#";
				} else {
					hash = "";
				}
				$("#hex-color").val(hash + item.colorHex);
			} else {
				item.colorHex = "";
			}
			if (item.transparencyOpacity) {
				$("#opacity-value").val((item.transparencyOpacity * 100).toFixed(0) + "%");
				$("#opacity-range-slider").val((item.transparencyOpacity * 100).toFixed(0));
			} else {
				$("#opacity-value").val("0%");
				$("#opacity-range-slider").val(0);
				item.transparencyOpacity = 0;
			}
			if (item.transparencySimilarity) {
				$("#similarity-range-slider").val((item.transparencySimilarity * 100).toFixed(0));
				$("#similarity-value").val((item.transparencySimilarity * 100).toFixed(0) + "%");
			} else {
				$("#similarity-range-slider").val(10);
				$("#similarity-value").val(("10%"));
				item.transparencySimilarity = 0.1;
			}
			isHexOk();
		} else {
			$('.transparency-check').prop('checked', false);
			$("#transparency-options-inner").hide();
			$(".default-transparency-options").hide();
			isHexOk();
		}
	} else {
		$("#opacity-value").val("0%");
		$("#opacity-range-slider").val(0);
		item.transparencyOpacity = 0;
		$("#similarity-range-slider").val(10);
		$("#similarity-value").val(("10%"));
		item.transparencySimilarity = 0.1;
		$('.transparency-check').prop('checked', false);
		$("#transparency-options-inner").hide();
		$(".default-transparency-options").hide();
		isHexOk();
	}
}
function isSimilarityOk(value) {
	return !isNaN(parseInt(value.replace('%')));
}
function isOpacityOk(value) {
	return !isNaN(parseInt(value.replace('%')));
}
function handleSimilarityChange(element, isChangeFromInput) {
	let isOk = true;
	if (isChangeFromInput === true) {
		isOk = isSimilarityOk(element.value);
	}
	if (isOk) {
		let value = null;
		if (isChangeFromInput === false) {
			$("#similarity-value").val((element.value) + "%");
			value = element.value;
		} else {
			value = parseInt(element.value.replace('%'));
			$("#similarity-range-slider").val(value);
		}
		transparencySimilarity = (parseInt(value) / 100).toFixed(2);
		currentRec.transparencySimilarity = (parseInt(value) / 100).toFixed(2);
		$(element).removeClass('invalid-similarity');
	} else {
		$(element).addClass('invalid-similarity');
	}
}
function handleOpacityChange(element, isChangeFromInput) {
	const isOk = isOpacityOk(element.value);
	if (isOk) {
		let value = null;
		if (isChangeFromInput === false) {
			$("#opacity-value").val((element.value) + "%");
			value = element.value;
		} else {
			value = parseInt(element.value.replace('%'));
			$("#opacity-range-slider").val(value);
		}
		transparencyOpacity = (parseInt(value) / 100).toFixed(2);
		currentRec.transparencyOpacity = (parseInt(value) / 100).toFixed(2);
		$(element).removeClass('invalid-opacity');
	} else {
		$(element).addClass('invalid-opacity');
	}
}
function resetRecItemIndexes() {
	$('.rec-item-wrapper').each((index, elem) => {
		$(elem).find('.rec-item-sr-no').text((index + 1));
	});
}
function sortingOver(eve, ui) {
	resetRecItemIndexes();
}
function addSortableToRecList() {
	$(".recording-list-items").sortable({
		containment: $('.recording-list'),
		update: sortingOver
	});
}
function removeSelectedRecItem() {
	swal({
		title: 'Delete Recording',
		text: 'This will delete the recording ! Are you sure ?',
		showCancelButton: true,
		confirmButtonText: 'Cancel',
		cancelButtonText: 'Delete'
	}).then((res) => {
		if (res.dismiss === "cancel") {
			removeSelectedRecItemNow();
		}
	});
}
function removeSelectedRecItemNow() {
	saveSettings();
	deleteFinalMergerItemFiles();
	config.finalMerger.splice(currentFinalMergerIndex, 1);
	saveProject();
	if (config.finalMerger.length === 0) {
		backToHome();
	} else {
		window.location.reload();
	}
}
function deleteFinalMergerItemFiles() {
	const cameraFilePath = getCurrentCameraFilePath(currentRec);
	const screenFilePath = getCurrentScreenFilePath(currentRec);
	if (fs.existsSync(cameraFilePath)) {
		fs.unlinkSync(cameraFilePath);
	}
	if (fs.existsSync(screenFilePath)) {
		fs.unlinkSync(screenFilePath);
	}
	if (fs.existsSync(currentRec.cameraWaveFilePath)) {
		fs.unlinkSync(currentRec.cameraWaveFilePath);
	}
	if (fs.existsSync(currentRec.screenWaveFilePath)) {
		fs.unlinkSync(currentRec.screenWaveFilePath);
	}
}
function addEmptyRecordingToCurrentProject(recordingType, selectedFile) {
	// NOTE -> STEPS
	// TODO -> 1. create recordingTypeFolder if not exists
	// TODO -> 2. copy file to this folder
	// TODO -> 3. update config object and write to file
	// TODO -> 4. reload window it will load all settings properly
	// STEP - 1
	const folderPath = path.join(config.configFolderPath, recordingType);
	if (!fs.existsSync(folderPath)) {
		fs.mkdirSync(folderPath);
	}
	const currentTime = new Date().getTime().toString();
	const extension = selectedFile.split('.')[selectedFile.split('.').length - 1];
	const relativeFilePath = recordingType + '/' + currentTime + '.' + extension;
	const filePath = path.join(folderPath, currentTime + '.' + extension);
	// STEP - 2
	showLoader();
	if (extension.toLowerCase() === '.mts' || extension.toLowerCase() === 'mts') {
		// const newFileName = new Date().getTime().toString();
		// const newFilePath = path.join(sessionStorage.getItem('configFolderPath'), fileType, newFileName + '.mp4');
		convertFileToMp4(selectedFile, filePath).then((filePath) => {
			addEmptyRecordingToCurrentProjectHelper(recordingType, filePath, relativeFilePath);
			hideLoader();
		}).catch((err) => {
			hideLoader();
			showSimpleAlert('error', 'Could not convert file');
		});
	} else {
		fs.copyFile(selectedFile, filePath, async (err) => {
			hideLoader();
			if (err) {
				reject();
				throw err;
			} else {
				addEmptyRecordingToCurrentProjectHelper(recordingType, filePath, relativeFilePath);
			}
		});
	}
}
async function addEmptyRecordingToCurrentProjectHelper(recordingType, filePath, relativeFilePath) {
	const newRecItem = {
		type: recordingType,
		cpath: relativeFilePath
	}
	showLoader();
	const duration = await getFileDuration(filePath);
	hideLoader();
	newRecItem['startTime'] = 0;
	if (!isNaN(duration)) {
		newRecItem['endTime'] = duration;
	} else {
		newRecItem['endTime'] = Number.POSITIVE_INFINITY;
	}
	config.finalMerger.push(newRecItem);
	saveSettings();
	saveProject();
	window.location.reload();
}
function convertFileToMp4(filepath, newFileName) {
	return new Promise((resolve, reject) => {
		const { fork } = require('child_process');
		let convertToMp4Process = path.join(__dirname, 'background-process.js');
		forked = fork(convertToMp4Process);
		let basepath = app.getAppPath('exe');
		basepath = basepath.replace("app.asar", "")
		basepath = basepath.replace(/\\/g, "\\\\");
		basepath += "\\";
		;
		const msg = {
			message: 'convert-to-mp4',
			filepath,
			basepath,
			newFileName
		};
		showLoader();
		forked.send(msg);
		forked.on('message', (msg) => {
			if (msg.processName === 'convert-to-mp4') {
				hideLoader();
				if (msg.type === 'error') {
					reject();
					// swal({
					// 	type: 'error',
					// 	title: 'Error',
					// 	text: 'Something went wrong'
					// });
				} else if (msg.type === 'result') {
					resolve(newFileName);
				}
			}
		});
	});
}
function handleOpenScrollingTextProperties() {
	$('.scrolling-text-option').show();
}
function handleOpenLogoOptions() {
	$('.logo-options').show();
}
function handleOpenBrightnessOptions() {
	$('.adjust-brightness-options').show();
}
function toggleScrollingText(isChecked) {
	if (!currentRec.scrollingTextObj) {
		// currentRec.scrollingTextObj = {}
		initScrollingTextObj();
	}
	currentRec.scrollingTextObj.scrollingTextCheck = isChecked;
	if (currentRec.scrollingTextObj.scrollingTextCheck) {
		let scrollingTextWrapperId = '#scrolling-text-wrapper-' + currentFinalMergerIndex;
		$(scrollingTextWrapperId).show();
		// ? this solved problem which made top and left of fixed text to be null in config file
		adjustScrollTextWidths();
	} else {
		let scrollingTextWrapperId = '#scrolling-text-wrapper-' + currentFinalMergerIndex;
		$(scrollingTextWrapperId).hide();
	}
}
function toggleLogoVisibility(isChecked) {
	currentRec.isLogoVisible = isChecked;
	toggleLogoCanvas();
}
function toggleAdjustBrightness(isChecked) {
	if (!currentRec.adjustBrightnessValues) {
		currentRec.adjustBrightnessValues = { enableAdjustBrightness: isChecked };
	} else {
		currentRec.adjustBrightnessValues.enableAdjustBrightness = isChecked;
	}
}

function initScrollingText() {
	// ? hides all scrolling-text-wrapper
	$('.scrolling-text-wrapper').each((index, wrapper) => {
		$(wrapper).hide();
	})
	let scrollingTextWrapperId = '#scrolling-text-wrapper-' + currentFinalMergerIndex;
	let scrollingTextWrapClass = scrollingTextWrapperId + ' .scrolling-text-wrap';
	let fixedTextWrapClass = scrollingTextWrapperId + ' .fixed-text-wrap';
	// ? if already exists
	if (!currentRec.scrollingTextObj) {
		initScrollingTextObj();
		$(scrollingTextWrapperId).hide();
	}
	else {
		if (currentRec.scrollingTextObj.scrollingTextCheck === true) {
			$(scrollingTextWrapperId).show();
		} else {
			$(scrollingTextWrapperId).hide();
		}
		if (currentRec.scrollingTextObj.topLocationRawPx) {
			$(scrollingTextWrapperId).css('top', currentRec.scrollingTextObj.topLocationRawPx + 'px');
		} else {
			// ? set to some default location at the bottom maybe
			const elemHeight = $('#scrolling-text-wrapper-0').height();
			const previewHeight = $('#preview').height();
			const relativeTop = previewHeight - elemHeight - 6;
			$(scrollingTextWrapperId).css('top', relativeTop + 'px');
		}
	}
	$('.scrolling-text-check').prop('checked', currentRec.scrollingTextObj.scrollingTextCheck);
	$('#scrolling-text-font-size-input').val(currentRec.scrollingTextObj.fontSize);
	$('#scrolling-text-fixed-text-input').val(currentRec.scrollingTextObj.fixedText);
	$('#scrolling-text-scrolling-text-input').val(currentRec.scrollingTextObj.scrollingText);
	$(fixedTextWrapClass).text(currentRec.scrollingTextObj.fixedText);
	$(scrollingTextWrapClass).text(currentRec.scrollingTextObj.scrollingText);
	$('.scrolling-text-canvas-content').css('fontSize', currentRec.scrollingTextObj.fontSize + 'px');
	$('#fixed-text-color').val(currentRec.scrollingTextObj.fixedTextColor);
	$('#fixed-text-back-color').val(currentRec.scrollingTextObj.fixedTextBackColor);
	$('#scrolling-text-color').val(currentRec.scrollingTextObj.scrollTextColor);
	$('#scrolling-text-back-color').val(currentRec.scrollingTextObj.scrollTextBackColor);
	$(fixedTextWrapClass).css('color', currentRec.scrollingTextObj.fixedTextColor);
	$(scrollingTextWrapClass).css('color', currentRec.scrollingTextObj.scrollTextColor);
	$(fixedTextWrapClass).css('background', currentRec.scrollingTextObj.fixedTextBackColor);
	$(scrollingTextWrapClass).css('background', currentRec.scrollingTextObj.scrollTextBackColor);
	$('.scrolling-text-start-time-sec').val(secondsToHms(currentRec.scrollingTextObj.scrollTextStartTimeSec));
	$('.scrolling-text-running-time-sec').val(secondsToHms(currentRec.scrollingTextObj.scrollTextRunningTimeSec));
	adjustScrollTextWidths();
	setScrollingTextTopLocation($(scrollingTextWrapperId));
}
function scrollingTextValuesUpdated() {
	let startTimeIncorrectFlag = false;
	let runningTimeIncorrectFlag = false;

	const fontSize = $('#scrolling-text-font-size-input').val();
	let fixedText = $('#scrolling-text-fixed-text-input').val();
	let scrollingText = $('#scrolling-text-scrolling-text-input').val();
	const fixedTextColor = $('#fixed-text-color').val();
	const fixedTextBackColor = $('#fixed-text-back-color').val();
	const scrollTextColor = $('#scrolling-text-color').val();
	const scrollTextBackColor = $('#scrolling-text-back-color').val();
	const scrollTextStartTimeSec = $('.scrolling-text-start-time-sec').val();
	const scrollTextRunningTimeSec = $('.scrolling-text-running-time-sec').val();
	currentRec.scrollingTextObj = {
		fixedText: fixedText,
		scrollingText: scrollingText,
		fontSize: fontSize,
		fixedTextColor,
		fixedTextBackColor,
		scrollTextColor,
		scrollTextBackColor,
		scrollTextStartTimeSec: getSecondsFromHMS(scrollTextStartTimeSec).seconds,
		scrollTextRunningTimeSec: getSecondsFromHMS(scrollTextRunningTimeSec).seconds,
		scrollingTextCheck: currentRec.scrollingTextObj.scrollingTextCheck
	};
	 
	if(isNaN(currentRec.scrollingTextObj.scrollTextStartTimeSec)) {
		currentRec.scrollingTextObj.scrollTextStartTimeSec = $('.scrolling-text-start-time-sec').val();
	}
	if(isNaN(currentRec.scrollingTextObj.scrollTextRunningTimeSec)) {
		currentRec.scrollingTextObj.scrollTextRunningTimeSec = $('.scrolling-text-running-time-sec').val();
	}


	let scrollingTextWrapperId = '#scrolling-text-wrapper-' + currentFinalMergerIndex;
	let scrollingTextWrapClass = scrollingTextWrapperId + ' .scrolling-text-wrap';
	let fixedTextWrapClass = scrollingTextWrapperId + ' .fixed-text-wrap';
	$(fixedTextWrapClass).text(currentRec.scrollingTextObj.fixedText);
	$(scrollingTextWrapClass).text(currentRec.scrollingTextObj.scrollingText);
	$('.scrolling-text-canvas-content').css('fontSize', currentRec.scrollingTextObj.fontSize + 'px');
	$('#fixed-text-color').val(currentRec.scrollingTextObj.fixedTextColor);
	$('#fixed-text-back-color').val(currentRec.scrollingTextObj.fixedTextBackColor);
	$('#scrolling-text-color').val(currentRec.scrollingTextObj.scrollTextColor);
	$('#scrolling-text-back-color').val(currentRec.scrollingTextObj.scrollTextBackColor);
	$(fixedTextWrapClass).css('color', currentRec.scrollingTextObj.fixedTextColor);
	$(scrollingTextWrapClass).css('color', currentRec.scrollingTextObj.scrollTextColor);
	$(fixedTextWrapClass).css('background', currentRec.scrollingTextObj.fixedTextBackColor);
	$(scrollingTextWrapClass).css('background', currentRec.scrollingTextObj.scrollTextBackColor);
	
	if(startTimeIncorrectFlag) {
		$('.scrolling-text-start-time-sec').val(secondsToHms(currentRec.scrollingTextObj.scrollTextStartTimeSec));
	}
	if(runningTimeIncorrectFlag) {
		$('.scrolling-text-running-time-sec').val(secondsToHms(currentRec.scrollingTextObj.scrollTextRunningTimeSec));
	}
	
	adjustScrollTextWidths();
	// ? fix to solve bug of topLocationPx value gets removed from
	// ? scrolling text object because of reassignment
	setScrollingTextTopLocation($(scrollingTextWrapperId));
}
function showScrollTextPreview() {
	let previewcc = require('bindings')('previewprocess');
	const res = previewcc.showScrollTextPreview(currentRec.scrollingTextObj);
}
function setTotalRecordingDuration() {
	let currentTotalDuration = (currentRec.endTime - currentRec.startTime);
	if ("cameraCutStartTimeSeconds" in currentRec && "cameraCutEndTimeSeconds" in currentRec) {
		currentTotalDuration = (currentRec.cameraCutEndTimeSeconds - currentRec.cameraCutStartTimeSeconds);
	}
	if ("screenCutStartTimeSeconds" in currentRec && "screenCutEndTimeSeconds" in currentRec) {
		currentTotalDuration = (currentRec.screenCutEndTimeSeconds - currentRec.screenCutStartTimeSeconds);
	}
	$('.play-time-current-input').val('00:00:00:0');
	$('.play-time-end-text').text(secondsToHms(currentTotalDuration));
	$('#play-time-range-slider').prop('max', currentTotalDuration);
	$('#play-time-range-slider').prop('min', 0);
	$('#play-time-range-slider').val(0);
}
function updateCurrentPlayingTime(sliderElem) {
	const currentTimeText = secondsToHms(sliderElem.value);
	$('.play-time-current-input').val(currentTimeText);
}
function openScrollColorPicker(elem) {
	const type = elem.getAttribute('type');
	switch (type) {
		case 'fix-text':
			$('#fixed-text-color').click();
			break;
		case 'fix-back':
			$('#fixed-text-back-color').click();
			break;
		case 'scroll-text':
			$('#scrolling-text-color').click();
			break;
		case 'scroll-back':
			$('#scrolling-text-back-color').click();
			break;
		default:
			break;
	}
}
function handleRecItemClicked(clickedElem) {
	if(currentRec) {
		stopCurrentCanvasVideos();
	}
	currentPlaybackPaused = true;
	let finalMergerIndex = parseInt(clickedElem.getAttribute('final-merger-index'));
	currentFinalMergerIndex = finalMergerIndex;

	currentRec = config.finalMerger[finalMergerIndex];
	setTotalRecordingDuration();
	initScrollingText();
	initVideoAdjustBrightness();
	toggleTransparencyOptions(finalMergerIndex);
	toggleLogoCanvas();
	// handleOpenGreenScreenProperties();
	openPropertiesPanel('nothing');
	if (currentRec.type == "screen") {
		$("#btn-open-crop-mode").show();
		$("#btn-open-camera-crop-mode").hide();
		currentCropCanvas = $("#canvas-camera-" + finalMergerIndex + '-wrap');
	} else if (currentRec.type == "both") {
		$("#btn-open-camera-crop-mode").show();
		$("#btn-open-crop-mode").show();
		// TODO - decide this later
		// currentCropCanvas = $("#canvas-screen-" + finalMergerIndex + '-wrap');
	} else if (currentRec.type === 'camera') {
		$("#btn-open-camera-crop-mode").show();
		$("#btn-open-crop-mode").hide();
		currentCropCanvas = $("#canvas-camera-" + finalMergerIndex + '-wrap');
	}
	showInCanvas(finalMergerIndex);
	$(".rec-item").removeClass("active-rec-item");
	$("#btn-crop").addClass("disabled");
	$(clickedElem).addClass("active-rec-item");
	replaceBackSlashFlag = false;

}
function validatePreview(currentRecLocalItem) {
	let validObj = {
		valid: true
	};
	if (currentRecLocalItem.transparencyCheck === true) {
		validObj = validateTransparencySettings(currentRecLocalItem);
		if (validObj.valid === false) {
			return validObj;
		}
	}
	if (currentRecLocalItem.scrollingTextObj && currentRecLocalItem.scrollingTextObj.scrollingTextCheck === true) {
		validObj = validateScrollingTextSettings(currentRecLocalItem);
		if (validObj.valid === false) {
			return validObj;
		}
	}
	if (currentRecLocalItem.adjustBrightnessValues && currentRecLocalItem.adjustBrightnessValues.enableAdjustBrightness === true) {
		validObj = validateBrightnessSettings(currentRecLocalItem);
		if (validObj.valid === false) {
			return validObj;
		}
	}
	return validObj;
}
function validateBrightnessSettings(currentRecLocalItem) {
	const validationObj = { valid: true };
	const gammaVal = parseFloat(currentRecLocalItem.adjustBrightnessValues.gamma);
	const saturationVal = parseFloat(currentRecLocalItem.adjustBrightnessValues.saturation);
	const brightnessVal = parseFloat(currentRecLocalItem.adjustBrightnessValues.brightness);
	const contrastVal = parseFloat(currentRecLocalItem.adjustBrightnessValues.contrast);
	if (isNaN(gammaVal) || gammaVal < 0.1 || gammaVal > 10) {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Brightness settings';
		validationObj.text = 'invalid gamma value specified, valid range is  0.1 to 10';
	}
	if (isNaN(saturationVal) || saturationVal < 0 || saturationVal > 3) {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Brightness settings';
		validationObj.text = 'invalid saturation value specified , valid range is  0 to 3';
	}
	if (isNaN(brightnessVal) || brightnessVal < -1 || brightnessVal > 1) {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Brightness settings';
		validationObj.text = 'invalid brightness value specified , valid range is -1 to 1';
	}
	if (isNaN(contrastVal) || contrastVal < -1000 || contrastVal > 1000) {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Brightness settings';
		validationObj.text = 'invalid gamma value specified , valid range is -1000 to 1000';
	}
	return validationObj;
}
function validateScrollingTextSettings(currentRecLocalItem) {
	const validationObj = { valid: true };
	if (currentRecLocalItem.scrollingTextObj.fixedText === '') {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Scroll Text Settings';
		validationObj.text = 'Fixed text cannot be empty';
	}
	if (currentRecLocalItem.scrollingTextObj.scrollingText === '') {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Scroll Text Settings';
		validationObj.text = 'Scrolling text cannot be empty';
	}
	if (currentRecLocalItem.scrollingTextObj.fontSize === '') {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Scroll Text Settings';
		validationObj.text = 'Font size cannot be empty';
	}
	if (isNaN(currentRecLocalItem.scrollingTextObj.scrollTextStartTimeSec)) {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Scroll Text Settings';
		validationObj.text = 'Scroll text start time not correct';
	} else {
		const duration = currentRecLocalItem.endTime - currentRecLocalItem.startTime;
		if (currentRecLocalItem.scrollingTextObj.scrollTextStartTimeSec > duration) {
			validationObj.valid = false;
			validationObj.title = 'Incorrect Scroll Text Settings';
			validationObj.text = 'Video duration is less than Scroll Start Time';
		}
	}
	if (isNaN(currentRecLocalItem.scrollingTextObj.scrollTextRunningTimeSec)) {
		validationObj.valid = false;
		validationObj.title = 'Incorrect Scroll Text Settings';
		validationObj.text = 'Scroll Text running time not correct';
	}
	return validationObj;
}
function scrollTextTimeSecChanged(elem) {
	const timetype = elem.getAttribute('timetype');
	const value = elem.value;
	let tmp = value.split(':');
	let isTimeValid = true;
	if (tmp.length === 3 || tmp.length === 4) {
		if (isNaN(tmp[0]) || isNaN(tmp[1]) || isNaN(tmp[2])) {
			isTimeValid = false;
		}
		if (tmp[3] && isNaN(tmp[3])) {
			isTimeValid = false;
		}
	} else {
		isTimeValid = false;
	}
	if (isTimeValid === true) {
		$(elem).css('border', '1px solid lightgrey');
	} else {
		$(elem).css('border', '1px solid red');
	}
	if (timetype === 'start-time') {
		currentRec.scrollingTextObj.scrollTextStartTimeSec = getSecondsFromHMS(value).seconds;
	} else if (timetype === 'running-time') {
		currentRec.scrollingTextObj.scrollTextRunningTimeSec = getSecondsFromHMS(value).seconds;
	}
}
// NOTE -> returns width without preview width
function setScreenContentDimensions(win) {
	// const { width, height } = screen.getPrimaryDisplay().workAreaSize
	// win.setSize(width, height);
	// ! SUPER IMPORTANT NOTE -> keep this preview width consitent with CSS
	const previewWidth = 704;
	// ? 10 for 10px space after every section
	let remainingWidth = document.body.clientWidth - (previewWidth + 10 + 10 + 10);
	let editingOptionsWidth = (remainingWidth * 0.34).toFixed(0) + 'px';
	let recordingsListWidth = (remainingWidth * 0.29).toFixed(0) + 'px';
	let propertiesWidth = (remainingWidth * 0.34).toFixed(0) + 'px';
	// ? 100vh - height of header - top of section
	let sectionHeight = 'calc(100vh - 60px - 10px)';
	$('.options-side-nav-wrapper').width(editingOptionsWidth);
	$('.side-nav').width(recordingsListWidth);
	$('.properties-panel-wrapper').width(propertiesWidth);
	$('.options-side-nav-wrapper').height(sectionHeight);
	$('.side-nav').height(sectionHeight);
	$('.properties-panel-wrapper').height(sectionHeight);
}
function setScrollingTextTopLocation(elem) {
	let fixedTextWrapClass = '#scrolling-text-wrapper-' + currentFinalMergerIndex + ' .fixed-text-wrap';
	const element = $(fixedTextWrapClass).get(0);
	if (element) {
		currentRec.scrollingTextObj['topLocationRawPx'] = parseInt($(elem).css('top').replace('px', ''));
		currentRec.scrollingTextObj['fixedTextWidthRawPx'] = $(fixedTextWrapClass).width();
	}
}
function adjustScrollTextWidths() {
	let scrollingTextWrapperId = '#scrolling-text-wrapper-' + currentFinalMergerIndex;
	const element = $(scrollingTextWrapperId).get(0);
	if (element) {
		const totalWidth = parseInt($(scrollingTextWrapperId).css('width').replace('px', ''), 10);
		const fixedTextWidth = parseInt($(scrollingTextWrapperId).find('.fixed-text-wrap').css('width').replace('px', ''), 10);
		scrollingTextWidth = (totalWidth - fixedTextWidth - 20 - 1) + 'px';
		$(scrollingTextWrapperId).find('.scrolling-text-wrap').css('width', scrollingTextWidth);
	}
}
function initScrollingTextObj() {
	currentRec.scrollingTextObj = {
		fixedText: 'HiFi Recorder',
		scrollingText: 'Contact us on www.hifitechnologysolutions.com',
		fontSize: 8,
		fixedTextColor: '#000002',
		fixedTextBackColor: '#fdffff',
		scrollTextColor: '#000002',
		scrollTextBackColor: '#fdffff',
		scrollTextStartTimeSec: 0,
		scrollTextRunningTimeSec: 20,
		scrollingTextCheck: false
	};
}
function sanitizeForFFMPEG(width, height, left, top) {
	if (width + left > config.outputRes.width) {
		left--;
		width++;
	}
	if (height + top > config.outputRes.height) {
		top--;
		height++;
	}
	const obj = { width, height, left, top }
	return obj;
}
function setFinalMergerSequence() {
	const tmpArr = [];
	$('.rec-item-wrapper').each((index, elem) => {
		const finalMergerIndex = $(elem).attr('final-merger-index');
		tmpArr.push(config.finalMerger[finalMergerIndex]);
	});
	config.finalMerger = tmpArr;
}
function saveProject(showAlert) {
	const tmpPath = path.join(config.configFolderPath, 'settings.config');
	fs.writeFileSync(tmpPath, JSON.stringify(config), 'utf-8');
	sessionStorage.removeItem('currentConfigObject');
	if (showAlert === true) {
		swal.fire({
			type: 'success',
			title: 'Project Saved',
			toast: true,
			position: 'top-right',
			timer: 3000
		});
	}
}
function getSettingsPreviewDimension(dimensionKey, recType, settings, finalMergerIndex) {
	const recItem = config.finalMerger[finalMergerIndex];
	if (dimensionKey in recItem) {
		return recItem[dimensionKey];
	} else {
		return settings.preview[recType][dimensionKey];
	}
}
function initVideoScrollTexts() {
	for (let i = 0; i < config.finalMerger.length; i++) {
		let obj = config.finalMerger[i];
		let aScrollWrapper = scrollingTextWrapperElement;
		aScrollWrapper = aScrollWrapper.replace(/merger-index/g, i);
		aScrollWrapper = aScrollWrapper.replace(/scrolling-text-wrapper-id-here/g, 'scrolling-text-wrapper-' + i);
		$('#preview').append(aScrollWrapper).ready(function (elem, data) {
			// $(".resizable")
			$(".scrolling-text-wrapper").draggable({
				cursor: "-webkit-grab",
				containment: "#preview",
				snap: "#preview",
				snapMode: "inner",
				snapTolerance: 1,
				drag: function () {
					setScrollingTextTopLocation(this);
				}
			});
		});
	}
}
function toggleLogoCanvas() {
	if (currentRec.isLogoVisible === false) {
		$('.logo-canvas').hide();
	} else {
		$('.logo-canvas').show();
	}
}
function toggleLogoCheck() {
	if (currentRec.isLogoVisible === false) {
		$('.add-remove-logo-check').prop('checked', false);
	} else {
		$('.add-remove-logo-check').prop('checked', true);
	}
}
// function formatScrollinText(formatScrollinText) {
// 	let updatedText = '';
// 	// const symbols = ['~','`','!','@','#','$','%','^','&','*','(',')','_','+','-','=','\\','|',';',':','\'','"',',','<','.','>','/','?'];
// 	const symbols = ['%'];
// 	for (const index in formatScrollinText) {
// 		if(symbols.includes(formatScrollinText[index])) {
// 			updatedText += ('\\' + formatScrollinText[index]);
// 		} else {
// 			updatedText += formatScrollinText[index];
// 		}
// 	}
// 	return updatedText;
// }
function resizeCanvasBySettings() {
	$('.canvas-wrap').each(function () {
		let mergerIndex = parseInt(this.getAttribute('mergerindex'));
		let rectype = this.getAttribute('type');
		let subtype = this.getAttribute('subtype');
		let width, height, left, top;
		let widthInner, heightInner, leftInner, topInner;
		let widthVideoInner, heightVideoInner, leftVideoInner, topVideoInner;
		if (rectype == 'camera' || rectype == 'screen') {
			// width = settings.preview[rectype].cwidthRaw;
			// height = settings.preview[rectype].cheightRaw;
			// left = settings.preview[rectype].cleftRaw;
			// top = settings.preview[rectype].ctopRaw;
			width = getSettingsPreviewDimension('cwidthRaw', rectype, settings, mergerIndex);
			height = getSettingsPreviewDimension('cheightRaw', rectype, settings, mergerIndex);
			left = getSettingsPreviewDimension('cleftRaw', rectype, settings, mergerIndex);
			top = getSettingsPreviewDimension('ctopRaw', rectype, settings, mergerIndex);

			widthInner = getSettingsPreviewDimension('cwidthRawInner', rectype, settings, mergerIndex);
			heightInner = getSettingsPreviewDimension('cheightRawInner', rectype, settings, mergerIndex);
			leftInner = getSettingsPreviewDimension('cleftRawInner', rectype, settings, mergerIndex);
			topInner = getSettingsPreviewDimension('ctopRawInner', rectype, settings, mergerIndex);

			const originalVideoRes = {
				width: $(this).find('video').get(0).videoWidth,
				height: $(this).find('video').get(0).videoHeight,
			};
			widthVideoInner = getVideoInnerDimensions('width',rectype,settings,mergerIndex, originalVideoRes, this);
			console.log('widthVideoInner: ', widthVideoInner);
			heightVideoInner = getVideoInnerDimensions('height',rectype,settings,mergerIndex, originalVideoRes, this);
			console.log('heightVideoInner: ', heightVideoInner);
			// widthVideoInner = getVideoInnerDimensions('width',rectype,settings,mergerIndex);
			// widthVideoInner = getVideoInnerDimensions('width',rectype,settings,mergerIndex);
		} else if (rectype == 'both') {
			let keyPrefix;
			if (subtype == 'camera') {
				keyPrefix = 'c'
			} else if (subtype == 'screen') {
				keyPrefix = 's';
			}
			// width = settings.preview.both[keyPrefix + 'widthRaw'];
			// height = settings.preview.both[keyPrefix + 'heightRaw'];
			// left = settings.preview.both[keyPrefix + 'leftRaw'];
			// top = settings.preview.both[keyPrefix + 'topRaw'];
			width = getSettingsPreviewDimension(keyPrefix + 'widthRaw', 'both', settings, mergerIndex);
			height = getSettingsPreviewDimension(keyPrefix + 'heightRaw', 'both', settings, mergerIndex);
			left = getSettingsPreviewDimension(keyPrefix + 'leftRaw', 'both', settings, mergerIndex);
			top = getSettingsPreviewDimension(keyPrefix + 'topRaw', 'both', settings, mergerIndex);
			
			widthInner = getSettingsPreviewDimension(keyPrefix + 'widthRawInner', 'both', settings, mergerIndex);
			heightInner = getSettingsPreviewDimension(keyPrefix + 'heightRawInner', 'both', settings, mergerIndex);
			leftInner = getSettingsPreviewDimension(keyPrefix + 'leftRawInner', 'both', settings, mergerIndex);
			topInner = getSettingsPreviewDimension(keyPrefix + 'topRawInner', 'both', settings, mergerIndex);
		
			const originalVideoRes = {
				width: $(this).find('video').get(0).videoWidth,
				height: $(this).find('video').get(0).videoHeight,
			};
			widthVideoInner = getVideoInnerDimensions('width',subtype,settings,mergerIndex, originalVideoRes, this);
			console.log('widthVideoInner: ', widthVideoInner);
			heightVideoInner = getVideoInnerDimensions('height',subtype,settings,mergerIndex, originalVideoRes, this);
			console.log('heightVideoInner: ', heightVideoInner);
		}
		if(isNaN(widthInner)) {
			widthInner = width;
		}
		if(isNaN(heightInner)) {
			heightInner = height;
		}
		if(isNaN(leftInner)) {
			if(typeof leftInner === "string") {
				leftInner = parseInt(leftInner.replace("px",""));
			} else {
				leftInner = 0;
			}
		}
		if(isNaN(topInner)) {
			if(typeof topInner === "string") {
				topInner = parseInt(topInner.replace("px",""));
			} else {
				topInner = 0;
			}
		}

		$(this).width(width); 
		$(this).find('video').width(widthInner); //could you give 100% 100% here??
		$(this).height(height);
		$(this).find('video').height(heightInner);
		$(this).css('left', left);
		$(this).find('video').css('left', leftInner);
		$(this).css('top', top);
		$(this).find('video').css('top', topInner);
	});

}
function adjustBrightnessValuesUpdated() {
	const gamma = $('#adjust-brightness-gamma-input').val();
	const saturation = $('#adjust-brightness-saturation-input').val();
	const brightness = $('#adjust-brightness-brightness-input').val();
	const contrast = $('#adjust-brightness-contrast-input').val();
	const isChecked = $('.adjust-brightness-check').prop('checked');

	if (!currentRec.adjustBrightnessValues) {
		currentRec.adjustBrightnessValues = {};
	}
	currentRec.adjustBrightnessValues['gamma'] = gamma;
	currentRec.adjustBrightnessValues['saturation'] = saturation;
	currentRec.adjustBrightnessValues['brightness'] = brightness;
	currentRec.adjustBrightnessValues['contrast'] = contrast;
	currentRec.adjustBrightnessValues['enableAdjustBrightness'] = isChecked;


}
function initVideoAdjustBrightness() {
	if (!currentRec.hasOwnProperty('adjustBrightnessValues')) {
		currentRec.adjustBrightnessValues = {
			enableAdjustBrightness: false,
			gamma: 1,
			saturation: 1,
			brightness: 0,
			contrast: 1
		}
	}
	$('#adjust-brightness-gamma-input').val(currentRec.adjustBrightnessValues.gamma);
	$('#adjust-brightness-saturation-input').val(currentRec.adjustBrightnessValues.saturation);
	$('#adjust-brightness-brightness-input').val(currentRec.adjustBrightnessValues.brightness);
	$('#adjust-brightness-contrast-input').val(currentRec.adjustBrightnessValues.contrast);
	$('.adjust-brightness-check').prop('checked', currentRec.adjustBrightnessValues.enableAdjustBrightness);

}
function resetCurrentBrightnessValues() {
	if (currentRec.hasOwnProperty('adjustBrightnessValues')) {
		currentRec.adjustBrightnessValues = {
			enableAdjustBrightness: currentRec.adjustBrightnessValues.enableAdjustBrightness,
			gamma: 1,
			saturation: 1,
			brightness: 0,
			contrast: 1
		};
		$('#adjust-brightness-gamma-input').val(currentRec.adjustBrightnessValues.gamma);
		$('#adjust-brightness-saturation-input').val(currentRec.adjustBrightnessValues.saturation);
		$('#adjust-brightness-brightness-input').val(currentRec.adjustBrightnessValues.brightness);
		$('#adjust-brightness-contrast-input').val(currentRec.adjustBrightnessValues.contrast);

		$('#gamma-slider').val(1);
		$('#saturation-slider').val(1);
		$('#brightness-slider').val(0);
		$('#contrast-slider').val(1);
	}
}
function handleAdjustBrightnessChanged(filter, element, changeFromInput) {

	switch (filter) {
		case 'gamma':
			$('#adjust-brightness-gamma-input').val(element.value);
			break;
		case 'saturation':
			$('#adjust-brightness-saturation-input').val(element.value);
			break;
		case 'brightness':
			$('#adjust-brightness-brightness-input').val(element.value);
			break;
		case 'contrast':
			$('#adjust-brightness-contrast-input').val(element.value);
			break;
		default:
			break;
	}
	adjustBrightnessValuesUpdated();
}
function validateTransparencySettings(currentRecLocalItem) {
	const validationObj = { valid: true };

	if (config.backImgPath == "false" && currentRecLocalItem.type == "camera") {
		validationObj.title = "Background image not set";
		validationObj.text = "Please select background image for using transparency in camera only mode"
		validationObj.valid = false;
	}

	let isOk = /^(?:[0-9a-f]{3}){1,2}$/i.test(currentRecLocalItem.colorHex);
	if (isOk === false) {
		validationObj.title = "Incorrect transparency settings";
		validationObj.text = "Color code incorrect"
		validationObj.valid = false;
	}

	return validationObj;
}
function playCurrentCanvasVideos() {
	if (currentRec.type == "camera" || currentRec.type == "screen") {
		// const video = $("#video-camera-"+currentFinalMergerIndex).get(0);
		const canvas = $("#canvas-camera-"+currentFinalMergerIndex).get(0);
		canvas.currentTime = getCurrentTimeToPlay(currentRec.type);
		keepPlayingVideo(canvas,"camera-or-screen");
	} else if (currentRec.type === "both") {
		// const videoCamera = $("#video-camera-"+currentFinalMergerIndex).get(0);
		// const videoScreen = $("#video-screen-"+currentFinalMergerIndex).get(0);
		const canvasCamera = $("#canvas-camera-"+currentFinalMergerIndex).get(0);
		const canvasScreen = $("#canvas-screen-"+currentFinalMergerIndex).get(0);

		canvasCamera.currentTime = getCurrentTimeToPlay("camera");
		canvasScreen.currentTime = getCurrentTimeToPlay("screen");
		keepPlayingVideo(canvasCamera,"both-camera");
		keepPlayingVideo(canvasScreen,"both-screen");
  }
  currentPlaybackPaused = false;
}
function pauseCurrentCanvasVideos() {
	$(".play-btn > img").attr("src","../assets/images/play-button-blue.png");
	$(".play-btn").attr("mode","play");
	if (currentRec.type == "camera" || currentRec.type == "screen") {
		const canvas = $("#canvas-camera-"+currentFinalMergerIndex).get(0);
		canvas.pause();
	} else if (currentRec.type === "both") {
		const canvasCamera = $("#canvas-camera-"+currentFinalMergerIndex).get(0);
		const canvasScreen = $("#canvas-screen-"+currentFinalMergerIndex).get(0);
		canvasCamera.pause();
		canvasScreen.pause();
  }
}
function stopCurrentCanvasVideos() {

	$(".play-btn > img").attr("src","../assets/images/play-button-blue.png");
	$(".play-btn").attr("mode","play");
	$('#play-time-range-slider').val(0);
	updateCurrentPlayingTime($('#play-time-range-slider').get(0))
	if (currentRec.type == "camera" || currentRec.type == "screen") {
		const canvas = $("#canvas-camera-"+currentFinalMergerIndex).get(0);
		canvas.pause();
		canvas.currentTime = 0;
	} else if (currentRec.type === "both") {
		const canvasCamera = $("#canvas-camera-"+currentFinalMergerIndex).get(0);
		const canvasScreen = $("#canvas-screen-"+currentFinalMergerIndex).get(0);
		canvasCamera.pause();
		canvasCamera.currentTime = 0;
		canvasScreen.pause();
		canvasScreen.currentTime = 0;
	}
}
function keepPlayingVideo(canvas, mode) {
	return new Promise((resolve, reject) => {
    canvas.muted = false;
		let promise = canvas.play();
		if (promise !== undefined) {
			promise.then(function () {
				// start Automatic playback;
				(function loop() {
					if (!canvas.paused && !canvas.ended) {
            if(mode !== "both-screen") {
              $('#play-time-range-slider').val(canvas.currentTime);
              updateCurrentPlayingTime($('#play-time-range-slider').get(0))
            }
						// drawOnCanvas(video, canvas)
						setTimeout(loop, 1000 / 30); // drawing at 30fps
					}
					// NOTE - stop video if current time reaches cut end time
					if(currentRec.cameraCutEndTimeSeconds && canvas.currentTime >= currentRec.cameraCutEndTimeSeconds) {
						canvas.pause();
						stopCurrentCanvasVideos();
					}
					if(currentRec.screenCutEndTimeSeconds && canvas.currentTime >= currentRec.screenCutEndTimeSeconds) {
						canvas.pause();
						stopCurrentCanvasVideos();
          }
          if((canvas.currentTime) >= (currentRec.endTime - currentRec.startTime - 1)) {
            stopCurrentCanvasVideos();
          }
				})();
			}).catch(function (error) {
				reject()
			});
		} else {
			reject();
		}
	});
}
function getCurrentTimeToPlay(subType) {
	let currentTime = parseInt($('#play-time-range-slider').val());
	if(subType === "camera" && currentRec.cameraCutStartTimeSeconds) {
		currentTime += currentRec.cameraCutStartTimeSeconds;
	}
	if(subType === "screen" && currentRec.screenCutStartTimeSeconds) {
		currentTime += currentRec.screenCutStartTimeSeconds;
	}

	return currentTime;
}
function setCurrentTemplateName() {
    // ! NOTE -> reduce this in a function
    let readSettingsFile = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
    for(let index=0; index < readSettingsFile.length ; index++){
      let item = readSettingsFile[index];
      if(item.defaultkey == true){
        document.getElementById('inner-span').innerText = item.name;
      }
    }
    if(sessionStorage.getItem('previewMode') == 'cancelMode'){
      let templateName = sessionStorage.getItem('templateName');
      let templateIndex = sessionStorage.getItem('templateIndex');
      if(templateName) {
        if(templateName == undefined){
          if(readSettingsFile[templateIndex].defaultkey==true)
            document.getElementById('inner-span').innerText = readSettingsFile[templateIndex].name
        }
        else{
          document.getElementById('inner-span').innerText = templateName;
        }
      }
    }
    if(sessionStorage.getItem('previewMode') == 'confirmMode'){
      let templateName = sessionStorage.getItem('templateName');
      if(sessionStorage.getItem('templateName') == null){
        $('.name-side-nav-title').hide();
      }
      else{
        document.getElementById('inner-span').innerText = templateName;
      }
    }
}
function changeTemplate(){
		sessionStorage.setItem('currentSettingsMode','changeTemplateMode');
		sessionStorage.setItem('autoclickindex',autotickindex);
		ipcRenderer.send('show-template-list');
}

function playTimeRangeSliderMouseDown() {
  $(".play-btn").get(0).setAttribute("mode", "play");
    $(".play-btn > img").attr("src","../assets/images/play-button-blue.png");
    pauseCurrentCanvasVideos();

}
function playTimeRangeSliderMouseUp() {

  if(! currentPlaybackPaused) {
    const elem  = $(".play-btn").get(0);
    playPauseCurrentCanvasVideosWrapper(elem);
  }
}
function playPauseCurrentCanvasVideosWrapper(elem) {
  const mode = elem.getAttribute("mode");

	if(mode === "play") {
		elem.setAttribute("mode", "pause");
    $(".play-btn > img").attr("src","../assets/images/pause-button-blue.png");
		playCurrentCanvasVideos();
	} else if(mode === "pause") {
		elem.setAttribute("mode", "play");
    $(".play-btn > img").attr("src","../assets/images/play-button-blue.png");
    currentPlaybackPaused = true;
		pauseCurrentCanvasVideos();
	}
}
function playTimeInputFocusedIn() {
  const elem = $(".play-btn").get(0);
  elem.setAttribute("mode", "play");
    $(".play-btn > img").attr("src","../assets/images/play-button-blue.png");
    currentPlaybackPaused = true;
		pauseCurrentCanvasVideos();
}
function playTimeInputFocusedOut() {
  let newCurrentTime = $(".play-time-current-input").val();
  const isValid = isValidHMS(newCurrentTime);

  if(isValid) {
    // ? update current time of videos
    newCurrentTime = getSecondsFromHMS(newCurrentTime);

    $("#play-time-range-slider").val(newCurrentTime.seconds);
  } else {
    swal.fire({
      icon: 'error',
      title: 'Invalid Timing',
      text: 'Timing must be of format hh:mm:ss.'
    });
  }
}
function isValidHMS(hms) {
  let time = 0;
  try {
    const a = hms.split(':'); // split it at the colons
    const time = { seconds: 0, milliseconds: 0, floatSeconds: 0 };
    // minutes are worth 60 seconds. Hours are worth 60 minutes.
    const seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);
    if (isNaN(seconds)) {
      return false;
    } else {
      if(a[0] > 59 || a[1] > 59 || a[2] > 59) {
        return false;
      }
      time.seconds = seconds;
    }
    time.milliseconds = a[3];
    time.floatSeconds = parseFloat(time.seconds + '.' + time.milliseconds);
  }catch(exp) {

    return false;
  }
	return true;
}

function showCanvasInFullscreen(elem) {
  const mode = elem.getAttribute("mode");
  toggleCanvasFullscreen(mode);
  if(mode === "view-fullscreen") {
    $("#preview").removeClass("preview-content");
    $("#preview").addClass("preview-content-fullscreen");

    $(".play-pause-container").addClass("play-pause-container-fullscreen");
    elem.setAttribute("mode","reset-fullscreen");
    $(".preview-wrap").css('pointer-events','none');
  } else {
    $("#preview").removeClass("preview-content-fullscreen");
    $("#preview").addClass("preview-content");

    $(".play-pause-container").removeClass("play-pause-container-fullscreen");
    elem.setAttribute("mode","view-fullscreen");
    $(".preview-wrap").css('pointer-events','all');
  }
}

function toggleCanvasFullscreen(mode) {
	let background = {
		top: 0,
		left: 0,
		width: $("#preview").width(),
		height: $("#preview").height(),
  }

  const size = electron.remote.screen.getPrimaryDisplay().workAreaSize;
  let expectedOutput = {
    left: 0,
    top: 0,
    width: (size.width - 6),
    height: (size.height - 6)
  }
  if(mode === "reset-fullscreen") {
    expectedOutput.width = 704;
    expectedOutput.height = 396;
  }
  $('.canvas-wrap').each(function (index, elem) {
	expectedOutput = {
		left: 0,
		top: 0,
		width: (size.width - 6),
		height: (size.height - 6)
	  }
	  if(mode === "reset-fullscreen") {
		expectedOutput.width = 704;
		expectedOutput.height = 396;
	  }
      let type = this.getAttribute('type');

      let subtype = this.getAttribute('subtype');

      let mergerIndex = parseInt(this.getAttribute('mergerindex'));


      if(mergerIndex === currentFinalMergerIndex) {
        // NOTE -> wrapper width height is relative to fullcanvas
        // ? we want it relative to fs

        let width = getCanvasDimension('width', $(this).width(), background, isForFullscreen = true,expectedOutput);
        let height = getCanvasDimension('height', $(this).height(), background, isForFullscreen = true,expectedOutput);
        let left = getCanvasDimension('left', $(this).css('left'), background, isForFullscreen = true,expectedOutput);
        let top = getCanvasDimension('top', $(this).css('top'), background, isForFullscreen = true,expectedOutput);

        const wrapperOldValues = {
          width: $(this).width(),
          height: $(this).height(),
          left: $(this).css('left').replace("px",""),
          top: $(this).css('top').replace("px","")
        }
        if(wrapperOldValues.top == "auto") {
          wrapperOldValues.top = 0;
        }
        if(wrapperOldValues.left == "auto") {
          wrapperOldValues.left = 0;
        }
        wrapperOldValues.top = parseInt(wrapperOldValues.top);
        wrapperOldValues.left = parseInt(wrapperOldValues.left);

        $(this).width(width);
        $(this).height(height);
        $(this).css('left', left);
        $(this).css('top',top);

        // ? wrapper's new width height
        expectedOutput = {
          width,
          height,
          left,
          top
        };

        // ? video was relative to wrapper's old width height, we want it relative to wrapper's new width height
        let videoWidth = getCanvasDimension('width', $(this).find('video').width(), wrapperOldValues, isForFullscreen = true,expectedOutput);
        let videoHeight = getCanvasDimension('height', $(this).find('video').height(), wrapperOldValues, isForFullscreen = true,expectedOutput);
        let videoLeft = getCanvasDimension('left', $(this).find('video').css('left'), wrapperOldValues, isForFullscreen = true,expectedOutput);
        let videoTop = getCanvasDimension('top', $(this).find('video').css('top'), wrapperOldValues, isForFullscreen = true,expectedOutput);
        $(this).find('video').width(videoWidth);
        $(this).find('video').height(videoHeight);
        $(this).find('video').css('left', videoLeft);
        $(this).find('video').css('top',videoTop);
      }
	});
	let logoCanvas = $(".logo-canvas-wrap").get(0);
	if (logoCanvas) {
    let width = getCanvasDimension('width', $(logoCanvas).width(), background);
		let height = getCanvasDimension('height', $(logoCanvas).height(), background);
		let left = getCanvasDimension('left', $(logoCanvas).css('left'), background);
		let top = getCanvasDimension('top', $(logoCanvas).css('top'), background);

    $(logoCanvas).width(width);
    $(logoCanvas).height(width);
    $(logoCanvas).css('left', left);
    $(logoCanvas).css('top',top);
	}
	for (let i = 0; i < config.finalMerger.length; i++) {
		// ? handle scrolling text check
		if (config.finalMerger[i].scrollingTextObj && config.finalMerger[i].scrollingTextObj.scrollingTextCheck === true) {
      let top = getCanvasDimension('top', config.finalMerger[i].scrollingTextObj['topLocationRawPx'], background);

      let width = getCanvasDimension('width', config.finalMerger[i].scrollingTextObj['fixedTextWidthRawPx'], background);


      $(".scrolling-text-wrapper").css("top",top);
		}
	}
}
function calculateResizedDimensions(event, elem) {

  // ? new resized values
  const outerWidth = $(elem).width();
  const outerHeight = $(elem).height();
  const outerLeft = $(elem).css('left');
  const outerTop = $(elem).css('top');

  const innerWidth = $(elem).find('video').width();
  const innerHeight = $(elem).find('video').height();
  const innerLeft = $(elem).find('video').css('left');
  const innerTop = $(elem).find('video').css('top');

}
function updateCanvasAfterCrop(canvas, cropCoords, selectedCanvas) {
  let currentWidth = $(canvas).width();
  let currentHeight = $(canvas).height();

//   const fullWidth = $("#preview").width();
//   const fullHeight = $("#preview").height();
  const previewWidth = $(selectedCanvas).width();
  const previewHeight = $(selectedCanvas).height();

  const xFactor = previewWidth / cropCoords.selection.w;

  const yFactor = previewHeight / cropCoords.selection.h;


  const videoWidth = previewWidth * xFactor;
  const videoHeight = previewHeight * yFactor;

  let videoLeft = (videoWidth * cropCoords.selection.x) / previewWidth;
  let videoTop = (videoHeight * cropCoords.selection.y) / previewHeight;

  $(canvas).width(videoWidth);
  $(canvas).height(videoHeight);
  $(canvas).css('left','-' + videoLeft + 'px');
  $(canvas).css('top','-' + videoTop + 'px');
}
function canvasResizeStarted(event) {

	oldOuterCanvasResizeCoordinates = {
	  width: $(event.target).width(),
	  height: $(event.target).height(),
	  left: $(event.target).css('left').replace("px",""),
	  top: $(event.target).css('top').replace("px","")
	};


	oldInnerCanvasResizeCoordinates = {
	  width: $(event.target).find('video').width(),
	  height: $(event.target).find('video').height(),
	  left: $(event.target).find('video').css('left').replace("px",""),
	  top: $(event.target).find('video').css('top').replace("px","")
	};
	if(oldInnerCanvasResizeCoordinates.left == "auto") {
	  oldInnerCanvasResizeCoordinates.left = 0;
	}
	if(oldInnerCanvasResizeCoordinates.top == "auto") {
	  oldInnerCanvasResizeCoordinates.top = 0;
	}

	oldInnerCanvasResizeCoordinates.left = parseInt(oldInnerCanvasResizeCoordinates.left);
	oldInnerCanvasResizeCoordinates.top = parseInt(oldInnerCanvasResizeCoordinates.top);

	if(oldOuterCanvasResizeCoordinates.left == "auto") {
	  oldOuterCanvasResizeCoordinates.left = 0;
	}
	if(oldOuterCanvasResizeCoordinates.top == "auto") {
	  oldOuterCanvasResizeCoordinates.top = 0;
	}
	oldOuterCanvasResizeCoordinates.left = parseInt(oldOuterCanvasResizeCoordinates.left);
	oldOuterCanvasResizeCoordinates.top = parseInt(oldOuterCanvasResizeCoordinates.top);
}
function trimScrollingTextInputs() {
	let fontSize = $('#scrolling-text-font-size-input').val();
	let fixedText = $('#scrolling-text-fixed-text-input').val();
	let scrollingText = $('#scrolling-text-scrolling-text-input').val();
	let fixedTextColor = $('#fixed-text-color').val();
	let fixedTextBackColor = $('#fixed-text-back-color').val();
	let scrollTextColor = $('#scrolling-text-color').val();
	let scrollTextBackColor = $('#scrolling-text-back-color').val();
	let scrollTextStartTimeSec = $('.scrolling-text-start-time-sec').val();
	let scrollTextRunningTimeSec = $('.scrolling-text-running-time-sec').val();

	fontSize = fontSize.trim();
	fixedText = fixedText.trim();
	scrollingText = scrollingText.trim();
	
	$('#scrolling-text-font-size-input').val(fontSize);
	$('#scrolling-text-fixed-text-input').val(fixedText);
	$('#scrolling-text-scrolling-text-input').val(scrollingText);
}
function getVideoInnerDimensions(dimension,rectype,settings,mergerIndex, originalVideoRes, wrapperElement) {
	const recItem = config.finalMerger[mergerIndex];
	let key = "";
	const preview = {
		width: $(wrapperElement).width(),
		height: $(wrapperElement).height(),
		left: $(wrapperElement).css('left'),
		top: $(wrapperElement).css('top')
	};
	if(rectype === "camera") {
		key = "cameraWindowCoords";
	} else if(rectype === "screen") {
		key = "screenWindowCoords";
	}

	if(recItem[key]) {
		let videoInnerDimension = (recItem[key][dimension] * preview[dimension]) / originalVideoRes[dimension];
		videoInnerDimension = preview[dimension] - videoInnerDimension;
		return -videoInnerDimension;
	} else {
		return 0;
	}
}