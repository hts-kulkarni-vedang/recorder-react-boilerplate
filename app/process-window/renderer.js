const { ipcRenderer, shell } = require("electron");
var create = require('bindings')('createList');
var addon = require('bindings')('preprocess');
var p = require('process');
var final = require("bindings")('finalmerge');
var overlayCount = 0;
let prtime = 0;
var pre_obj, over_obj, final_obj;
const electron = require("electron");
const { dialog } = require("electron").remote;
var fs = require('fs')
var videoUtils = require('../assets/js/videoUtils')
var pre_obj;
var configFilePath, configObj
const swal = require('sweetalert2')
var os = require('os')
var spawn_proc_count = 0
var child_process = require('child_process');
var remote = require('electron').remote;
var exec = require('child_process').exec;
var documentsFolderPath = os.homedir + '/Documents/HiFi_Recorder Files'
var cameraFiles = []
var screenFiles = []
var rimraf = require('rimraf');
var doneDuration = 0, totalDuration = 0
var overlayDurations = []
var totalOverlayDuration = 0;
var overlayPcount = 0
var finalFilePath = ""
var transparencyCheck = false
var transparencyOpacity;
var transparencySimilarity;
var colorHex = ''
var previewProc, configFolderPath;
//////////////////////////////////////////////////////
let otime;//= msg.split("time=")[1].split(" ")[0].split(":");
let ohrs;//= parseInt(otime[0]);
let omins;//= parseInt(otime[1]);
let osecs;//= parseFloat(parseFloat(otime[2]).toFixed(2));
let oTimeSecs;// = (ohrs*60)+(omins*60)+osecs;
//////////////////////////////////////////////////////
//var errLogstream,ffmpegLogstream
var logStream = null;
var currentWindow;
var forked = null;
const path = require("path");
const app = require("electron").remote.app;

ipcRenderer.on('message', (event, message) => {
	// logs out "Hello second window!"
})

// ipcRenderer.on('auto-updater', (event, param) => {
// 	handleAutoUpdateEvent(param);
// });
// ipcRenderer.on('request-auto-updater-status', (event, param) => {
// 	// handleAutoUpdateEvent(param);
//
// 	switch(param){
// 		case "checking-for-update" :
// 			break;
// 		case "update-available" :
// 			$(".new-image").show();
// 			$(".infinite-loader").hide();
// 			$(".install-update").hide();
// 			break;
// 		case "update-not-available" :
// 			break;
// 		case "error" :
// 			break;
// 		case "download-progress" :
// 			$(".new-image").hide();
// 			$(".infinite-loader").show();
// 			$(".install-update").hide();

// 			break;
// 		case "update-downloaded" :
// 			$(".new-image").hide();
// 			$(".infinite-loader").hide();
// 			$(".install-update").show();
// 			break;
// 	}
// });

$(document).ready(function () {

	//
	processVideos();
	currentWindow = electron.remote.getCurrentWindow();
	// checkAutoUpdaterStatus();
});
$("#close").on('click', function () {
	// electron.remote.getCurrentWindow().close()
	// ipcRenderer.send('open-menu');
	if (forked) {
		forked.kill('SIGINT');
	}
	ipcRenderer.send("close-app");
})
$("#minimize").on('click', function () {
	electron.remote.getCurrentWindow().minimize()
})
$(".config-choose-btn").on("click", function () {
	$(".progress").hide()
	$(".show-file-wrap").hide();
	$(".transparency-preview-wrap").hide()
	$(".process-wrap").hide()
	dialog.showOpenDialog({
		browserWindow: electron.remote.getCurrentWindow(),
		title: 'Select File',
		buttonLabel: 'Select File',
		filters: [
			{ name: 'Configuration', extensions: ['CONFIG'] }
		],
		properties: ['openFile']
	}, function (filename) {
		if (filename != undefined) {
			configFilePath = filename[0];
			$(".config-choose-wrap").hide();
			$(".process-wrap").show();
			configObj = JSON.parse(fs.readFileSync(configFilePath, 'utf8'));
			testIntegrity();
			$(".progresfs").show();
			$(".show-file-wrap").hide();

		}
	})
})
$("#show-file").on("click", function () {
	shell.showItemInFolder(finalFilePath)
});
$("#go-home").on("click", function () {
	currentWindow.setSize(800, 600);
	let bounds = electron.remote.screen.getPrimaryDisplay().bounds;
	let x = bounds.x + ((bounds.width - 800) / 2);
	let y = bounds.y + ((bounds.height - 600) / 2);
	let obj = {
		progress: -1,
		mode: "none"
	};
	ipcRenderer.send("set-taskbar-progress", obj);

	$("button").prop("disabled", "true");
	currentWindow.setPosition(x, y);
	currentWindow.loadFile("index.html");
});
$(".back-btn").on("click", function () {
	electron.remote.getCurrentWindow().hide()
	ipcRenderer.send('open-menu')
})
// $(document).on("click","#view-log",function(){
// 	if($("#the-log").css("display") == "block")
// 	{
// 		$("#the-log").hide()
// 		$(this).text("View Log")
// 	}
// 	else
// 	{
// 		$("#the-log").show()
// 		$(this).text("Hide Log")
// 	}
// });
$('#transparency-check').on('click', function (ev) {
	transparencyCheck = $(this).prop('checked')
	if ($(this).prop('checked')) {
		$("#hex-color").css('visibility', 'visible')
		$("#tl").css('display', 'none')
		$("#tr").css('display', 'none')
		$("#transparency-options").css('display', 'inline-block')
		$("#transparency-options").css('width', 'calc(80% - 20px)')

	}
	else {
		$("#hex-color").css('visibility', 'hidden')
		$("#tl").css('display', 'block')
		$("#tr").css('display', 'block')
		$("#transparency-options").css('display', 'none')
	}
})
$("#similarity-range-slider").on('input change', function () {
	$("#similarity-value").text((this.value) + "%")
	transparencySimilarity = this.value;
})
$("#opacity-range-slider").on('input change', function () {
	$("#opacity-value").text((this.value) + "%")
	transparencyOpacity = (this.value);
});
$("#original-video").on('play', function (e) {
	let video = document.getElementById("preview-video")
	video.play()
});
$("#original-video").on('pause', function (e) {
	let video = document.getElementById("preview-video")
	video.pause()
});
$('#hex-color').on('input', function (ev) {
	let val = $(this).val();
	val = val.toUpperCase();
	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
	colorHex = val.replace("#", "");
	// colorHex = val;
	if (isOk) {
		$(this).removeClass('error-input');
		$(this).addClass('correct-input');
	} else {
		$(this).removeClass('correct-input');
		$(this).addClass('error-input');
	}
})
// $('#show-preview').on('click', function () {
// 	let val = $("#hex-color").val();
// 	let isOk = /^#(?:[0-9a-f]{3}){1,2}$/i.test(val);
// 	if (isOk) {

// 		if (previewProc != undefined) {
// 			previewProc.kill()
// 		}
// 		let args = [
// 			'ffplay',
// 			configFolderPath + configObj.finalMerger[0].cpath,
// 			'-vf', 'chromakey=0x' + val.replace("#", '') + ':' + (transparencySimilarity / 100) + ':' + (transparencyOpacity / 100),
// 			'-x', 640,
// 			'-y', 480
// 		];
//
// 		let cmd = args.shift();
// 		previewProc = child_process.spawn(cmd, args);
// 		previewProc.on('close', function (x, y) {
//
// 		});
// 		previewProc.stdout.on('data', function (data) {
//
// 		});
// 	} else {
// 		swal.fire({
// 			'type': 'error',
// 			'title': 'incorrect data'
// 		});
// 	}
// })
// $('#start-processing').on('click',function(){
// 	configObj.colorHex = $('#hex-color').val().replace('#','');
// 	configObj.transparencySimilarity = transparencySimilarity / 100;
// 	configObj.transparencyOpacity = transparencyOpacity / 100;
//
// 	// debugger
// 	preprocess();
// })
// $(document).on("click",".new-image",function(){
// 	onUpdateAvailableClick();
// });
// $(document).on("click",".install-update",function(){
// 	onUpdateDownloadClick();
// });
function testIntegrity() {
	let integrityFlag = 1;
	configFolderPath = configFilePath.replace("settings.config", "")
	if (configFolderPath.lastIndexOf('\\') != configFolderPath.length - 1) {
		configFolderPath += '/';
	}
	for (let i = 0; i < configObj.finalMerger.length; i++) {
		let obj = configObj.finalMerger[i]
		if (obj.type == 'camera') {
			if (!fs.existsSync(configFolderPath + obj.cpath)) {
				integrityFlag = -1;
				break;
			}
		}
		else if (obj.type == 'screen') {
			if (!fs.existsSync(configFolderPath + obj.cpath)) {
				integrityFlag = -1;
				break;
			}
		}
		else if (obj.type == 'both') {
			if (!fs.existsSync(configFolderPath + obj.cpath)) {
				integrityFlag = -1;
				break;
			}
			if (!fs.existsSync(configFolderPath + obj.spath)) {
				integrityFlag = -1;
				break;
			}
		}
	}
	return integrityFlag;
}

function preprocess() {
	// configFilePath = configFilePath.replace("\settings.config","")
	// $("#the-log").html("");
	writeLog("********************PREPROCESS******************");
	$(".back-btn").hide();
	$("#progress-text").html("Preprocessing Files ..");
	$(".process-wrap").show();
	$(".transparency-preview-wrap").hide();
	overlayDurations[overlayCount] = {};
	const { fork } = require('child_process');//
	let preprocessJs = __dirname + '\\preprocess.js';
	forked = fork(preprocessJs);


	// configFilePath = configFilePath.replace("\\settings.config","")
	if (!fs.existsSync(configFilePath + '/output')) {
		fs.mkdirSync(configFilePath + '/output')
	}
	let x1;//= (((msg.final.toString()).split("time=")[1]).split(" ")[0]).split(":")
	let hrs; //= parseInt(x1[0])
	let mins; //= parseInt(x1[1])
	let secs;//= parseFloat(parseFloat(x1[2]).toFixed(2))
	doneDuration = (hrs * 60) + (mins * 60) + secs
	for (let i = 0; i < configObj.finalMerger.length; i++) {
		if (configObj.finalMerger[i].type == 'both') {
			spawn_proc_count += 2;
		}
		else {
			spawn_proc_count += 1;
		}
	}

	forked.on('message', (msg) => {

		if (msg.preprocess) {
			if (msg.preprocess.ExceptionOccured) {
				swal.fire({
					type: "error",
					title: "Unspecified Error",
					text: "Please reload this config file and try again!"
				}).then(() => {
				});
			}
			if (msg.preprocess.validationError == true) {
				if (msg.preprocess.reason == "connected-no-internet") {
					swal.fire({
						type: 'info',
						text: 'Seems you are connected to a network , but we cannot detect any internet access !,Please re-connect to a network and try again'
					})
				} else {
					swal.fire({
						type: "error",
						title: "Invalid License"
					}).then(() => {
						forked.kill('SIGINT');
						if (fs.existsSync(documentsFolderPath + "\\sample.dat")) {
							fs.unlinkSync(documentsFolderPath + "\\sample.dat");
							currentWindow.setSize(400, 400);
							currentWindow.setResizable(false);
							currentWindow.loadFile("validation.html");
						}
					});
				}
			}
			if (msg.preprocess.output) {
				writeLog(msg.preprocess.output);

				if (msg.preprocess.output.includes("time=")) {
					otime = msg.preprocess.output.split("time=")[1].split(" ")[0].split(":");
					ohrs = parseInt(otime[0]);
					omins = parseInt(otime[1]);
					osecs = parseFloat(parseFloat(otime[2]).toFixed(2));
					// ! old incorrect statement
					// oTimeSecs = (ohrs * 60) + (omins * 60) + osecs;
					// ! new correct statement
					oTimeSecs = (ohrs * 60 * 60) + (omins * 60) + osecs;


					let widthPer = (((oTimeSecs) / msg.preprocess.duration) * 100).toFixed(0);

					// debugger
					// $(".progressbar-new-inner").css('width',(widthPer)+"%");
					if (widthPer <= 100) {
						$(".progressbar-new-inner").width(widthPer + "%");
						let obj = {
							progress: parseFloat((widthPer / 100).toFixed(1)),
							mode: "normal"
						};
						ipcRenderer.send("set-taskbar-progress", obj);
					}

				}
			}
			// $("#the-log").append("<div class='a-log'>"+msg.preprocess.output+"</div>");
			// $("#the-log").scrollTop($("#the-log").prop("scrollHeight"));


			if (msg.preprocess.remainingCount) {
				$("#progress-text").html("Preprocessing Files .. <br />Remaining : " + msg.preprocess.remainingCount);
			}
		}
		else if (msg.overlay) {
			startOverlaying(msg.overlay);
		}
		else if (msg.finalMerge) {

			finalMerge(msg);
		}
		else if (msg.final) {

			writeLog(msg.final.toString());
			// $("#the-log").append("<div class='a-log'>"+msg.final.toString()+"</div>")
			// $("#the-log").scrollTop($("#the-log").prop("scrollHeight"))
			if ((msg.final.toString()).includes("time=")) {
				x1 = (((msg.final.toString()).split("time=")[1]).split(" ")[0]).split(":");
				hrs = parseInt(x1[0]);
				mins = parseInt(x1[1]);
				secs = parseFloat(parseFloat(x1[2]).toFixed(2));
				doneDuration = (hrs * 60) + (mins * 60) + secs;
				$("#progress-text").html("Packing video together ...");

				let widthPer = ((doneDuration / totalDuration) * 100).toFixed(0);
				if (widthPer <= 100) {
					$(".progressbar-new-inner").css('width', widthPer + '%');
					let obj = {
						progress: parseFloat((widthPer / 100).toFixed(1)),
						mode: "normal"
					};
					ipcRenderer.send("set-taskbar-progress", obj);
				}
			}
			if (typeof msg.final == "object") {
				if (msg.final.status == "complete") {
					finalMerge({ outputPath: msg.final.outputFileName });
				}
			}
		}
	}
	);
	var basepath = app.getAppPath('exe');

	basepath = basepath.replace("app.asar", "")
	basepath += "\\";

	var appDataFolderPath = app.getPath('userData');

	forked.send({ 'configObj': configObj, 'configFolderPath': configFolderPath, basepath: basepath, appDataFolderPath: appDataFolderPath });

	forked.on("exit", function (code) {
		// finalMerge("{exit : true}");

		//debugger
	});
	forked.on("close", function (code) {
		// finalMerge("{exit : true}");

		//	debugger
	});
	forked.on("error", function (code) {
		// finalMerge("{exit : true}");

	});
	forked.on("disconnect", function (code) {
		// finalMerge("{exit : true}");

	});

}
function startOverlaying(msg) {
	// $("#the-log").append("<div class='a-log'>"+msg.output+"</div>")
	// $("#the-log").scrollTop($("#the-log").prop("scrollHeight"));
	if (msg.remainingCount) {
		$("#progress-text").html("Mixing Camera and Screen Files .. <br />Remaining : " + msg.remainingCount);
	}


	if (msg.output) {
		writeLog(msg.output);

		if (msg.output.includes("time=")) {
			otime = msg.output.split("time=")[1].split(" ")[0].split(":");
			ohrs = parseInt(otime[0]);
			omins = parseInt(otime[1]);
			osecs = parseFloat(parseFloat(otime[2]).toFixed(2));
			oTimeSecs = (ohrs * 60) + (omins * 60) + osecs;



			let widthPer = (((msg.previousCompletedDur + oTimeSecs) / msg.totalOverlayDur) * 100).toFixed(0);


			if (widthPer <= 100)
				$(".progressbar-new-inner").css('width', (widthPer) + "%")

		}
		else if (msg.output.includes("Duration")) {
			odur = msg.output.toString().split("Duration:")[1].split(",")[0].split(":")
			odurhrs = parseInt(odur[0])
			odurmins = parseInt(odur[1])
			odursecs = parseFloat(parseFloat(odur[2]).toFixed(2))
			odurTimeSecs = (odurhrs * 60) + (odurmins * 60) + odursecs
			overlayDurations[overlayCount].totalDuration = odurTimeSecs;
			totalOverlayDuration += odurTimeSecs;

		}
	}
	// else if(msg.includes("Qavg"))
	// 	{
	// 	overlayCount++;
	// 	overlayDurations[overlayCount] = {};
	// 	prtime=0;

	// }
}
function finalMerge(msg) {
	//optname = configFolderPath+'.mp4';
	$("#progress-text").text("Video generated successfully !");
	$(".progress").hide();
	$(".show-file-wrap").show();
	closeLog();
	$(".back-btn").show();
	ipcRenderer.send('clear-for-both');
	let obj = {
		progress: 1,
		mode: "normal"
	};
	ipcRenderer.send("set-taskbar-progress", obj);
	swal({
		title: 'Enter output filename',
		input: 'text',
		inputValue: '',
		showCancelButton: true
	}).then(res => {
		$(".progressbar-new-inner").width("100%");
		prepareFileOutput(res, msg);
		// if (filename && (filename.value != undefined)) {

		// 	if (msg.outputPath[0] == "\"") {
		// 		msg.outputPath = msg.outputPath.substr(1, msg.outputPath.length - 2);
		// 	}
		// 	let lastName = msg.outputPath.split('\\')[msg.outputPath.split('\\').length - 1];
		// 	let folderName = msg.outputPath.replace(lastName, '');
		// 	let finalFileName = folderName.replace(lastName, '') + filename.value + '.mp4';
		// 	// fs.renameSync(msg.outputPath, finalFileName);
		// 	fs.rename(msg.outputPath, finalFileName, (err) => {
		// 		if (err) {
		// 			console.log('finalFileName: ', finalFileName);
		// 			console.log('msg.outputPath: ', msg.outputPath);
		// 			finalFilePath = msg.outputPath;
		// 			console.error('error renaming the file', err);
		// 		} else {
		// 			finalFilePath = finalFileName;
		// 		}
		// 		// NOTE -> enable button only when the "then" function of swal gets called
		// 		$("#show-file").prop('disabled', false);
		// 	});

		// }
		// else if (filename && (filename.value != undefined) && msg.finalMerge == "notdone") {
		// 	const newPath = path.join(configFolderPath, 'output', 'output.mp4');
		// 	// fs.renameSync(configFolderPath + msg.configObj.finalMerger[0].cpath, finalFilePath)
		// 	const oldPath = path.join(configFolderPath, msg.configObj.finalMerger[0].cpath);
		// 	fs.rename(oldPath, newPath, (err) => {
		// 		if (err) {
		// 			console.log('newPath: ', newPath);
		// 			console.log('oldPath: ', oldPath);
		// 			finalFilePath = oldPath;
		// 			console.error('error renaming the file', err);
		// 		} else {
		// 			finalFilePath = newPath;
		// 		}
		// 		// NOTE -> enable button only when the "then" function of swal gets called
		// 		$("#show-file").prop('disabled', false);
		// 	});
		// } else if(filename.dismiss === "cancel" filename.dismiss === "overlay") {

		// }
		// ? check if the file exists
	});

}

function replaceNews() {

	getTotalDuration()
}
function getTotalDuration() {
	let duration = 0.0;
	//
	var testFiles = []
	fs.readdirSync(configFolderPath + 'output/').forEach(file => {
		testFiles.push(configFolderPath + 'output/' + file)
	})
	for (let i = 0; i < testFiles.length; i++) {
		var argsDur = [
			'ffprobe',
			'-v', 'error',
			'-show_entries',
			'format=duration',
			testFiles[i],
		];
		var cmdDur = argsDur.shift();
		var procDur = child_process.spawn(cmdDur, argsDur);
		procDur.stderr.on('data', (data) => {
			/*			//*/
		})
		procDur.stdout.on('data', (data) => {
			if (data.includes("duration")) {
				let x = parseFloat(parseFloat(data.toString().split("duration=")[1].split("[")[0].trim(" ")).toFixed(2));
				duration = duration + x;
				//
			}
		})
		procDur.on('close', function (data) {
			if (testFiles.length - i == 1) {
				totalDuration = (duration);
			}
		})
	}
}


function replaceBeforeOverlay() {
	var newFiles = [], oldFiles = []
	fs.readdirSync(configFolderPath + 'camera').forEach(file => {
		if (file.includes("new_"))
			newFiles.push(configFolderPath + 'camera/' + file)
		else {
			oldFiles.push(configFolderPath + 'camera/' + file)
		}
	})
	for (let i = 0; i < newFiles.length; i++) {
		let x = newFiles[i].split("_")[newFiles[i].split("_").length - 1];
		for (let j = 0; j < oldFiles.length; j++) {
			if (oldFiles[j].includes(x)) {
				fs.unlinkSync(oldFiles[j])
				fs.renameSync(newFiles[i], oldFiles[j])
			}
		}
	}
	newFiles = []
	oldFiles = []
	fs.readdirSync(configFolderPath + 'screen').forEach(file => {
		if (file.includes("new_"))
			newFiles.push(configFolderPath + 'screen/' + file)
		else {
			oldFiles.push(configFolderPath + 'screen/' + file)
		}
	})
	for (let i = 0; i < newFiles.length; i++) {
		let x = newFiles[i].split("_")[newFiles[i].split("_").length - 1];
		for (let j = 0; j < oldFiles.length; j++) {
			if (oldFiles[j].includes(x)) {
				fs.unlinkSync(oldFiles[j])
				fs.renameSync(newFiles[i], oldFiles[j])
			}
		}
	}
}
function fetchBackupFolders() {
	//rimraf.sync(configFolderPath+'/camera/')
	//rimraf.sync(configFolderPath+'/screen/')
	let camFiles = fs.readdirSync(configFolderPath + "/backup/camera/")
	let screenFiles = fs.readdirSync(configFolderPath + "/backup/screen/")
	if (!fs.existsSync(configFolderPath + '/camera/')) {
		fs.mkdirSync(configFolderPath + '/camera/')
	}
	if (!fs.existsSync(configFolderPath + '/screen/')) {
		fs.mkdirSync(configFolderPath + '/screen/')
	}
	for (let i = 0; i < camFiles.length; i++) {
		fs.copyFileSync(configFolderPath + "/backup/camera/" + camFiles[i], configFolderPath + '/camera/' + camFiles[i])
	}
	for (let i = 0; i < screenFiles.length; i++) {
		fs.copyFileSync(configFolderPath + "/backup/screen/" + screenFiles[i], configFolderPath + '/screen/' + screenFiles[i])
	}
	//
}

window.onerror = function (errorMsg, url, lineNumber, column, errorObj) {
	//errLogstream.write('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
	//+ ' Column: ' + column + ' StackTrace: ' +  errorObj.toString()+'\n')
	alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber
		+ ' Column: ' + column + ' StackTrace: ' + errorObj.toString() + '\n')
}


function processVideos() {


	let tmp = localStorage.getItem('configFromPreview');
	electron.remote.getCurrentWindow().setResizable(true);
	electron.remote.getCurrentWindow().setSize(600, 450);
	let bounds = electron.remote.screen.getPrimaryDisplay().bounds;
	let x = bounds.x + ((bounds.width - 600) / 2);
	let y = bounds.y + ((bounds.height - 450) / 2);

	electron.remote.getCurrentWindow().setPosition(x, y);
	// electron.remote.getCurrentWindow().setPosition(600,450);

	if (tmp) {
		configObj = JSON.parse(tmp);
		configFolderPath = configObj.configFolderPath + '\\';
		configFilePath = configObj.configFolderPath + '\\';

		openLog();
		writeLog("/******** Log File for processing***********/ \n -> Please do not change or delete contens of this file \n");

		// create intermidiate folder
		let intermidiateFolderPath = configFolderPath + "intermediate\\";
		if (!fs.existsSync(intermidiateFolderPath)) {
			fs.mkdirSync(intermidiateFolderPath);
		}
		preprocess();


	} else {

	}
}
async function checkFFMPEG() {
	return new Promise((resolve, reject) => {

	});
}

function writeLog(line) {
	if (logStream && typeof line == "string") {
		logStream.write(line);
	}
}
function closeLog() {
	logStream.end();
	logStream.on('finish', () => {

	});
}
function openLog() {
	let logFilePath = configFolderPath + '\\log.txt';
	if (fs.existsSync(logFilePath)) {
		fs.truncateSync(logFilePath);
	}
	logStream = fs.createWriteStream(logFilePath);
}

function prepareFileOutput(res, msg) {
	console.log('msg: ', msg);
	console.log('res: ', res);

	if(msg.outputPath[0] === '"') {
		msg.outputPath = msg.outputPath.replace(/"/g,"");
	}

	const sourceFile = path.normalize(msg.outputPath);
	let destinationFile = path.dirname(sourceFile);
	let shouldRename = false;
	if(res.value) {
		destinationFile = path.join(destinationFile,res.value + ".mp4");
		shouldRename = true;
	} else {
		shouldRename = false;
		if(msg.hasOwnProperty('finalMerge') && msg.finalMerge === 'notdone') {
			destinationFile = path.join(destinationFile,"camera_0.mp4");
		} else {
			destinationFile = path.join(destinationFile,"output.mp4");	
		}
	}
	if(shouldRename) {
		fs.renameSync(sourceFile, destinationFile);
	}
	finalFilePath = destinationFile;
	console.log('finalFilePath: ', finalFilePath);
	console.log('finalFilePath: ', finalFilePath);
}