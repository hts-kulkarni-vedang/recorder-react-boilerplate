const env = require("../environment");
process.on('message', (msg) => {
    var addon = require('bindings')('preprocess');
    var create=require('bindings')('createList');
    var final=require("bindings")('finalmerge');
    var p=require('process');
    var fs=require('fs');
    var settings = {};
    var os = require('os');
    const homeDir = os.homedir + '/Documents/HiFi_Recorder Files/';

    // process.send({ preprocess : {validationError : true}});



    if (fs.existsSync(env.settingsFilePath)){
		settings=JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));	
        settings['exists'] = true;
    }else{
        settings['exists'] = false;
    }
    var configObj=msg.configObj;
    configObj['settings'] = settings;
    var configFolderPath=msg.configFolderPath;
    var basepath = msg.basepath;
    

    
    var appDataFolderPath = msg.appDataFolderPath;
    
    var pre_obj=addon.preprocess(configObj,configFolderPath,function(msg){
        
        process.send({ preprocess: msg });
    },p.pid,homeDir,env.serverPath,basepath,function(isLicenseInvalid){
        
        if(isLicenseInvalid.Exception){
            process.send({ preprocess : {ExceptionOccured : true,reason : isLicenseInvalid.Exception}});
        }
        if(isLicenseInvalid.areContentsSimilar != true || isLicenseInvalid.areDatesValid != true){
            process.send({ preprocess : {validationError : true}});
        }else{
            if(isLicenseInvalid.areContentsSimilar == true && isLicenseInvalid.areDatesValid == true){
                if(isLicenseInvalid.ServerValidation){
                    if(isLicenseInvalid.ServerValidation != 'true'){
                        process.send({ preprocess : {validationError : true}});
                    }
                }
            }
            
        }
        // if(isLicenseInvalid.LocalValidation != true){
        //     process.send({ preprocess : {validationError : true}});
        // }
    },appDataFolderPath);
    
    if(pre_obj.LicenseInvalid != "true"){
        var flag=0;
        for(let i=0;i<configObj.finalMerger.length;i++)
        {
            if(configObj.finalMerger[i].type=='both')
            {
              flag=1
            }
        }
        if(flag==1){
            var pos ="";
            if(configObj.overlayPos == "bl");
            pos = "(0):(H-h)";
            if(configObj.overlayPos == "br");
            pos = "(W-w):(H-h)";
            if(configObj.overlayPos == "tl");
            pos = "(0):(0)";
            if(configObj.overlayPos == "tr");
            pos = "(W-w):(0)";
            if(configObj.overlayPos == "hidden");
            pos = "hidden";

            // var over_obj=over_addon(pre_obj,configFolderPath,pos,function(msg){
            //     process.send({ overlay: msg });
            // },p.pid);
        }
        
        
        if(configObj.finalMerger.length == 1)
        {
            
            process.send({finalMerge:"notdone",outputPath : configObj.finalMerger[0].intermidiate.cpath});
        }
        else{
            
            
                let createMergingListResponse = create.createList(configObj,function(msg){
                    
                    if(msg == "merging-list-created"){
                        final(configObj,configFolderPath,'output\\output.mp4',function(msg){
                            process.send({ final: msg });   
                        },p.pid,basepath);
                        process.exit();
                    }
                },(configFolderPath+"mergingList.txt"));
            }    
    }else{
        
        process.send({ preprocess : {validationError : true , reason : "connected-no-internet"}});
    }
    
});
