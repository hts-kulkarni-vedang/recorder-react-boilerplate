console.log('main template')
const { ipcRenderer } = require('electron')
var fs = require('fs');
const config = require('./hts-aws-credentials.json');
var swal = require("sweetalert2");
var AWS = require('aws-sdk');
var S3 = require('aws-sdk/clients/s3');
const env = require("../environment");
const electron = require('electron');
let win =null;
let totalFilesWRitten = 0;
let currentWindow = null;
const path = require('path');
var app = require('electron').remote.app;


$(document).on('click', '.back-btn', () => {
  if(sessionStorage.getItem('currentSettingsMode') =='changeTemplateMode'){
    electron.remote.getCurrentWindow().loadFile('preview-output-window/output-preview.html');
}
else if(sessionStorage.getItem('currentSettingsMode')=='liveStreamingMode'){
    console.log("dskjbvhjfbg")
    electron.remote.getCurrentWindow().loadFile('preview-output-window/output-preview.html');
} else {
  sessionStorage.setItem('currentSettingsMode', 'show-settings');
  ipcRenderer.send('open-recorder-settings');
}
});
$(document).on('click', '.minimize-img', () => {
    // ipcRenderer.send('backSettings');
    if (currentWindow) {
        currentWindow.minimize();
    }
});
$(document).on('click', '.close-img', () => {
    ipcRenderer.send('close-app');
});
$(document).on('click', '.buttonaddtemplate', () => {
    addNewTemplate();
});
$(document).on("click", "#cancelbtn", function () {
  outputPreviewCancelButton(this);
});
$(document).on("click", "#confirmbtn", function () {
outputPreviewConfirmButton(this);
});
var templateName = null;
var templateIndex=null;
function outputPreviewCancelButton(element){
    if(sessionStorage.getItem('currentSettingsMode') == 'changeTemplateMode'){
        sessionStorage.setItem('previewMode','cancelMode');
        electron.remote.getCurrentWindow().loadFile('preview-output-window/output-preview.html');
    }
}
function outputPreviewConfirmButton(element){
    if(sessionStorage.getItem('currentSettingsMode') == 'changeTemplateMode'){

      if(templateIndex && templateName) {
        sessionStorage.setItem('previewMode','confirmMode');
        sessionStorage.setItem('templateIndex',templateIndex);
        sessionStorage.setItem('templateName',templateName);
        electron.remote.getCurrentWindow().loadFile('preview-output-window/output-preview.html');
      } else {
        swal.fire({
          title: 'Please select at least one template'
        });
      }
    }
}
function addNewTemplate() {
    sessionStorage.setItem('indexoftemplatefolder', -1);
    sessionStorage.setItem('currentSettingsMode', 'add-new-template');
    ipcRenderer.send('open-recorder-settings');
}
function editTemplate(elem) {
    const templateIndex = elem.getAttribute('templateindex')
    console.log('templateIndex: ', templateIndex);
    sessionStorage.setItem('currentSettingsMode', 'edit-template');
    sessionStorage.setItem('indexoftemplatefolder', templateIndex);
    // $('#preview').prepend('<img id="theImg" src=tree.jpg />');
    // $("#preview").html("<img src='" + image + "' alt='description' />");
    // ipcRenderer.send('change-url', 'settings/settings.html');
    ipcRenderer.send('open-recorder-settings');
}
async function download() {
    try {
        AWS.config.setPromisesDependency();
        AWS.config.update({
            accessKeyId: "AKIAI7TCQNFNFSCMUDFA",
            secretAccessKey: "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",
            region: "ap-south-1"
        });
        const s3 = new AWS.S3();
        const response = await s3.listObjectsV2({
            Bucket: "hifi-recorder-templates"
        }).promise();
        for (var index = 0; index < response.KeyCount; index++) {
            var key = response.Contents[index].Key
            var params = {
                Bucket: 'hifi-recorder-templates',
                //Key: 'Background Images with Podium/oie_OziDtjDIwCMj.png'
                Key: key
            };
            var fileExt = key.substr(key.lastIndexOf('/') + 1);
            var file = fs.createWriteStream('/home/sanyukta/Documents/HiFi_Recorder Files/template/background_images/' + fileExt);
            s3.getObject(params).createReadStream().pipe(file).on('finish', () => {
                Progress(response);
            });
        }
    } catch (e) {
    }
}
function Progress(response) {
    totalFilesWRitten++;
    var element = document.getElementById("myprogressBar");
    var percentagecount = Math.round((totalFilesWRitten / response.KeyCount) * 100);
    element.style.width = percentagecount + '%';
    element.innerHTML = percentagecount + '%';
    console.log('total Files Written: ', totalFilesWRitten);
    if (totalFilesWRitten === response.KeyCount) {
        console.log('all files written !');
        const { value: templateName } = swal.fire({
            title: 'Download Complete',
            type: 'success',
            showCancelButton: false,
            confirmButtonColor: 'blue',
            confirmButtonText: 'Ok',
            // timer: 3000
        });
    }
}
var wrapper, imageName, deleteImage, newImage;
var readSettingsFile = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
$(document).ready(function () {
  currentWindow = electron.remote.getCurrentWindow();
  currentWindow.maximize();
  console.log(sessionStorage.getItem('currentSettingsMode'))
  showTemplateListInUI();
  if(sessionStorage.getItem('currentSettingsMode') == 'show-settings') {
      console.log("general");
      $('#confirmbtn').hide();
      $('#cancelbtn').hide();
      $('#buttonaddtemplate').show();
  }
  else if(sessionStorage.getItem('currentSettingsMode') == 'liveStreamingMode') {
      $('#confirmbtn').show();
      $('#cancelbtn').show();
      $('#buttonaddtemplate').hide();
  }
  else if(sessionStorage.getItem('currentSettingsMode') == 'changeTemplateMode'){
      console.log("not General");
      $('#confirmbtn').show();
      $('#cancelbtn').show();
      $('#buttonaddtemplate').hide();
  }
  else if(sessionStorage.getItem('currentSettingsMode') =='edit-template') {
      $('#confirmbtn').hide();
      $('#cancelbtn').hide();
      $('#buttonaddtemplate').show();
  }
});
$(document).on('click', 'img.templateclass, .template-name-wrapper', function () {
  if(sessionStorage.getItem('currentSettingsMode') == 'changeTemplateMode'){
    templateIndex = this.getAttribute('templateindex');
    templateName = readSettingsFile[templateIndex].name;
}
else{
    editTemplate(this);
}
});

$(document).on('click', 'button.makedefaulttemplate', function () {
    // const{value: defaultkey};
    const templateIndex = this.getAttribute('templateindex');
    var myArray = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
    for (var index = 0; index < myArray.length; index++) {
        myArray[index].defaultkey = false;
    }
    myArray[templateIndex].defaultkey = true;
    $("button.makedefaulttemplate").removeClass("active");
    $(this).addClass("active");
    $(this).text("Default Template");
    fs.writeFile(env.settingsFilePath, JSON.stringify(myArray), 'utf8', (err) => {
        if (err) throw err
        console.log('The file has been saved!');
        console.log(env.settingsFilePath);
        //console.log(settings);
        swal.fire({
            type: 'success',
            toast: true,
            position: 'top-end',
            title: 'Settings Saved!',
            timer: 3000
        })
    });
})
$(document).on('click', 'button.deletetemplate', function () {
    var myArray = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));
    const templateIndex = this.getAttribute('templateindex');
    const objToDelete = myArray[templateIndex];
    if (objToDelete && objToDelete.defaultkey === true) {
        swal.fire({
            icon: 'error',
            title: 'Cannot delete default template',
            text: 'Please select another template as default and then delete this template'
        });
    } else {
        if (myArray.length === 1) {
            swal.fire({
                icon: 'error',
                title: 'Cannot delete template',
                text: 'There must be atleast a single template'
            });
        } else {
            const { value: templateName } = swal.fire({
                title: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: true,
                closeOnCancel: true,
                // timer: 3000
            }).then((res) => {
                console.log(res.closeOnCancel)
                if (res.value) {
                    myArray.splice(templateIndex, 1);
                    fs.writeFile(env.settingsFilePath, JSON.stringify(myArray), 'utf8', (err) => {
                        if (err) throw err
                        console.log('The file has been saved!');
                        console.log(env.settingsFilePath);

                        // TODO -> delete from the UI as well
                        showTemplateListInUI();
                        swal.fire({
                            type: 'success',
                            toast: true,
                            position: 'top-end',
                            title: 'Settings Saved!',
                            timer: 3000
                        })
                    });
                }
                else {
                    swal.fire({
                        type: 'cancelled',
                        toast: true,
                        position: 'top-end',
                        title: 'Cancelled',
                        timer: 3000
                    });
                }
            }).catch(function (err) {
                console.error('oops, something went wrong!', err);
            })
        }
    }
})
function showTemplateListInUI() {
    readSettingsFile = JSON.parse(fs.readFileSync(env.settingsFilePath, 'utf8'));

    var container = document.getElementById("wrapperdiv");
    $(".image-wrapper").remove();
    var urls = readSettingsFile;
    const mode = sessionStorage.getItem('currentSettingsMode');
    console.log(mode)
    for (var index = 0; index < urls.length; index++) {
        let templatePath = urls[index].templatePath.replace(/ /g, "%20");
        const possibleDefaultTemplatePath = path.join(env.settingsTemplatePath, 'Auto Generated Template.png');
        if ((templatePath === 'auto-generated-template' || templatePath === '../assets/images/auto-generated-template.png') && (!fs.existsSync(possibleDefaultTemplatePath))) {
            var basepath = app.getAppPath('exe');
            basepath = basepath.replace("app.asar", "");
            // templatePath = path.join(basepath,'assets','images','auto-generated-template.png');
            templatePath = '../assets/images/auto-generated-template.png';
            templatePath = templatePath.replace(/ /g, '%20');
            templatePath = (templatePath.replace(/\(/g, "\\("));
            templatePath = (templatePath.replace(/\)/g, "\\)"));
            console.log('templatePath: ', templatePath);
        }
        if ((templatePath === 'auto-generated-template' || templatePath === '../assets/images/auto-generated-template.png') && (fs.existsSync(possibleDefaultTemplatePath))) {
            templatePath = possibleDefaultTemplatePath;
            templatePath = templatePath.replace(/ /g, '%20');
            templatePath = (templatePath.replace(/\(/g, "\\("));
            templatePath = (templatePath.replace(/\)/g, "\\)"));
        }

        // ? new Image is diff is mode is diff
        if(sessionStorage.getItem('currentSettingsMode') == 'changeTemplateMode'){
          autoTickSelectedTemplate();
          newImage = "<div class='image-wrapper'><label><img src=" + templatePath + " 'width=300px' + height=200px + class='templateclass' templateindex=" + index + " /><input type='radio' name='selimg'"+index+"><span class='caption'><span>"+(urls[index].name)+"</span></span></label>";
          deleteImage = "";
        } else {
          newImage = "<div class='image-wrapper'><img src=" + templatePath + " 'width=300px' + height=200px + class='templateclass' templateindex=" + index + " />";
          deleteImage = "<button type='button' class='deletetemplate' id='deletetemplate' templateindex=" + index + "> Delete</button></div>";
        }


        imageName = "<b><figcaption>" + (urls[index].name) + "</figcaption></b>";
        makeDefault = "<button type='button' class='makedefaulttemplate' id='makedefaulttemplate' templateindex=" + index + ">Set as Default</button>";
        const templateName = "<p class='template-name'>" + urls[index].name + " </p>";
        container.innerHTML += "<div class='a-template' isdefault=" + urls[index].defaultkey + ">" + newImage + "<br>" + " <div class='template-name-wrapper' templateindex=" + index + ">" + templateName + "</div>  <div class='template-btns-wrap'>" + makeDefault + deleteImage + "</div> </div>";
    }
    markDefaultTemplate();
}
function markDefaultTemplate() {
    $('.a-template').each((index, elem) => {
        const isDefault = elem.getAttribute('isdefault');
        console.log(index + 'isDefault: ' + isDefault);
        if (isDefault == 'true') {
            $(elem).find('.makedefaulttemplate').addClass("active");
            $(elem).find('.makedefaulttemplate').text("Default Template");
        }
    });
}
function autoTickSelectedTemplate(){
  var autoclickindex = sessionStorage.getItem('autoclickindex');
  $('.templateclass').eq(autoclickindex).click();
}
