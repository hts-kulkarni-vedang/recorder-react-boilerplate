
var addon = require('bindings')('validation');
const { ipcRenderer, remote,shell } = require("electron");
const electron = require("electron");
const { Menu, MenuItem } = remote;
var os = require('os')
var fs = require('fs')
const request = require('request');
const child_process = require("child_process");
const env = require('./environment');

const swal = require('sweetalert2');

const homeDir = os.homedir + '/Documents/HiFi_Recorder Files';
// ! ensure it is same as in renderer.js
if (!fs.existsSync(homeDir)) {
    fs.mkdirSync(homeDir)
}
if (!fs.existsSync(homeDir + '/assets')) {
    fs.mkdirSync(homeDir + '/assets')
}
var app = require('electron').remote.app;
var environmentJs = require('./environment');

$(document).ready(function () {
    console.log('ancd');
    setupMenu();
    $(".button").click(function () {
      activateNow();
    });
});
$(document).on('click', '#close', function () {
    ipcRenderer.send('close-app');
});
$(document).on('click', '.btn-get-free-trial', function () {
    getFreeTrialLicense();
});
$(document).on('click', '.log-in-recorder-website', function () {
    shell.openExternal(env.recorderWebsiteURLConfig.loginPage);
});
$(document).on('click', '.buy-now-recorder-website', function () {
    shell.openExternal(env.recorderWebsiteURLConfig.homePageBuyNowSection);
});

function setupMenu() {
    electron.remote.getCurrentWindow().setMenu(null)
    const menu = Menu.buildFromTemplate(getTemplate())
    electron.remote.getCurrentWindow().setMenu(menu)
}
function getTemplate() {
    const template =
        [
        ]
    return template
}
function showSwalWithErrorCode(responseObj) {
    let text = "", flag = 0;
    console.log(responseObj);
    if (responseObj.error == environmentJs.errorCodes.KEY_NOT_FOUND) {
        text = "Key Not Found";
    }
    if (responseObj.error == environmentJs.errorCodes.LICENSE_NOT_FOUND) {
        text = "License Not Found";
    }
    if (responseObj.error == environmentJs.errorCodes.LICENSE_EXPIRED) {
        text = "License Expired";
    }
    if (responseObj.error == environmentJs.errorCodes.LICENSE_CANCELLED) {
        text = "License Cancelled";
    }
    if (responseObj.error == environmentJs.errorCodes.KEY_ALREADY_USED) {
        text = "Key Already Used";
    }
    if (responseObj.error == environmentJs.errorCodes.PC_REGISTERED_WITH_DIFFERENT_USER) {
        text = "This PC was registered with different user!,Please contact admin";
    }
    if (responseObj.error == environmentJs.errorCodes.HTTP_RESPONSE_ERROR) {
        text = "HiFi Recorder could not connect with server, Please try again or Contact Admin";
    }
    if (responseObj.error == environmentJs.errorCodes.LICENSE_VALID) {
        electron.remote.getCurrentWindow().setSize(800, 600);
        ipcRenderer.send('load_Recorder', (param) => { });
        flag = 1;
    }
    if (flag == 0) {
        swal.fire({
            type: "error",
            text: text
        });
    }
}

function getFreeTrialLicense() {
    console.log('came to get free trial license');

    // ! WHAT IS THIS !!! ???
    // TODO 1. check if registry and server does not contain any data 
    // ? for this serialnumber
    addon.setSerialNumberInRegistry();

    // TODO 2. validate and check on server
    var basepath = app.getAppPath('exe');
    basepath = basepath.replace("app.asar", "")
    basepath += "\\";
    var appDataPath = app.getPath('userData');
    const serverPath = environmentJs.serverPath;
    var valid = addon.validateOnline(basepath, serverPath, appDataPath);
    console.log('online validation result : ', valid);

    // TODO 1. check if registry and server does not contain any data 
    // ? for this serialnumber
    addon.setSerialNumberInRegistry();

    // TODO 3 - if server validation responds with error code -2
    // ? that means license with this serial number was not found on server
    // ? only in that case generate a trial license 

    if (valid.ServerValidation && valid.ServerValidation.ServerPhpResponse) {
        const response = JSON.parse(valid.ServerValidation.ServerPhpResponse);
        if (response.error === environmentJs.errorCodes.LICENSE_NOT_FOUND) {
            swal.fire({
                title: 'Enter Information to generate license',
                html:
                    '<input placeholder="Enter Email" id="swal-input1" class="swal2-input">' +
                    '<input placeholder="Enter Name" id="swal-input2" class="swal2-input">',
                preConfirm: function () {
                    return new Promise(function (resolve) {
                        resolve([
                            $('#swal-input1').val(),
                            $('#swal-input2').val()
                        ])
                    })
                },
                onOpen: function () {
                    $('#swal-input1').focus()
                }
            }).then(function (result) {
                console.log('result: ', result);
                if(result.value) {
                    if (validateEmail(result.value[0]) && result.value[1].length > 3) {
                        console.log('user should be allowed to generate the free trial license');
                        const email = result.value[0];
                        let name = result.value[1];
                        name = name.replace(/ /g, '-');
                        const response = addon.generateTrialLicense(email, name, basepath, serverPath);
                        console.log('response from generate trial license: ', response);
                        let resObj = JSON.parse(response.response);
                        if(resObj.error == false && resObj.success == true) {
                            var x = resObj.generatedKey;
                            let newKey = x.slice(0,4) + '-' + x.slice(4,8) + '-' + x.slice(8,12) + '-' + x.slice(12,16)
                            $('#keys').val(newKey);
                            activateNow();
                        } else {
                            let text = 'Could not generate free license';
                            if(resObj.errorCode == 'email-or-serial-number-already-registered') {
                                text = 'This PC or email has been already registered, Please use key associated with this email or choose another email.'
                            }
                            swal.fire({
                                icon: 'error',
                                type: 'error',
                                text
                            });
                        }
                    } else {
                        swal.fire({
                            title: 'Please enter correct data'
                        });
                    }
                }
            })

        }
        else if(response.isValid == true) {
            showSwalWithErrorCode(response);
        }
    }
}
function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}
function activateNow() {
    var activationKey = $('#keys').val();
    var appDataPath = app.getPath('userData');

    if (activationKey.length == 19) {
        serverPath = environmentJs.serverPath;
        var basepath = app.getAppPath('exe');
        basepath = basepath.replace("app.asar", "")
        console.log("basepat is : ", basepath);
        basepath = basepath + "\\";

        // response = addon.RunCommandHelper("command",function(resObj){
        //     console.log(resObj);
        // });
        // debugger
        // response = response.replace(/(\r\n|\n|\r)/gm, "");

        // console.log(response);
        // debugger;
        response = addon.validateWithKey(activationKey, basepath, serverPath, homeDir, appDataPath);
        console.log("response from validate in main js : ", response);
        // swal({
        //     title : "Response from server ",
        //     text : JSON.stringify(response)
        // })
        var responseObj = JSON.parse(response.ServerPhpResponse);
        showSwalWithErrorCode(responseObj);
    } else {
        swal.fire({
            type: "error",
            title: "Please enter valid Key"
        });
    }
}