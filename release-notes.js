//  ? release version 1.0.24
// NOTE - ISSUES RESOLVED


// ? 1. When clicking on Openfolder after Edit Video Later, it does not navigate to the respective folder 
// JIRA issue number - REC 55

// ?  2.By default resolution of output video should be 1080p
// JIRA issue number - REC 56

// ? 3. Scroll Start Time and Scroll Duration fields accepts space bar value as a first value
// JIRA issue number - REC 54

// ? 4. Transparency error while reprocessing
// JIRA issue number - REC 57

// ? 5. While Recording [In Record Camera Mode] Application allows to select audio sources,
// JIRA issue number -  REC 44

// ? 6. "Show File" Button Not Working
// JIRA issue number -  REC 35